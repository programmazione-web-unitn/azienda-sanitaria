import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
class GenerateHash 
{
	//algorithm: MD5, SHA-256, SHA-512
	static String getHash(String data, String algorithm){
		String hash="none";
		try {
            MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
            messageDigest.update(data.getBytes());
            byte[] messageDigestMD5 = messageDigest.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte bytes : messageDigestMD5) {
                stringBuffer.append(String.format("%02x", bytes & 0xff));
            }
            hash=stringBuffer.toString();
        } catch (NoSuchAlgorithmException exception){}
        return hash;
	}
    public static void main(String args[]) 
    {
    	System.out.println(getHash("ciao", "SHA-256"));
    } 
} 