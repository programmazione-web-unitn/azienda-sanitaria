import person
import random
import person
import os
import shutil

PAZIENTI_A_MEDICO = 16
NUM_MEDICI = 14
NUM_SSP = 7
NUM_FARMACIE = 20
NUM_PROVINCE = NUM_MEDICI//2

ll = ["\connect \"AziendaSanitaria\"\n"]
static_ll = ["\connect \"AziendaSanitaria\"\n"]

def carica_province():
    province = list()
    with open("province.csv") as f_province:
        ll = f_province.readlines()
        for l in ll:
            nome, regione, sigla = l.split(",")
            nome = nome.replace("'", "''")
            regione = regione.replace("'", "''")
            province.append({"nome": nome.strip(), "sigla":sigla.strip(), "regione":regione.strip()})
    return province # nome, sigla, regione

def carica_citta():
    citta = [{"nome": d["comune"].replace("'", "''"), "sigla": d["sigla_provincia"].replace("'", "''")} for k, d in person.comuni.items()]
    return citta # nome_citta, sigla_provincia

province = carica_province()
citta = carica_citta()

# province_scelte = random.sample(province, NUM_PROVINCE)
province_scelte = [{"sigla":"TN"}, {"sigla":"BZ"}, {"sigla":"PD"}, {"sigla":"VR"}]
comuni_scelti = person.filtra_comuni(province_scelte)

def delete():
    ll.append("DELETE FROM foto;")
    ll.append("DELETE FROM visita_di_base;")
    ll.append("DELETE FROM visita_specialistica;")
    ll.append("DELETE FROM ricetta;")
    ll.append("DELETE FROM esame;\n")

    ll.append("DELETE FROM utente;")
    ll.append("DELETE FROM medico_specialista;")
    ll.append("DELETE FROM medico_di_base;")
    ll.append("DELETE FROM paziente;")
    ll.append("DELETE FROM ssp;")
    ll.append("DELETE FROM farmacia;")
    ll.append("DELETE FROM ssn;\n")

    static_ll.append("DELETE FROM provincia;")
    static_ll.append("DELETE FROM tipo_di_visita;")
    static_ll.append("DELETE FROM tipo_di_esame;")
    static_ll.append("DELETE FROM farmaco;\n")

def insert_province(province):
    s = "INSERT INTO provincia VALUES\n"
    vv = []
    for pr in province:
        vv.append("('%s', '%s', '%s')" % (pr["sigla"], pr["nome"], pr["regione"]))

    s += ",\n".join(vv)+";\n"
    static_ll.append(s)


def insert_utenti(utenti, tipo_utente):
    su = "INSERT INTO utente VALUES\n"
    uu = []
    for p in utenti:
        uu.append("(%d, '%s', '%s', '%s', '%s')" % (p.id, p.email, p.pwd, p.salt, tipo_utente))

    su += ",\n".join(uu)+";\n"
    ll.append(su)

def insert_pazienti(pazienti):
    s = "INSERT INTO paziente VALUES\n"
    vv = []
    for p in pazienti:
        vv.append("(%d, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % \
            (p.id, p.firstName.replace("'", "''"), p.lastName.replace("'", "''"),
            p.strBirthDate, p.birthPlace.replace("'", "''"), p.provincia_nascita,
            p.cf, p.gender, p.sigla_provincia))

    s += ",\n".join(vv)+";\n"
    ll.append(s)

def insert_foto(pazienti, medici_di_base, medici_specialisti):
    s = "INSERT INTO foto (id_paziente, path_foto, data_ora_foto) VALUES\n"
    vv = []
    for i, p in enumerate(pazienti+medici_di_base+medici_specialisti):
        vv.append("(%d, '%s', '%s')" % (p.id, p.path_foto, p.data_ora_foto))

    s += ",\n".join(vv)+";\n"
    ll.append(s)

def insert_medici_di_base(medici):
    sm = "INSERT INTO medico_di_base VALUES\n"
    mm = []
    for m in medici:
        mm.append("(%d, '%s', '%s')" % (m.id, m.comune_medico.replace("'", "''"), m.sigla_provincia_medico))

    sm += ",\n".join(mm)+";\n"
    ll.append(sm)

def insert_medici_specialisti(medici_specialisti):
    sm = "INSERT INTO medico_specialista VALUES\n"
    mm = []
    for m in medici_specialisti:
        mm.append("(%d)" % (m.id))

    sm += ",\n".join(mm)+";\n"
    ll.append(sm)

def link_paziente_medico(pazienti):
    for p in pazienti:
        s = "UPDATE paziente SET id_medico_di_base = %d WHERE id = %d;" % (p.medico.id, p.id)
        ll.append(s)
    ll.append("")

def insert_ssp(province, start_id):
    ssps = list()
    id_ssp = start_id
    for pr in province_scelte:
        email = "ssp%s@gmail.com" % pr["sigla"]
        ssp = person.User(id_ssp, email)
        ssp.sigla_provincia = pr["sigla"]
        ssps.append(ssp)
        id_ssp += 1

    insert_utenti(ssps, 'SSP')

    s = "INSERT INTO ssp VALUES\n"
    ss = []
    for ssp in ssps:
        ss.append("(%d, '%s')" % (ssp.id, ssp.sigla_provincia))

    s += ",\n".join(ss)+";\n"
    ll.append(s)
    return ssps

def insert_farmacie(start_id):
    farmacie = list()
    nomi = person.lastNames
    i = 1
    for id_farmacia in range(start_id, start_id+NUM_FARMACIE):
        nome = nomi[i-1]
        email = "farmacia.%s@gmail.com" % nome.lower().replace(" ","").replace("'","")
        i += 1

        f = person.User(id_farmacia, email)
        f.nome = nome
        farmacie.append(f)

    insert_utenti(farmacie, 'F')

    s = "INSERT INTO farmacia VALUES\n"
    ss = []
    for f in farmacie:
        codice_comune = random.choice(list(comuni_scelti.keys()))
        ss.append("(%d, '%s', '%s', '%s')" % (f.id, f.nome.replace("'", "''"),
                    person.comuni[codice_comune]["comune"].replace("'", "''"),
                    person.comuni[codice_comune]["sigla_provincia"]))

    s += ",\n".join(ss)+";\n"
    ll.append(s)
    return farmacie

def insert_ssn(start_id):
    email = "ssn@gmail.com"
    ssn = person.User(start_id, email)
    insert_utenti([ssn], 'SSN')

    s = "INSERT INTO ssn VALUES\n"
    s += "(%d);\n" % (ssn.id)
    ll.append(s)

def insert_tipi_di_visite():
    with open("visite.txt") as f:
        visite = f.readlines()
        s = "INSERT INTO tipo_di_visita VALUES\n"
        ss = list()
        for i,v in enumerate(visite):
            ss.append("(%d, '%s', %d)"%(i+1,v.strip().replace("'", "''"), 50))
        s += ",\n".join(ss)+";\n"
        static_ll.append(s)
        return visite

def insert_tipi_di_esami():
    with open("esami.txt") as f:
        esami = f.readlines()
        s = "INSERT INTO tipo_di_esame VALUES\n"
        ss = list()
        for i,v in enumerate(esami):
            ss.append("(%d, '%s', %d)"%(i+1,v.strip().capitalize().replace("'", "''"), 11))
        s += ",\n".join(ss)+";\n"
        static_ll.append(s)
        return esami

def insert_farmaci():
    with open("farmaci.txt") as f:
        farmaci = f.readlines()
        s = "INSERT INTO farmaco VALUES\n"
        ss = list()
        for i,v in enumerate(farmaci):
            ss.append("(%d, '%s', %d)"%(i+1,v.strip().replace("'", "''"), 3))
        s += ",\n".join(ss)+";\n"
        static_ll.append(s)
        return farmaci

def insert_visite_di_base(pazienti, medici_di_base):
    s = "INSERT INTO visita_di_base (id_medico_di_base, id_paziente, data_ora, report) VALUES\n"
    ss = []
    id_visita = 1
    for p in pazienti:
        for i in range(random.randint(4, 10)):
            data_ora_visita, _ = p.random_date_esame()
            ss.append("(%d, %d, '%s', '%s')" % (p.medico.id, p.id, data_ora_visita, p.report_visita_di_base().replace("'", "''")))
            id_visita += 1

    s += ",\n".join(ss)+";\n"
    ll.append(s)

def insert_visite_specialistiche(pazienti, medici_di_base, medici_specialisti, visite):
    id_visita = 1
    num_visite = len(visite)

    # visite fatte
    s = "INSERT INTO visita_specialistica (id_medico_di_base, id_paziente, id_tipo_visita, data_ora_prescrizione, id_medico_spec, data_ora_erogazione, report) VALUES\n"
    ss = []
    tipi_di_visite = dict()
    for p in pazienti:
        for i in range(random.randint(10, 100)):
            data_ora_prescrizione, data_ora_erogazione = p.random_date_esame()
            id_medico_specialista = random.choice(medici_specialisti).id
            while(id_medico_specialista == p.id):
                id_medico_specialista = random.choice(medici_specialisti).id

            try:
                tipi_di_visite[p.id].append((random.randint(1, num_visite), data_ora_erogazione))
            except:
                tipi_di_visite[p.id] = [(random.randint(1, num_visite), data_ora_erogazione)]

            ss.append("(%d, %d, %d, '%s', %d, '%s', '%s')" % (p.medico.id, p.id,
                tipi_di_visite[p.id][-1][0], data_ora_prescrizione,
                id_medico_specialista, data_ora_erogazione, p.report_visita_specialistica(visite[tipi_di_visite[p.id][-1][0] - 1].strip()).replace("'", "''")))
            id_visita += 1

    s += ",\n".join(ss)+";\n"
    ll.append(s)

    # visite da fare
    s = "INSERT INTO visita_specialistica (id_medico_di_base, id_paziente, id_tipo_visita, data_ora_prescrizione) VALUES\n"
    ss = []
    for p in pazienti:
        for i in range(random.randint(4, 6)):
            data_ora_prescrizione = p.random_date_prescrizione()
            ss.append("(%d, %d, %d, '%s')" % (p.medico.id, p.id,
                random.randint(1, num_visite), data_ora_prescrizione))
            id_visita += 1

    s += ",\n".join(ss)+";\n"
    ll.append(s)

    # richiami fatti
    visite_richiami = dict()
    s = "INSERT INTO visita_specialistica (id_paziente, id_tipo_visita, data_ora_prescrizione, id_medico_spec, data_ora_erogazione, report) VALUES\n"
    ss = []
    for p in pazienti:
        visite_richiami[p.id] = random.sample(tipi_di_visite[p.id], random.randint(2, 5))
        for tv in visite_richiami[p.id][1:]:
            data_ora_prescrizione, data_ora_erogazione = p.random_date_richiamo(tv[1])
            id_medico_specialista = random.choice(medici_specialisti).id
            while(id_medico_specialista == p.id):
                id_medico_specialista = random.choice(medici_specialisti).id
            ss.append("(%d, %d, '%s', %d, '%s', '%s')" % (p.id,
                tv[0], data_ora_prescrizione, id_medico_specialista, data_ora_erogazione, p.report_richiamo(visite[tv[0]-1].strip()).replace("'", "''")))
            id_visita += 1

    s += ",\n".join(ss)+";\n"
    ll.append(s)

    # richiami da fare
    s = "INSERT INTO visita_specialistica (id_paziente, id_tipo_visita, data_ora_prescrizione) VALUES\n"
    ss = []
    for p in pazienti:
        data_ora_prescrizione = p.random_date_prescrizione_richiamo(tv[1])
        ss.append("(%d, %d, '%s')" % (p.id,
            visite_richiami[p.id][0][0], data_ora_prescrizione))
        id_visita += 1

    s += ",\n".join(ss)+";\n"
    ll.append(s)

def insert_ricette(pazienti, medici_di_base, medici_specialisti, farmacie, num_farmaci):
    id_ricetta = 1

    # ricette erogate
    s = "INSERT INTO ricetta (id_farmaco, quantita, id_medico_di_base, id_paziente, data_ora_prescrizione, id_farmacia, data_ora_erogazione) VALUES\n"
    ss = []
    for p in pazienti:
        for i in range(random.randint(15, 100)):
            data_ora_prescrizione, data_ora_erogazione = p.random_date_esame()
            ss.append("(%d, %d, %d, %d, '%s', %d, '%s')" % (
                random.randint(1, num_farmaci), random.randint(1, 3), p.medico.id,
                p.id, data_ora_prescrizione, random.choice(farmacie).id,
                data_ora_erogazione))
            id_ricetta += 1

    s += ",\n".join(ss)+";\n"
    ll.append(s)

    # ricette prescritte
    s = "INSERT INTO ricetta (id_farmaco, quantita, id_medico_di_base, id_paziente, data_ora_prescrizione) VALUES\n"
    ss = []
    for p in pazienti:
        for i in range(random.randint(4, 6)):
            data_ora_prescrizione = p.random_date_prescrizione()
            ss.append("(%d, %d, %d, %d, '%s')" % (
                random.randint(1, num_farmaci), random.randint(1, 3), p.medico.id,
                p.id, data_ora_prescrizione))
            id_ricetta += 1

    s += ",\n".join(ss)+";\n"
    ll.append(s)

def insert_esami(pazienti, ssps, esami):
    num_esami = len(esami)

    # esami fatti
    s = "INSERT INTO esame (id_paziente, id_medico_di_base, id_tipo_esame, data_ora_prescrizione, id_ssp, data_ora_erogazione, report) VALUES\n"
    ss = []
    for i, p in enumerate(pazienti):
        for j in range(random.randint(10, 40)):
            dt_prescrizione, dt_erogazione = p.random_date_esame()

            id_ssp = None
            for ssp in ssps:
                if ssp.sigla_provincia == p.sigla_provincia:
                    id_ssp = ssp.id
            assert(id_ssp is not None)

            id_esame = random.randint(1, num_esami)

            ss.append("(%d, %d, %d, '%s', %d, '%s', '%s')" % (p.id,
                        p.medico.id, id_esame, dt_prescrizione,
                        id_ssp, dt_erogazione, p.report_esame(esami[id_esame-1].strip().capitalize()).replace("'", "''")))

    s += ",\n".join(ss)+";\n"
    ll.append(s)

    # esami da fare
    s = "INSERT INTO esame (id_paziente, id_medico_di_base, id_tipo_esame, data_ora_prescrizione) VALUES\n"
    ss = []
    for i, p in enumerate(pazienti):
        for j in range(random.randint(4, 6)):
            dt_prescrizione = p.random_date_prescrizione()
            ss.append("(%d, %d, %d, '%s')" % (p.id, p.medico.id,
                    random.randint(1, num_esami), dt_prescrizione))

    s += ",\n".join(ss)+";\n"
    ll.append(s)

def db_population():
    medici_di_base, medici_specialisti, pazienti = person.generate_persons(NUM_MEDICI,
                                        NUM_MEDICI, PAZIENTI_A_MEDICO, province_scelte)

    delete()

    # dati statici
    insert_province(province)
    visite = insert_tipi_di_visite()
    esami = insert_tipi_di_esami()
    farmaci = insert_farmaci()

    with open("database/static_data.sql", "w") as f:
        f.write('\n'.join(static_ll))

    # dati dinamici
    insert_utenti(medici_di_base, 'MB')
    insert_utenti(medici_specialisti, 'MS')
    insert_utenti(pazienti, 'P')

    insert_pazienti(pazienti+medici_di_base+medici_specialisti)
    insert_medici_di_base(medici_di_base)
    insert_medici_specialisti(medici_specialisti)
    link_paziente_medico(pazienti+medici_di_base+medici_specialisti)

    insert_foto(pazienti, medici_di_base, medici_specialisti)

    ssps = insert_ssp(province, start_id=NUM_MEDICI*PAZIENTI_A_MEDICO+1)
    farmacie = insert_farmacie(start_id=NUM_MEDICI*PAZIENTI_A_MEDICO+NUM_SSP+1)
    insert_ssn(start_id=NUM_MEDICI*PAZIENTI_A_MEDICO+NUM_SSP+NUM_FARMACIE+1)

    insert_visite_di_base(medici_di_base+medici_specialisti+pazienti, medici_di_base)
    insert_visite_specialistiche(medici_di_base+medici_specialisti+pazienti,
                                medici_di_base, medici_specialisti, visite)
    insert_ricette(medici_di_base+medici_specialisti+pazienti, medici_di_base,
                    medici_specialisti, farmacie, len(farmaci))
    insert_esami(medici_di_base+medici_specialisti+pazienti, ssps, esami)

    with open("database/population.sql", "w") as f:
        f.write('\n'.join(ll))

    return pazienti+medici_di_base+medici_specialisti

def dir_hierarchy(pazienti):
    if os.path.exists("pazienti") and os.path.isdir("pazienti"):
        shutil.rmtree("pazienti")

    os.mkdir("pazienti")
    for p in pazienti:
        os.mkdir("pazienti/"+str(p.id))
        os.mkdir("pazienti/"+str(p.id)+"/foto")

        gender = "female"
        if p.gender == "M":
            gender = "male"

        shutil.copy2('../people/'+gender+"/"+p.id_foto, "pazienti/%d/foto/%s"%(p.id, p.path_foto))

if __name__ == '__main__':
    pazienti = db_population()
    dir_hierarchy(pazienti)
