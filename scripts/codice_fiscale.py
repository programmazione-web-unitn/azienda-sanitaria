import sqlite3
import string
import sys
import itertools as it
import operator
from functools import partial
from datetime import date

MESI = "ABCDEHLMPRST"
DISPARI = [1, 0, 5, 7, 9, 13, 15, 17, 19, 21, 2, 4, 18,
           20, 11, 3, 6, 8, 12, 14, 16, 10, 22, 25, 24, 23]
ORD_0 = ord("0")
ORD_A = ord("A")

vocale_pred = partial(operator.contains, set("AEIOUÀÈÉÌÒÙ"))


def pari(char):
    return ord(char) - (ORD_0 if char.isdigit() else ORD_A)


def dispari(char):
    return DISPARI[ord(char) - (ORD_0 if char.isdigit() else ORD_A)]


def calcola_ultimo_carattere(resto):
    return chr(ORD_A + resto)


def partition(pred, iterable):
    partitions = [], []
    for element in iterable:
        partitions[int(pred(element))].append(element)
    return partitions


def codifica_nome(nome, is_cognome=True):
    nome = nome.upper().replace(" ", "")

    consonanti, vocali = partition(vocale_pred, nome)

    if not is_cognome and len(consonanti) > 3:
        del consonanti[1]

    nome = "".join(consonanti + vocali)[:3]
    return nome.ljust(3, "X")


def codifica_data(data, sesso):
    offset = 40 if sesso in "fF" else 0
    return "{:>02}{}{:>02}".format(data.year % 100,
                                   MESI[data.month - 1],
                                   data.day + offset)


def codifica_comune(nome_comune):
    try:
        nome_comune = nome_comune.upper()
        conn = sqlite3.connect("comuni.db")
        result_set = conn.execute("select code from comuni where name = ?", [nome_comune])
        result = result_set.fetchone()
        return result[0]
    except TypeError:  # result is None
        raise ValueError("Comune non trovato!")
    finally:
        conn.close()


def calcola_codice_controllo(code):
    acc_d = sum(dispari(x) for x in code[::2])
    acc_p = sum(pari(x) for x in code[1::2])
    return calcola_ultimo_carattere((acc_d + acc_p) % 26)


def calcola_cf(cognome, nome, data, sesso, comune, code_comune=None):
    if code_comune is None:
        code_comune = codifica_comune(comune)

    assert(len(code_comune)==4)

    codice = "{}{}{}{}".format(codifica_nome(cognome),
                               codifica_nome(nome, is_cognome=False),
                               codifica_data(data, sesso),
                               code_comune)
    return "".join([codice, calcola_codice_controllo(codice)])

if __name__ == '__main__':
    cf = calcola_cf("Rossi", "Mario", date(1998, 1, 1), 'M', "Trento")
    print(cf)
