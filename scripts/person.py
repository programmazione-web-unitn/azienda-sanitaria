import random
import datetime
from datetime import datetime as dt
import time
import sys
from codice_fiscale import calcola_cf
import math
import hashlib
import crypt
import os
from dateutil.relativedelta import relativedelta
from time import mktime

lastNames = ['Ursomando', 'Rossi','Russo','Ferrari','Esposito','Bianchi','Romano','Colombo','Ricci', 'Panusa', 'Pedrotti', 'Marino','Greco','Bruno','Gallo','Conti','De Luca','Mancini','Costa','Giordano','Rizzo','Lombardi','Stam','Moretti','White','Barbieri','Fontana','Santoro','Mariani','Bizzotto','Martello','Rinaldi', 'Puz','Cane','Caruso','Ferrara','Galli','Martini','Leone','Longo','Gentile','Martinelli','Vitale','Lombardo','Serra','Coppola','De Santis','Dangelo','Marchetti','Parisi','Villa','Conte','Ferraro','Ferri','Fabbri','Bianco','Marini','Grasso','Valentini','Messina','Sala','De Angelis','Gatti','Pellegrini','Palumbo','Sanna','Farina','Rizzi','Monti','Cattaneo','Morelli','Amato','Silvestri','Mazza','Testa','Grassi','Pellegrino','Carbone','Giuliani','Benedetti','Barone','Rossetti','Caputo','Montanari','Guerra','Palmieri','Bernardi','Martino','Fiore','De rosa','Ferretti','Bellini','Basile','Riva','Donati','Piras','Vitali','Battaglia','Sartori','Neri','Costantini','Milani','Pagano','Ruggiero','Sorrentino','Orlando','Pesole','Negri']
male_firstNames = ['Andrea','Demetrio', 'Filippo', 'Luca', 'Gianluca', 'Marco', 'Paolo', 'Francesco', 'Roberto', 'Matteo', 'Salvatore', 'Alessandro', 'Emanuele', 'Davide', 'Edoardo', 'Simone', 'Enrico', 'Federico', 'Vincenzo', 'Lorenzo', 'Nikolai', 'Walter','Bruno','Mattia', 'Leonardo', 'Santo Domenico','Stefano', 'Jacopo', 'Giuseppe', 'Manuel', 'Riccardo', 'Mirko', 'Daniele', 'Tommaso', 'Michele', 'Pietro', 'Alessio', 'Luigi', 'Antonio', 'Giorgio', 'Giovanni', 'Angelo', 'Nicola', 'Dario', 'Gabriele', 'Valerio', 'Fabio', 'Domenico', 'Alberto', 'Claudio', 'Giacomo', 'Alex', 'Giulio', 'Christian']
female_firstNames = ['Giulia', 'Beatrice', 'Chiara', 'Valeria', 'Francesca', 'Michela', 'Federica', 'Serena', 'Sara', 'Camilla', 'Martina', 'Irene', 'Valentina', 'Cristina', 'Alessia', 'Simona', 'Silvia', 'Maria', 'Elisa', 'Noemi', 'Ilaria', 'Stefania', 'Eleonora', 'Erika','Maruska', 'Regina','Giorgia', 'Sofia', 'Elena', 'Lucia', 'Laura', 'Vanessa', 'Alice', 'Greta', 'Alessandra', 'Debora', 'Jessica', 'Nicole', 'Arianna', 'Angela', 'Marta', 'Paola', 'Veronica', 'Caterina', 'Roberta', 'Monica', 'Anna', 'Erica', 'Giada', 'Lisa', 'Claudia', 'Gaia']


def carica_comuni():
    comuni = dict()
    with open("comuni.txt") as comuni_f:
        ll = comuni_f.readlines()[1:]
        for l in ll:
            c = l.split(";")
            comuni[c[6]] = {"comune": c[1], "sigla_provincia": c[2]}
    return comuni

comuni = carica_comuni()

def carica_foto():
    foto = dict()
    foto["M"] = os.listdir("../people/male")
    foto["F"] = os.listdir("../people/female")
    return foto

foto = carica_foto()
random.shuffle(foto["M"])
random.shuffle(foto["F"])

class User:
    id = None
    email = None
    pwd = None # sha256(email)

    def __init__(self, id_user, email):
        self.id = id_user
        self.email = email
        self.salt = crypt.mksalt(crypt.METHOD_SHA512)[3:]
        self.pwd = hashlib.sha256((self.email+self.salt).encode()).hexdigest()

class Patient(User):
    firstName = None
    lastName = None
    birthDate = None
    strBirthDate = None
    birthPlace = None
    provincia_nascita = None
    sigla_provincia = None
    gender = None
    cf = None

    medico = None

    def __init__(self, id, firstName, lastName, gender, birthDate, birthPlaceCode, sigla_provincia):
        self.id = id

        self.firstName = firstName
        self.lastName = lastName

        self.email = self.firstName.lower().replace(" ","").replace("'","")+ \
            '.'+self.lastName.lower().replace(" ","").replace("'","")+"@gmail.com"
        self.salt = crypt.mksalt(crypt.METHOD_SHA512)[3:]
        self.pwd = hashlib.sha256((self.email+self.salt).encode()).hexdigest()

        self.strBirthDate = birthDate
        self.birthDate = time.strptime(self.strBirthDate, '%Y-%m-%d')

        self.gender = gender

        self.birthPlace = comuni[birthPlaceCode]["comune"]
        self.provincia_nascita = comuni[birthPlaceCode]["sigla_provincia"]
        self.sigla_provincia = sigla_provincia

        self.cf = calcola_cf(self.lastName, self.firstName,
            datetime.date(self.birthDate.tm_year, self.birthDate.tm_mon, self.birthDate.tm_mday),
            self.gender, self.birthPlace, birthPlaceCode)

        data = myDate.randomDate(self.strBirthDate, "2019-1-1", random.random())
        ora = random.randint(0, 23)
        minuti = random.randint(0, 59)
        secondi = random.randint(0, 59)
        self.id_foto = foto[gender].pop()
        self.data_ora_foto = "%s %02d:%02d:%02d"%(data, ora, minuti, secondi)
        self.path_foto = "%s_%02d:%02d:%02d.jpeg" % (data, ora, minuti, secondi)

    def __str__(self):
        return self.firstName + ", " + self.lastName + ", " + \
            self.strBirthDate + ", " + self.gender + ", " + \
            self.birthPlace + ", " + self.cf + ", " + self.email

    def random_date_esame(self):
        dt_prescrizione = myDate.randomDateTime(start = dt.fromtimestamp(mktime(self.birthDate)),
                                    end = dt.strptime("2017-1-1", '%Y-%m-%d'))
        dt_erogazione = myDate.randomDateTime(start = dt_prescrizione + relativedelta(days=1),
                                    end = dt_prescrizione + relativedelta(years=1))
        # return (dt.strftime(dt_prescrizione, '%Y-%m-%d %H:%M:%S'),
        #         dt.strftime(dt_erogazione, '%Y-%m-%d %H:%M:%S'))
        return dt_prescrizione, dt_erogazione

    def random_date_prescrizione(self):
        dt_prescrizione = myDate.randomDateTime(start = dt.strptime("2018-1-1", '%Y-%m-%d'),
                                    end = dt.strptime("2019-1-1", '%Y-%m-%d'))
        return dt_prescrizione

    def random_date_prescrizione_richiamo(self, dt_erogazione):
        dt_richiamo = myDate.randomDateTime(start = dt_erogazione + relativedelta(months=1),
                                    end = dt.strptime("2019-11-1", '%Y-%m-%d'))
        return dt_richiamo

    def random_date_richiamo(self, dt_erogazione):
        dt_richiamo_prescrizione = myDate.randomDateTime(start = dt_erogazione + relativedelta(months=1),
                                    end = dt.strptime("2018-4-1", '%Y-%m-%d'))
        dt_richiamo_erogazione = myDate.randomDateTime(start = dt_richiamo_prescrizione + relativedelta(days=1),
                                    end = dt_richiamo_prescrizione + relativedelta(months=4))
        return dt_richiamo_prescrizione, dt_richiamo_erogazione

    def report_esame(self, esame):
        return "L'esame '%s' effettuato sul paziente %s %s è negativo. Si raccomanda di effettuare nuovamente l'esame in caso di necessità." % (esame, self.firstName, self.lastName)

    def report_richiamo(self, visita):
        return "Richiamo della visita specialistica '%s'." % (visita)

    def report_visita_di_base(self):
        return "Visita di base effettuata sul paziente %s %s." % (self.firstName, self.lastName)

    def report_visita_specialistica(self, visita):
        return "La visita specialistica '%s' effettuata sul paziente %s %s non presenta particolari anomalie. Pertanto non è richiesta la prescrizione di alcun farmaco." % (visita, self.firstName, self.lastName)

class Medico(Patient):
    comune_medico = None
    sigla_provincia_medico = None

    def __init__(self, id, firstName, lastName, gender, birthDate, birthPlaceCode, codice_comune_medico):
        super().__init__(id, firstName, lastName, gender, birthDate, birthPlaceCode,
                            comuni[codice_comune_medico]["sigla_provincia"])

        self.comune_medico = comuni[codice_comune_medico]["comune"]
        self.sigla_provincia_medico = comuni[codice_comune_medico]["sigla_provincia"]

    def __str__(self):
        return "medico " + super().__str__()


class myDate:
    def strTimeProp(start, end, format, prop):
        """Get a time at a proportion of a range of two formatted times.

        start and end should be strings specifying times formated in the
        given format (strftime-style), giving an interval [start, end].
        prop specifies how a proportion of the interval to be taken after
        start.  The returned time will be in the specified format.
        """

        stime = time.mktime(time.strptime(start, format))
        etime = time.mktime(time.strptime(end, format))

        ptime = stime + math.log(1+prop*9)/math.log(10) * (etime - stime)

        return time.strftime(format, time.localtime(ptime))

    def randomDate(start, end, prop):
        return myDate.strTimeProp(start, end, '%Y-%m-%d', prop)

    def randomDateTime(start, end):
        delta = random.randint(24*60*60, (end-start).total_seconds())
        return start + relativedelta(seconds=delta)

def filtra_comuni(province_scelte):
    cc = dict()
    sigle_province = [p["sigla"] for p in province_scelte]
    for k in comuni.keys():
        if comuni[k]["sigla_provincia"] in sigle_province:
            cc[k] = comuni[k]
    return cc

def random_dati_persona():
    if(random.randint(0,1) == 0):
        first = male_firstNames[random.randint(0,len(male_firstNames)-1)]
        gender = 'M'
    else:
        first = female_firstNames[random.randint(0,len(female_firstNames)-1)]
        gender = 'F'

    last = lastNames[random.randint(0,len(lastNames)-1)]
    birthDate = myDate.randomDate("1930-1-1", "2010-1-1", random.random())
    birthPlaceCode = random.choice(list(comuni.keys()))

    return first, last, gender, birthDate, birthPlaceCode

def random_comune_by_provincia(sigla_provincia):
    cc = list()
    for k in comuni.keys():
        if comuni[k]["sigla_provincia"] == sigla_provincia:
            cc.append(k)
    return random.choice(cc)

def generate_persons(num_medici_base, num_specialisti, pazienti_per_medico, province_scelte):
    # comuni_filtrati = filtra_comuni(province_scelte)

    persons = dict()
    emails = dict()

    medici = list()
    specialisti = list()
    pazienti = list()

    mario = Medico(1, "Mario", "Rossi", "M", "1970-08-05", 'L378', 'L378')
    persons[mario.cf] = mario
    emails[mario.email] = True
    medici.append(mario)

    i = 1
    while i < num_medici_base:
        first, last, gender, birthDate, birthPlaceCode = random_dati_persona()
        sigla_provincia_medico = province_scelte[min(i//4, num_medici_base//4-1)]["sigla"]
        codice_comune_medico = random_comune_by_provincia(sigla_provincia_medico)
        p = Medico(i+1, first, last, gender, birthDate, birthPlaceCode, codice_comune_medico)

        # verifico non venga generata la stessa persona
        if p.cf not in persons:
            while p.email in emails:
                p.email = p.email.split("@")[0]+str(p.birthDate.tm_year)+"@gmail.com"

            if min(i//4, num_medici_base//4-1)*2 < i:
                p.medico = medici[-1]
                medici[-1].medico = p

            emails[p.email] = True
            persons[p.cf] = p
            medici.append(p)

            i += 1

    for m in medici:
        j = 0
        while j < pazienti_per_medico-1 and i < num_medici_base*pazienti_per_medico:
            first, last, gender, birthDate, birthPlaceCode = random_dati_persona()
            p = Patient(i+1, first, last, gender, birthDate, birthPlaceCode, m.sigla_provincia_medico)
            p.medico = m

            # verifico non venga generata la stessa persona
            if p.cf not in persons:
                while p.email in emails:
                    p.email = p.email.split("@")[0]+str(p.birthDate.tm_year)+"@gmail.com"

                emails[p.email] = True
                persons[p.cf] = p
                pazienti.append(p)

                i += 1
                j += 1

    specialisti = pazienti[:num_specialisti]
    pazienti = pazienti[num_specialisti:]

    return medici, specialisti, pazienti

if __name__ == '__main__':
    PAZIENTI_A_MEDICO = 15
    NUM_MEDICI = 7
    medici, specialisti, pazienti = generate_persons(NUM_MEDICI, NUM_MEDICI, PAZIENTI_A_MEDICO, [{"sigla":"TN"}, {"sigla":"BZ"}, {"sigla":"PD"}, {"sigla":"VR"}])

    for m in medici:
        print("%s %s -> %s %s" % (m.firstName, m.lastName, m.medico.firstName, m.medico.lastName))

    print("\n\n")
    for s in specialisti:
        print("%s %s -> %s %s" % (s.firstName, s.lastName, s.medico.firstName, s.medico.lastName))

    print("\n\n")
    for p in pazienti:
        print("%s %s -> %s %s" % (p.firstName, p.lastName, p.medico.firstName, p.medico.lastName))
