DROP DATABASE IF EXISTS "AziendaSanitaria";
CREATE DATABASE "AziendaSanitaria";

\connect "AziendaSanitaria"

CREATE TYPE gender AS ENUM ('M', 'F', 'N'); -- male/famale/none
CREATE TYPE user_type AS ENUM ('P', 'MB', 'MS', 'SSP', 'SSN', 'F'); -- paziente/medico di base/medico specialista/SSP/farmacia

CREATE TABLE provincia (
    sigla           CHAR(2) PRIMARY KEY,
    nome            TEXT NOT NULL UNIQUE,
    regione         TEXT NOT NULL
);

-- ATTORI ----------------------------------------------------------------------
CREATE TABLE utente (
    id              INTEGER PRIMARY KEY,
    email           TEXT NOT NULL UNIQUE,
    password        TEXT NOT NULL,
    salt            TEXT NOT NULL,
    tipo            USER_TYPE NOT NULL,

    -- recupero password
    recupero_token  TEXT,
    scadenza_recupero     TIMESTAMP,

    -- remember me
    token           TEXT UNIQUE,
    scadenza_token  TIMESTAMP
);

CREATE TABLE paziente (
    id              INTEGER PRIMARY KEY REFERENCES utente ON DELETE CASCADE,
    nome            TEXT NOT NULL,
    cognome         TEXT NOT NULL,
    data_nascita    DATE NOT NULL,
    luogo_nascita   TEXT NOT NULL,
    sigla_provincia_nascita CHAR(2) NOT NULL REFERENCES provincia,
    codice_fiscale  TEXT NOT NULL UNIQUE,
    sesso           GENDER NOT NULL,
    sigla_provincia CHAR(2) NOT NULL REFERENCES provincia
);

CREATE TABLE medico_di_base (
    id              INTEGER PRIMARY KEY REFERENCES paziente ON DELETE CASCADE,
    comune_medico   TEXT NOT NULL,
    sigla_provincia_medico CHAR(2) NOT NULL REFERENCES provincia
);

ALTER TABLE paziente
ADD id_medico_di_base INTEGER REFERENCES medico_di_base ON DELETE SET NULL;

ALTER TABLE paziente
ADD CHECK (id_medico_di_base != id);

CREATE TABLE medico_specialista (
    id              INTEGER PRIMARY KEY REFERENCES paziente ON DELETE CASCADE
);

CREATE TABLE ssp (
    id              INTEGER PRIMARY KEY REFERENCES utente ON DELETE CASCADE,
    sigla_provincia CHAR(2) NOT NULL REFERENCES provincia
);

CREATE TABLE farmacia (
    id              INTEGER PRIMARY KEY REFERENCES utente ON DELETE CASCADE,
    nome            TEXT NOT NULL,
    comune          TEXT NOT NULL,
    sigla_provincia CHAR(2) NOT NULL REFERENCES provincia
);

CREATE TABLE ssn (
    id              INTEGER PRIMARY KEY REFERENCES utente ON DELETE CASCADE
);
-- /ATTORI ---------------------------------------------------------------------

CREATE TABLE foto (
    id              SERIAL PRIMARY KEY,
    id_paziente     INTEGER NOT NULL REFERENCES paziente ON DELETE CASCADE,
    path_foto       TEXT NOT NULL,
    data_ora_foto       TIMESTAMP WITHOUT TIME ZONE NOT NULL

    -- CHECK (data_foto > (SELECT P.data_nascita FROM paziente P WHERE P.id = id_paziente))
);

CREATE TABLE visita_di_base (
    id              SERIAL PRIMARY KEY,
    id_medico_di_base       INTEGER NOT NULL REFERENCES medico_di_base,
    id_paziente     INTEGER NOT NULL REFERENCES paziente,
    data_ora        TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    report          TEXT
);

-- visita medico specialista
CREATE TABLE tipo_di_visita (
    id              INTEGER PRIMARY KEY,
    nome            TEXT NOT NULL UNIQUE,
    ticket          INTEGER NOT NULL, -- 50 euro

    CHECK (ticket >= 0)
);

CREATE TABLE visita_specialistica (
    id              SERIAL PRIMARY KEY,
    id_medico_di_base  INTEGER REFERENCES medico_di_base, -- NULL se è un richiamo
    id_paziente     INTEGER NOT NULL REFERENCES paziente,
    id_tipo_visita  INTEGER NOT NULL REFERENCES tipo_di_visita,
    data_ora_prescrizione TIMESTAMP WITHOUT TIME ZONE NOT NULL,

    -- visita specialistica erogata
    id_medico_spec  INTEGER REFERENCES medico_specialista,
    data_ora_erogazione TIMESTAMP WITHOUT TIME ZONE,
    report          TEXT, -- anamnesi

    CHECK (id_medico_di_base != id_paziente),
    CHECK (id_medico_spec != id_paziente)
);


CREATE TABLE farmaco (
    id              INTEGER PRIMARY KEY,
    nome            TEXT NOT NULL UNIQUE,
    ticket          INTEGER NOT NULL, -- 3 euro a ricetta

    CHECK (ticket >= 0)
);

CREATE TABLE ricetta (
    id              SERIAL PRIMARY KEY,
    id_farmaco      INTEGER NOT NULL REFERENCES farmaco, -- un solo farmaco a ricetta
    quantita        INTEGER NOT NULL,
    id_medico_di_base  INTEGER NOT NULL REFERENCES medico_di_base,
    id_paziente     INTEGER NOT NULL REFERENCES paziente,
    data_ora_prescrizione TIMESTAMP WITHOUT TIME ZONE NOT NULL,

    -- ricetta erogata
    id_farmacia     INTEGER REFERENCES farmacia,
    data_ora_erogazione TIMESTAMP WITHOUT TIME ZONE,

    CHECK (id_medico_di_base != id_paziente),
    CHECK (quantita > 0)
);

-- esame SSP
CREATE TABLE tipo_di_esame (
    id                INTEGER PRIMARY KEY,
    nome              TEXT NOT NULL UNIQUE,
    ticket            INTEGER NOT NULL, -- 11 euro

    CHECK (ticket >= 0)
);

CREATE TABLE esame (
    id                SERIAL PRIMARY KEY,
    id_paziente       INTEGER NOT NULL REFERENCES paziente,
    id_medico_di_base    INTEGER NOT NULL REFERENCES medico_di_base,
    id_tipo_esame     INTEGER NOT NULL REFERENCES tipo_di_esame,
    data_ora_prescrizione TIMESTAMP WITHOUT TIME ZONE NOT NULL,

    -- esame erogato da SSP
    id_ssp            INTEGER REFERENCES ssp,
    data_ora_erogazione   TIMESTAMP WITHOUT TIME ZONE,
    report            TEXT, -- anamnesi

    CHECK (id_medico_di_base != id_paziente)
);
