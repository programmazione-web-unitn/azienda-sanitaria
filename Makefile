USER="postgres"

main: help

populate_db:
	@psql -h localhost --username=$(USER)  --file=scripts/database/schema.sql --file=scripts/database/static_data.sql --file=scripts/database/population.sql

clean_srv:
	@sudo rm -rf /srv/pazienti

setup_srv: clean_srv
	@sudo cp -R scripts/pazienti /srv/pazienti
	@sudo chmod o+rw -R /srv/pazienti
	@sudo cp AziendaSanitaria/src/main/webapp/images/logo/logo.png /srv/logo.png
	@sudo chmod o+rw -R /srv/logo.png

setup: populate_db setup_srv

help:
	@echo "\
\033[1mAzienda Sanitaria\033[0m\n\n\
\033[1mMain options\033[0m\n\n\
\t1. \033[1mmake populate_db\033[0m : create and populate the database.\n\
\t2. \033[1mmake clean_srv\033[0m   : remove the users in /srv/pazienti. \n\
\t3. \033[1mmake setup_srv\033[0m   : setup the users in /srv/pazienti.\n\
\t4. \033[1mmake setup\033[0m       : populate_db and setup_srv all at once.\n\n\
\033[1mNotes\033[0m\n\n\
The default database user is \"postgres\".\n\
It is possible to change the user for the db commands.\n\n\
\t1. \033[1mmake populate_db USER=foo\033[0m : create and populate the database for the user "foo".\n\
\t2. \033[1mmake setup USER=foo\033[0m       : setup the database for the user "foo".\n"
