# Azienda sanitaria

Progetto di "introduzione alla programmazione per il web" per l'anno 2018-2019.<br>
Il progetto è stato sviluppato e testato in ambiente Linux.

## Gruppo
* Damaschin Dumitru : `192304`
* Lusuardi Davide: `192271`
* Masina Gabriele: `193594`
* Tomaselli Francesco: `193486`
* Zotta Giovanni: `196707`

# Configurazione DB

## Scaricare dipendenze
*  **postgresql drivers** - Su Netbeans, tasto destro su Dependencies-->Download Declared Dependencies
*  **librerie per DAO** -
	* Clonare in una cartella [le librerie per DAO](https://gitlab.com/unitn.it/disi/web-programming/commons/persistence/api)  
		ssh: git@gitlab.com:unitn.it/disi/web-programming/commons/persistence/api.git
	* Aprire in Netbeans i progetti persistence-api e AziendaSanitaria
	* Effettuare "Clean & Build" nell'ordine di persistence-api e AziendaSanitaria
	* Verificare che siano presenti le dipendenze corrette in AziendaSanitaria-->Dependecies e che il progetto compili.

## Interfacciarsi con PostgreSQL
* Si assume che `postgresql` e `Tomcat` siano installati e in ascolto sulle loro porte di default.
* Modificare nel deployment descriptor (web.xml):
	* `dbuser` -> utente postgres configurato nel sistema (default = postgres)
	* `password` -> password dell'utente postgres configurato nel sistema (default = password)

## Generare e popolare il database
* Generare il database
	* Nel `makefile` è definita una regola per generare e popolare il database:
	```
    make setup
	```
	* Di default, viene utilizzato l'utente denominato `postgres`.
	* Per utilizzare un utente diverso da quello di default:
	```
	make setup USER=nome_utente
	```
	* Per avere più informazione delle funzionalità del Makefile:
	```
	make help
	```
