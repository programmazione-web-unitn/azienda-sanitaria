// Initialize the platform object:
var platform = new H.service.Platform({
    'apikey': '0Inb1t3P9gy3nb8D1j49PPtW6w63PO8po7La6gaUf9g'
});

var map;
var ui;

// create a group object to hold map markers
var group = new H.map.Group();
var GPSmarker;
var bubble;

var idMapContainer;

var idNotification;
var idNotificationText;

var onShowCallback;
var maxDist;

function openBubble(position, text, ui) {
    if (!bubble) {
        bubble = new H.ui.InfoBubble(position, {content: text});
        ui.addBubble(bubble);
    } else {
        bubble.setPosition(position);
        bubble.setContent(text);
        bubble.open();
    }
}

function openMarker(idMarker) {
    if (!group){
        console.log("error openMarker: group undefined");
        return;
    }
    
    if (!ui){
        console.log("error openMarker: ui undefined");
        return;
    }

    var objects = group.getObjects();
    openBubble(objects[idMarker].getGeometry(), objects[idMarker].getData(), ui);
    map.setCenter(objects[idMarker].getGeometry());

}

function addGPSMarker(map, position) {
    // Define a variable holding SVG mark-up that defines an icon image:
    var svgGPS = '<svg width="40" height="40" xmlns="http://www.w3.org/2000/svg">' +
            '<circle cx="20" cy="20" r="20" fill="rgba(66, 134, 243, 0.4)" />' +
            '<circle cx="20" cy="20" r="15" stroke="white" stroke-width="1" fill="rgba(66, 134, 243, 1)" />' +
            '</svg>';

    var icon = new H.map.Icon(svgGPS);
    GPSmarker = new H.map.Marker({lat: position.coords.latitude, lng: position.coords.longitude}, {icon: icon});
    map.addObject(GPSmarker);
}

function initMap(position) {

    // Obtain the default map types from the platform object
    var maptypes = platform.createDefaultLayers();

    // Instantiate (and display) a map object:
    map = new H.Map(
            document.getElementById(idMapContainer),
            maptypes.vector.normal.map,
            {
                zoom: 14,
                center: {lng: position.coords.longitude, lat: position.coords.latitude}
            });

    // add a resize listener to make sure that the map occupies the whole container
    window.addEventListener('resize', () => map.getViewPort().resize());

    // creazione controller mappa interattiva
    ui = H.ui.UI.createDefault(map, maptypes, 'it-IT'); // ui globale

    // Enable the event system on the map instance:
    var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

    // add your position marker
    addGPSMarker(map, position);    

    // add the 'tap' event listener
    group.addEventListener('tap', function (evt) {
//            map.setCenter(evt.target.getGeometry());
        openBubble(evt.target.getGeometry(), evt.target.getData(), ui);
    }, false);

    // add the group object to the map
    map.addObject(group);
    
}

var options = {
    enableHighAccuracy: true,
    timeout: 10000,
    maximumAge: 0
};

// onError Callback receives a PositionError object
//
function onError(error) {
    console.log('code: '    + error.code    + '\n' +
          'message: ' + error.message + '\n');
}

// onSuccess Callback
//   This method accepts a `Position` object, which contains
//   the current GPS coordinates
//
var pos = null;
function onSuccess(position) {
    const err = 0.0005;
    if(!pos || 
        position.coords.longitude > pos.coords.longitude+err || position.coords.longitude < pos.coords.longitude-err ||
        position.coords.latitude > pos.coords.latitude+err || position.coords.latitude < pos.coords.latitude-err){
        
        pos = position;
        console.log("posizione cambiata");
        findPharmacies(position);
    }
}

async function showMap(idMap, callback) {    
    if (!navigator.geolocation) {
        alert("Geolocalizzazione non supportata da questo browser.");
        return;
    }
    
    idMapContainer = idMap;
    onShowCallback = callback;    
    
    navigator.geolocation.getCurrentPosition(initMap);
    navigator.geolocation.watchPosition(onSuccess, onError, options);
    
}

function findNearestMarker(coords) {
    if (!group){
        console.log("error findNearestMarker: group undefined");
        return;
    }        

    var minDist = Infinity,
            nearest = null,
            markerDist,
            // get all objects added to the map
            objects = group.getObjects(),
            len = objects.length,
            i;

    // iterate over objects and calculate distance between them
    for (i = 0; i < len; i += 1) {
        markerDist = objects[i].getGeometry().distance(coords); // distance in meters
        if (markerDist < minDist) {
            minDist = markerDist;
            nearest = objects[i];
        }
    }

    if (nearest) {
        console.log(nearest.name);
        console.log(minDist);
        
        if(idNotification && minDist < maxDist){
            document.getElementById(idNotificationText).textContent = nearest.name + " distante " + Math.round(minDist) + " metri circa";
            notificationShow(idNotification); // bootstrap-italia function
            console.log("notifica farmacia vicina mostrata");
        }        

        if(ui) openBubble(nearest.getGeometry(), nearest.getData(), ui);
        if(GPSmarker) GPSmarker.setGeometry(coords);
        if(map) map.setCenter(coords);
        
    } else {
        console.log('Nessuna farmacia vicina');
    }
}

function findPharmacies(position) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            try {
                var data = JSON.parse(this.responseText);

                group.removeAll();
                var places = [];

                let i=0;
                for (let place of data.results) {
                    if (place["position"] != undefined) {
                        var marker = new H.map.Marker({lat: place["position"][0], lng: place["position"][1]});
                        marker.setData("<div><b>" + place.title + "</b></div>" +
                                "<div>" + place.vicinity + "</div>");
                        marker.name = place.title;
                        
                        group.addObject(marker);
                        
                        let markerDist = marker.getGeometry().distance({lng: position.coords.longitude, lat: position.coords.latitude}); // distance in meters
                        let row = [place.title, Math.round(markerDist)];
                        row.idMarker = i;
                        places.push(row);
                        i++;
                    }
                }

                findNearestMarker({lng: position.coords.longitude, lat: position.coords.latitude});
                
                if(onShowCallback){
                    onShowCallback(places);
                }

            } catch (err) {
                console.log(err.message);
                alert("Si è verificato un errore. Ricarica la pagina per riprovare.");
                return;
            }
        }
    };
    xhttp.open("GET", "https://places.cit.api.here.com/places/v1/autosuggest?at=" + position.coords.latitude + "," + position.coords.longitude + "&q=farmacia&app_id=83DNptdZV2OSIyRaIlIq&app_code=Rugkd1oQwi5UZ2srhHCWBA", true);
    xhttp.send();
}

function checkPharmacies(idNot, idNotText, dist){
    if (!navigator.geolocation) {
        alert("Geolocalizzazione non supportata da questo browser.");
        return;
    }
    
    idNotification = idNot;
    idNotificationText = idNotText;
    maxDist = dist;
    
    navigator.geolocation.watchPosition(onSuccess, onError, options);

}