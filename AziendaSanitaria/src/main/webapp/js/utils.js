/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 */

//--------- FUNZIONI FORM GENERICO - SHOW-HIDE ---------------
function primaryListener(e) {
    const primary = e.target;
    const secondary = primary.secondary[0];
    $(primary).removeClass('animated fadeOutLeft faster').addClass("d-none");
    $(secondary).removeClass("d-none").addClass("animated fadeInRight faster");
    secondary.removeEventListener('animationend', secondaryListener, true);
}

function secondaryListener(e) {
    const secondary = e.target;
    const primary = secondary.primary[0];
    $(secondary).removeClass('animated fadeOutRight faster').addClass("d-none");
    $(primary).removeClass("d-none").addClass("animated fadeInLeft  faster");
    primary.removeEventListener('animationend', primaryListener, true);
}


function showSecondary(e, primary, secondary) {
    e.preventDefault();
    primary.removeClass("fadeInLeft").addClass('fadeOutLeft');
    primary[0].secondary = secondary;
    primary[0].addEventListener('animationend', primaryListener, true);
}

function showPrimary(e, primary, secondary) {
    e.preventDefault();
    secondary.removeClass("fadeInRight").addClass('fadeOutRight');
    secondary[0].primary = primary;
    secondary[0].addEventListener('animationend', secondaryListener, true);
}


//--------- FUNZIONI FORM SSP EROGA ESAME -------------------
function putInfos(e, itemName, infos) {
    const id = e.target.id.split('-')[1];
    for (info of infos) {
        $(`#${info}Form`).html($(`#${info}-${id}`).html());
    }
    $(`#${itemName}`).val(id);
}

//function showReportEsame(e) {
//    e.preventDefault();
//    const tabEsami = $('#esamiDaErogare');
//    const idEsame = e.target.id.split('-')[1];
//    $('#tipoDiEsameForm').html($('#tipoDiEsame-' + idEsame).html());
//    $('#dataOraPrescForm').html($('#dataOraPresc-' + idEsame).html());
//    $('#idEsame').val(idEsame);
//
//    tabEsami.removeClass('fadeInLeft').addClass('fadeOutLeft');
//    tabEsami[0].addEventListener('animationend', esamiListener, true);
//}
//
//function hideReportEsame(e) {
//    e.preventDefault();
//    const report = $('#scriviReport');
//    report.removeClass('fadeInRight').addClass('fadeOutRight');
//    report[0].addEventListener('animationend', reportEsameListener, true);
//}
//
////--------- FUNZIONI FORM SPECIALISTA EROGA VISITA -------------------
//
//
//function visiteListener() {
//    const report = $('#scriviReport');
//    $('#visiteDaErogare').removeClass('animated fadeOutLeft faster').addClass("d-none");
//    report.removeClass("d-none").addClass("animated fadeInRight faster");
//    report[0].removeEventListener('animationend', reportVisitaListener, true);
//}
//
//function reportVisitaListener() {
//    const tabVisite = $('#visiteDaErogare');
//    $('#scriviReport').removeClass('animated fadeOutRight faster').addClass("d-none");
//    tabVisite.removeClass("d-none").addClass("animated fadeInLeft faster");
//    tabVisite[0].removeEventListener('animationend', visiteListener, true);
//
//}
//
//function showReportVisita(e) {
//    e.preventDefault();
//
//    const tabVisite = $('#visiteDaErogare');
//    const idVisita = e.target.id.split('-')[1];
//    $('#tipoDiVisitaForm').html($('#tipoDiVisita-' + idVisita).html());
//    $('#dataOraPrescForm').html($('#dataOraPresc-' + idVisita).html());
//    $('#idVisita').val(idVisita);
//
//    tabVisite.removeClass('fadeInLeft').addClass('fadeOutLeft');
//    tabVisite[0].addEventListener('animationend', visiteListener, true);
//}
//
//function hideReportVisita(e) {
//    e.preventDefault();
//    const report = $('#scriviReport');
//    report.removeClass('fadeInRight').addClass('fadeOutRight');
//    report[0].addEventListener('animationend', reportVisitaListener, true);
//}

//------------------



$(function () {
    $("#scrollButton").on('click', function (e) {
        e.preventDefault();
        var navHeight = document.getElementById('homepageNavbar').clientHeight;
        $('html, body').animate({scrollTop: $($(this).attr('href')).offset().top - navHeight}, 500, 'linear');
    });
});
//
// $.fn.isInViewport = function() {
//     var elementTop = $(this).offset().top;
//     var elementBottom = elementTop + $(this).outerHeight();
//     var viewportTop = $(window).scrollTop();
//     var viewportBottom = viewportTop + $(window).height();
//     return elementBottom > viewportTop && elementTop < viewportBottom;
// };
//
//
// $(".immaginiHome").isInViewport(function(px){
//     if(px) $(this).addClass('animated', 'fadeInLeft') ;
//
// });

$(document).ready(function ($) {
    $('*[data-href]').on('click', function () {
        window.location = $(this).data("href");
    });
});
//function showPassword(idInputPassword) {
//    $("input[name='" + idInputPassword + "']").prop("type", "text");
//}
//
//function hidePassword(idInputPassword) {
//    $("input[name='" + idInputPassword + "']").prop("type", "password");
//}

function lessDate(date1, date2) {
    const [d1, m1, y1] = date1.split('/');
    const [d2, m2, y2] = date2.split('/');
    return new Date(`${y1}-${m1}-${d1}`) <= new Date(`${y2}-${m2}-${d2}`);
}

function setDateError(field, error) {
    field.setCustomValidity('error');
    const fieldParent = field.closest('div.input-group');
    $(fieldParent).removeClass('date-success').addClass('date-danger');
    $(error).removeClass('text-success').addClass('text-danger');
}

function setDateSuccess(field, error) {
    field.setCustomValidity('');
    const fieldParent = field.closest('div.input-group');
    $(fieldParent).removeClass('date-danger').addClass('date-success');
    $(error).removeClass('text-danger').addClass('text-success');
}

function checkDateRange(startDate, endDate) {
    const startError = $(`[data-error-for='${startDate.id}']`);
    const endError = $(`[data-error-for='${endDate.id}']`);
    const rangeError = $(`[data-error-for='${startDate.id.split('StartDate')[0]}RangeDate']`);
    let form = startDate.closest('form');
    let submit = $(form).find(':submit');
    submit.prop('disabled', true);
    if (startDate.value == "" && endDate.value == "") {
        // onload
        startDate.setCustomValidity('error');
    } else if (startDate.value == "" && endDate.value != "") {
        setDateError(startDate, startError);
        setDateSuccess(endDate, endError);
    } else if (startDate.value != "" && endDate.value == "") {
        setDateError(endDate, endError);
        setDateSuccess(startDate, startError);
    } else if (!lessDate(startDate.value, endDate.value)) {
        setDateSuccess(startDate, startError);
        setDateSuccess(endDate, endError);
        $(rangeError).removeClass('text-success').addClass('text-danger');
    } else {
        $(rangeError).removeClass('text-danger').addClass('text-success');

        setDateSuccess(startDate, startError);
        setDateSuccess(endDate, endError);
        submit.prop('disabled', false);
    }
}

function checkAgeRange(minAge, maxAge) {

    const minError = $(`[data-error-for='${minAge.id}']`);
    const maxError = $(`[data-error-for='${maxAge.id}']`);
    const rangeError = $(`[data-error-for='range${minAge.id.split('min')[1]}']`);
    let form = minError.closest('form');
    let submit = $(form).find(':submit');
    submit.prop('disabled', true);
    if (minAge.value == "" && maxAge.value == "") {
        minAge.setCustomValidity('error');
    } else if (minAge.value == "" && maxAge.value != "") {
        setInputError(minAge, minError);
        setInputSuccess(maxAge, maxError);
    } else if (minAge.value != "" && maxAge.value == "") {
        setInputError(maxAge, minError);
        setInputSuccess(minAge, minError);
    } else {
        setInputSuccess(minAge, minError);
        setInputSuccess(maxAge, maxError);
        if (parseInt(minAge.value) > parseInt(maxAge.value)) {
            minAge.setCustomValidity('error');
            $(rangeError).removeClass('text-success').addClass('text-danger');
        } else {
            minAge.setCustomValidity('');
            $(rangeError).removeClass('text-danger').addClass('text-success');
            let allValid = true;
            $(form).find('input, select').each(function () {
                allValid &= this.checkValidity();
            });
            if (allValid) {
                submit.prop('disabled', false);
            }
        }
    }
}

function setInputSuccess(field, error, icon) {
    field.setCustomValidity("");
    $(field).removeClass('border-danger').addClass('border-success');
    error.removeClass('text-danger').addClass('text-success');
    if (icon) {
        icon.removeClass('border-danger').addClass('border-success');
        icon.children(0).removeClass('icon-danger').addClass('icon-success');
    }
}

function setInputError(field, error, icon) {
    field.setCustomValidity('error');
    $(field).removeClass('border-success').addClass('border-danger');
    error.removeClass('text-success').addClass('text-danger');
    if (icon) {
        icon.removeClass('border-success').addClass('border-danger');
        icon.children(0).removeClass('icon-success').addClass('icon-danger');
    }
}

function checkPassword(passwordField, confirmField, load) {
    let re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
    let form = passwordField.closest('form');
    let submit = $(form).find(':submit');
    let errorField = $(`[data-error-for='${passwordField.id}']`);
    let lockIcon = $(`[data-lock-for='${passwordField.id}']`);
    if (load) {
        passwordField.setCustomValidity('error');
        submit.prop('disabled', true);
        return;
    }
    if (!re.test(passwordField.value)) {
        setInputError(passwordField, errorField, lockIcon);
        submit.prop('disabled', true);
    } else {
        setInputSuccess(passwordField, errorField, lockIcon);
        if (confirmField.checkValidity()) {
            submit.prop('disabled', false);
        }
    }
}

function checkConfirmPassword(passwordField, confirmField, load) {
    let form = passwordField.closest('form');
    let submit = $(form).find(':submit');
    let errorField = $(`[data-error-for='${confirmField.id}']`);
    let lockIcon = $(`[data-lock-for='${confirmField.id}']`);
    if (load) {
        confirmField.setCustomValidity('error');
        submit.prop('disabled', true);
        return;
    }
    if (passwordField.value != confirmField.value) {
        setInputError(confirmField, errorField, lockIcon);
        submit.prop('disabled', true);
    } else {
        setInputSuccess(confirmField, errorField, lockIcon);
        if (passwordField.checkValidity()) {
            submit.prop('disabled', false);
        }
    }
}


function checkRequired(field, load) {
    const error = $(`[data-error-for='${field.id}'`)
    const form = field.closest('form');
    const submit = $(form).find(':submit');
    const icon = $(`[data-lock-for='${field.id}']`);
    if (field.value == "") {
        if (load)
            field.setCustomValidity('error');
        else
            setInputError(field, error, icon);
        submit.prop('disabled', true);
    } else {
        setInputSuccess(field, error, icon);
        let allValid = true;
        $(form).find('input, select').each(function () {
            allValid &= this.checkValidity();
        });
        if (allValid) {
            submit.prop('disabled', false);
        }
    }
}

function checkNotZero(field, load) {
    const error = $(`[data-error-for='${field.id}'`)
    const form = field.closest('form');
    const submit = $(form).find(':submit');
    const icon = $(`[data-lock-for='${field.id}']`);
    if (field.value == "" || parseInt(field.value) == 0) {
        if (load)
            field.setCustomValidity('error');
        else
            setInputError(field, error, icon);
        submit.prop('disabled', true);
    } else {
        setInputSuccess(field, error, icon);
        let allValid = true;
        $(form).find('input, select').each(function () {
            allValid &= this.checkValidity();
        });
        if (allValid) {
            submit.prop('disabled', false);
        }
    }
}
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function checkEmail(field, load) {
    const error = $(`[data-error-for='${field.id}'`)
    const form = field.closest('form');
    const submit = $(form).find(':submit');
    if (!validateEmail(field.value)) {
        if (load)
            field.setCustomValidity('error');
        else
            setInputError(field, error);
        submit.prop('disabled', true);
    } else {
        setInputSuccess(field, error);
        let allValid = true;
        $(form).find('input, select').each(function () {
            allValid &= this.checkValidity();
        });
        if (allValid) {
            submit.prop('disabled', false);
        }
    }
}

function readURL(input, idPreview) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(`#${idPreview}`).children(0).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

// Restricts input for each element in the set of matched elements to the given inputFilter.
(function ($) {
    $.fn.inputFilter = function (inputFilter) {
        return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            } else {
                this.value = "";
            }
        });
    };
}(jQuery));
$(document).ready(function () {
    $('.secure-password-input').each(function () {
        let passwordField = this;
        let confirmField = $(`[data-confirm-password-for='${passwordField.id}']`)[0];
        checkPassword(passwordField, confirmField, true);
        checkConfirmPassword(passwordField, confirmField, true);
        passwordField.addEventListener("input", function (event) {
            checkPassword(passwordField, confirmField);
            checkConfirmPassword(passwordField, confirmField);
        });
        confirmField.addEventListener("input", function (event) {
            checkConfirmPassword(passwordField, confirmField);
        });
    });
    $('.it-date-datepicker').datepicker({
        inputFormat: ["dd/MM/yyyy"],
        outputFormat: 'dd/MM/yyyy',
    });
    $('.it-date-datepicker').on('click', function (e) {
        e.stopPropagation();
        $(this).siblings('a').trigger("click", !0);
    });
    $('.it-date-datepicker').each(function () {
        if (this.id.endsWith('StartDate')) {
            const endDate = $(`#${this.id.split('StartDate')[0]}EndDate`)[0];
            const startDate = this;
            checkDateRange(startDate, endDate);
            $(startDate).on('change', function () {
                checkDateRange(startDate, endDate);
            });
            $(endDate).on('change', function () {
                checkDateRange(startDate, endDate);
            });
        }
    });
    $(".posIntReq").each(function () {
        $(this).inputFilter(function (value) {
            return /^\d*$/.test(value) && (value == "" || parseInt(value) <= 999);
        });
//        checkRequired(this, true);
        checkNotZero(this, true);
        $(this).on('input', function () {
//            checkRequired(this);
            checkNotZero(this);
        });
    });
    $(".ageField").each(function () {
        $(this).inputFilter(function (value) {
            return /\d*$/.test(value) && (value === "" || parseInt(value) <= 999);
        });
        if (this.id.startsWith('min')) {
            const maxAge = $(`#max${this.id.split('min')[1]}`)[0];
            const minAge = this;
            checkAgeRange(minAge, maxAge);
            $(minAge).on('input', function () {
                checkAgeRange(minAge, maxAge);
            });
            $(maxAge).on('input', function () {
                checkAgeRange(minAge, maxAge);
            });
        }
    });
    $(".simple-select2").each(function () {
        checkRequired(this, true);
    });
    $(".simple-select2").on("change input", function (e) {
        checkRequired(this);
    })

});