<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>APSS - Servizi online</title>
        <%@include file="/html/jsCssInclude.html" %>

        <script>
            function getCookie(name) {
                var dc = document.cookie;
                var prefix = name + "=";
                var begin = dc.indexOf("; " + prefix);
                if (begin == -1) {
                    begin = dc.indexOf(prefix);
                    if (begin != 0) return null;
                } else
                    {
                        begin += 2;
                        var end = document.cookie.indexOf(";", begin);
                        if (end == -1) {
                            end = dc.length;
                        }
                    }
                    // because unescape has been deprecated, replaced with decodeURI
                    //return unescape(dc.substring(begin + prefix.length, end));
                    return decodeURI(dc.substring(begin + prefix.length, end));
                }
            $(document).ready(
                function() {
                    var cookie_consent = getCookie("cookies_consent");

                    if (cookie_consent == null) {
                        document.getElementById("cookie_consent").style.display = 'flex';
                    }
                }
            );
        </script>
    </head>

    <body>
        <div class="cookiebar" id="cookie_consent" style="display: none !important;">
            <p>Questo sito utilizza cookie tecnici, analytics e di terze parti. <br>Proseguendo nella navigazione accetti l’utilizzo dei cookie.</p>
            <div class="cookiebar-buttons">
                <a href="/AziendaSanitaria/cookies" class="cookiebar-btn">Informativa<span class="sr-only">cookies</span></a>
                <button data-accept="cookiebar" class="cookiebar-btn cookiebar-confirm">Accetto<span class="sr-only"> i cookies</span></button>
            </div>
        </div>

        <%@include file="/html/headers/homepageHeader.jsp" %>

        <div class="it-hero-wrapper it-dark it-overlay it-text-centered full-height shadow">
              <!-- - img-->
              <div class="img-responsive-wrapper">
                <div class="img-responsive">
                  <div class="img-wrapper">
                    <img
                      src="/AziendaSanitaria/images/home/home.jpg"
                      title="img title"
                      alt="imagealt"
                    />
                  </div>
                </div>
              </div>
              <!-- - texts-->
              <div class="container">
                <div class="row">
                  <div class="col-12">
                    <div class="it-hero-text-wrapper bg-dark">
                      <!-- <span class="it-category">Category</span> -->
                      <h1>Azienda Sanitaria</h1>
                      <p class="lead">
                        Accedi alla tua cartella clinica ovunque tu sia in totale comodità
                    </p>
                      <div class="it-btn-container">
                          <a class="btn btn-secondary btn-sm" href="#features">
                              <svg class="icon icon-sm icon-white">
                                  <use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-arrow-down"></use>
                              </svg>
                              <span class="sr-only">Vai giù</span>
                          </a>
                      </div>





                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="section shadow-lg section-muted" id="features">

              <div class="section-content">
                <!-- contenuto di esempio START -->
                <div class="container">
                  <div class="row">
                    <div class="immaginiHome offset-lg-1 offset-xl-1 col-12 col-lg-4 col-xl-4 col-md-6 col-sm-12 col-xs-12 pr-0 pr-md-5 mb-3">
                        <div class="img-responsive-wrapper shadow">
                          <div class="img-responsive">
                            <div class="img-wrapper">
                              <img
                                src="/AziendaSanitaria/images/features/esamiHome.jpg"
                                title="img title"
                                alt="imagealt"
                              />
                            </div>
                          </div>
                        </div>
                    </div>

                    <div class="offset-lg-1 offset-xl-1 col-12 col-lg-4 col-xl-4 col-md-6 col-sm-12 col-xs-12 pr-md-5 mb-3">
                        <h2>Esami</h2>
                        <p class="text-justify">
                            Visualizza gli esiti dei tuoi esami e delle tue visite specialistiche,
                            controlla il report dei medici e visualizza i tuoi prossimi appuntamenti.
                        </p>
                    </div>

                  </div>
                </div>
                <!-- contenuto di esempio END -->
              </div>
            </div>

            <div class="section shadow-lg section-primary white-color z-index-1">
              <div class="section-content">
                <!-- contenuto di esempio START -->
                <div class="container">
                  <div class="row">
                    <div class="immaginiHome offset-lg-1 offset-xl-1 col-12 col-lg-4 col-xl-4 col-md-6 col-sm-12 col-xs-12 pr-0 pr-md-5 mb-3">
                        <div class="img-responsive-wrapper shadow">
                          <div class="img-responsive">
                            <div class="img-wrapper">
                              <img
                                src="/AziendaSanitaria/images/features/cartellaClinicaHome.jpg"
                                title="img title"
                                alt="imagealt"
                              />
                            </div>
                          </div>
                        </div>
                    </div>

                    <div class="offset-lg-1 offset-xl-1 col-12 col-lg-4 col-xl-4 col-md-6 col-sm-12 col-xs-12 pr-md-5 mb-3">
                        <h2>Cartella clinica</h2>
                        <p class="text-justify">
                            Visualizza la tua cartella clinica ovunque tu sia in totale comodit&agrave;,
                            controlla i tuoi dati e modifica il tuo profilo.
                        </p>
                    </div>

                  </div>
                </div>
                <!-- contenuto di esempio END -->
              </div>
            </div>

            <div class="section section-muted z-index-2">
              <div class="section-content">
                <!-- contenuto di esempio START -->
                <div class="container">
                  <div class="row">
                    <div class="immaginiHome offset-lg-1 offset-xl-1 col-12 col-lg-4 col-xl-4 col-md-6 col-sm-12 col-xs-12 pr-0 pr-md-5 mb-3">
                        <div class="img-responsive-wrapper  shadow">
                          <div class="img-responsive">
                            <div class="img-wrapper">
                              <img
                                src="/AziendaSanitaria/images/features/ricetteHome.jpg"
                                title="img title"
                                alt="imagealt"
                              />
                            </div>
                          </div>
                        </div>
                    </div>

                    <div class="offset-lg-1 offset-xl-1 col-12 col-lg-4 col-xl-4 col-md-6 col-sm-12 col-xs-12 pr-md-5 mb-3">
                        <h2>Ricette e farmacie</h2>
                        <p class="text-justify">
                            Visualizza le ultime ricette prescritte e scopri in un attimo quali sono le farmacie pi&ugrave; vicine a te.
                        </p>
                    </div>

                  </div>
                </div>
                <!-- contenuto di esempio END -->
              </div>
            </div>


        <%@include  file="/html/footer.html" %>

    </body>

</html>
