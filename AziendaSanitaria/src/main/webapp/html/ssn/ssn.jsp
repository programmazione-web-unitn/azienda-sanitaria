<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <title>Azienda Sanitaria - Ssn homepage</title>
        <%@include file="/html/jsCssInclude.html" %>

        <!-- Chart css -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.css">

        <!-- Chart.js -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>

        <script>
            $(document).ready(function() {
            var ctx = document.getElementById('myChart').getContext('2d');
            var myChart = new Chart(ctx, {
            type: 'bar',
                    data: {
                    labels: [
            <c:forEach items="${spesaNazionale}" var="annoSpesa">
                    '${annoSpesa.get(0)}',
            </c:forEach>
                    ],
                            datasets: [{
                            label: 'Dataset',
                                    data: [
            <c:forEach items="${spesaNazionale}" var="annoSpesa">
                                    '${annoSpesa.get(1)}',
            </c:forEach>
                                    ],
                                    backgroundColor: [
                                            'rgba(255, 99, 132, 0.2)',
                                            'rgba(54, 162, 235, 0.2)',
                                            'rgba(255, 206, 86, 0.2)',
                                            'rgba(75, 192, 192, 0.2)',
                                            'rgba(153, 102, 255, 0.2)',
                                            'rgba(255, 159, 64, 0.2)'
                                    ],
                                    borderColor: [
                                            'rgba(255, 99, 132, 1)',
                                            'rgba(54, 162, 235, 1)',
                                            'rgba(255, 206, 86, 1)',
                                            'rgba(75, 192, 192, 1)',
                                            'rgba(153, 102, 255, 1)',
                                            'rgba(255, 159, 64, 1)'
                                    ],
                                    borderWidth: 1
                            }]
                    },
                    options: {
                    scales: {
                    yAxes: [{
                    ticks: {
                    beginAtZero: true
                    },
                            scaleLabel: {
                            display: true,
                                    labelString: 'Spesa pubblica [€]'
                            }
                    }],
                            xAxes: [{
                            scaleLabel: {
                            display: true,
                                    labelString: 'Anno'
                            }
                            }]
                    },
                            title: {
                            text: 'Spesa nazionale per la sanità',
                                    display: true
                            }
                    }
            });
            });
        </script>
    </head>
    <body>
        <%@include file="/html/headers/ssnHeader.jsp" %>
        <div class="full-height">

            <div class="container p-4 ">
                <div class="row mt-5">
                    <div class="col-12">
                        <h2>
                            Servizio sanitario nazionale
                        </h2>
                    </div>
                    <div class="col-12 col-lg-4 ">
                        <!--start card-->
                        <div class="card-wrapper">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Controlla la spesa delle ricette farmaceutiche</h5>
                                    <p class="card-text">Controllare la spesa pubblica per la sanità delle ricette farmaceutiche erogate.</p>
                                    <button class="btn btn-outline-primary btn-icon" onclick="location.href = 'spesaRicette';">
                                        <svg class="icon icon-primary mr-2">
                                        <use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-files"></use>
                                        </svg>
                                        <span>Scopri di più</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <!--end card-->
                    </div>
                    <div class="col-12 col-lg-4">
                        <!--start card-->
                        <div class="card-wrapper">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Controlla la spesa degli esami</h5>
                                    <p class="card-text">Controllare la spesa pubblica per la sanità degli esami erogati dai Servizi danitari provinciali.</p>
                                    <button class="btn btn-outline-primary btn-icon" onclick="location.href = 'spesaEsami';">
                                        <svg class="icon icon-primary mr-2">
                                        <use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-files"></use>
                                        </svg>
                                        <span>Scopri di più</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <!--end card-->
                    </div>
                    <div class="col-12 col-lg-4">
                        <!--start card-->
                        <div class="card-wrapper">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Controlla la spesa delle visite specialistiche</h5>
                                    <p class="card-text">Controllare la spesa pubblica per la sanità delle visite specialistiche erogate.</p>
                                    <button class="btn btn-outline-primary btn-icon" onclick="location.href = 'spesaVisiteSpecialistiche';">
                                        <svg class="icon icon-primary mr-2">
                                        <use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-files"></use>
                                        </svg>
                                        <span>Scopri di più</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <!--end card-->
                    </div>
                </div>
                <div class="row mb-5">
                    <canvas id="myChart"></canvas>
                </div>
            </div>
        </div>
    </body>
    <%@include  file="/html/footer.html" %>

</html>
