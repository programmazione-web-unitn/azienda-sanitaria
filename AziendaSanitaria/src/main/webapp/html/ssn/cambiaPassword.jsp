<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Azienda Sanitaria - Ssn profilo</title>
        <%@include file="/html/jsCssInclude.html" %>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    </head>
    <body>
        <%@include file="/html/headers/ssnHeader.jsp" %>
        <div class="container p-4 full-height">

            <div class="row p-3">   
                <a href="#" class="go-back"><i class="fas fa-arrow-left fa-sm mr-2 "></i>Torna indietro</a>
            </div>
            <div class="row mb-5">
                <div class="col-12">            
                    <%@include file="/html/errorSuccess.jsp" %>
                </div>
                <div class="col-lg-5 col-md-12  p-3">
                    <h3 class="font-weight-semibold">Modifica Password</h3><br/>
                    <form action="CambiaPassword" method="POST" novalidate>
                        <%@ include file="/html/cambiaPasswordForm.jsp" %>
                    </form>
                </div>
            </div>
        </div>
    </body>

    <%@include  file="/html/footer.html" %>
</html>
