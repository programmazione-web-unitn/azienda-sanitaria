<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <title>Azienda Sanitaria - Ssn spesa esami</title>
        <%@include file="/html/jsCssInclude.html" %>

        <!-- Chart css -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.css">

        <!-- Chart.js -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>

        <script>
            var transparentize = function(color, opacity) {
            var alpha = opacity === undefined ? 0.5 : 1 - opacity;
            return Color(color).alpha(alpha).rgbString();
            }
            var presets = {red: "rgb(255, 99, 132)"};
            var options = {
            maintainAspectRatio: false,
                    spanGaps: false,
                    elements: {
                    line: {
                    tension: 0.000001
                    }
                    },
                    plugins: {
                    filler: {
                    propagate: false
                    }
                    },
                    scales: {
                    xAxes: [{
                    ticks: {
                    autoSkip: false,
                            maxRotation: 0
                    }
                    }]
                    }
            };
            $(document).ready(function() {
            var ctx = document.getElementById('myChart').getContext('2d');
            var myChart = new Chart(ctx, {
            type: 'line',
                    data: {
                    labels: [
            <c:forEach items="${spesaEsami}" var="annoSpesa">
                    '${annoSpesa.get(0)}',
            </c:forEach>
                    ],
                            datasets: [{
                            backgroundColor: transparentize(presets.red),
                                    borderColor: presets.red,
                                    data: [
            <c:forEach items="${spesaEsami}" var="annoSpesa">
                                    '${annoSpesa.get(1)}',
            </c:forEach>
                                    ],
                                    label: 'Dataset',
                                    fill: "start"
                            }]
                    },
                    options: Chart.helpers.merge(options, {
                    scales: {
                    yAxes: [{
                    scaleLabel: {
                    display: true,
                            labelString: 'Spesa pubblica [€]'
                    }
                    }],
                            xAxes: [{
                            scaleLabel: {
                            display: true,
                                    labelString: 'Anno'
                            }
                            }]
                    },
                            title: {
                            text: 'Spesa nazionale degli esami',
                                    display: true
                            }
                    })
            });
            });
        </script>
    </head>
    <body>
        <%@include file="/html/headers/ssnHeader.jsp" %>
        <div class="full-height">
            <div class="container p-4">
                <div class="row p-3">   
                    <a href="#" class="go-back"><i class="fas fa-arrow-left fa-sm mr-2 "></i>Torna indietro</a>
                </div>
                <div class="row">
                    <div class="col-12">
                        <h2 class="font-weight-semibold">
                            Spesa pubblica degli esami
                        </h2>
                    </div>
                </div>
                <div class="row mt-5 mb-5">
                    <canvas id="myChart" width="1000px" height="700px"></canvas>
                </div>
            </div>
        </div>
    </body>
    <%@include  file="/html/footer.html" %>

</html>
