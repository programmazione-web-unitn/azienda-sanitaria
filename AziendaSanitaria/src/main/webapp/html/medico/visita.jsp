<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix ="fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="it_IT" />


<!DOCTYPE html>
<html>
    <head>
        <title>Azienda Sanitaria - Medico visita paziente</title>
        <%@include file="/html/jsCssInclude.html" %>
        <link rel="stylesheet" type="text/css" href="/AziendaSanitaria/DataTables/datatables.min.css"/>

        <script type="text/javascript" src="/AziendaSanitaria/DataTables/datatables.min.js"></script>            


        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>




        <script>
            var properties = {
                autoWidth: false,
                "language": {
                    "sEmptyTable": "Nessun dato presente nella tabella",
                    "sInfo": "",
                    "sInfoEmpty": "Vista da 0 a 0 di 0 elementi",
                    "sInfoFiltered": "(filtrati da _MAX_ elementi totali)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "Visualizza _MENU_ elementi",
                    "sLoadingRecords": "Caricamento...",
                    "sProcessing": "Elaborazione...",
                    "sSearch": "Cerca:",
                    "sZeroRecords": "La ricerca non ha portato alcun risultato.",
                    "oPaginate": {
                        "sFirst": "Inizio",
                        "sPrevious": "Precedente",
                        "sNext": "Successivo",
                        "sLast": "Fine"
                    },
                    "oAria": {
                        "sSortAscending": ": attiva per ordinare la colonna in ordine crescente",
                        "sSortDescending": ": attiva per ordinare la colonna in ordine decrescente"
                    }

                },
                "aaSorting": [[0, "desc"]]
            };

            $(document).ready(function () {
                for (table of ["tabellaRicette", "tabellaEsami", "tabellaVisiteSpec", "tabellaVisiteBase"]) {
                    $("#" + table).DataTable(properties);
                }
                $("#tab2b").removeClass("show active");
                $("#tab3b").removeClass("show active");

                $("#autocompleteEsami").select2({
                    placeholder: "Inserisci nome esame",
                    allowClear: true,
                    ajax: {
                        url: function (request) {
                            return "/AziendaSanitaria/services/listaTipiDiEsame/" + (request.term || "");
                        },
                        dataType: "json"
                    }
                });

                $("#autocompleteEsami").on("select2:select", function (e) {
                    var id = e.params.data.id;
                    document.getElementById("idTipoDiEsame").value = id;
                });

                $("#autocompleteVisita").select2({
                    placeholder: "Inserisci nome visita",
                    allowClear: true,
                    ajax: {
                        url: function (request) {
                            return "/AziendaSanitaria/services/listaTipiDiVisita/" + (request.term || "");
                        },
                        dataType: "json"
                    }
                });
                $("#autocompleteVisita").on("select2:select", function (e) {
                    var id = e.params.data.id;
                    document.getElementById("idTipoDiVisita").value = id;
                });


                $("#autocompleteRicetta").select2({
                    placeholder: "Inserisci nome farmaco",
                    allowClear: true,
                    ajax: {
                        url: function (request) {
                            return "/AziendaSanitaria/services/listaFarmaci/" + (request.term || "");
                        },
                        dataType: "json"
                    }
                });
                $("#autocompleteRicetta").on("select2:select", function (e) {
                    var id = e.params.data.id;
                    document.getElementById("idFarmaco").value = id;
                });

                const report = $('#report');
                checkRequired(report[0], true);
                report.on('input', function () {
                    checkRequired(report[0])
                });

            });
        </script>

    </head>






    <body>
        <%@include file="/html/headers/medicoHeader.jsp" %>
        <div class="full-height">

            <div class="container p-4">
                <div class="row p-3">   
                    <a href="#" class="go-back"><i class="fas fa-arrow-left fa-sm mr-2 "></i>Torna indietro</a>
                </div>
                <div class="row">
                    <div class="col-12">
                        <%@include file="/html/errorSuccess.jsp" %>
                    </div>
                    <div class="col-lg-4 col-md-5  col-sm-12 col-xs-12 p-4 text-center">
                        <div class="avatar-upload-wrapper size-xxl">
                            <div class="avatar size-xxxl avatar-upload">
                                <img src="/AziendaSanitaria/foto?idFoto=${fotoProfiloPaziente.id}" alt="imagealt">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12 p-4">

                        <h3 class="font-weight-semibold">
                            ${paziente.nome} ${paziente.cognome}
                        </h3>
                        <dl class="row">
                            <dt class="col-6"> Data di nascita </dt>
                            <dd class="col-6"> <fmt:formatDate pattern = "dd/MM/yyyy" value = "${paziente.dataNascita}" /></dd>
                            <dt class="col-6"> Luogo di nascita </dt>
                            <dd class="col-6"> ${paziente.luogoNascita}</dd>
                            <dt class="col-6"> Provincia di residenza</dt>
                            <dd class="col-6"> ${paziente.provincia.nome}, ${paziente.provincia.sigla}</dd>
                            <dt class="col-6"> Sesso</dt>
                            <dd class="col-6"> ${paziente.sesso}</dd>
                            <dt class="col-6"> Codice fiscale</dt>
                            <dd class="col-6"> ${paziente.codiceFiscale}</dd>
                        </dl>
                    </div>

                </div>
            </div>       

            <div class="container">
                <div class="section p-4 mt-2 mb-3">
                    <div class="section-content ">
                        <!-- contenuto di esempio START -->
                        <div class="container">
                            <div class="row mb-3">
                                <div class="col-12">
                                    <h2 class="font-weight-semibold">Scrivi report</h2>
                                </div>
                            </div>  
                            <div class="row mt-2">
                                <div class="col-12">

                                    <form id="nuovoReprt"method="POST" action="/AziendaSanitaria/Medico/paziente/visita" novalidate>
                                        <div class="form-group">
                                            <textarea id="report" name="report" rows="5"></textarea>

                                            <input type="hidden" id="idPazienteEsame" name="idPaziente" value="${paziente.id}">
                                            <small data-error-for="report" class="form-text text-secondary">
                                                Inserisci un report
                                            </small>
                                        </div>
                                        <input type="submit" value="Inserisci report" class="btn btn-primary" id="scriviReport" disabled>
                                    </form>



                                </div>
                            </div>


                        </div>
                    </div>
                </div>

                <div class="nav-tabs-hidescroll">
                    <ul class="nav nav-tabs nav-tabs auto " id="myTab3" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="tab1c-tab" data-toggle="tab" href="#tab1b" role="tab" aria-controls="tab1b" aria-selected="true">
                                <h4>Esami</h4>

                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="tab2b-tab" data-toggle="tab" href="#tab2b" role="tab" aria-controls="tab2b" aria-selected="false">
                                <h4>Visite</h4>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="tab3b-tab" data-toggle="tab" href="#tab3b" role="tab" aria-controls="tab3b" aria-selected="false">
                                <h4>Ricette</h4>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="tab-content" id="myTab3Content">
                    <div class="tab-pane p-4 fade show active" id="tab1b" role="tabpanel" aria-labelledby="tab1c-tab">

                        <div class="container p-3">

                            <div class="row  mt-2 mb-5">
                                <div class="col-12">
                                    <h4>Prescrivi un nuovo esame</h4>


                                    <form id="nuovoEsame" method="POST" action="/AziendaSanitaria/Medico/paziente/esame" novalidate>
                                        <div class="form-group">
                                            <select id="autocompleteEsami" name="autocompleteEsami" class="form-control select2-allow-clear simple-select2 w-100">
                                            </select>
                                            <small data-error-for="autocompleteEsami" class="form-text text-secondary">
                                                Seleziona un esame
                                            </small>
                                            <input type="hidden" id="idTipoDiEsame" name="idTipoDiEsame">
                                            <input type="hidden" id="idPazienteVisita" name="idPaziente" value="${paziente.id}">

                                        </div>
                                        <input type="submit" value="Prescrivi" class="btn btn-primary" id="prescriviEsame" disabled>
                                    </form>
                                </div>

                            </div>
                            <div class="row">
                                <h4>Ultimi esami</h4>

                                <div class="col-12">
                                    <c:choose>
                                        <c:when test="${empty tuttiEsami}">
                                            <span>Nessun esame effettuato</span>
                                        </c:when>
                                        <c:otherwise>

                                            <div class = "table-responsive mt-3">

                                                <table id="tabellaEsami" class="table table-striped dt-responsive">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">Data di prescrizione</th>
                                                            <th scope="col">Tipo</th>
                                                            <th scope="col">Data erogazione</th>
                                                            <th scope="col">Vedi</th>
                                                        </tr>
                                                    </thead>    

                                                    <tbody>
                                                        <c:forEach var="esame" items="${tuttiEsami}">
                                                            <tr>
                                                                <td data-order="${esame.dataOraPrescrizione}">
                                                                    <fmt:formatDate pattern = "E dd/MM/yyyy" value = "${esame.dataOraPrescrizione}" />
                                                                </td>

                                                                <td>${esame.tipoDiEsame.nome}</td>

                                                                <c:choose>
                                                                    <c:when test="${esame.dataOraErogazione!= null}">
                                                                        <td data-order="${esame.dataOraErogazione}">
                                                                            <fmt:formatDate pattern = "E dd/MM/yyyy" value = "${esame.dataOraErogazione}" />
                                                                        </td>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <td data-order="Non erogato">Non erogato</td>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                                <td>
                                                                    <a class="btn btn-primary" href="paziente/esami/info?idPaziente=${paziente.id}&idEsame=${esame.id}">
                                                                        Vedi
                                                                    </a> 
                                                                </td>
                                                            </tr>

                                                        </c:forEach>
                                                    </tbody>
                                                </table>
                                            </div>

                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>




                        </div>

                    </div>
                    <div class="tab-pane p-4 fade show active" id="tab2b" role="tabpanel" aria-labelledby="tab2b-tab">
                        <div class="container p-3 ">
                            <div class="row mt-2 mb-5">
                                <div class="col-12">
                                    <h4>Prescrivi una nuova visita specialistica</h4>

                                    <form id="nuovaVisita" method="POST" action="/AziendaSanitaria/Medico/paziente/visitaSpecialistica" novalidate>
                                        <div class="form-group">
                                            <select id="autocompleteVisita" name="autocompleteVisita" class="form-control select2-allow-clear simple-select2 w-100">
                                            </select>
                                            <small data-error-for="autocompleteVisita" class="form-text text-secondary">
                                                Seleziona una visita specialistica
                                            </small>
                                            <input type="hidden" id="idTipoDiVisita" name="idTipoDiVisita">
                                            <input type="hidden" id="idPaziente" name="idPaziente" value="${paziente.id}">

                                        </div>
                                        <input type="submit" value="Prescrivi" class="btn btn-primary" id="prescriviVisita" disabled>


                                    </form>
                                </div>

                            </div>
                            <div class="row">

                                <h4>Ultime visite specialistiche</h4>

                                <div class="col-12">
                                    <div class="row">
                                        <c:choose>
                                            <c:when test="${empty tutteVisiteSpec}">
                                                <span>Nessuna visita specialistica effettuata</span>
                                            </c:when>
                                            <c:otherwise>
                                                <div class="table-responsive mt-3">

                                                    <table id="tabellaVisiteSpec" class="table table-striped dt-responsive">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Data prescrizione</th>
                                                                <th scope="col">Tipo</th>
                                                                <th scope="col">Data erogazione</th>
                                                                <th scope="col">Richiamo</th>
                                                                <th scope="col">Vedi</th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                            <c:forEach var="visita" items="${tutteVisiteSpec}">
                                                                <tr>
                                                                    <td data-order="${visita.dataOraPrescrizione}">
                                                                        <fmt:formatDate pattern = "E dd/MM/yyyy" value = "${visita.dataOraPrescrizione}" />
                                                                    </td>

                                                                    <td>${visita.tipoDiVisita.nome}</td>

                                                                    <c:choose>
                                                                        <c:when test="${visita.dataOraErogazione!= null}">
                                                                            <td data-order="${visita.dataOraErogazione}">
                                                                                <fmt:formatDate pattern = "E dd/MM/yyyy" value = "${visita.dataOraErogazione}" />
                                                                            </td>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <td data-order="Non erogato">Non erogata</td>
                                                                        </c:otherwise>
                                                                    </c:choose>


                                                                    <td>
                                                                        <svg class="icon align-bottom">
                                                                        <c:choose>
                                                                            <c:when test="${visita.medicoDiBase == null}">
                                                                                <use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-check"></use>
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-close"></use>
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                        </svg>
                                                                    </td>
                                                                    <td>
                                                                        <a class="btn btn-primary" href="paziente/visite/specialistiche/info?idPaziente=${paziente.id}&idVisita=${visita.id}">
                                                                            Vedi
                                                                        </a> 
                                                                    </td>
                                                                </tr>


                                                            </c:forEach>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                    <div>
                                        <h4 class="mt-5 ">Ultime visite di base</h4>

                                        <div class="row">
                                            <c:choose>
                                                <c:when test="${empty tutteVisiteBase}">
                                                    <span>Nessuna visita di base effettuata</span>
                                                </c:when>
                                                <c:otherwise>
                                                    <div class="table-responsive">


                                                        <table id="tabellaVisiteBase" class="table table-striped dt-responsive">
                                                            <thead>
                                                                <tr>
                                                                    <th scope="col">Data</th>
                                                                    <th scope="col">Vedi</th>
                                                                </tr>
                                                            </thead>

                                                            <tbody>

                                                                <c:forEach var="visita" items="${tutteVisiteBase}">
                                                                    <tr>
                                                                        <td data-order="${visita.dataOra}">
                                                                            <fmt:formatDate pattern = "E dd/MM/yyyy" value = "${visita.dataOra}" />
                                                                        </td>
                                                                        <td>
                                                                            <a class="btn btn-primary" href="paziente/visite/base/info?idPaziente=${paziente.id}&idVisita=${visita.id}">
                                                                                Vedi
                                                                            </a> 
                                                                        </td>
                                                                    </tr>

                                                                </c:forEach>
                                                            </tbody>
                                                        </table>

                                                    </div>
                                                </c:otherwise>
                                            </c:choose>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                    <div class="tab-pane p-4 fade show active" id="tab3b" role="tabpanel" aria-labelledby="tab3b-tab">
                        <div class="container p-3">
                            <div class="row mt-2 mb-5">
                                <div class="col-12">
                                    <h4>Prescrivi una nuova ricetta</h4>

                                    <form id="nuovaRicetta" method="POST" action="/AziendaSanitaria/Medico/paziente/ricetta" novalidate>
                                        <div class="form-group">
                                            <select id="autocompleteRicetta" name="autocompleteRicetta" class="form-control select2-allow-clear simple-select2 w-100">
                                            </select>
                                            <small data-error-for="autocompleteRicetta" class="form-text text-secondary">
                                                Seleziona un farmaco
                                            </small>
                                        </div>
                                        <div class="form-group">
                                            <p calss="mt-2">Quantità </p>
                                            <input type="text" id="quantita" name="quantita" class="posIntReq">
                                            <small data-error-for="quantita" class="form-text text-secondary">
                                                Inserisci il numero di confezioni
                                            </small>
                                            <input type="hidden" id="idFarmaco" name="idFarmaco">
                                            <input type="hidden" id="idPazienteRicetta" name="idPaziente" value="${paziente.id}">
                                        </div>

                                        <input type="submit" value="Prescrivi" class="btn btn-primary" id="prescriviRicetta" disabled>


                                    </form>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <h4>Ultime ricette</h4>

                                    <c:choose>
                                        <c:when test="${empty tutteRicette}">
                                            <span>Nessuna ricetta prescritta</span>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="table-responsive mt-3">


                                                <table id="tabellaRicette" class="table table-striped dt-responsive">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">Data prescrizione</th>
                                                            <th scope="col">Farmaco</th>
                                                            <th scope="col">Quantità</th>
                                                            <th scope="col">Data erogazione</th>
                                                            <th scope="col">Vedi</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        <c:forEach var="ricetta" items="${tutteRicette}">
                                                            <tr>
                                                                <td data-order="${ricetta.dataOraPrescrizione}">
                                                                    <fmt:formatDate pattern = "E dd/MM/yyyy" value = "${ricetta.dataOraPrescrizione}" />
                                                                </td>
                                                                <td>${ricetta.farmaco.nome}</td>
                                                                <td>${ricetta.quantita}</td>
                                                                <c:choose>
                                                                    <c:when test="${ricetta.dataOraErogazione == null}">
                                                                        <td data-order="Non erogato">Non erogata</td>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <td data-order="${ricetta.dataOraErogazione}">
                                                                            <fmt:formatDate pattern = "E dd/MM/yyyy" value = "${ricetta.dataOraErogazione}" />
                                                                        </td>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                                <td>
                                                                    <a class="btn btn-primary" href="paziente/ricette/info?idPaziente=${paziente.id}&idRicetta=${ricetta.id}">
                                                                        Vedi
                                                                    </a> 
                                                                </td>
                                                            </tr>
                                                        </c:forEach>
                                                    </tbody>
                                                </table>

                                            </div>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>




            </div>

        </div>
    </body>
    <%@include  file="/html/footer.html" %>

</html>
