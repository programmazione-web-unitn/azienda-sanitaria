<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!DOCTYPE html>
<html>
    <head>
        <title>Azienda Sanitaria - Medico parco pazienti</title>
        <%@include file="/html/jsCssInclude.html" %>
        <link rel="stylesheet" type="text/css" href="/AziendaSanitaria/DataTables/datatables.min.css"/>

        <script type="text/javascript" src="/AziendaSanitaria/DataTables/datatables.min.js"></script>           



        <script>
            $(document).ready(function () {

                $('#tabellaPazienti').DataTable({
                    "ajax": {
                        "url": "/AziendaSanitaria/services/parcoPazienti",
                        "dataSrc": function (json) {
                            var data = [];
                            for (var i = 0; i < json.results.length; i++) {
                                let paziente = json.results[i].pazienteInfo.paziente;
                                let ultima_visita_presc = json.results[i].pazienteInfo.ultima_visita_presc;
                                let ultima_ricetta_presc = json.results[i].pazienteInfo.ultima_ricetta_presc;
                                let ultimo_esame_presc = json.results[i].pazienteInfo.ultimo_esame_presc;
                                let ultima_visita_presc_timestamp = json.results[i].pazienteInfo.ultima_visita_presc_timestamp;
                                let ultima_ricetta_presc_timestamp = json.results[i].pazienteInfo.ultima_ricetta_presc_timestamp;
                                let ultimo_esame_presc_timestamp = json.results[i].pazienteInfo.ultimo_esame_presc_timestamp;
                                
                                data[i] = [paziente.codiceFiscale, 
                                    paziente.cognome + ' ' + paziente.nome, 
                                    ultima_visita_presc,
                                    ultima_visita_presc_timestamp,
                                    ultima_ricetta_presc,
                                    ultima_ricetta_presc_timestamp,
                                    ultimo_esame_presc,
                                    ultimo_esame_presc_timestamp,
                                    "<a class='btn btn-primary' href='/AziendaSanitaria/Medico/paziente?idPaziente="+paziente.id+"'>Vedi</a>"
                                ];
                            }
                            return data;
                        }
                    },
                    "columnDefs": [
                        {
                            'targets': [0],
                            'visible': false,
                            'searchable': false
                        },
                        { "orderData": 3, "targets": 2 },
                        {
                            'targets': [3],
                            'visible': false,
                            'searchable': false
                        },
                        { "orderData": 5, "targets": 4 },
                        {
                            'targets': [5],
                            'visible': false,
                            'searchable': false
                        },
                        { "orderData": 7, "targets": 6 },
                        {
                            'targets': [7],
                            'visible': false,
                            'searchable': false
                        }
                    ],
                    autoWidth: false,
                    responsive: true,
                    "language": {
                        "sEmptyTable": "Nessun dato presente nella tabella",
                        "sInfo": "Vista da _START_ a _END_ di _TOTAL_ elementi",
                        "sInfoEmpty": "Vista da 0 a 0 di 0 elementi",
                        "sInfoFiltered": "(filtrati da _MAX_ elementi totali)",
                        "sInfoPostFix": "",
                        "sInfoThousands": ".",
                        "sLengthMenu": "Visualizza _MENU_ elementi",
                        "sLoadingRecords": "Caricamento...",
                        "sProcessing": "Elaborazione...",
                        "sSearch": "Cerca:",
                        "sZeroRecords": "La ricerca non ha portato alcun risultato.",
                        "oPaginate": {
                            "sFirst": "Inizio",
                            "sPrevious": "Precedente",
                            "sNext": "Successivo",
                            "sLast": "Fine"
                        },
                        "oAria": {
                            "sSortAscending": ": attiva per ordinare la colonna in ordine crescente",
                            "sSortDescending": ": attiva per ordinare la colonna in ordine decrescente"
                        }

                    }
                })
            });

        </script>

    </head>

    <body>
        <%@include file="/html/headers/medicoHeader.jsp" %>
        <div class="full-height">
            
            <div class="container p-4">
                <div class="row p-3">   
                    <a href="#" class="go-back"><i class="fas fa-arrow-left fa-sm mr-2 "></i>Torna indietro</a>
                </div>
                <div class="row mb-4">
                    <div class="col-12">
                        <h2 class="font-weight-semibold">Parco pazienti</h2>
                    </div>

                </div>
                <div class="row mb-5">

                    <div class="table-responsive">

                        <table id="tabellaPazienti" class="table table-striped dt-responsive">
                            <thead>
                                <tr>
                                    <th scope="col" class="d-none">Codice fiscale</th>
                                    <th scope="col">Cognome e nome</th>
                                    <th scope="col">Ultima visita prescritta</th>
                                    <th scope="col" class="d-none">Ultima visita prescritta timestamp</th>
                                    <th scope="col">Ultima ricetta prescritta</th>
                                    <th scope="col" class="d-none">Ultima ricetta prescritta timestamp</th>
                                    <th scope="col">Ultimo esame prescritto</th>
                                    <th scope="col" class="d-none">Ultimo esame prescritto timestamp</th>
                                    <th scope="col">Vedi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        <%@include  file="/html/footer.html" %>

    </body>
</html>
