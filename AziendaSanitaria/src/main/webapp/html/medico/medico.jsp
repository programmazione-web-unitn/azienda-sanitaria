<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix ="fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="it_IT" />
<!DOCTYPE html>
<html>
    <head>
        <title>Azienda Sanitaria - Medico homepage</title>
        <%@include file="/html/jsCssInclude.html" %>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>



        <script>
            $(document).ready(function () {
                $("#autocomplete").select2({
                    placeholder: "Inserisci nome, cognome o codice fiscale",
                    allowClear: true,
                    ajax: {
                        url: function (request) {
                            return "/AziendaSanitaria/services/listaPazienti/" + (request.term || "");
                        },
                        dataType: "json"
                    }
                });

//                $("#autocomplete").val(null).trigger("change");
            });
        </script>

    </head>

    <body>
        <%@include file="/html/headers/medicoHeader.jsp" %>
        <div class="full-height">
            <%@include file="/html/modalGallery.jsp" %>

            <div class="container p-4">
                <div class="row mt-5">
                    <div class="col-12 pl-4">            
                        <h2>Medico di base</h2>
                    </div>
                    <div class="col-lg-4 col-md-5  col-sm-12 col-xs-12 p-4 text-center">

                        <div class="avatar-upload-wrapper">
                            <div class="avatar size-xxxl avatar-upload avatar-clickable">
                                <img src="/AziendaSanitaria/foto?idFoto=${fotoProfilo.id}"  data-toggle="modal" data-target="#gallery">
                            </div>
                            <div class="avatar-upload-icon">
                                <svg class="icon icon-lg" aria-hidden="true"><use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-camera"></use></svg>
                            </div>

                        </div>

                    </div>
                    <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12 p-4">

                        <h3 class="font-weight-semibold">
                            ${user.nome} ${user.cognome}
                        </h3>
                        <dl class="row">
                            <dt class="col-6"> Data di nascita </dt>
                            <dd class="col-6"> <fmt:formatDate pattern = "dd/MM/yyyy" value = "${user.dataNascita}" /></dd>
                            <dt class="col-6"> Luogo di nascita </dt>
                            <dd class="col-6"> ${user.luogoNascita}</dd>
                            <dt class="col-6"> Provincia di residenza</dt>
                            <dd class="col-6"> ${user.provincia.nome}, ${user.provincia.sigla}</dd>
                            <dt class="col-6"> Sesso</dt>
                            <dd class="col-6"> ${user.sesso}</dd>
                            <dt class="col-6"> Codice fiscale</dt>
                            <dd class="col-6"> ${user.codiceFiscale}</dd>
                        </dl>
                    </div>


                </div>
            </div>
            <div class="container p-4">
                <div class="row mb-5">
                    <div class="col-12">
                        <form id="datiPaziente" method="GET" action="paziente" novalidate>
                            <h2 class="font-weight-semibold">Ricerca Paziente</h2>
                            <div class="form-group">
                                <p class="form-text">Inserisci nome, cognome o codice fiscale.</p>
                                <select id="autocomplete" name="idPaziente" class="form-control select2-allow-clear simple-select2 w-100" >
                                </select>
                                <small data-error-for="autocomplete" class="form-text text-secondary">
                                    Seleziona un paziente
                                </small>
                            </div>
                            <input type="submit" value="Visita" class="btn btn-primary btn-icon" disabled>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </body>
    <%@include  file="/html/footer.html" %>

</html>
