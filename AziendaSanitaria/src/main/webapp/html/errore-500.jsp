<%-- 
    Document   : errore-500
    Created on : Oct 31, 2019, 5:04:55 PM
    Author     : davide
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" isErrorPage="true"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Azienda Sanitaria - Errore 500</title>  
        <%@include file="/html/jsCssInclude.html" %>
        <%@include file="/html/headers/loginHeader.jsp" %>

    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="col-md-4 offset-md-4 p-4 text-center">
                    <h2 class="font-weight-semibold">Errore 500</h2>
                    <h5>Malfunzionamento del server</h5>
                    <p>Ci dispiace. C'è stato un errore sui nostri server. Riprova più tardi.</p>
                    <br/>
                    <a href="/AziendaSanitaria">
                        <i class="fas fa-arrow-left fa-sm mr-2 "></i>
                        Torna alla homepage
                    </a>
                </div>
            </div>            
        </div>
        
        <c:if test="${initParam['debug'] eq 'true'}">
        <div class="container">
            <div class="row p-4">
                <div class="col-12 mb-5">    
                    <ul>
                        <li>Exception: ${requestScope['javax.servlet.error.exception']}</li>
                        <li>Exception type: ${requestScope['javax.servlet.error.exception_type']}</li>
                        <li>Exception message: ${requestScope['javax.servlet.error.message']}</li>
                        <li>Request URI: ${requestScope['javax.servlet.error.request_uri']}</li>
                        <li>Servlet name: ${requestScope['javax.servlet.error.servlet_name']}</li>
                        <li>Status code: ${requestScope['javax.servlet.error.status_code']}</li>
                    </ul>

                </div>
            </div>
        </div>
        </c:if>
        
    </body>
</html>
