<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix ="fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>


<!DOCTYPE html>
<html>
    <head>
        <title>Azienda Sanitaria - Farmacia ricette paziente</title>
        <%@include file="/html/jsCssInclude.html" %>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/AziendaSanitaria/DataTables/datatables.min.css"/>

        <script type="text/javascript" src="/AziendaSanitaria/DataTables/datatables.min.js"></script>            


        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>




        <script>
            var properties = {
                autoWidth: false,

                "language": {
                    "sEmptyTable": "Nessun dato presente nella tabella",
                    "sInfo": "",
                    "sInfoEmpty": "Vista da 0 a 0 di 0 elementi",
                    "sInfoFiltered": "(filtrati da _MAX_ elementi totali)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "Visualizza _MENU_ elementi",
                    "sLoadingRecords": "Caricamento...",
                    "sProcessing": "Elaborazione...",
                    "sSearch": "Cerca:",
                    "sZeroRecords": "La ricerca non ha portato alcun risultato.",
                    "oPaginate": {
                        "sFirst": "Inizio",
                        "sPrevious": "Precedente",
                        "sNext": "Successivo",
                        "sLast": "Fine"
                    },
                    "oAria": {
                        "sSortAscending": ": attiva per ordinare la colonna in ordine crescente",
                        "sSortDescending": ": attiva per ordinare la colonna in ordine decrescente"
                    }

                },
                "aaSorting": [[0, "desc"]]
            };
            $(document).ready(function () {
                for (table of ["tabellaRicette"]) {
                    $("#" + table).DataTable(properties);
                }

                $("#autocompleteRicetta").select2({
                    placeholder: "Inserisci nome farmaco",
                    allowClear: true,
                    ajax: {
                        url: function (request) {
                            return "/AziendaSanitaria/services/listaFarmaci/" + (request.term || "");
                        },
                        dataType: "json"
                    }
                });
                $("#autocompleteRicetta").on("select2:select", function (e) {
                    var id = e.params.data.id;
                    document.getElementById("idFarmaco").value = id;
                });

            });
        </script>

    </head> 

    <body>
        <%@include file="/html/headers/farmaciaHeader.jsp" %>
        <div class="full-height">
            <div class="container p-4">
                <div class="row p-3">   
                    <a href="#" class="go-back"><i class="fas fa-arrow-left fa-sm mr-2 "></i>Torna indietro</a>
                </div>
                <div class="row">
                    <div class="col-12">
                        <%@include file="/html/errorSuccess.jsp" %>
                    </div>
                    <div class="col-lg-4 col-md-5  col-sm-12 col-xs-12 p-4 text-center">
                        <div class="avatar-upload-wrapper size-xxl">
                            <div class="avatar size-xxxl avatar-upload">
                                <img src="/AziendaSanitaria/foto?idFoto=${fotoProfiloPaziente.id}" alt="imagealt">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12 p-4">

                        <h3 class="font-weight-semibold">
                            ${paziente.nome} ${paziente.cognome}
                        </h3>
                        <dl class="row">
                            <dt class="col-6"> Data di nascita </dt>
                            <dd class="col-6"> <fmt:formatDate pattern = "dd/MM/yyyy" value = "${paziente.dataNascita}" /></dd>
                            <dt class="col-6"> Luogo di nascita </dt>
                            <dd class="col-6"> ${paziente.luogoNascita}</dd>
                            <dt class="col-6"> Provincia di residenza</dt>
                            <dd class="col-6"> ${paziente.provincia.nome}, ${paziente.provincia.sigla}</dd>
                            <dt class="col-6"> Sesso</dt>
                            <dd class="col-6"> ${paziente.sesso}</dd>
                            <dt class="col-6"> Codice fiscale</dt>
                            <dd class="col-6"> ${paziente.codiceFiscale}</dd>
                        </dl>
                    </div>

                </div>
            </div>     

            <div class="container p-4">
                <div class="row">
                    <h2 class="font-weight-semibold">Ricette</h2>
                </div>
                <div class="row mt-2" >
                    <div class="col-12">
                        <c:choose>
                            <c:when test="${empty tutteRicette}">
                                <span>Nessuna ricetta prescritta</span>
                            </c:when>
                            <c:otherwise>
                                <div class="table-responsive">


                                    <table id="tabellaRicette" class="table table-striped dt-responsive">
                                        <thead>
                                            <tr>
                                                <th scope="col">Data prescrizione</th>
                                                <th scope="col">Farmaco</th>
                                                <th scope="col">Quantità</th>
                                                <th scope="col">Eroga</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <c:forEach var="ricetta" items="${tutteRicette}">
                                                <tr>
                                                    <td data-order="${ricetta.dataOraPrescrizione}">
                                                        <fmt:formatDate pattern = "E dd/MM/yyyy" value = "${ricetta.dataOraPrescrizione}" />
                                                    </td>
                                                    <td>${ricetta.farmaco.nome}</td>
                                                    <td>${ricetta.quantita}</td>
                                                    <td>
                                                        <a class="btn btn-primary buttonVisite" href="/AziendaSanitaria/Farmacia/ricetta?idRicetta=${ricetta.id}&idPaziente=${ricetta.paziente.id}" >Eroga</a>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>

                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <%@include  file="/html/footer.html" %>

</html>
