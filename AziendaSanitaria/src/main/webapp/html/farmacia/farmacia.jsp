<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Azienda Sanitaria - Farmacia homepage</title>
        <%@include file="/html/jsCssInclude.html" %>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>



        <script>
            $(document).ready(function () {
                $("#autocomplete").select2({
                    placeholder: "Inserisci nome, cognome o codice fiscale",
                    allowClear: true,
                    ajax: {
                        url: function (request) {
                            return "/AziendaSanitaria/services/listaPazienti/" + (request.term || "");
                        },
                        dataType: "json"
                    }
                });
//                $("#autocomplete").val(null).trigger("change");
            });
        </script>
    </head>
    <body>
        <%@include file="/html/headers/farmaciaHeader.jsp" %>
        <div class="container p-5 full-height">
            <div class="row mb-5">
                <div class="col-12">
                    <h2>
                        Farmacia ${user.nome}
                    </h2>
                </div>
                <div class="col-12 pt-4">
                    <form id="datiPaziente" method="GET" action="paziente" novalidate>
                        <h2 class="font-weight-semibold">Ricerca Persona</h2>
                        <div class="form-group">
                            <p class="form-text">Inserisci nome, cognome o codice fiscale.</p>
                            <select id="autocomplete" name="idPaziente" class="form-control select2-allow-clear simple-select2 w-100">
                            </select>
                            <small data-error-for="autocomplete" class="form-text text-secondary">
                                Seleziona una persona
                            </small>
                        </div>
                        <input type="submit" value="Visualizza ricette" class="btn btn-primary btn-icon" disabled>
                    </form>
                </div>
            </div>
        </div>
    </body>
    
    <%@include  file="/html/footer.html" %>
</html>
