<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <meta charset="utf-8">
        <%@include file="/html/jsCssInclude.html" %>
        <title>Azienda Sanitaria - Aggiorna password</title>
    </head>
    <body>

        <%@include file="/html/headers/loginHeader.jsp" %>

        <div class="container">
            <div class="row">
                <div class="offset-lg-3 col-lg-6 p-4">
                    <h3 class="font-weight-semibold">Aggiorna Password</h3><br/>
                    <form action="/AziendaSanitaria/PasswordDimenticata" method="POST" novalidate>
                        <input type="hidden" name="recuperoToken" value="<%= request.getParameter("recuperoToken")%>">
                        <%@ include file="/html/cambiaPasswordForm.jsp" %>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>