<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>

<div class="modal fade" id="gallery" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content gallery-content">
            <div class="modal-header gallery-header">
                <button type="button" class="close gallery-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body gallery-body">
                <div id="galleryIndicators" class="carousel slide " data-ride="carousel" data-interval="false">

                    <div class="carousel-inner">
                        <c:if test="${fn:length(fotoPaziente)>1}">
                            <c:forEach var="foto" items="${fotoPaziente}" begin="0" end="${fn:length(fotoPaziente)-2}">
                                <div class="carousel-item gallery-item">
                                    <img src="/AziendaSanitaria/foto?idFoto=${foto.id}" class="d-block w-100">
                                </div>
                            </c:forEach>
                        </c:if>
                        <div class="carousel-item gallery-item active">
                            <img src="/AziendaSanitaria/foto?idFoto=${fotoPaziente[fn:length(fotoPaziente)-1].id}" class="d-block w-100">
                        </div>

                    </div>

                    <a class="carousel-control-prev " href="#galleryIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon gallery-control-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#galleryIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon gallery-control-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    <div class="modal-footer">
                        <ol class="carousel-indicators">
                            <c:if test="${fn:length(fotoPaziente) gt 1}">
                                <c:forEach var="i" begin="0" end="${fn:length(fotoPaziente)-2}">
                                    <li data-target="#galleryIndicators" data-slide-to="${i}"></li>
                                    </c:forEach>
                                </c:if>
                            <li data-target="#galleryIndicators" data-slide-to="${fn:length(fotoPaziente)}" class="active"></li>

                        </ol>
                    </div>
                   
                </div>
            </div>

        </div>
    </div>
</div>