<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Azienda Sanitaria - Ssp stampa report</title>
        <%@include file="/html/jsCssInclude.html" %>
    </head>
    <body>
        <%@include file="/html/headers/sspHeader.jsp" %>
        <div class="full-height">
            <div class="container p-4">
                <div class="row p-3">   
                    <a href="#" class="go-back"><i class="fas fa-arrow-left fa-sm mr-2 "></i>Torna indietro</a>
                </div>
                <!-- Three columns of text below the carousel -->
                <div class="row">
                    <div class="col-12">

                        <h2 class="font-weight-semibold">
                            Stampa report PDF delle prestazioni erogate nella provincia di ${user.provincia.nome}
                        </h2>

                        <div class="row mt-5">


                            <div class="col-lg-12 col-md-12  ">
                                <h3 class="font-weight-semibold">Seleziona l'intervallo di tempo</h3>
                                <form class="form-signin row" action="report" method="GET" novalidate>
                                    <input type="hidden" name="report" value="pdf"/>
                                    <input type="hidden" name="type" value="sspPrestazioni"/>
                                    <input type="hidden" name="siglaProvincia" value="${user.provincia.sigla}"/>


                                    <div class="it-datepicker-wrapper mt-5 col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-label-group mb-0">
                                            <label for="prestStartDate">Dal giorno</label>
                                            <input class="form-control it-date-datepicker" id="prestStartDate"
                                                   name="prestStartDate"
                                                   type="text" 
                                                   placeholder=" "
                                                   readonly>
                                        </div>
                                        <small data-error-for="prestStartDate" class="form-text text-secondary">
                                            Inserisci una data in formato gg/mm/aaa
                                        </small>

                                    </div>
                                    <div class="it-datepicker-wrapper mt-5 col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-label-group mb-0">
                                            <label for="prestEndDate">Al giorno</label>
                                            <input class="form-control it-date-datepicker" id="prestEndDate"
                                                   name="prestEndDate"
                                                   type="text" 
                                                   placeholder=" "
                                                   readonly>
                                        </div>
                                        <small data-error-for="prestEndDate" class="form-text text-secondary">
                                            Inserisci una data in formato gg/mm/aaa
                                        </small>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 mb-5">
                                        <small data-error-for="prestRangeDate" class="form-text text-secondary">
                                            La data di inizio deve essere minore o uguale alla data di fine
                                        </small>
                                    </div>
                                    <div class="modal-footer d-flex">
                                        <input type="submit" name ="Submit" value="Report PDF" class="btn btn-primary" disabled>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-12 ">

                        <h2 class="font-weight-semibold">
                            Stampa report XLS delle ricette erogate nella provincia di ${user.provincia.nome}
                        </h2>

                        <div class="row mt-5">


                            <div class="col-lg-12 col-md-12  p-3">
                                <h3 class="font-weight-semibold">Seleziona l'intervallo di tempo</h3>
                                <form class="form-signin row" action="report" method="GET" novalidate>
                                    <input type="hidden" name="report" value="xls"/>
                                    <input type="hidden" name="type" value="sspRicetteErogate"/>
                                    <input type="hidden" name="siglaProvincia" value="${user.provincia.sigla}"/>


                                    <div class="it-datepicker-wrapper mt-5 col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-label-group mb-0">
                                            <label for="ricStartDate">Dal giorno</label>
                                            <input class="form-control it-date-datepicker" id="ricStartDate"
                                                   name="ricStartDate"
                                                   type="text" 
                                                   placeholder=" "
                                                   oninput=""
                                                   readonly>
                                        </div>
                                        <small data-error-for="ricStartDate" class="form-text text-secondary">
                                            Inserisci una data in formato gg/mm/aaa
                                        </small>

                                    </div>
                                    <!--                                    
                                    checkDateRange(this, document.getElementById('ricEndDate'));
                                    
                                    --><div class="it-datepicker-wrapper mt-5 col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-label-group mb-0">
                                            <label for="ricEndDate">Al giorno</label>
                                            <input class="form-control it-date-datepicker" id="ricEndDate"
                                                   name="ricEndDate"
                                                   type="text" 
                                                   placeholder=" "
                                                   readonly>
                                        </div>
                                        <small data-error-for="ricEndDate" class="form-text text-secondary">
                                            Inserisci una data in formato gg/mm/aaa
                                        </small>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 mb-5">
                                        <small data-error-for="ricRangeDate" class="form-text text-secondary">
                                            La data di inizio deve essere minore o uguale alla data di fine
                                        </small>
                                    </div>
                                    <div class="modal-footer d-flex">
                                        <input type="submit" name ="Submit" value="Report XLS" class="btn btn-primary" disabled>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </body>
    <%@include  file="/html/footer.html" %>
</html>