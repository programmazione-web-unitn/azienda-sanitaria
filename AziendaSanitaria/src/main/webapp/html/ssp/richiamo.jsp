<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Azienda Sanitaria - Ssp effettua richiamo</title>
        <%@include file="/html/jsCssInclude.html" %>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

        <script>
            $(document).ready(function () {
                $("#autocompleteVisiteSpec").select2({
                    placeholder: "Inserisci nome visita specialistica",
                    allowClear: true,
                    ajax: {
                        url: function (request) {
                            return "/AziendaSanitaria/services/listaTipiDiVisita/" + (request.term || "");
                        },
                        dataType: "json"
                    }
                });
//                $("#autocompleteVisiteSpec").val(null).trigger("change");
                $("#autocompleteVisiteSpec").on("select2:select", function (e) {
                    var id = e.params.data.id;
                    document.getElementById("idTipoDiVisita").value = id;
                });
            });


        </script>
    </head>
    <body>
        <%@include file="/html/headers/sspHeader.jsp" %>
        <div class="full-height"><div class="container p-4">
                <div class="row p-3">   
                    <a href="#" class="go-back"><i class="fas fa-arrow-left fa-sm mr-2 "></i>Torna indietro</a>
                </div>
                <!-- Three columns of text below the carousel -->
                <div class="row mb-5">
                    
                    <div class="col-12">
                        <%@include file="/html/errorSuccess.jsp" %>
                    </div>
                    <div class="col-12">

                        <h2 class="font-weight-semibold">
                            Effettua un richiamo per i pazienti della provincia
                        </h2>

                        <div class="row mt-5 mb-5">


                            <div class="col-lg-12 col-md-12  p-3">
                                <form class="form-signin row" action="effettuaRichiamo" method="POST" novalidate>
                                    <div class="col-12">
                                        <label for="autocompleteVisiteSpec">Nome della visita specialistica</label>
                                        <select id="autocompleteVisiteSpec" name="autocompleteVisiteSpec" 
                                                class="form-control select2-allow-clear simple-select2 w-100"
                                                value="">
                                        </select>
                                        <small data-error-for="autocompleteVisiteSpec" class="form-text text-secondary">
                                            Seleziona una visita specialistica
                                        </small>
                                    </div>
                                    
                                    <div class="col-lg-6 col-md-6 col-sm-12 mt-5">
                                        <label for="minEta">
                                            Età minima
                                        </label>
                                        <span class="input-number">
                                            <input type="text" id="minEta" name="minEta" class="ageField"/>
                                        </span>
                                        <small data-error-for="minEta" class="form-text text-secondary">
                                            Inserisci un'età valida
                                        </small>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 mt-5">
                                        <label for="maxEta">
                                            Età massima
                                        </label>
                                        <span class="input-number">
                                            <input type="text" id="maxEta" name="maxEta" class="ageField"/>
                                        </span>
                                        <small data-error-for="maxEta" class="form-text text-secondary">
                                            Inserisci un'età valida
                                        </small>
                                    </div>
                                    <div class="col-12">
                                        <small data-error-for="rangeEta" class="form-text text-secondary">
                                            L'età minima deve essere minore o uguale a quella massima
                                        </small>
                                    </div>

                                    <div class=" col-12 mt-5">
                                        <div class="form-check">
                                            <input name="soloGiaFatto" id="soloGiaFatto" type="checkbox">
                                            <label for="soloGiaFatto">Effettua il richiamo solo per chi l'ha già fatto negli anni precedenti</label>
                                        </div>
                                    </div>
                                    <div class="modal-footer d-flex">
                                        <input type="submit" name ="Submit" value="Effettua richiamo" class="btn btn-primary" disabled>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </body>
    <%@include  file="/html/footer.html" %>
</html>