<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Azienda Sanitaria - Ssp homepage</title>
        <%@include file="/html/jsCssInclude.html" %>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>



        <script>
            $(document).ready(function () {
                $("#autocomplete").select2({
                    placeholder: "Inserisci nome, cognome o codice fiscale",
                    allowClear: true,
                    ajax: {
                        url: function (request) {
                            return "/AziendaSanitaria/services/listaPazienti/" + (request.term || "");
                        },
                        dataType: "json"
                    }
                });
//                $("#autocomplete").val(null).trigger("change");
                $("#autocomplete").on("select2:select", function (e) {
                    var id = e.params.data.id;
                    a = document.getElementById("visitaPaziente");
                    a.setAttribute("href", "/AziendaSanitaria/Ssp/paziente?idPaziente=" + id);

                });
            });
        </script>

    </head>






    <body>
        <%@include file="/html/headers/sspHeader.jsp" %>
        <div class="container p-4 full-height">
            <div class="row mb-5 mt-5">
                <div class="col-12">
                    <h2>
                        Servizio sanitario della provincia di ${user.provincia.nome}
                    </h2>
                </div>
                <div class="col-12 pt-4">
                    <form id="datiPaziente" method="GET" action="paziente" novalidate>
                        <h2 class="font-weight-semibold">Ricerca Paziente</h2>
                        <div class="form-group">
                            <p class="form-text">Inserisci nome, cognome o codice fiscale.</p>
                            <select id="autocomplete" name="idPaziente" class="form-control select2-allow-clear simple-select2 w-100">
                            </select>
                            <small data-error-for="autocomplete" class="form-text text-secondary">
                                Seleziona un paziente
                            </small>
                        </div>
                        <input type="submit" value="Visualizza paziente" class="btn btn-primary btn-icon" disabled>
                    </form>
                </div>
            </div>
        </div>
    </body>
    <%@include  file="/html/footer.html" %>

</html>
