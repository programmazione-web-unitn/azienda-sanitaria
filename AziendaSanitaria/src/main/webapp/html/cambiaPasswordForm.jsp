<div class="form-label-group mb-5">

    <label for="nuovaPassword">Nuova password</label>
    <div class="form-group mb-0">
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text" data-lock-for="nuovaPassword"><svg class="icon icon-sm icon-danger"><use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-locked"></use></svg></div>
            </div>
            <input type="password" class="form-control input-password secure-password-input" 
                   id="nuovaPassword" name="nuovaPassword" autocomplete="new-password"/>          

            <span class="password-icon" aria-hidden="true">
                <svg class="password-icon-visible icon icon-sm"><use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-password-visible"></use></svg>
                <svg class="password-icon-invisible icon icon-sm d-none"><use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-password-invisible"></use></svg>
            </span>

        </div>
    </div>
    <small data-error-for="nuovaPassword" class="form-text text-secondary">
        La password deve essere lunga almeno 8 caratteri, deve contenere almeno un carattere minuscolo, 
        uno maiuscolo, un numero e un carattere speciale (!, @, #, $, %, ^, &, *)
    </small>
</div>


<div class="form-label-group mb-5">
    <label for="confermaPassword">Conferma password</label>
    <div class="form-group mb-0">
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text" data-lock-for="confermaPassword"><svg class="icon icon-sm icon-danger"><use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-locked"></use></svg></div>
            </div>
            <input type="password" class="form-control input-password"
                   id="confermaPassword" name="confermaPassword" autocomplete="new-password"
                   data-confirm-password-for="nuovaPassword"/>      
            <span class="password-icon" aria-hidden="true">
                <svg class="password-icon-visible icon icon-sm"><use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-password-visible"></use></svg>
                <svg class="password-icon-invisible icon icon-sm d-none"><use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-password-invisible"></use></svg>
            </span>
        </div>

    </div>
    <small data-error-for="confermaPassword" class="form-text text-secondary">
        La nuova password e la conferma devono essere uguali
    </small>
</div>

<div class="modal-footer justify-content-center">
    <input type="submit" name ="Submit" value="Modifica" class="btn btn-primary" disabled>
</div>
