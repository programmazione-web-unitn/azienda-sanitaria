<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%--<c:if test="${not empty user}">  
    <c:set var="home" value="${Authentication.getUserPath(user)}/home"></c:set>
    <c:redirect url="${home}"/>
</c:if>--%>

<c:choose>
    <c:when test="${user.tipo eq 'SSP'}">
        <c:redirect url="Ssp/home"/>
    </c:when>
    <c:when test="${user.tipo eq 'SSN'}">
        <c:redirect url="Ssn/home"/>
    </c:when>
    <c:when test="${user.tipo eq 'F'}">
        <c:redirect url="Farmacia/home"/>
    </c:when>
    <c:when test="${user.tipo eq 'P'}">
        <c:redirect url="Paziente/home"/>
    </c:when>
    <c:when test="${user.tipo eq 'MB'}">
        <c:redirect url="Medico/home"/>
    </c:when>
    <c:when test="${user.tipo eq 'MS'}">
        <c:redirect url="Specialista/home"/>
    </c:when>
</c:choose>

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <%@include file="/html/jsCssInclude.html" %>
        <script src="/AziendaSanitaria/js/utils.js"></script>
        <script>
            $(document).ready(function () {
                const email = $('#inputEmail');
                const forgotEmail = $('#forgotEmail');

                const password = $('#inputPassword');

                checkRequired(password[0], true);
                password.on('input', function () {
                    checkRequired(password[0])
                });

                checkEmail(email[0], true);
                email.on('input', function () {
                    checkEmail(email[0]);
                });

                checkEmail(forgotEmail[0], true);
                forgotEmail.on('input', function () {
                    checkEmail(forgotEmail[0]);
                });
            })
        </script>
        <title>Azienda Sanitaria - Login</title>
    </head>
    <body>

        <%@include file="/html/headers/loginHeader.jsp" %>

        <div class="container full-height">
            <div class="row">
                <div class="offset-lg-3 col-lg-6 p-4">
                    <form class="form-signin" action="/AziendaSanitaria/LoginServlet" method="post" id="loginForm" novalidate>
                        <h2 class="font-weight-semibold">Login</h2>

                        <%@include file="/html/errorSuccess.jsp" %>

                        <div class="form-label-group mt-3">
                            <label for="inputEmail">Email</label>
                            <input type="email" id="inputEmail" name="inputEmail" class="form-control" autofocus>
                            <small data-error-for="inputEmail" class="form-text text-secondary">
                                Inserisci un indirizzo email valido
                            </small>
                        </div>
                        <div class="form-label-group mb-5">
                            <label for="inputPassword">Password</label>
                            <div class="form-group mb-0">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text" data-lock-for="inputPassword"><svg class="icon icon-sm icon-danger"><use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-locked"></use></svg></div>
                                    </div>
                                    <input type="password" class="form-control input-password"
                                           id="inputPassword" name="inputPassword" autocomplete="new-password"/>      
                                    <span class="password-icon" aria-hidden="true">
                                        <svg class="password-icon-visible icon icon-sm"><use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-password-visible"></use></svg>
                                        <svg class="password-icon-invisible icon icon-sm d-none"><use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-password-invisible"></use></svg>
                                    </span>
                                </div>

                            </div>
                            <small data-error-for="inputPassword" class="form-text text-secondary">
                                Inserisci la password
                            </small>
                        </div>

<!--                        <div class="form-label-group mt-3 mb-3">
                            <label for="inputPassword">Password</label>

                            <input type="password" id="inputPassword" name="inputPassword" class="form-control" >
                            <small data-error-for="inputPassword" class="form-text text-secondary">
                                Inserisci la password
                            </small>
                        </div>-->

                        <div class="form-check mb-3">
                            <input id="ricordami" name="ricordami" value="ricordami" type="checkbox">
                            <label for="ricordami">Ricordami</label>
                        </div>

                        <div>
                            <a href="#" class="pull-right" id="forgotLink" onclick="showSecondary(event, $('#loginForm'), $('#forgotForm'))">
                                Hai dimenticato la password?
                            </a>
                        </div>

                        <div class="modal-footer d-flex justify-content-center">
                            <input type="submit" value="Login" class="btn btn-primary">
                        </div>

                    </form>

                    <form class="form-signin d-none" action="/AziendaSanitaria/PasswordDimenticata" method="get" id="forgotForm" novalidate>
                        <h2 class="font-weight-semibold" style="white-space: nowrap">Recupera password</h2>
                        <div class="form-label-group mt-3 mb-3">
                            <label for="forgotEmail" style="white-space: nowrap">Inserisci email</label>

                            <input type="email" id="forgotEmail" name="forgotEmail" class="form-control" autofocus>
                            <small data-error-for="forgotEmail" class="form-text text-secondary">
                                Inserisci un'indirizzo email valido
                            </small>
                        </div>

                        <div>
                            <a href="#" class="pull-right mt-2" id="loginBack" onclick="showPrimary(event, $('#loginForm'), $('#forgotForm'))">Torna al login</a>
                        </div>

                        <div class="modal-footer d-flex justify-content-center">
                            <input type="submit" value="Recupera" class="btn btn-primary ">
                        </div>

                    </form>


                </div>
            </div>


        </div>

    <%@include  file="/html/footer.html" %>

    </body>
</html>
