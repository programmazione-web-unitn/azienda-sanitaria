<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Azienda Sanitaria - Privacy</title>
        <%@include file="/html/jsCssInclude.html" %>
    </head>
    <body>
        <%@include file="/html/headers/homepageHeader.jsp" %>

        
        <div class="container p-4 mb-5">
            <div class="row p-3">   
                <a href="#" class="go-back"><i class="fas fa-arrow-left fa-sm mr-2 "></i>Torna indietro</a>
            </div>
            <div class="row pt-3">   
                <h2>Informativa sulla privacy</h2>
            </div>

            <div class="row pt-3">   
                <h2 class="font-weight-semibold">Base giuridica del trattamento</h2>
                <p class="text-justify">
                    Il presente sito tratta i dati prevalentemente in base al consenso degli utenti. Il conferimento del consenso avviene tramite il banner posto in fondo alla pagina, oppure tramite l’uso o la consultazione del sito, quale comportamento concludente. Con l'uso o la consultazione del sito i visitatori e gli utenti approvano la presente informativa privacy e acconsentono al trattamento dei loro dati personali in relazione alle modalità e alle finalità di seguito descritte, compreso l'eventuale diffusione a terzi se necessaria per l'erogazione di un servizio. Tramite i moduli di comunicazione o di richiesta di servizi vengono raccolti ulteriori consensi relativi alla finalità specifica del servizio. <br>
                    Il conferimento dei dati e quindi il consenso alla raccolta e al trattamento dei dati è facoltativo, l'Utente può negare il consenso, e può revocare in qualsiasi momento un consenso già fornito (tramite il banner posto a fondo pagina o le impostazioni del browser per i cookie, o il link Contatti). Tuttavia negare il consenso può comportare l'impossibilità di erogare alcuni servizi e l'esperienza di navigazione nel sito sarebbe compromessa.  <br>
                    I dati per la sicurezza del sito e per la prevenzione da abusi e SPAM, nonché i dati per l’analisi del traffico del sito (statistica) in forma aggregata, sono trattati in base al legittimo interesse del Titolare del trattamento alla tutela del sito e degli utenti stessi. In tali casi l'utente ha sempre il diritto di opporsi al trattamento dei dati (vedi par. Diritti dell'utente). 
                </p>

                <p class="text-justify"> 
                    Finalità Il trattamento dei dati raccolti dal sito, oltre alle finalità connesse, strumentali e necessarie alla fornitura del servizio, è volto alle seguenti finalità:
                <ul>
                    <li>Statistica (analisi): Raccolta di dati e informazioni in forma esclusivamente aggregata e anonima al fine di verificare il corretto funzionamento del sito. Nessuna di queste informazioni è correlata alla persona fisica-Utente del sito, e non ne consentono in alcun modo l'identificazione. Non occorre il consenso. </li>

                    <li>Sicurezza: Raccolta di dati e informazioni al fine di tutelare la sicurezza del sito (filtri antispam, firewall, rilevazione virus) e degli Utenti e per prevenire o smascherare frodi o abusi a danno del sito web. I dati sono registrati automaticamente e possono eventualmente comprendere anche dati personali (indirizzo Ip) che potrebbero essere utilizzati, conformemente alle leggi vigenti in materia, al fine di bloccare tentativi di danneggiamento al sito medesimo o di recare danno ad altri utenti, o comunque attività dannose o costituenti reato. Tali dati non sono mai utilizzati per l'identificazione o la profilazione dell'Utente e vengono cancellati periodicamente. Non occorre il consenso. </li>

                    <li>Attività accessorie: Comunicare i dati a terze parti che svolgono funzioni necessarie o strumentali all'operatività del servizio (es. box commenti), e per consentire a terze parti di svolgere attività tecniche, logistiche e di altro tipo per nostro conto. I fornitori hanno accesso solo ai dati personali che sono necessari per svolgere i propri compiti, e si impegnano a non utilizzare i dati per altri scopi, e sono tenuti a trattare i dati personali in conformità delle normative vigenti. </li>
                </ul>
            </div>
            <div class="row pt-3">   
                <h2 class="font-weight-semibold">Dati raccolti</h2>
                Durante la navigazione degli Utenti possono essere raccolte le seguenti informazioni che vengono conservate nei file di log del server (hosting) del sito: 
                <ul>
                    <li>indirizzo internet protocol (IP);</li>
                    <li>tipo di browser;</li>
                    <li>parametri del dispositivo usato per connettersi al sito;</li>
                    <li>nome dell'internet service provider (ISP);</li>
                    <li>data e orario di visita;</li>
                    <li>pagina web di provenienza del visitatore (referral) e di uscita;</li>
                    <li>eventualmente il numero di click.</li>

                </ul>

                Questi dati sono utilizzati a fini di statistica e analisi, in forma esclusivamente aggregata. L’indirizzo IP è utilizzato esclusivamente a fini di sicurezza e non è incrociato con nessun altro dato.

                Il sito può raccogliere altri dati in caso di utilizzo volontario di servizi da parte degli utenti, quali servizi di commenti, di comunicazione (moduli per contatti, box commenti), e verranno utilizzati esclusivamente per l'erogazione del servizio richiesto: 
                <ul>
                    <li>nome;</li>
                    <li>indirizzo email.</li>
                </ul>

            </div>
            <div class="row pt-3">   
                <h2 class="font-weight-semibold">Luogo del trattamento</h2>
                <p class="text-justify">
                    I dati sono trattati presso la sede del Titolare del Trattamento, e presso il datacenter del web Hosting (VHosting Solution s.r.l). Il web hosting (VHosting Solution s.r.l Unipersonale - Via A. Telesino 67, Palermo, IT), è responsabile del trattamento, elaborando i dati per conto del titolare. VHosting si trova nello Spazio Economico Europeo e agisce in conformità delle norme europee (policy privacy di VHosting). 
                </p>
            </div>
            <div class="row pt-3">   
                <h2 class="font-weight-semibold">Periodo di conservazione dei dati</h2>
                <p class="text-justify">
                    I dati sono trattati presso la sede del Titolare del Trattamento, e presso il datacenter del web Hosting (VHosting Solution s.r.l). Il web hosting (VHosting Solution s.r.l Unipersonale - Via A. Telesino 67, Palermo, IT), è responsabile del trattamento, elaborando i dati per conto del titolare. VHosting si trova nello Spazio Economico Europeo e agisce in conformità delle norme europee (policy privacy di VHosting). <br>
                    I dati raccolti dal sito durante il suo funzionamento sono conservati per il tempo strettamente necessario a svolgere le attività precisate. Alla scadenza i dati saranno cancellati o anonimizzati, a meno che non sussistano ulteriori finalità per la conservazione degli stessi. <br>
                    I dati (indirizzo IP) utilizzati a fini di sicurezza del sito (blocco tentativi di danneggiare il sito) sono conservati per 30 giorni. <br>
                    I dati per finalità di analytics (statistica) sono conservati in forma aggregata per 24 mesi. <br>
                </p>
            </div>
            <h2 class="font-weight-semibold">Trasferimento dei dati raccolti a terze parti</h2>
            <p class="text-justify">
                I dati rilevati dal sito generalmente non vengono forniti a terzi, tranne che in casi specifici: legittima richiesta da parte dell’autorità giudiziaria e nei soli casi previsti dalla legge; qualora sia necessario per la fornitura di uno specifico servizio richiesto dell'Utente; per l'esecuzione di controlli di sicurezza o di ottimizzazione del sito.
            </p>
            <div class="row pt-3">   
                <h2 class="font-weight-semibold">Trasferimento dei dati in paesi extra UE</h2>
                <p class="text-justify">
                    Il presente sito potrebbe condividere alcuni dei dati raccolti con servizi localizzati al di fuori dell'area dell'Unione Europea. In particolare con Google, Facebook e Microsoft (LinkedIn) tramite i social plugin e il servizio di Google Analytics. Il trasferimento è autorizzato in base a specifiche decisioni dell'Unione Europea e del Garante per la tutela dei dati personali, in particolare la decisione 1250/2016 (Privacy Shield - qui la pagina informativa del Garante italiano), per cui non occorre ulteriore consenso. Le aziende sopra menzionate garantiscono la propria adesione al Privacy Shield.
                </p>
            </div>
            <div class="row pt-3">   
                <h2 class="font-weight-semibold">Misure di sicurezza</h2>
                <p class="text-justify">
                    security Trattiamo i dati dei visitatori/utenti in maniera lecita e corretta, adottando le opportune misure di sicurezza volte ad impedire accessi non autorizzati, divulgazione, modifica o distruzione non autorizzata dei dati. Ci impegniamo a tutelare la sicurezza dei tuoi dati personali durante il loro invio, utilizzando il software Secure Sockets Layer (SSL), che cripta le informazioni in transito. Il trattamento viene effettuato mediante strumenti informatici e/o telematici, con modalità organizzative e con logiche strettamente correlate alle finalità indicate. Oltre al Titolare, in alcuni casi, potrebbero avere accesso ai dati categorie di incaricati coinvolti nell’organizzazione del sito ovvero soggetti esterni (come fornitori di servizi tecnici terzi, hosting provider). 
                </p>
                <h2 class="font-weight-semibold">Diritti dell'utente</h2>
                <p class="text-justify">
                    Ai sensi del Regolamento europeo 679/2016 (GDPR) l'Utente può, secondo le modalità e nei limiti previsti dalla vigente normativa, esercitare i deguenti diritti:
                <ul>
                    <li>opporsi in tutto o in parte, per motivi legittimi al trattamento dei dati personali che lo riguardano ai fini di invio di materiale pubblicitario o di vendita diretta o per il compimento di ricerche di mercato o di comunicazione commerciale;</li>
                    <li>conoscerne l'origine;</li>
                    <li>riceverne comunicazione intelligibile;</li>
                    <li>avere informazioni circa la logica, le modalità e le finalità del trattamento;</li>
                    <li>richiederne l'aggiornamento, la rettifica, l'integrazione, la cancellazione, la trasformazione in forma anonima, il blocco dei dati trattati in violazione di legge, ivi compresi quelli non più necessari al perseguimento degli scopi per i quali sono stati raccolti;</li>
                    <li>nei casi di trattamento basato su consenso, ricevere al solo costo dell’eventuale supporto, i suoi dati forniti al titolare, in forma strutturata e leggibile da un elaboratore di dati e in un formato comunemente usato da un dispositivo elettronico;</li>
                    <li>il diritto di presentare un reclamo all’Autorità di controllo (Garante Privacy – link alla pagina del Garante);</li>
                    <li>nonché, più in generale, esercitare tutti i diritti che gli sono riconosciuti dalle vigenti disposizioni di legge.</li>
                </ul>

                <br/>
            </div>
        </div>
        <%@include  file="/html/footer.html" %>
    </body>
</html>
