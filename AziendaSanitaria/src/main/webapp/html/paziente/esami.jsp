<%--
    Document   : esami
    Created on : Oct 14, 2019, 4:35:54 PM
    Author     : fran
--%>

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix ="fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>




<!DOCTYPE html>
<html>
    <head>
        <title>Azienda Sanitaria - Paziente esami</title>
        <%@include file="/html/jsCssInclude.html" %>
        <%@include file="/html/paziente/jsCssPazienteInclude.html" %>

        <script>
            <c:if test="${ricetteDaErogare.size() gt 0}">
            $(document).ready(checkPharmacies("notification", "notificationText", 1000));
            </c:if>
        </script>



        <link rel="stylesheet" type="text/css" href="/AziendaSanitaria/DataTables/datatables.min.css"/>

        <script type="text/javascript" src="/AziendaSanitaria/DataTables/datatables.min.js"></script>




        <script>
            $(document).ready(function () {

                $('#tabellaEsami').DataTable({

                    autoWidth: false,
                    "language": {
                        "sEmptyTable": "Nessun dato presente nella tabella",
                        "sInfo": "Vista da _START_ a _END_ di _TOTAL_ elementi",
                        "sInfoEmpty": "Vista da 0 a 0 di 0 elementi",
                        "sInfoFiltered": "(filtrati da _MAX_ elementi totali)",
                        "sInfoPostFix": "",
                        "sInfoThousands": ".",
                        "sLengthMenu": "Visualizza _MENU_ elementi",
                        "sLoadingRecords": "Caricamento...",
                        "sProcessing": "Elaborazione...",
                        "sSearch": "Cerca:",
                        "sZeroRecords": "La ricerca non ha portato alcun risultato.",
                        "oPaginate": {
                            "sFirst": "Inizio",
                            "sPrevious": "Precedente",
                            "sNext": "Successivo",
                            "sLast": "Fine"
                        },
                        "oAria": {
                            "sSortAscending": ": attiva per ordinare la colonna in ordine crescente",
                            "sSortDescending": ": attiva per ordinare la colonna in ordine decrescente"
                        }

                    },
                    "aaSorting": [[0, "desc"]]
                });

                $('#esamiPossibili').DataTable({
                    "ajax": {
                        "url": "/AziendaSanitaria/services/listaTipiDiEsame/",
                        "dataSrc": function (json) {
                            
                            var data = [];
                            for (var i = 0; i < json.results.length; i++) {
                                data[i] = [json.results[i].text];
                            }
                            return data;
                        }
                    },
                    autoWidth: false,
                    "language": {
                        "sEmptyTable": "Nessun dato presente nella tabella",
                        "sInfo": "Vista da _START_ a _END_ di _TOTAL_ elementi",
                        "sInfoEmpty": "Vista da 0 a 0 di 0 elementi",
                        "sInfoFiltered": "(filtrati da _MAX_ elementi totali)",
                        "sInfoPostFix": "",
                        "sInfoThousands": ".",
                        "sLengthMenu": "Visualizza _MENU_ elementi",
                        "sLoadingRecords": "Caricamento...",
                        "sProcessing": "Elaborazione...",
                        "sSearch": "Cerca:",
                        "sZeroRecords": "La ricerca non ha portato alcun risultato.",
                        "oPaginate": {
                            "sFirst": "Inizio",
                            "sPrevious": "Precedente",
                            "sNext": "Successivo",
                            "sLast": "Fine"
                        },
                        "oAria": {
                            "sSortAscending": ": attiva per ordinare la colonna in ordine crescente",
                            "sSortDescending": ": attiva per ordinare la colonna in ordine decrescente"
                        }

                    }
                });


            });
        </script>

    </head>

    <body>
        <%@include file="/html/headers/pazienteHeader.jsp" %>

        <fmt:setLocale value="it_IT" />
        <div class="full-height">

            <div class="container p-4">
                <div class="row p-3">   
                    <a href="#" class="go-back"><i class="fas fa-arrow-left fa-sm mr-2 "></i>Torna indietro</a>
                </div>
                <div class="row mb-4">
                    <div class="col-12">
                        <h2 class="font-weight-semibold">Esami</h2>
                    </div>

                </div>
                <div class="row mb-5">

                    <div class = "table-responsive">

                        <table id="tabellaEsami" class="table table-striped dt-responsive">
                            <thead>
                                <tr>
                                    <th scope="col">Data di prescrizione</th>
                                    <th scope="col">Tipo</th>
                                    <th scope="col">Data erogazione</th>
                                    <th scope="col">Vedi</th>
                                </tr>
                            </thead>    

                            <tbody>
                                <c:forEach var="esame" items="${esamiDaErogare}">
                                    <tr>
                                        <td data-order="${esame.dataOraPrescrizione}">
                                            <fmt:formatDate pattern = "E dd/MM/yyyy" value = "${esame.dataOraPrescrizione}" />
                                        </td>

                                        <td>${esame.tipoDiEsame.nome}</td>
                                        <td data-order="Non erogato">Non erogato</td>
                                        <td>
                                            <a class="btn btn-primary" href="esami/info?idEsame=${esame.id}">
                                                Vedi
                                            </a> 
                                        </td>
                                    </tr>
                                </c:forEach>
                                <c:forEach var="esame" items="${esamiErogati}">
                                    <tr>
                                        <td data-order="${esame.dataOraPrescrizione}">
                                            <fmt:formatDate pattern = "E dd/MM/yyyy" value = "${esame.dataOraPrescrizione}" />
                                        </td>

                                        <td>${esame.tipoDiEsame.nome}</td>
                                        <td data-order="${esame.dataOraErogazione}">
                                            <fmt:formatDate pattern = "E dd/MM/yyyy" value = "${esame.dataOraErogazione}" />
                                        </td>
                                        <td>
                                            <a class="btn btn-primary" href="esami/info?idEsame=${esame.id}">
                                                Vedi
                                            </a> 
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>

                </div>



                <div id="collapseDiv1" class="collapse-div mt-5" role="tablist">
                    <div class="collapse-header" id="heading1">
                        <button data-toggle="collapse" data-target="#collapse1" aria-expanded="false" aria-controls="collapse1">
                            <h3 class="font-weight-semibold">Lista esami possibili </h3>
                        </button>
                    </div>
                    <div id="collapse1" class="collapse" role="tabpanel" aria-labelledby="heading1">
                        <div class="collapse-body">


                            <table id="esamiPossibili" class="table table-striped dt-responsive">
                                <thead>
                                    <tr>
                                        <th scope="col">Nome</th>
                                    </tr>
                                </thead>    
                            </table>



                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@include  file="/html/footer.html" %>

    </body>
</html>
