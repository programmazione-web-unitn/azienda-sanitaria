<%--
    Document   : esami
    Created on : Oct 14, 2019, 4:35:54 PM
    Author     : fran
--%>

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix ="fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>



<!DOCTYPE html>
<html>
    <head>
        <title>Azienda Sanitaria - Paziente visite di base</title>




        <%@include file="/html/jsCssInclude.html" %>        
        <%@include file="/html/paziente/jsCssPazienteInclude.html" %>

        <script>
            <c:if test="${ricetteDaErogare.size() gt 0}">
            $(document).ready(checkPharmacies("notification", "notificationText", 1000));
            </c:if>
        </script>

        <link rel="stylesheet" type="text/css" href="/AziendaSanitaria/DataTables/datatables.min.css"/>

        <script type="text/javascript" src="/AziendaSanitaria/DataTables/datatables.min.js"></script>

        <script>
            $(document).ready(function () {

                $('#tabellaVisiteBase').DataTable({

                    autoWidth: false,
                    "language": {
                        "sEmptyTable": "Nessun dato presente nella tabella",
                        "sInfo": "Vista da _START_ a _END_ di _TOTAL_ elementi",
                        "sInfoEmpty": "Vista da 0 a 0 di 0 elementi",
                        "sInfoFiltered": "(filtrati da _MAX_ elementi totali)",
                        "sInfoPostFix": "",
                        "sInfoThousands": ".",
                        "sLengthMenu": "Visualizza _MENU_ elementi",
                        "sLoadingRecords": "Caricamento...",
                        "sProcessing": "Elaborazione...",
                        "sSearch": "Cerca:",
                        "sZeroRecords": "La ricerca non ha portato alcun risultato.",
                        "oPaginate": {
                            "sFirst": "Inizio",
                            "sPrevious": "Precedente",
                            "sNext": "Successivo",
                            "sLast": "Fine"
                        },
                        "oAria": {
                            "sSortAscending": ": attiva per ordinare la colonna in ordine crescente",
                            "sSortDescending": ": attiva per ordinare la colonna in ordine decrescente"
                        }

                    },
                    "aaSorting": [[0, "desc"]]
                });

            });
        </script>

    </head>

    <body>
        <%@include file="/html/headers/pazienteHeader.jsp" %>

        <fmt:setLocale value="it_IT" />
        <div class="full-height">

            <div class="container p-4">
                <div class="row p-3">   
                    <a href="#" class="go-back"><i class="fas fa-arrow-left fa-sm mr-2 "></i>Torna indietro</a>
                </div>
                <div class="row mb-4">
                    <div class="col-12">
                        <h2 class="font-weight-semibold">Visite di base</h2>
                    </div>

                </div>
                <div class="row">
                    <div class="table-responsive">


                        <table id="tabellaVisiteBase" class="table table-striped dt-responsive">
                            <thead>
                                <tr>
                                    <th scope="col">Data</th>                                    
                                    <th scope="col">Vedi</th>
                                </tr>
                            </thead>

                            <tbody>

                                <c:forEach var="visita" items="${tutteVisiteBase}">
                                    <tr>
                                        <td data-order="${visita.dataOra}">
                                            <fmt:formatDate pattern = "E dd/MM/yyyy" value = "${visita.dataOra}" />
                                        </td>
                                        <td>
                                           
                                            <a class="btn btn-primary" href="base/info?idVisita=${visita.id}">
                                                Vedi
                                            </a> 
                                        </td>
                                    </tr>

                                </c:forEach>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
        <%@include  file="/html/footer.html" %>

    </body>
</html>
