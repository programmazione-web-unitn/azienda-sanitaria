<%--
    Document   : esami
    Created on : Oct 14, 2019, 4:35:54 PM
    Author     : fran
--%>

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix ="fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>




<!DOCTYPE html>
<html>
    <head>
        <title>Azienda Sanitaria - Paziente farmacie</title>

        <%@include file="/html/jsCssInclude.html" %>

        <%@include file="/html/paziente/jsCssPazienteInclude.html" %>     

        <link rel="stylesheet" type="text/css" href="/AziendaSanitaria/DataTables/datatables.min.css"/>
        <script type="text/javascript" src="/AziendaSanitaria/DataTables/datatables.min.js"></script>        

        <script>
            $(document).ready(function () {

                var table = $('#listaFarmacie').DataTable({
                    createdRow: function (row, data, dataIndex) {
                        // Set the data-status attribute, and add a class
                        $(row)
                                .attr('data-idmarker', data.idMarker)
                                .addClass('open-marker');
                        $(row).click(function () {
                            openMarker(data.idMarker);
                        });
                    },
                    autoWidth: false,
                    "lengthChange": false,
                    "pageLength": 6,
                    "order": [[1, 'asc']],

                    "language": {
                        "sEmptyTable": "Nessuna farmacia presente nelle vicinanze. Assicurati che la posizione sia rilevabile.",
                        "sInfo": "Vista da _START_ a _END_ di _TOTAL_",
                        "sInfoEmpty": "Vista da 0 a 0 di 0 elementi",
                        "sInfoFiltered": "",
                        "sInfoPostFix": "",
                        "sInfoThousands": ".",
                        "sLengthMenu": "Visualizza _MENU_ elementi",
                        "sLoadingRecords": "Caricamento...",
                        "sProcessing": "Elaborazione...",
                        "sSearch": "Cerca:",
                        "sZeroRecords": "La ricerca non ha portato alcun risultato.",
                        "oPaginate": {
                            "sFirst": "Inizio",
                            "sPrevious": "Precedente",
                            "sNext": "Successivo",
                            "sLast": "Fine"
                        },
                        "oAria": {
                            "sSortAscending": ": attiva per ordinare la colonna in ordine crescente",
                            "sSortDescending": ": attiva per ordinare la colonna in ordine decrescente"
                        }

                    }
                });

                showMap('mapContainer', function (places) {
                    table.clear();
                    table.rows.add(places);
                    table.draw();
                });

            });
        </script>
    </head>

    <body>
        <%@include file="/html/headers/pazienteHeader.jsp" %>

        <fmt:setLocale value="it_IT" />
        <div class="full-height">

            <div class="container p-4">
                <div class="row p-3">   
                    <a href="#" class="go-back"><i class="fas fa-arrow-left fa-sm mr-2 "></i>Torna indietro</a>
                </div>
                <div class="row mb-4 ml">
                    <h2 class="font-weight-semibold">Farmacie vicine a te</h2>
                </div>
                <div class="row pb-5">
                    <div class="col-lg-5 col-md-12 p-3">
                        <div class="row">
                            <table id="listaFarmacie" class="table table-hover table-striped dt-responsive">
                                <thead>
                                    <tr>
                                        <th scope="col">Nome</th>
                                        <th scope="col">Distanza [m]</th>
                                    </tr>
                                </thead>    
                                <tbody>                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-12 p-3" id="mapContainer" style="height:70vh"></div>
                </div>
            </div>
        </div>
        <%@include  file="/html/footer.html" %>

    </body>
</html>
