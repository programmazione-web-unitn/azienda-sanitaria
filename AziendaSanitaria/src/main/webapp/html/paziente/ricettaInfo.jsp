<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix ="fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="it_IT" />

<!DOCTYPE html>
<html>
    <head>
        <title>Azienda Sanitaria - Informazioni ricetta</title>
        <%@include file="/html/jsCssInclude.html" %>
        <%@include file="/html/paziente/jsCssPazienteInclude.html" %>

        <link rel="stylesheet" type="text/css" href="/AziendaSanitaria/DataTables/datatables.min.css"/>

        <script type="text/javascript" src="/AziendaSanitaria/DataTables/datatables.min.js"></script>            


        <script>
            <c:if test="${fn:contains(pageContext.request.requestURI, '/Paziente') and ricetteDaErogare.size() gt 0}">
            $(document).ready(checkPharmacies("notification", "notificationText", 1000));
            </c:if>
        </script>
    </head>

    <body>
        <c:choose>
            <c:when test="${fn:contains(pageContext.request.requestURI, '/Paziente')}">
                <%@include file="/html/headers/pazienteHeader.jsp" %>
                <c:set var = "userType" scope = "page" value = "Paziente"/>
            </c:when>
            <c:when test="${fn:contains(pageContext.request.requestURI, '/Medico')}">
                <%@include file="/html/headers/medicoHeader.jsp" %>
                <c:set var = "userType" scope = "page" value = "Medico"/>
            </c:when>
            <c:when test="${fn:contains(pageContext.request.requestURI, '/Specialista')}">
                <%@include file="/html/headers/specialistaHeader.jsp" %>
                <c:set var = "userType" scope = "page" value = "Specialista"/>
            </c:when>
            <c:when test="${fn:contains(pageContext.request.requestURI, '/Ssp')}">
                <%@include file="/html/headers/sspHeader.jsp" %>
                <c:set var = "userType" scope = "page" value = "Ssp"/>
            </c:when>

        </c:choose>
        <div class="full-height">
            

            <div class="container p-4">
                <div class="row p-3">   
                    <a href="#" class="go-back"><i class="fas fa-arrow-left fa-sm mr-2 "></i>Torna indietro</a>
                </div>
                <div class="row mb-3">
                    <div class="col-12">
                        <h2 class="font-weight-semibold">Ricetta</h2>
                    </div>
                </div>
                <div>

                    <dl class="row">
                        <dt class="col-lg-4 col-md-4 col-sm-12"> Data prescrizione </dt>
                        <dd class="col-lg-8 col-md-8 col-sm-12"> <fmt:formatDate pattern = "dd/MM/yyyy" value = "${ricetta.dataOraPrescrizione}"/></dd>
                        <dt class="col-lg-4 col-md-4 col-sm-12"> Ora prescrizione</dt>
                        <dd class="col-lg-8 col-md-8 col-sm-12"> <fmt:formatDate pattern = "hh:mm" value = "${ricetta.dataOraPrescrizione}" /></dd>
                        <dt class="col-lg-4 col-md-4 col-sm-12"> Prescritta da </dt>
                        <dd class="col-lg-8 col-md-8 col-sm-12"> ${ricetta.medicoDiBase.nome} ${ricetta.medicoDiBase.cognome}</dd>
                        <dt class="col-lg-4 col-md-4 col-sm-12"> Farmaco </dt>
                        <dd class="col-lg-8 col-md-8 col-sm-12"> ${ricetta.farmaco.nome} </dd>
                        <dt class="col-lg-4 col-md-4 col-sm-12"> Quantità </dt>
                        <dd class="col-lg-8 col-md-8 col-sm-12"> ${ricetta.quantita} </dd>
                        <dt class="col-lg-4 col-md-4 col-sm-12"> Data erogazione </dt>
                        <c:choose>
                            <c:when test="${ricetta.dataOraErogazione == null}">
                                <dd class="col-lg-8 col-md-8 col-sm-12">Non erogata</dd> 
                            </c:when>
                            <c:otherwise>
                                <dd class="col-lg-8 col-md-8 col-sm-12"> <fmt:formatDate pattern = "dd/MM/yyyy" value = "${ricetta.dataOraErogazione}"/></dd>
                                <dt class="col-lg-4 col-md-4 col-sm-12"> Ora erogazione</dt>
                                <dd class="col-lg-8 col-md-8 col-sm-12"> <fmt:formatDate pattern = "hh:mm" value = "${ricetta.dataOraErogazione}" /></dd>
                                <dt class="col-lg-4 col-md-4 col-sm-12"> Farmacia</dt>
                                <dd class="col-lg-8 col-md-8 col-sm-12"> ${ricetta.farmacia.nome}, ${ricetta.farmacia.comune} (${ricetta.farmacia.provincia.sigla} ) </dd>

                                <dt class="col-lg-4 col-md-4 col-sm-12"> Scarica report </dt>
                                <dd class="col-lg-8 col-md-8 col-sm-12">
                                    <a href="/AziendaSanitaria/${userType}/ricetta/report?type=ricetta&report=pdf&idRicetta=${ricetta.id}">
                                        <i class="fas fa-file-download fa-2x"></i>
                                    </a>
                                </dd>



                            </c:otherwise>
                        </c:choose>

                    </dl>


                </div>

            </div>
        </div>
        <%@include  file="/html/footer.html" %>
    </body>

</html>
