<%-- 
    Document   : ticket
    Created on : Nov 1, 2019, 10:46:40 AM
    Author     : gabriele
--%>

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix ="fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>


<!DOCTYPE html>
<html>
    <head>
        <title>Azienda Sanitaria - Paziente ticket</title>




        <%@include file="/html/jsCssInclude.html" %>
        <%@include file="/html/paziente/jsCssPazienteInclude.html" %>

        <script>
            <c:if test="${ricetteDaErogare.size() gt 0}">
            $(document).ready(checkPharmacies("notification", "notificationText", 1000));
            </c:if>
        </script>

        <link rel="stylesheet" type="text/css" href="/AziendaSanitaria/DataTables/datatables.min.css"/>

        <script type="text/javascript" src="/AziendaSanitaria/DataTables/datatables.min.js"></script>

        <script>
            var properties = {
                autoWidth: false,

                "language": {
                    "sEmptyTable": "Nessun dato presente nella tabella",
                    "sInfo": "Vista da _START_ a _END_ di _TOTAL_ elementi",
                    "sInfoEmpty": "Vista da 0 a 0 di 0 elementi",
                    "sInfoFiltered": "(filtrati da _MAX_ elementi totali)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "Visualizza _MENU_ elementi",
                    "sLoadingRecords": "Caricamento...",
                    "sProcessing": "Elaborazione...",
                    "sSearch": "Cerca:",
                    "sZeroRecords": "La ricerca non ha portato alcun risultato.",
                    "oPaginate": {
                        "sFirst": "Inizio",
                        "sPrevious": "Precedente",
                        "sNext": "Successivo",
                        "sLast": "Fine"
                    },
                    "oAria": {
                        "sSortAscending": ": attiva per ordinare la colonna in ordine crescente",
                        "sSortDescending": ": attiva per ordinare la colonna in ordine decrescente"
                    }
                },
                "aaSorting": [[0, "desc"]]
            };
            $(document).ready(function () {
                for (table of ["tabellaRicette", "tabellaVisiteSpec", "tabellaEsami"]) {
                    $("#" + table).DataTable(properties);
                }
            });

        </script>


    </head>

    <body>
        <%@include file="/html/headers/pazienteHeader.jsp" %>

        <fmt:setLocale value="it_IT" />
        <div class="full-height">

            <div class="container p-4">
                <div class="row p-3">   
                    <a href="#" class="go-back"><i class="fas fa-arrow-left fa-sm mr-2 "></i>Torna indietro</a>
                </div>
                <div class="row mb-4">
                    <div class="col-12">
                        <h2 class="font-weight-semibold">Ticket pagati</h2>
                        <button class="btn btn-outline-primary btn-icon" 
                                onclick="location.href = '/AziendaSanitaria/Paziente/report?type=allPaidTickets&report=pdf&idPaziente=${user.id}';">
                            <svg class="icon icon-primary mr-2">
                            <use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-download"></use>
                            </svg>
                            <span>Scarica tutti i ticket</span>
                        </button>

                    </div>

                </div>



                <div class="row mb-5">
                    <div class="table-responsive">
                        <table id="tabellaRicette" class="table table-striped dt-responsive">

                            <thead>
                                <tr>
                                    <th scope="col">Data erogazione</th>
                                    <th scope="col">Tipo ticket</th>     
                                    <th scope="col">Dettagli</th>
                                    <th scope="col">Costo ticket</th>
                                </tr>
                            </thead>

                            <tbody>
                                <%-- RICETTE EROGATE --%>
                                <c:forEach var="ricetta" items="${ricetteErogate}">
                                    <tr>
                                        <td data-order="${ricetta.dataOraErogazione}">
                                            <fmt:formatDate pattern = "E dd/MM/yyyy" value = "${ricetta.dataOraErogazione}" />
                                        </td>
                                        <td>Ricetta</td>
                                        <td>${ricetta.farmaco.nome}, quantità ${ricetta.quantita}</td>
                                        <td>€${ricetta.farmaco.ticket * ricetta.quantita},00</td>
                                    </tr>
                                </c:forEach>
                                <%-- VISITE SPECIALISTICHE EROGATE --%>
                                <c:forEach var="visita" items="${visiteSpecErogate}">
                                    <tr>
                                        <td data-order="${visita.dataOraErogazione}">
                                            <fmt:formatDate pattern = "E dd/MM/yyyy" value = "${visita.dataOraErogazione}" />
                                        </td>
                                        <td>Visita specialistica</td>
                                        <td>${visita.tipoDiVisita.nome}</td>
                                        <td>€${(visita.medicoDiBase == null? 0 : visita.tipoDiVisita.ticket)},00</td>
                                    </tr>
                                </c:forEach>
                                <%-- ESAMI EROGATI --%>
                                <c:forEach var="esame" items="${esamiErogati}">
                                    <tr>
                                        <td data-order="${esame.dataOraErogazione}">
                                            <fmt:formatDate pattern = "E dd/MM/yyyy" value = "${esame.dataOraErogazione}" />
                                        </td>
                                        <td>Esame</td>
                                        <td>${esame.tipoDiEsame.nome}</td>
                                        <td>€${esame.tipoDiEsame.ticket},00</td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
        <%@include  file="/html/footer.html" %>

    </body>
</html>

