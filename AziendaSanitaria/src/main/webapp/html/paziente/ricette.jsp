<%--
    Document   : esami
    Created on : Oct 14, 2019, 4:35:54 PM
    Author     : fran
--%>

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix ="fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>




<!DOCTYPE html>
<html>
    <head>
        <title>Azienda Sanitaria - Paziente ricette</title>




        <%@include file="/html/jsCssInclude.html" %>
        <%@include file="/html/paziente/jsCssPazienteInclude.html" %>

        <script>
            <c:if test="${ricetteDaErogare.size() gt 0}">
            $(document).ready(checkPharmacies("notification", "notificationText", 1000));
            </c:if>
        </script>

        <link rel="stylesheet" type="text/css" href="/AziendaSanitaria/DataTables/datatables.min.css"/>

        <script type="text/javascript" src="/AziendaSanitaria/DataTables/datatables.min.js"></script>




        <script>
            $(document).ready(function () {

                $('#tabellaRicette').DataTable({

                    autoWidth: false,
                    "language": {
                        "sEmptyTable": "Nessun dato presente nella tabella",
                        "sInfo": "Vista da _START_ a _END_ di _TOTAL_ elementi",
                        "sInfoEmpty": "Vista da 0 a 0 di 0 elementi",
                        "sInfoFiltered": "(filtrati da _MAX_ elementi totali)",
                        "sInfoPostFix": "",
                        "sInfoThousands": ".",
                        "sLengthMenu": "Visualizza _MENU_ elementi",
                        "sLoadingRecords": "Caricamento...",
                        "sProcessing": "Elaborazione...",
                        "sSearch": "Cerca:",
                        "sZeroRecords": "La ricerca non ha portato alcun risultato.",
                        "oPaginate": {
                            "sFirst": "Inizio",
                            "sPrevious": "Precedente",
                            "sNext": "Successivo",
                            "sLast": "Fine"
                        },
                        "oAria": {
                            "sSortAscending": ": attiva per ordinare la colonna in ordine crescente",
                            "sSortDescending": ": attiva per ordinare la colonna in ordine decrescente"
                        }
                    },
                    "aaSorting": [[0, "desc"]]
                });

            });
        </script>

    </head>

    <body>
        <%@include file="/html/headers/pazienteHeader.jsp" %>

        <fmt:setLocale value="it_IT" />
        <div class="full-height">

            <div class="container p-4">
                <div class="row p-3">   
                    <a href="#" class="go-back"><i class="fas fa-arrow-left fa-sm mr-2 "></i>Torna indietro</a>
                </div>
                <div class="row mb-4">
                    <div class="col-12">
                        <h2 class="font-weight-semibold">Ricette</h2>
                    </div>

                </div>
                <div class="row">
                    <div class="table-responsive">


                        <table id="tabellaRicette" class="table table-striped dt-responsive">
                            <thead>
                                <tr>
                                    <th scope="col">Data prescrizione</th>
                                    <th scope="col">Farmaco</th>
                                    <th scope="col">Quantità</th>         
                                    <th scope="col">Data erogazione</th>
                                    <th scope="col">Vedi</th>
                                </tr>
                            </thead>

                            <tbody>
                                <c:forEach var="ricetta" items="${ricetteDaErogare}">
                                    <tr>
                                        <td data-order="${ricetta.dataOraPrescrizione}">
                                            <fmt:formatDate pattern = "E dd/MM/yyyy" value = "${ricetta.dataOraPrescrizione}" />
                                        </td>
                                        <td>${ricetta.farmaco.nome}</td>
                                        <td>${ricetta.quantita}</td>
                                        <td data-order="Non erogato">Non erogata</td>
                                        <td>
                                            <a class="btn btn-primary" href="ricette/info?idRicetta=${ricetta.id}">
                                                Vedi
                                            </a> 
                                        </td>
                                    </tr>
                                </c:forEach>
                                <c:forEach var="ricetta" items="${ricetteErogate}">
                                    <tr>
                                        <td data-order="${ricetta.dataOraPrescrizione}">
                                            <fmt:formatDate pattern = "E dd/MM/yyyy" value = "${ricetta.dataOraPrescrizione}" />
                                        </td>
                                        <td>${ricetta.farmaco.nome}</td>
                                        <td>${ricetta.quantita}</td>
                                        <td data-order="${ricetta.dataOraErogazione}">
                                            <fmt:formatDate pattern = "E dd/MM/yyyy" value = "${ricetta.dataOraErogazione}" />
                                        </td>
                                        <td>
                                            <a class="btn btn-primary" href="ricette/info?idRicetta=${ricetta.id}">
                                                Vedi
                                            </a> 
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
        <%@include  file="/html/footer.html" %>

    </body>
</html>
