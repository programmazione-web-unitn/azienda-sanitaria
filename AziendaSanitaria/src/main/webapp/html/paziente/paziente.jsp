<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix ="fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="it_IT" />

<!DOCTYPE html>
<html>
    <head>
        <title>Azienda Sanitaria - Paziente homepage</title>
        <%@include file="/html/jsCssInclude.html" %>
        <%@include file="/html/paziente/jsCssPazienteInclude.html" %>

        <link rel="stylesheet" type="text/css" href="/AziendaSanitaria/DataTables/datatables.min.css"/>

        <script type="text/javascript" src="/AziendaSanitaria/DataTables/datatables.min.js"></script>            


        <script>
            <c:if test="${ricetteDaErogare.size() gt 0}">
            $(document).ready(checkPharmacies("notification", "notificationText", 1000));
            </c:if>
            var properties = {
                autoWidth: false,
                "lengthChange": false,
                "paging": false,
                "searching": false,

                "language": {
                    "sEmptyTable": "Nessun dato presente nella tabella",
                    "sInfo": "",
                    "sInfoEmpty": "Vista da 0 a 0 di 0 elementi",
                    "sInfoFiltered": "(filtrati da _MAX_ elementi totali)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "Visualizza _MENU_ elementi",
                    "sLoadingRecords": "Caricamento...",
                    "sProcessing": "Elaborazione...",
                    "sSearch": "Cerca:",
                    "sZeroRecords": "La ricerca non ha portato alcun risultato.",
                    "oPaginate": {
                        "sFirst": "Inizio",
                        "sPrevious": "Precedente",
                        "sNext": "Successivo",
                        "sLast": "Fine"
                    },
                    "oAria": {
                        "sSortAscending": ": attiva per ordinare la colonna in ordine crescente",
                        "sSortDescending": ": attiva per ordinare la colonna in ordine decrescente"
                    }

                },
                "aaSorting": [[0, "desc"]]
            };
            $(document).ready(function () {
                for (table of ["tabellaRicette", "tabellaVisite", "tabellaEsami"]) {
                    $("#" + table).DataTable(properties);
                }
            });

        </script>
    </head>

    <body>
        <%@include file="/html/headers/pazienteHeader.jsp" %>

        <div class="full-height">
            <%@include file="/html/modalGallery.jsp" %>

            <div class="container p-4">
                <div class="row mt-5">
                    <div class="col-12 pl-4">            
                        <h2>Paziente</h2>
                    </div>
                    <div class="col-lg-4 col-md-5  col-sm-12 col-xs-12 p-4 text-center">
                        <div class="avatar-upload-wrapper">
                            <div class="avatar size-xxxl avatar-upload avatar-clickable">
                                <img src="/AziendaSanitaria/foto?idFoto=${fotoProfilo.id}"  data-toggle="modal" data-target="#gallery">
                            </div>
                            <div class="avatar-upload-icon">
                                <svg class="icon icon-lg" aria-hidden="true"><use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-camera"></use></svg>
                            </div>

                        </div>

                    </div>

                    <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12 p-4">

                        <h3 class="font-weight-semibold">
                            ${user.nome} ${user.cognome}
                        </h3>
                        <dl class="row">
                            <dt class="col-6"> Data di nascita </dt>
                            <dd class="col-6"> <fmt:formatDate pattern = "dd/MM/yyyy" value = "${user.dataNascita}" /></dd>
                            <dt class="col-6"> Luogo di nascita </dt>
                            <dd class="col-6"> ${user.luogoNascita}</dd>
                            <dt class="col-6"> Provincia di residenza</dt>
                            <dd class="col-6"> ${user.provincia.nome}, ${user.provincia.sigla}</dd>
                            <dt class="col-6"> Sesso</dt>
                            <dd class="col-6"> ${user.sesso}</dd>
                            <dt class="col-6"> Codice fiscale</dt>
                            <dd class="col-6"> ${user.codiceFiscale}</dd>
                            <dt class="col-6"> Medico di base</dt>
                            <dd class="col-6"> ${user.medicoDiBase.nome} ${user.medicoDiBase.cognome}</dd>
                        </dl>
                    </div>
                </div>
            </div>

            <div class="container">

                <div class="section p-4 mt-2">
                    <div class="section-content ">
                        <!-- contenuto di esempio START -->
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="font-weight-semibold">Esami da erogare</h2>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-12">
                                    <div class ="table-responsive" >
                                        <c:choose>
                                            <c:when test="${empty esamiDaErogare}">
                                                <span>Nessun esame da erogare</span>
                                            </c:when>
                                            <c:otherwise>
                                                <table class="table table-striped dt-responsive" id="tabellaEsami">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">Data di prescrizione</th>
                                                            <th scope="col">Tipo</th>
                                                            <th scope="col">Vedi</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        <c:forEach var="esame" items="${esamiDaErogare}">
                                                            <tr>
                                                                <td data-order="${esame.dataOraPrescrizione}">
                                                                    <fmt:formatDate pattern = "E dd/MM/yyyy" value = "${esame.dataOraPrescrizione}" />
                                                                </td>

                                                                <td>${esame.tipoDiEsame.nome}</td>
                                                                <td>
                                                                    <a class="btn btn-primary" href="esami/info?idEsame=${esame.id}">
                                                                        Vedi
                                                                    </a> 
                                                                </td>
                                                            </tr>

                                                        </c:forEach>
                                                    </tbody>
                                                </table>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3 col-12">
                                <a href="/AziendaSanitaria/Paziente/esami">
                                    Visualizza tutti gli esami 
                                    <i class="fas fa-arrow-right fa-sm ml-2"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="section p-4 mt-2">
                    <div class="section-content ">
                        <!-- contenuto di esempio START -->
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="font-weight-semibold">Visite specialistiche da erogare</h2>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class ="table-responsive" >

                                        <c:choose>
                                            <c:when test="${empty visiteSpecDaErogare}">
                                                <span>Nessuna visita specialistica da erogare</span>
                                            </c:when>
                                            <c:otherwise>
                                                <table class="table table-striped dt-responsive" id="tabellaVisite">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">Data di prescrizione</th>
                                                            <th scope="col">Tipo</th>
                                                            <th scope="col">Richiamo</th>
                                                            <th scope="col">Vedi</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>

                                                        <c:forEach var="visitaSpec" items="${visiteSpecDaErogare}">
                                                            <tr>
                                                                <td data-order="${visitaSpec.dataOraPrescrizione}">
                                                                    <fmt:formatDate pattern = "E dd/MM/yyyy" value = "${visitaSpec.dataOraPrescrizione}" />
                                                                </td>
                                                                <td>${visitaSpec.tipoDiVisita.nome}</td>
                                                                <td>
                                                                    <svg class="icon align-bottom">
                                                                    <c:choose>
                                                                        <c:when test="${visitaSpec.medicoDiBase == null}">
                                                                            <use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-check"></use>
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-close"></use>
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                    </svg>
                                                                </td>
                                                                <td>
                                                                    <a class="btn btn-primary" href="visite/specialistiche/info?idVisita=${visitaSpec.id}">
                                                                        Vedi
                                                                    </a> 
                                                                </td>
                                                            </tr>

                                                        </c:forEach>
                                                    </tbody>
                                                </table>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>

                                </div>
                            </div>
                            <div class="row mt-3 col-12">
                                <a href="/AziendaSanitaria/Paziente/visite/specialistiche">
                                    Visualizza tutte le visite specialistiche
                                    <i class="fas fa-arrow-right fa-sm ml-2"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="section p-4 mt-2 mb-5">
                    <div class="section-content ">
                        <!-- contenuto di esempio START -->
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="font-weight-semibold">Ricette da erogare</h2>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class ="table-responsive" >

                                        <c:choose>
                                            <c:when test="${empty ricetteDaErogare}">
                                                <span>Nessuna ricetta da erogare</span>
                                            </c:when>
                                            <c:otherwise>
                                                <table class="table table-striped dt-responsive"id="tabellaRicette">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">Data prescrizione</th>
                                                            <th scope="col">Farmaco</th>
                                                            <th scope="col">Quantità</th>
                                                            <th scope="col">Vedi</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        <c:forEach var="ricetta" items="${ricetteDaErogare}">
                                                            <tr>
                                                                <td data-order="${ricetta.dataOraPrescrizione}">
                                                                    <fmt:formatDate pattern = "E dd/MM/yyyy" value = "${ricetta.dataOraPrescrizione}" />
                                                                </td>
                                                                <td>${ricetta.farmaco.nome}</td>
                                                                <td>${ricetta.quantita}</td>
                                                                <td>
                                                                    <a class="btn btn-primary" href="ricette/info?idRicetta=${ricetta.id}">
                                                                        Vedi
                                                                    </a> 
                                                                </td>
                                                            </tr>

                                                        </c:forEach>
                                                    </tbody>
                                                </table>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>

                                </div>
                            </div>
                            <div class="row mt-3 col-12">
                                <a href="/AziendaSanitaria/Paziente/ricette">
                                    Visualizza tutte le ricette
                                    <i class="fas fa-arrow-right fa-sm ml-2"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <%@include  file="/html/footer.html" %>
    </body>

</html>
