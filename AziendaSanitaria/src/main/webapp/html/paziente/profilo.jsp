<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix ="fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="it_IT" />
<!DOCTYPE html>
<html>
    <head>
        <title>Azienda Sanitaria - Paziente profilo</title>
        <%@include file="/html/jsCssInclude.html" %>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

        <script>
            $(document).ready(function () {
                $("#idMedico").select2({
                    placeholder: "Inserisci nome, cognome",
                    allowClear: true,
                    ajax: {
                        url: function (request) {
                            return "/AziendaSanitaria/services/listaMedici/" + (request.term || "");
                        },
                        dataType: "json"
                    }
                });


                $("#uploadedImage").on('change', function () {
                    var file = this.files[0];
                    var fileType = file["type"];
                    console.log(fileType);
                    var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
                    $("#uploadImageName").html($(this).get(0).files.item(0).name);
                    $("#uploadImageDefault").addClass('d-none');
                    $("#uploadImagePreview").removeClass('d-none');
                    if (validImageTypes.includes(fileType) && file.size <= 2000000) {
                        $("[data-error-for='uploadedImage']").removeClass("text-danger").addClass("text-success");
                        $("#uploadImageSubmit").removeClass('d-none');
                    } else {
                        $("[data-error-for='uploadedImage']").removeClass("text-success").addClass("text-danger");
                        $("#uploadImageSubmit").addClass('d-none');
                    }
                    readURL(this, 'uploadImagePreview');
                });
            });
        </script>



    </head>

    <body>
        <%@include file="/html/headers/pazienteHeader.jsp" %>
        <div class="full-height">
            <%@include file="/html/modalGallery.jsp" %>

            <div class="container p-4">
                <div class="row p-3">   
                    <a href="#" class="go-back"><i class="fas fa-arrow-left fa-sm mr-2 "></i>Torna indietro</a>
                </div>
                <div class="row">
                    <div class="col-12">
                        <%@include file="/html/errorSuccess.jsp" %>
                    </div>
                    <div class="col-lg-4 col-md-5  col-sm-12 col-xs-12 pt-4 px-4 text-center">
                        <div class="avatar-upload-wrapper">
                            <div class="avatar size-xxxl avatar-upload avatar-clickable">
                                <img src="/AziendaSanitaria/foto?idFoto=${fotoProfilo.id}"  data-toggle="modal" data-target="#gallery">
                            </div>
                            <div class="avatar-upload-icon">
                                <svg class="icon icon-lg" aria-hidden="true"><use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-camera"></use></svg>
                            </div>

                        </div>
                        <div class="upload-file-list upload-file-list-image" id="uploadImageInfo">
                            <form  method="post" action="UploadServlet" enctype="multipart/form-data">
                                <input type="file" name="uploadedImage" id="uploadedImage" class="d-none" accept=".png,.jpg,.jpeg"/>
                                <div class="upload-file ">
                                    <label for="uploadedImage" id="uploadImagePreview" class="upload-image d-none">
                                        <img src="" >
                                    </label>
                                    <label for="uploadedImage" id="uploadImageDefault" class="">
                                        <svg class="icon" aria-hidden="true"><use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-file"></use></svg>
                                    </label>

                                    <label for="uploadedImage" id="uploadImageName" class="">Carica immagine...</label>
                                    <button type="submit" id="uploadImageSubmit" class="d-none">
                                        <svg class="icon" aria-hidden="true">
                                        <use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-upload"></use>
                                        </svg>
                                    </button>
                                </div>
                                <small data-error-for="uploadedImage" class="form-text text-secondary">
                                    Seleziona un'immagine [max 2MB]
                                </small>
                            </form>
                        </div>
                    </div>

                    <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12 p-4">

                        <h3 class="font-weight-semibold">
                            ${user.nome} ${user.cognome}
                        </h3>
                        <dl class="row">
                            <dt class="col-6"> Data di nascita </dt>
                            <dd class="col-6"> <fmt:formatDate pattern = "dd/MM/yyyy" value = "${user.dataNascita}" /></dd>
                            <dt class="col-6"> Luogo di nascita </dt>
                            <dd class="col-6"> ${user.luogoNascita}</dd>
                            <dt class="col-6"> Provincia di residenza</dt>
                            <dd class="col-6"> ${user.provincia.nome}, ${user.provincia.sigla}</dd>
                            <dt class="col-6"> Sesso</dt>
                            <dd class="col-6"> ${user.sesso}</dd>
                            <dt class="col-6"> Codice fiscale</dt>
                            <dd class="col-6"> ${user.codiceFiscale}</dd>
                            <dt class="col-6"> Medico di base</dt>
                            <dd class="col-6"> ${user.medicoDiBase.nome} ${user.medicoDiBase.cognome}</dd>


                        </dl>
                    </div>

                </div>

                <div class="row">
                    <div class="col-12 p-4">

                        <h2 class="font-weight-semibold">
                            Impostazioni
                        </h2>

                        <div class="row mt-5 mb-5">

                            <div class="col-lg-6 col-md-12  p-3">
                                <h3 class="font-weight-semibold">Modifica Password</h3><br/>
                                <form action="CambiaPassword" method="POST" novalidate>
                                    <%@ include file="/html/cambiaPasswordForm.jsp" %>
                                </form>
                            </div>

                            <div class=" col-lg-6 col-md-12  p-3">
                                <form class="form-signin" action="CambiaMedico" method="POST">
                                    <h3 class="font-weight-semibold">Modifica medico di base</h3><br/>
                                    <div class="form-label-group">

                                        <label for="idMedico">Ricerca medico</label>
                                        <select id="idMedico" name="idMedico" class="form-control select2-allow-clear simple-select2 w-100" >
                                        </select>
                                        <small data-error-for="idMedico" class="form-text text-secondary">
                                            Seleziona un medico di base
                                        </small>
                                    </div>
                                    <div class="modal-footer d-flex justify-content-center">
                                        <input type="submit" name ="Submit" value="Conferma" class="btn btn-primary" disabled>
                                    </div>

                                </form>
                            </div>


                        </div>



                    </div>
                </div>


            </div>
        </div>
        <%@include  file="/html/footer.html" %>
    </body>

</html>
