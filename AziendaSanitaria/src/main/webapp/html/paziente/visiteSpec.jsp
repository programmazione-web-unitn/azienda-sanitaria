<%--
    Document   : esami
    Created on : Oct 14, 2019, 4:35:54 PM
    Author     : fran
--%>

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix ="fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>



<!DOCTYPE html>
<html>
    <head>
        <title>Azienda Sanitaria - Paziente visite specialistiche</title>




        <%@include file="/html/jsCssInclude.html" %>
        <%@include file="/html/paziente/jsCssPazienteInclude.html" %>

        <script>
            <c:if test="${ricetteDaErogare.size() gt 0}">
            $(document).ready(checkPharmacies("notification", "notificationText", 1000));
            </c:if>
        </script>

        <link rel="stylesheet" type="text/css" href="/AziendaSanitaria/DataTables/datatables.min.css"/>

        <script type="text/javascript" src="/AziendaSanitaria/DataTables/datatables.min.js"></script>

        <script>
            $(document).ready(function () {

                $('#tabellaVisiteSpec').DataTable({

                    autoWidth: false,
                    "language": {
                        "sEmptyTable": "Nessun dato presente nella tabella",
                        "sInfo": "Vista da _START_ a _END_ di _TOTAL_ elementi",
                        "sInfoEmpty": "Vista da 0 a 0 di 0 elementi",
                        "sInfoFiltered": "(filtrati da _MAX_ elementi totali)",
                        "sInfoPostFix": "",
                        "sInfoThousands": ".",
                        "sLengthMenu": "Visualizza _MENU_ elementi",
                        "sLoadingRecords": "Caricamento...",
                        "sProcessing": "Elaborazione...",
                        "sSearch": "Cerca:",
                        "sZeroRecords": "La ricerca non ha portato alcun risultato.",
                        "oPaginate": {
                            "sFirst": "Inizio",
                            "sPrevious": "Precedente",
                            "sNext": "Successivo",
                            "sLast": "Fine"
                        },
                        "oAria": {
                            "sSortAscending": ": attiva per ordinare la colonna in ordine crescente",
                            "sSortDescending": ": attiva per ordinare la colonna in ordine decrescente"
                        }

                    },
                    "aaSorting": [[0, "desc"]]
                });

            });
        </script>

    </head>

    <body>
        <%@include file="/html/headers/pazienteHeader.jsp" %>

        <fmt:setLocale value="it_IT" />
        <div class="full-height">

            <div class="container p-4">
                <div class="row p-3">   
                    <a href="#" class="go-back"><i class="fas fa-arrow-left fa-sm mr-2 "></i>Torna indietro</a>
                </div>
                <div class="row mb-4">
                    <div class="col-12">
                        <h2 class="font-weight-semibold">Visite specialistiche</h2>
                    </div>

                </div>
                <div class="row">
                    <div class="table-responsive">


                        <table id="tabellaVisiteSpec" class="table table-striped dt-responsive">
                            <thead>
                                <tr>
                                    <th scope="col">Data di prescrizione</th>
                                    <th scope="col">Tipo</th>
                                    <th scope="col">Data erogazione</th>
                                    <th scope="col">Richiamo</th>
                                    <th scope="col">Vedi</th>

                                </tr>
                            </thead>

                            <tbody>
                                <c:forEach var="visita" items="${visiteSpecDaErogare}">
                                    <tr>
                                        <td data-order="${visita.dataOraPrescrizione}">
                                            <fmt:formatDate pattern = "E dd/MM/yyyy" value = "${visita.dataOraPrescrizione}" />
                                        </td>

                                        <td >${visita.tipoDiVisita.nome}</td>
                                        <td data-order="Non erogato">Non erogata</td>
                                        <td>
                                            <svg class="icon align-bottom">
                                            <c:choose>
                                                <c:when test="${visita.medicoDiBase == null}">
                                                    <use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-check"></use>
                                                </c:when>
                                                <c:otherwise>
                                                    <use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-close"></use>
                                                </c:otherwise>
                                            </c:choose>
                                            </svg>
                                        </td>
                                        <td>
                                            <a class="btn btn-primary" href="specialistiche/info?idVisita=${visita.id}">
                                                Vedi
                                            </a> 
                                        </td>
                                    </tr>

                                </c:forEach>
                                <c:forEach var="visita" items="${visiteSpecErogate}">
                                    <tr>
                                        <td data-order="${visita.dataOraPrescrizione}">
                                            <fmt:formatDate pattern = "E dd/MM/yyyy" value = "${visita.dataOraPrescrizione}" />
                                        </td>

                                        <td>${visita.tipoDiVisita.nome}</td>

                                        <td data-order="${visita.dataOraErogazione}">
                                            <fmt:formatDate pattern = "E dd/MM/yyyy" value = "${visita.dataOraErogazione}" />
                                        </td>
                                        <td>
                                            <svg class="icon align-bottom">
                                            <c:choose>
                                                <c:when test="${visita.medicoDiBase == null}">
                                                    <use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-check"></use>
                                                </c:when>
                                                <c:otherwise>
                                                    <use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-close"></use>
                                                </c:otherwise>
                                            </c:choose>
                                            </svg>
                                        </td>
                                        <td>
                                            <a class="btn btn-primary" href="specialistiche/info?idVisita=${visita.id}">
                                                Vedi
                                            </a> 
                                        </td>
                                    </tr>

                                </c:forEach>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>

        <%@include  file="/html/footer.html" %>

    </body>
</html>
