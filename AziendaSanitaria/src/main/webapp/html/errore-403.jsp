<%@page contentType="text/html" pageEncoding="UTF-8" isErrorPage="true" %>
<!DOCTYPE html>
<html>

    <head>  
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Azienda Sanitaria - Errore 403</title>  
        <%@include file="/html/jsCssInclude.html" %>
        <%@include file="/html/headers/loginHeader.jsp" %>
    </head>
    <body>        

        <div class="container">
            <div class="row">
                <div class="col-md-4 offset-md-4 p-4 text-center">
                    <h2 class="font-weight-semibold">Errore 403</h2>
                    <h5>Accesso non consentito</h5>
                    <p>Ci dispiace. Non hai l'autorizzazione per accedere a questa pagina.</p>
                    <br/>
                    <a href="/AziendaSanitaria">
                        <i class="fas fa-arrow-left fa-sm mr-2 "></i>
                        Torna alla homepage
                    </a>
                </div>
            </div>            
        </div>

    </body>
</html>