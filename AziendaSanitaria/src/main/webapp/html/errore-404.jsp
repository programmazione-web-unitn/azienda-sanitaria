<%-- 
    Document   : errore-404
    Created on : Oct 30, 2019, 3:11:53 PM
    Author     : davide
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" isErrorPage="true" %>
<!DOCTYPE html>
<html>

    <head>  
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Azienda Sanitaria - Errore 404</title>  
        <%@include file="/html/jsCssInclude.html" %>
        <%@include file="/html/headers/loginHeader.jsp" %>
    </head>
    <body>        

        <div class="container">
            <div class="row">
                <div class="col-md-4 offset-md-4 p-4 text-center">
                    <h2 class="font-weight-semibold">Errore 404</h2>
                    <h5>Pagina non disponibile</h5>
                    <p>Ci dispiace. La pagina richiesta non è disponibile sui nostri server.</p>
                    <br/>
                    <a href="/AziendaSanitaria">
                        <i class="fas fa-arrow-left fa-sm mr-2 "></i>
                        Torna alla homepage
                    </a>
                </div>
            </div>            
        </div>

    </body>
</html>
