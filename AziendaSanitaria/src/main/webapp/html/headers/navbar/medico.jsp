<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="mb" value = "MB"></c:set>
<c:set var="ms" value = "MS"></c:set>

<li class="nav-item">
    <a class="nav-link" href="/AziendaSanitaria/Paziente/home">
        <span>Profilo paziente</span>
    </a>
</li>
    
<c:if test="${user.tipo eq mb}">
<li class="nav-item">
    <a class="nav-link" href="/AziendaSanitaria/Medico/home">
        <span>Profilo medico</span>
    </a>
</li>
</c:if>
<c:if test="${user.tipo eq ms}">
<li class="nav-item">
    <a class="nav-link" href="/AziendaSanitaria/Specialista/home">
        <span>Profilo specialista</span>
    </a>
</li>
</c:if>
<li class="nav-item">
    <a class="nav-link" href="/AziendaSanitaria/Paziente/profilo">
        <span>Modifica</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link text-danger" href="/AziendaSanitaria/Logout">
        <span>Logout</span>
    </a>
</li>