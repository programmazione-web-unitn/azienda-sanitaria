<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="mb" value = "MB"></c:set>
<c:set var="ms" value = "MS"></c:set>

<li class="nav-item">
    <a class="nav-link" href="/AziendaSanitaria/Ssn/home">
        <span>Home</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link" href="/AziendaSanitaria/Ssn/profilo">
        <span>Modifica</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link text-danger" href="/AziendaSanitaria/Logout">
        <span>Logout</span>
    </a>
</li>