<%@page import="it.unitn.disi.as.aziendasanitaria.utilities.Authentication" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<header>
    <div class="it-header-wrapper it-header-sticky">
        <div class="it-nav-wrapper theme-light">


            <div class="it-header-center-wrapper theme-light" id="homepageNavbar">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="it-header-center-content-wrapper">
                                <div class="it-brand-wrapper">
                                    <a href="/AziendaSanitaria/">
                                        <!-- <svg class="icon">
                                          <use xlink:href="svg/sprite.svg#it-code-circle"></use>
                                        </svg> -->
                                        <img src="/AziendaSanitaria/images/logo/icon.png" class="icon" alt="">
                                        <div class="it-brand-text">
                                            <h2 class="no_toc">Azienda Sanitaria</h2>
                                            <h3 class="no_toc d-none d-md-block">Servizi online</h3>
                                        </div>
                                    </a>
                                </div>
                                <div class="it-right-zone">

                                    <c:choose>
                                        <c:when test="${user != null}">
                                            <c:choose>
                                                <c:when test="${user.tipo eq 'SSP'}">
                                                    <%@include file="/html/headers/dropdown/ssp.jsp" %>
                                                </c:when>
                                                <c:when test="${user.tipo eq 'SSN'}">
                                                    <%@include file="/html/headers/dropdown/ssn.jsp" %>
                                                </c:when>
                                                <c:when test="${user.tipo eq 'F'}">
                                                    <%@include file="/html/headers/dropdown/farmacia.jsp" %>
                                                </c:when>
                                                <c:otherwise>
                                                    <%@include file="/html/headers/dropdown/paziente.jsp" %>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:when>
                                        <c:otherwise>
                                            <a href="/AziendaSanitaria/login" class="btn btn-primary btn-icon btn-full" id="homepageLoginButton">
                                                <span class="rounded-icon">
                                                    <svg class="icon icon-primary">
                                                    <use
                                                        xlink:href="svg/sprite.svg#it-user"
                                                        ></use>
                                                    </svg>
                                                </span>
                                                <span class="d-none d-sm-block">Accedi all'area personale</span>
                                            </a>
                                        </c:otherwise>
                                    </c:choose>


                                    <%-- <a href="#" class="btn btn-primary btn-icon btn-full">
                                      <span class="rounded-icon">
                                        <svg class="icon icon-primary">
                                          <use
                                            xlink:href="svg/sprite.svg#it-user"
                                          ></use>
                                        </svg>
                                      </span>
                                      <span class="d-none d-lg-block">Accedi all'area personale</span>
                                    </a> --%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <c:choose>
                <c:when test="${user != null}">
                    <div class="it-header-navbar-wrapper theme-light-desk d-md-none">
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <!--start nav-->
                                    <nav class="navbar navbar-expand-lg">
                                        <button class="custom-navbar-toggler" type="button" aria-controls="nav02" aria-expanded="false" aria-label="Toggle navigation" data-target="#nav02">
                                            <svg class="icon">
                                            <use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-burger"></use>
                                            </svg>
                                        </button>
                                        <div class="navbar-collapsable" id="nav02" style="display: none;">
                                            <div class="overlay" style="display: none;"></div>
                                            <div class="close-div sr-only">
                                                <button class="btn close-menu" type="button"><span class="it-close"></span>close</button>
                                            </div>
                                            <div class="menu-wrapper">
                                                <ul class="navbar-nav">
                                                    <c:choose>
                                                        <c:when test="${user.tipo eq 'P'}">
                                                            <%@include file="/html/headers/navbar/paziente.jsp" %>
                                                        </c:when>
                                                        <c:when test="${user.tipo eq 'MB'}">
                                                            <%@include file="/html/headers/navbar/medico.jsp" %>
                                                        </c:when>
                                                        <c:when test="${user.tipo eq 'MS'}">
                                                            <%@include file="/html/headers/navbar/specialista.jsp" %>
                                                        </c:when>
                                                        <c:when test="${user.tipo eq 'SSP'}">
                                                            <%@include file="/html/headers/navbar/ssp.jsp" %>
                                                        </c:when>
                                                        <c:when test="${user.tipo eq 'SSN'}">
                                                            <%@include file="/html/headers/navbar/ssn.jsp" %>
                                                        </c:when>
                                                        <c:when test="${user.tipo eq 'F'}">
                                                            <%@include file="/html/headers/navbar/farmacia.jsp" %>
                                                        </c:when>
                                                    </c:choose>
                                                    
                                                </ul>
                                            </div>      
                                        </div>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>

                </c:otherwise>
            </c:choose>



        </div>
    </div>

</header>

