<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="mb" value = "MB"></c:set>
<c:set var="ms" value = "MS"></c:set>
<c:set var = "pageName" value = "${requestScope['javax.servlet.forward.servlet_path']}"/>

<div class="btn-group d-none d-md-block">
    <button type="button" class="btn btn-primary dropdown-toggle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="rounded-icon mr-2">
            <div id="id-avatar" class="avatar size-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img src="/AziendaSanitaria/foto?fotoProfilo">
            </div>
        </span>
        <div class="mr-2"> Profilo </div>
        <svg class="icon-expand icon icon-sm ${fn:endsWith(pageContext.request.requestURI, '/AziendaSanitaria/') or fn:endsWith(pageContext.request.requestURI, '/AziendaSanitaria/index.jsp') or fn:endsWith(pageContext.request.requestURI, '/AziendaSanitaria/privacy') or fn:endsWith(pageContext.request.requestURI, '/AziendaSanitaria/cookies') ? 'icon-light' : 'icon-primary' }">
            <use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-expand"></use>
        </svg>
    </button>
    <div class="dropdown-menu">
        <div class="link-list-wrapper">
            <ul class="link-list nowrap">
                <li>
                    <a class="list-item large" style="white-space: nowrap" href="/AziendaSanitaria/Paziente/home">
                        <span>
                            <i class="fas fa-exchange-alt mr-2"></i>
                            Profilo paziente
                        </span>
                    </a>
                </li>
                <c:if test="${user.tipo eq mb}">
                    <li> <a class="list-item large" style="white-space: nowrap" href="/AziendaSanitaria/Medico/home"><span>
                                <i class="fas fa-exchange-alt mr-2"></i>
                                Profilo medico</span></a></li>
                            </c:if>
                            <c:if test="${user.tipo eq ms}">
                    <li> <a class="list-item large" style="white-space: nowrap" href="/AziendaSanitaria/Specialista/home"><span>
                                <i class="fas fa-exchange-alt mr-2"></i>
                                Profilo specialista</span></a></li>
                            </c:if>

                <li>
                    <a class="list-item large" href="/AziendaSanitaria/Paziente/profilo">
                        <span>
                            <i class="far fa-edit mr-2"></i>
                            Modifica
                        </span>
                    </a>
                </li>

                <li>
                    <span class="divider"></span>
                </li>

                <li>
                    <a class="list-item large" href="/AziendaSanitaria/Logout">
                        <span class="text-danger">
                            <i class="fas fa-sign-out-alt mr-2"></i>
                            Logout
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>