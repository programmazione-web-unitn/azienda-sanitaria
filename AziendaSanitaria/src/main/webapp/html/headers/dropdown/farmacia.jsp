<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var = "pageName" value = "${requestScope['javax.servlet.forward.servlet_path']}"/>

<div class="btn-group d-none d-md-block">
    <button type="button" class="btn btn-primary dropdown-toggle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="rounded-icon">
            <div id="id-avatar" class="avatar size-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img src="/AziendaSanitaria/images/features/doctoricon.jpg">
            </div>
        </span>
        <div class="mr-2"> <span> Profilo </span> </div>
        <svg class="icon-expand icon icon-sm ${fn:endsWith(pageContext.request.requestURI, '/AziendaSanitaria/') or fn:endsWith(pageContext.request.requestURI, '/AziendaSanitaria/index.jsp') or fn:endsWith(pageContext.request.requestURI, '/AziendaSanitaria/privacy') or fn:endsWith(pageContext.request.requestURI, '/AziendaSanitaria/cookies') ? 'icon-light' : 'icon-primary' }">
            <use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-expand"></use>
        </svg>
    </button>
    <div class="dropdown-menu">
        <div class="link-list-wrapper">
            <ul class="link-list nowrap">
                <c:if test="${fn:endsWith(pageContext.request.requestURI, '/AziendaSanitaria/') or fn:endsWith(pageContext.request.requestURI, '/AziendaSanitaria/index.jsp') or fn:endsWith(pageContext.request.requestURI, '/AziendaSanitaria/privacy') or fn:endsWith(pageContext.request.requestURI, '/AziendaSanitaria/cookies')}">
                    <li>
                        <a class="list-item large" style="white-space: nowrap" href="/AziendaSanitaria/Farmacia/home">
                            <span>
                                    <i class="fas fa-home mr-2"></i>
                                    Home
                            </span>
                        </a>
                    </li>
                </c:if>
                    
                <li> <a class="list-item large no-wrap" style="white-space: nowrap" href="/AziendaSanitaria/Farmacia/profilo"><span>
                            <i class="fas fa-edit mr-2"></i>
                            Modifica
                        </span></a></li>
                <li>
                    <span class="divider"></span>
                </li>
                <li><a class="list-item large" style="white-space: nowrap" href="/AziendaSanitaria/Logout">
                        <span class="text-danger">
                            <i class="fas fa-sign-out-alt mr-2"></i>
                            Logout
                        </span>
                    </a></li>
            </ul>
        </div>
    </div>
</div>
