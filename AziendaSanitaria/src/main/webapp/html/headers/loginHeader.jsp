<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<header>
    <div class="it-header-wrapper it-header-sticky">

    <div class="it-header-center-wrapper theme-light" id="homepageNavbar">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="it-header-center-content-wrapper">
              <div class="it-brand-wrapper">
                <a href="/AziendaSanitaria/">
                  <!-- <svg class="icon">
                    <use xlink:href="svg/sprite.svg#it-code-circle"></use>
                  </svg> -->
                  <img src="/AziendaSanitaria/images/logo/icon.png" class="icon" alt="">
                  <div class="it-brand-text">
                    <h2 class="no_toc">Azienda Sanitaria</h2>
                    <h3 class="no_toc d-none d-md-block">Servizi online</h3>
                  </div>
                </a>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>

</div>

</header>
