<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<header>

    <div class="it-header-wrapper it-header-sticky">
        <div class="it-nav-wrapper">
            <div class="it-header-center-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="it-header-center-content-wrapper">
                                <div class="it-brand-wrapper">
                                    <a href="/AziendaSanitaria/">
                                        <!-- <svg class="icon">
                                          <use xlink:href="svg/sprite.svg#it-code-circle"></use>
                                        </svg> -->
                                        <img src="/AziendaSanitaria/images/logo/icon.png" class="icon" alt="">
                                        <div class="it-brand-text">
                                            <h2 class="no_toc">Azienda Sanitaria</h2>
                                            <h3 class="no_toc d-none d-md-block">Servizi online</h3>
                                        </div>
                                    </a>
                                </div>
                                <div class="it-right-zone bg-dark d-none d-md-block">
                                    <%@include file="/html/headers/dropdown/farmacia.jsp" %> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="it-header-navbar-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <!--start nav-->
                            <nav class="navbar navbar-expand-lg">
                                <button class="custom-navbar-toggler" type="button" aria-controls="nav02" aria-expanded="false" aria-label="Toggle navigation" data-target="#nav02">
                                    <svg class="icon">
                                    <use xlink:href="/AziendaSanitaria/svg/sprite.svg#it-burger"></use>
                                    </svg>
                                </button>
                                <div class="navbar-collapsable" id="nav02" style="display: none;">
                                    <div class="overlay" style="display: none;"></div>
                                    <div class="close-div sr-only">
                                        <button class="btn close-menu" type="button"><span class="it-close"></span>close</button>
                                    </div>
                                    <div class="menu-wrapper">

                                        <c:set var = "pageName" value = "${requestScope['javax.servlet.forward.servlet_path']}"/>
                                        <ul class="navbar-nav">
                                            <li class="nav-item">
                                                <a class="nav-link ${fn:contains(pageContext.request.requestURI, '/home') ? 'active' : '' }" href="/AziendaSanitaria/Farmacia/home">
                                                    <span>Homepage</span>
                                                </a>
                                            </li>

                                            <li class="nav-item">
                                                <a class="nav-link ${fn:contains(pageContext.request.requestURI, '/paziente') ? 'active' : 'disabled' }" href="">
                                                    <span>Ricette</span>
                                                </a>
                                            </li>
                                            <li class="nav-item d-md-none">
                                                <a class="nav-link" href="/AziendaSanitaria/Farmacia/profilo">
                                                    <span>Modifica</span>
                                                </a>
                                            </li>
                                             <li class="nav-item d-md-none">
                                                <a class="nav-link text-danger" href="/AziendaSanitaria/Logout">
                                                    <span>Logout</span>
                                                </a>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
