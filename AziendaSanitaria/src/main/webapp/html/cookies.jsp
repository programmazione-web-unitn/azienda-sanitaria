<%-- 
    Document   : cookies
    Created on : Dec 3, 2019, 9:35:02 PM
    Author     : giovanni
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Azienda Sanitaria - Cookies</title>
        <%@include file="/html/jsCssInclude.html" %>
    </head>
    <body>
        <%@include file="/html/headers/homepageHeader.jsp" %>
        <div class="container p-4 mb-5" >
            <div class="row p-3">   
                <a href="#" class="go-back"><i class="fas fa-arrow-left fa-sm mr-2 "></i>Torna indietro</a>
            </div>

            <div class="row mt-3">
                <div class="col-12">
                    <h2>Informazioni da fornire sull’uso dei cookies</h2>
                    <p class="text-justify"> L’utilizzo di questo sito Web (e di ogni altro sito gestito da noi) comporta l’accettazione, da parte dell’utente, del ricorso ai cookies (e tecnologie analoghe) conformemente alla presente informativa. In particolare, si accetta l’uso dei cookies tecnici e di funzionalità per gli scopi di seguito descritti.<br />
                        &nbsp;</p>
                    <h2 class="font-weight-semibold">Cosa sono i cookies</h2>
                    <p class="text-justify">I cookies sono informazioni salvate sul disco fisso del terminale dell’utente e che sono inviate dal browser dell’utente a un Web server e che si riferiscono all’utilizzo della rete. Di conseguenza, permettono di conoscere i servizi, i siti frequentati e le opzioni che, navigando in rete, sono state manifestate. In altri termini, i cookies sono piccoli file, contenenti lettere e numeri, che vengono scaricati sul computer o dispositivo mobile dell’utente quando si visita un sito Web. I cookies vengono poi re-inviati al sito originario a ogni visita successiva, o a un altro sito Web che riconosce questi cookies. I cookies sono utili poiché consentono a un sito Web di riconoscere il dispositivo dell’utente.</p>
                    <p class="text-justify">Queste informazioni non sono, quindi, fornite spontaneamente e direttamente, ma lasciano traccia della navigazione in rete da parte dell&#8217;utente.</p>
                    <p class="text-justify">I cookies svolgono diverse funzioni e consentono di navigare tra le pagine in modo efficiente, ricordando le preferenze dell’utente e, più in generale, migliorando la sua esperienza. Possono anche contribuire a garantire che le pubblicità mostrate durante la navigazione siano di suo interesse e che le attività di marketing realizzate siano conformi alle sue preferenze, evitando iniziative promozionali sgradite o non in linea con le esigenze dell’utente.</p>
                    <p class="text-justify">Durante la navigazione su di un sito, l’utente può ricevere sul suo terminale anche cookies che sono inviati da siti o da web server diversi (c.d. “terze parti”), sui quali possono risiedere alcuni elementi (es.: immagini, mappe, suoni, specifici link a pagine di altri domini) presenti sul sito che lo stesso sta visitando. Possono esserci, quindi, sotto questo profilo:</p>
                    <ol>
                        <li><strong>Cookies diretti</strong>, inviati direttamente da questo sito al dispositivo dell’utente</li>
                        <li><strong>Cookies di terze parti</strong>, provenienti da una terza parte ma inviati per nostro conto. Il presente sito usa i cookies di terze parti per agevolare l’analisi del nostro sito Web e del suo utilizzo (come descritto di seguito).</li>
                    </ol>
                    <p class="text-justify">Inoltre, in funzione della finalità di utilizzazione dei cookies, questi si possono distinguere in:</p>
                    <ol>
                        <li><strong>Cookies tecnici</strong>: sono utilizzati per permettere la trasmissione di una comunicazione su una rete di comunicazione elettronica o per erogare un servizio espressamente richiesto dall’utente. Non sono utilizzati per altre finalità. Nella categoria dei cookies tecnici, la cui utilizzazione non richiede il consenso dell’utente, si distinguono:
                            <ol>
                                <li><strong>Cookies di navigazione o di sessione: </strong>garantiscono la normale navigazione e fruizione del sito Web (es.: per autenticarsi ed accedere alle aree riservate, per effettuare acquisti). L’uso questi cookies (che non sono memorizzati in modo persistente sul dispositivo dell’utente e sono automaticamente eliminati con la chiusura del browser) è strettamente limitato alla trasmissione d’identificativi di sessione (costituiti da numeri casuali generati dal server) necessari per consentire l’esplorazione sicura ed efficiente del sito. Detti cookies possono evitare il ricorso a tecniche informatiche potenzialmente pregiudizievoli per la riservatezza della navigazione degli utenti e non consentono l’acquisizione di dati personali identificativi dell’utente</li>
                                <li><strong>Cookies analytics:</strong> sono utilizzati soltanto per raccogliere informazioni in forma aggregata, sul numero di utenti che visitano il sito e come lo visitano</li>
                                <li><strong>Cookies di funzionalità:</strong> permettono all’utente dii navigare in base a una serie di criteri selezionati (es.: lingua o servizi cui si è aderito, prodotti selezionati per l’acquisto) per migliorare il servizio reso all’utente.</li>
                            </ol>
                        </li>
                        <li><strong>Cookies di profilazione</strong>: sono volti a creare profili relativi all’utente e utilizzati per inviare messaggi promozionali mirati in funzione delle preferenze manifestate durante la sua navigazione in rete. Questi cookies sono utilizzabili per tali scopi soltanto con il consenso dell’utente.</li>
                    </ol>
                    <p class="text-justify">&nbsp;</p>
                    <h2 class="font-weight-semibold">Come sono utilizzati i cookies da questo sito</h2>
                    <p class="text-justify">La tabella seguente riassume il modo in cui questo sito e le terzi parti utilizzano i cookies. Questi usi comprendono il ricorso ai cookies per:</p>
                    <ol>
                        <li>conoscere il numero totale di visitatori in modo continuativo oltre ai tipi di browser (ad es. Firefox, Safari o Internet Explorer / Microsoft Edge) e sistemi operativi (ad es. Windows o Macintosh) utilizzati;</li>
                        <li>monitorare le prestazioni del sito, incluso il modo in cui i visitatori lo utilizzano, nonché per migliorarne le funzionalità.</li>
                    </ol>
                    <p class="text-justify">&nbsp;</p>
                    <h2 class="font-weight-semibold">Quali categorie di cookies utilizziamo e come possono essere gestiti dall’utente</h2>
                    <p class="text-justify">I tipi di cookies utilizzati possono essere classificati in una delle categorie, di seguito riportate nella tabella seguente, che permette all’utente di prestare il proprio consenso o modificarlo (se precedentemente già espresso) per accettare i vari tipi di cookies:<br />
                        &nbsp;</p>
                    <p class="text-justify">&nbsp;</p>
                    <h2 class="font-weight-semibold">Come controllare o eliminare i cookies</h2>
                    <p class="text-justify">Nel ricordare che il presente sito utilizza soltanto cookies tecnici come sopra identificati, scegliendo di rifiutare i cookies, l’utente potrebbe non essere in grado di utilizzare tutte le funzionalità del sito.</p>
                    <p class="text-justify">È possibile bloccare i cookies usando seguendo le istruzioni fornite dal browser (in genere si trovano nei menu “Aiuto”, “Strumenti o “Modifica”). La disattivazione di un cookie o di una categoria di cookies non li elimina dal browser. Pertanto, tale operazione dovrà essere effettuata direttamente nel browser.</p>
                    <p class="text-justify">Se si vogliono cancellare, disabilitare o ripristinare i cookies presenti sul dispositivo usato, è possibile utilizzare le pagine di configurazione cookies messe a disposizione da eventuali fornitori di contenuti di terze parti, oppure tramite le opzioni del browser dell’utente.</p>
                    <p class="text-justify">Di seguito riportiamo un elenco di istruzioni per gestire i cookies in relazione ai browser più diffusi:</p>
                    <ul>
                        <li>Chrome &#8211; <a href="https://support.google.com/chrome/answer/95647?hl=en&amp;topic=14666&amp;ctx=topic">https://support.google.com/chrome/answer/95647?hl=en&amp;topic=14666&amp;ctx=topic</a></li>
                        <li>Firefox &#8211; <a href="https://support.mozilla.org/en-US/kb/cookies-information-websites-store-on-your-computer">https://support.mozilla.org/en-US/kb/cookies-information-websites-store-on-your-computer</a></li>
                        <li>Internet Explorer &#8211; <a href="http://windows.microsoft.com/en-US/internet-explorer/delete-manage-cookies#ie=ie-11">http://windows.microsoft.com/en-US/internet-explorer/delete-manage-cookies#ie=ie-11</a></li>
                        <li>Microsoft Edge &#8211; <a href="https://privacy.microsoft.com/it-IT/windows-10-microsoft-edge-and-privacy">https://privacy.microsoft.com/it-IT/windows-10-microsoft-edge-and-privacy</a></li>
                        <li>Opera &#8211; <a href="https://help.opera.com/en/latest/security-and-privacy/#tracking">https://help.opera.com/en/latest/security-and-privacy/#tracking</a></li>
                        <li>Safari &#8211; <a href="https://support.apple.com/kb/PH17191?viewlocale=en_US&amp;locale=en_GB">https://support.apple.com/kb/PH17191?viewlocale=en_US&amp;locale=en_GB</a></li>
                    </ul>
                    <p class="text-justify">Per ulteriori informazioni sui cookie, anche su come visualizzare quelli che sono stati impostati sul dispositivo, su come gestirli ed eliminarli, visitare <a href="http://www.allaboutcookies.org/">www.allaboutcookies.org</a> oppure <a href="http://www.youronlinechoices.com/it/le-tue-scelte">http://www.youronlinechoices.com/it/le-tue-scelte</a>.</p>
                    &nbsp;

                </div>
            </div>
        </div>
        <%@include  file="/html/footer.html" %>
    </body>
</html>
