/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.servlets;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.UtenteDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Utente;
import it.unitn.disi.as.aziendasanitaria.utilities.Authentication;
import it.unitn.disi.as.aziendasanitaria.utilities.Security;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author davide
 * attivo
 */
public class CambiaPasswordServlet extends HttpServlet {

    private UtenteDAO userDao;

    private static final Logger logger = LogManager.getLogger(CambiaPasswordServlet.class);

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory");
        }
        try {
            userDao = daoFactory.getDAO(UtenteDAO.class);
        } catch (DAOFactoryException ex) {
            logger.error("Impossibile ottenere il dao user", ex);
            throw new ServletException("Impossibile ottenere il dao user", ex);
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Utente user = (Utente) request.getAttribute("user");
        if (user == null) {
            logger.error("Nessun utente loggato");
            throw new ServletException("Nessun utente loggato");
        }

        String nuovaPassword = request.getParameter("nuovaPassword");
        if (nuovaPassword == null) {
            logger.debug("Parametro nuovaPassword non passato");
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        String confermaPassword = request.getParameter("confermaPassword");
        if (confermaPassword == null) {
            logger.debug("Parametro confermaPassword non passato");
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        try {
            if (!Security.setNuovaPassword(user, nuovaPassword, confermaPassword, userDao)) {
                logger.debug("Password non aggiornata");
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
        } catch (DAOException ex) {
            logger.error("Impossibile aggiornare la password dell'utente", ex);
            throw new ServletException("Impossibile aggiornare la password dell'utente", ex);
        }

        request.getSession().setAttribute("success", "Password aggiornata");
        logger.info("Password aggiornata per l'utente " + user.getId());
        Authentication.redirectToProfile(user, request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
