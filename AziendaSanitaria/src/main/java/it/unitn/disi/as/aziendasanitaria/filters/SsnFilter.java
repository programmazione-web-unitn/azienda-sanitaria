/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.filters;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.EsameDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.RicettaDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.VisitaSpecialisticaDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Ssn;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Utente;
import it.unitn.disi.as.aziendasanitaria.utilities.Authentication;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author davide
 */
public class SsnFilter implements Filter {
    
    private static final Logger logger = LogManager.getLogger(SsnFilter.class);

    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured. 
    private FilterConfig filterConfig = null;
    public SsnFilter() {
    }

    private int doBeforeProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {

        EsameDAO esameDao;
        RicettaDAO ricettaDao;
        VisitaSpecialisticaDAO visitaSpecialisticaDao;

        DAOFactory daoFactory = (DAOFactory) request.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory");
        }
        try {
            esameDao = daoFactory.getDAO(EsameDAO.class);
            ricettaDao = daoFactory.getDAO(RicettaDAO.class);
            visitaSpecialisticaDao = daoFactory.getDAO(VisitaSpecialisticaDAO.class);

        } catch (DAOFactoryException ex) {
            logger.error("Impossibile ottenere il dao factory", ex);
            throw new ServletException("Impossibile ottenere il dao factory", ex);
        }
        
        if (request instanceof HttpServletRequest) {
            Utente user = (Utente) ((HttpServletRequest) request).getAttribute("user");
            
            if (user == null) {
                logger.error("Nessun utente loggato");
                return HttpServletResponse.SC_FORBIDDEN; // nessun utente loggato
                
            } else if (user instanceof Ssn) {
                try {
                    int currentYear = Calendar.getInstance().get(Calendar.YEAR);
                    List<List<Integer>> spesaEsami = esameDao.getSpesaFrom(currentYear-7);
                    List<List<Integer>> spesaRicette = ricettaDao.getSpesaFrom(currentYear-7);
                    List<List<Integer>> spesaVisiteSpecialistiche = visitaSpecialisticaDao.getSpesaFrom(currentYear-7);
                                        
                    int size = Integer.min(Integer.min(spesaEsami.size(), spesaRicette.size()), spesaVisiteSpecialistiche.size());
                    List<List<Integer>> spesaNazionale = new ArrayList<List<Integer>>(size);
                    for(int i = 0; i < size; i++) {
                        List<Integer> annoSpesa = new ArrayList<>(2);
                        annoSpesa.add(spesaEsami.get(i).get(0));
                        annoSpesa.add(spesaEsami.get(i).get(1)+spesaRicette.get(i).get(1)+spesaVisiteSpecialistiche.get(i).get(1));
                        spesaNazionale.add(annoSpesa);
                    }
                                        
                    request.setAttribute("spesaEsami", spesaEsami);
                    request.setAttribute("spesaRicette", spesaRicette);
                    request.setAttribute("spesaVisiteSpecialistiche", spesaVisiteSpecialistiche);
                    request.setAttribute("spesaNazionale", spesaNazionale);
                    
                } catch (DAOException ex) {
                    logger.error("Impossibile caricare dati del ssn", ex);
                    throw new ServletException("Impossibile caricare dati del ssn", ex);
                }
                
                return HttpServletResponse.SC_OK;
                
            } else {
                logger.debug("Accesso proibito all'untente con id='"+user.getId()+"'");
                return HttpServletResponse.SC_FORBIDDEN;
            }

        }
        return HttpServletResponse.SC_FORBIDDEN;
    }

    private void doAfterProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
    }

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {        
        logger.debug("doFilter()");
        
        int statoRichiesta = doBeforeProcessing(request, response);

        if (statoRichiesta == HttpServletResponse.SC_OK) {
            chain.doFilter(request, response);
        } else if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
            if (request.getAttribute("user") != null) {
                ((HttpServletResponse) response).sendError(statoRichiesta);
            } else {
                Authentication.redirectToLogin((HttpServletRequest) request, (HttpServletResponse) response);
            }
        }

        doAfterProcessing(request, response);

    }

    /**
     * Return the filter configuration object for this filter.
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    public void destroy() {
    }

    /**
     * Init method for this filter
     */
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
        if (filterConfig != null) {

            logger.debug("Initializing filter");
        }
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("SsnFilter()");
        }
        StringBuffer sb = new StringBuffer("SsnFilter(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }
    
}
