/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.services;

import com.google.gson.Gson;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.PazienteDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoDiBase;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Utente;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * REST Web Service
 *
 * @author davide
 */
@Path("parcoPazienti")
public class ParcoPazientiService {

    @Context
    private ServletContext servletContext;
    
    private PazienteDAO pazienteDao;
    private static final Logger logger = LogManager.getLogger(ParcoPazientiService.class);

    /**
     * Creates a new instance of ParcoPazientiService
     */
    public ParcoPazientiService() {
    }

    /**
     * Retrieves representation of an instance of it.unitn.disi.as.aziendasanitaria.services.ParcoPazientiService
     * @param request
     * @return an instance of java.lang.String
     * @throws javax.servlet.ServletException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson(@Context HttpServletRequest request) throws ServletException {

        DAOFactory daoFactory = (DAOFactory) servletContext.getAttribute("daoFactory");
        try {
            pazienteDao = daoFactory.getDAO(PazienteDAO.class);
        } catch (DAOFactoryException ex) {
            logger.error("Impossibile ottenere il dao paziente", ex);
            throw new ServletException("Impossibile ottenere il dao paziente", ex);
        }
        
        Utente user = (Utente) request.getAttribute("user"); // medico di base

        List<TableElement> results = new ArrayList<>();
        List<Map<String, Object>> parcoPazienti;
        try {
            if(user instanceof MedicoDiBase)
                parcoPazienti = pazienteDao.getByMedicoDiBaseWithInfo((MedicoDiBase) user);

            else
                return "";
        } catch (DAOException ex) {
            logger.error("Impossibile ottenere la lista dei pazienti", ex);
            throw new ServletException("Impossibile ottenere la lista dei pazienti", ex);
        }
        
        for (Map<String, Object> pazienteInfo : parcoPazienti) {
            results.add(new TableElement(pazienteInfo));
        }
    
        Gson gson = new Gson();
        return gson.toJson(new TableResult(results.toArray(new TableElement[0])));
    }

}

class TableElement implements Serializable {

    private Map<String, Object> pazienteInfo;

    public TableElement(Map<String, Object> pazienteInfo) {
        setPazienteInfo(pazienteInfo);
    }

    /**
     * @return the pazienteInfo
     */
    public Map<String, Object> getPazienteInfo() {
        return pazienteInfo;
    }

    /**
     * @param pazienteInfo the pazienteInfo to set
     */
    public void setPazienteInfo(Map<String, Object> pazienteInfo) {
        this.pazienteInfo = pazienteInfo;
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("E dd/MM/yyyy", Locale.ITALIAN);
        
        String ultima_visita_presc = dateFormat.format(((Timestamp)this.pazienteInfo.get("ultima_visita_presc")).getTime());
        this.pazienteInfo.put("ultima_visita_presc_timestamp", ((Timestamp)this.pazienteInfo.get("ultima_visita_presc")).getTime());
        this.pazienteInfo.put("ultima_visita_presc", ultima_visita_presc);        
        
        String ultima_ricetta_presc = dateFormat.format(((Timestamp)this.pazienteInfo.get("ultima_ricetta_presc")).getTime());
        this.pazienteInfo.put("ultima_ricetta_presc_timestamp", ((Timestamp)this.pazienteInfo.get("ultima_ricetta_presc")).getTime());
        this.pazienteInfo.put("ultima_ricetta_presc", ultima_ricetta_presc);        
        
        String ultimo_esame_presc = dateFormat.format(((Timestamp)this.pazienteInfo.get("ultimo_esame_presc")).getTime());
        this.pazienteInfo.put("ultimo_esame_presc_timestamp", ((Timestamp)this.pazienteInfo.get("ultimo_esame_presc")).getTime());
        this.pazienteInfo.put("ultimo_esame_presc", ultimo_esame_presc);        
    }
}

class TableResult implements Serializable {

    private TableElement[] results;

    public TableResult(TableElement[] results) {
        this.results = results;
    }

    public TableElement[] getResults() {
        return results;
    }

    public void setResults(TableElement[] results) {
        this.results = results;
    }
}
