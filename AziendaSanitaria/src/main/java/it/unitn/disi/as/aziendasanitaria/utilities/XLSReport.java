/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.utilities;

import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoDiBase;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Provincia;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Ricetta;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author dima
 */
public class XLSReport {

    //la lista è ordinata per data erogazione
    public static XSSFWorkbook getXls(Provincia provincia, List<Ricetta> listRicetta, Date startDate, Date endDate) {
        XSSFWorkbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet();

        SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
        String titolo = "Esami erogati in provincia di " + provincia.getNome() + " tra il "
                + fmt.format(startDate) + " e il " + fmt.format(endDate);
        String[] col_names = {"Data erogazione", "Medico di base", "Codice fiscale paziente", "Farmacia", "Farmaco", "Quantità", "Tiket"};

        for (int row = 3; row < listRicetta.size() + 3; row++) {
            Row _row = sheet.createRow(row);
            Ricetta ricetta = listRicetta.get(row - 3);

            if (row == 3 || ricetta.getDataOraErogazione() != listRicetta.get(row - 4).getDataOraErogazione());
            _row.createCell(0).setCellValue(new SimpleDateFormat("dd/MM/yyyy").format(ricetta.getDataOraErogazione()));
            _row.createCell(1).setCellValue(ricetta.getMedicoDiBase().getCognome() + " " + ricetta.getMedicoDiBase().getNome());
            _row.createCell(2).setCellValue(ricetta.getPaziente().getCodiceFiscale());
            _row.createCell(3).setCellValue(ricetta.getFarmacia().getNome());
            _row.createCell(4).setCellValue(ricetta.getFarmaco().getNome());
            _row.createCell(5).setCellValue(ricetta.getQuantita() + "");
            _row.createCell(6).setCellValue("€" + ricetta.getFarmaco().getTicket() + ".00");
        }

        Row names_row = sheet.createRow(2);
        CellStyle names_style = sheet.getWorkbook().createCellStyle();
        XSSFFont defaultFont = workbook.createFont();
        defaultFont.setBold(true);
        names_style.setFont(defaultFont);
        names_row.setRowStyle(names_style);
        for (int i = 0; i < col_names.length; i++) {
            names_row.createCell(i).setCellValue(col_names[i]);
            sheet.autoSizeColumn(i);
        }

        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 6));
        Row title_row = sheet.createRow(0);
        Cell title_cell = title_row.createCell(0);
        title_cell.setCellValue(titolo);
        title_cell.setCellStyle(names_style);

        return workbook;
    }

    public static XSSFWorkbook getXlsForSsn(List<Ricetta> listRicetta) {
        XSSFWorkbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet();

        String titolo = "Spesa delle ricette per medico di base e provincia";
        String[] col_names = {"Provincia", "Medico di base", "Spesa in ricette"};
        TreeMap<Struct1, Integer> map = new TreeMap<Struct1, Integer>();
        for (Ricetta ricetta : listRicetta) {
            MedicoDiBase mb = ricetta.getMedicoDiBase();
            int a = mb.getId();
            Struct1 str = new Struct1(mb.getId(), ricetta.getFarmacia().getProvincia().getNome(), mb.getCognome() + " " + mb.getNome());
            if (!map.containsKey(str)) {
                map.put(str, 0);
            }
            int num = map.get(str);
            map.put(str, num + ricetta.getQuantita() * ricetta.getFarmaco().getTicket());
        }

        XSSFCellStyle cellStyle = (XSSFCellStyle) workbook.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        XSSFFont font = new XSSFFont();
        font.setFontHeight(20);
        cellStyle.setFont(font);

        Struct1 last = null;
        int row = 3;
        int firstEquals = row;
        for (Map.Entry<Struct1, Integer> entry : map.entrySet()) {
            Struct1 key = entry.getKey();

            Row _row = sheet.createRow(row);
            if (last == null || !last.provincia.equals(key.provincia)) {
                if (firstEquals != row) {
                    sheet.addMergedRegion(new CellRangeAddress(firstEquals, row - 1, 0, 0));
                }
                _row.createCell(0).setCellValue(key.provincia);
                _row.getCell(0).setCellStyle(cellStyle);
                firstEquals = row;
            }
            _row.createCell(1).setCellValue(key.nomeCognome);
            _row.createCell(2).setCellValue("€" + entry.getValue() + ".00");

            row++;
            last = key;
        }
        if (firstEquals != row) {
            sheet.addMergedRegion(new CellRangeAddress(firstEquals, row - 1, 0, 0));
        }

        Row names_row = sheet.createRow(2);
        CellStyle names_style = sheet.getWorkbook().createCellStyle();
        XSSFFont defaultFont = workbook.createFont();
        defaultFont.setBold(true);
        names_style.setFont(defaultFont);
        names_row.setRowStyle(names_style);
        for (int i = 0; i < col_names.length; i++) {
            names_row.createCell(i).setCellValue(col_names[i]);
            sheet.autoSizeColumn(i);
        }

        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 6));
        Row title_row = sheet.createRow(0);
        Cell title_cell = title_row.createCell(0);
        title_cell.setCellValue(titolo);
        title_cell.setCellStyle(names_style);

        return workbook;
    }
}

class Struct1 implements Comparable<Struct1> {

    String provincia, nomeCognome;
    Integer medicoDiBase;

    Struct1(Integer a, String b, String c) {
        medicoDiBase = a;
        provincia = b;
        nomeCognome = c;
    }

    Struct1(Integer a, String b) {
        medicoDiBase = a;
        provincia = b;
    }

    @Override
    public int compareTo(Struct1 arg0) {
        if (provincia.compareTo(arg0.provincia) != 0) {
            return provincia.compareTo(arg0.provincia);
        } else {
            return medicoDiBase.compareTo(arg0.medicoDiBase);
        }
    }
}
