/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao.jdbc_psql;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.FarmaciaDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Farmacia;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Provincia;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Utente;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gabriele
 */
public class JDBC_PSQLFarmaciaDAO extends JDBC_PSQL_DAO<Farmacia, Integer> implements FarmaciaDAO {

    public JDBC_PSQLFarmaciaDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try ( Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM farmacia");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count farmacia", ex);
        }

        return 0L;
    }

    @Override
    public Farmacia getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT p.sigla AS p_sigla, p.nome  AS p_nome, p.regione  AS p_regione, "
                + "       u.id     AS f_id,     u.email AS  f_email, u.password  AS f_password, "
                + "       u.tipo   AS f_tipo, f.nome as f_nome, f.comune AS f_comune, u.salt AS f_salt,"
                + "       u.recupero_token AS f_rec_token, u.scadenza_recupero AS f_scad_rec,"
                + "       u.token AS f_token, u.scadenza_token AS f_scad_token "
                + "FROM farmacia AS f "
                + "JOIN utente as u ON f.id = u.id "
                + "JOIN provincia as p ON f.sigla_provincia = p.sigla "
                + "WHERE f.id = ?")) {
            stm.setInt(1, primaryKey);
            try ( ResultSet rs = stm.executeQuery()) {
                rs.next();
                Provincia provincia = new Provincia(rs.getString("p_sigla"),
                        rs.getString("p_nome"),
                        rs.getString("p_regione"));

                Farmacia farmacia = new Farmacia(
                        rs.getInt("f_id"),
                        rs.getString("f_email"),
                        rs.getString("f_password"),
                        rs.getString("f_salt"),
                        rs.getString("f_tipo"),
                        rs.getString("f_rec_token"),
                        rs.getTimestamp("f_scad_rec"),
                        rs.getString("f_token"),
                        rs.getTimestamp("f_scad_token"),
                        rs.getString("f_nome"),
                        rs.getString("f_comune"),
                        provincia);
                return farmacia;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the farmacia for the passed primary key", ex);
        }
    }

    @Override
    public List<Farmacia> getAll() throws DAOException {
        List<Farmacia> farmacie = new ArrayList<>();

        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery(""
                    + "SELECT p.sigla AS p_sigla, p.nome  AS p_nome, p.regione  AS p_regione, "
                    + "       u.id     AS f_id,     u.email AS  f_email, u.password  AS f_password, "
                    + "       u.tipo   AS f_tipo, f.nome as f_nome, f.comune AS f_comune, u.salt AS f_salt, "
                    + "       u.recupero_token AS f_rec_token, u.scadenza_recupero AS f_scad_rec, "
                    + "       u.token AS f_token, u.scadenza_token AS f_scad_token "
                    + "FROM farmacia AS f "
                    + "JOIN utente as u ON f.id = u.id "
                    + "JOIN provincia as p ON f.sigla_provincia = p.sigla ORDER BY p.nome, f.id")) {
                while (rs.next()) {
                    Provincia provincia = new Provincia(rs.getString("p_sigla"),
                            rs.getString("p_nome"),
                            rs.getString("p_regione"));

                    Farmacia farmacia = new Farmacia(
                            rs.getInt("f_id"),
                            rs.getString("f_email"),
                            rs.getString("f_password"),
                            rs.getString("f_salt"),
                            rs.getString("f_tipo"),
                            rs.getString("f_rec_token"),
                            rs.getTimestamp("f_scad_rec"),
                            rs.getString("f_token"),
                            rs.getTimestamp("f_scad_token"),
                            rs.getString("f_nome"),
                            rs.getString("f_comune"),
                            provincia
                    );
                    farmacie.add(farmacia);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of farmacie", ex);
        }

        return farmacie;
    }

    @Override
    public Farmacia getByUtente(Utente utente) throws DAOException {
        if (utente == null) {
            throw new DAOException("utente is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT p.sigla AS p_sigla, p.nome  AS p_nome, p.regione  AS p_regione, "
                + "       f.nome as f_nome, f.comune AS f_comune "
                + "FROM farmacia AS f "
                + "JOIN provincia as p ON f.sigla_provincia = p.sigla "
                + "WHERE f.id = ?")) {
            stm.setInt(1, utente.getId());
            try ( ResultSet rs = stm.executeQuery()) {
                rs.next();
                Provincia provincia = new Provincia(rs.getString("p_sigla"),
                        rs.getString("p_nome"),
                        rs.getString("p_regione"));

                Farmacia farmacia = new Farmacia(
                        utente.getId(),
                        utente.getEmail(),
                        utente.getPassword(),
                        utente.getSalt(),
                        utente.getTipo(),
                        utente.getRecuperoToken(),
                        utente.getScadenzaRecupero(),
                        utente.getToken(),
                        utente.getScadenzaToken(),
                        rs.getString("f_nome"),
                        rs.getString("f_comune"),
                        provincia
                );
                
                return farmacia;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the farmacia for the passed utente", ex);
        }
    }

}
