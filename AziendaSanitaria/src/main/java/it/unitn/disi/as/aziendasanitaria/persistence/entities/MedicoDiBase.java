/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.entities;

import java.sql.Date;
import java.sql.Timestamp;

/**
 *
 * @author gabriele
 */
public class MedicoDiBase extends Paziente {
    private String comuneMedico;
    private Provincia provinciaMedico;
 
    public MedicoDiBase(Integer id, String email, String password, String salt, String tipo, String nome, String cognome, Date dataNascita, 
            String luogoNascita, Provincia provinciaNascita, String codiceFiscale, String sesso, Provincia provincia, MedicoDiBase medicoDiBase,  
            String comuneMedico, Provincia provinciaMedico) {
        super(id, email, password, salt, tipo, nome, cognome, dataNascita, luogoNascita, provinciaNascita, codiceFiscale, sesso, provincia, medicoDiBase);
        this.comuneMedico = comuneMedico;
        this.provinciaMedico = provinciaMedico;
    }

    public MedicoDiBase(Integer id, String email, String password, String salt, String tipo, String recuperoToken, 
            Timestamp scadenzaRecupero, String token, Timestamp scadenzaToken, String nome, 
            String cognome, Date dataNascita, String luogoNascita, Provincia provinciaNascita, 
            String codiceFiscale, String sesso, Provincia provincia, MedicoDiBase medicoDiBase,
            String comuneMedico, Provincia provinciaMedico) {
        
        super(id, email, password, salt, tipo, recuperoToken, scadenzaRecupero, 
                token, scadenzaToken, nome, cognome, dataNascita, luogoNascita, 
                provinciaNascita, codiceFiscale, sesso, provincia, medicoDiBase);
        this.comuneMedico = comuneMedico;
        this.provinciaMedico = provinciaMedico;
    }
    
    public MedicoDiBase(Integer id, String nome, String cognome){
        super(id, nome, cognome);
    }
    
    public String getComuneMedico() {
        return comuneMedico;
    }

    public void setComuneMedico(String comuneMedico) {
        this.comuneMedico = comuneMedico;
    }

    public Provincia getProvinciaMedico() {
        return provinciaMedico;
    }

    public void setProvinciaMedico(Provincia provinciaMedico) {
        this.provinciaMedico = provinciaMedico;
    }

    
    
}
