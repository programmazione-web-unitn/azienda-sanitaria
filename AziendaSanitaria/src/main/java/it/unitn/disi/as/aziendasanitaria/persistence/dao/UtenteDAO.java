/*
 * AziendaSanitaria
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao;

import it.unitn.disi.as.aziendasanitaria.persistence.entities.Utente;
import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.sql.Timestamp;

/**
 * Tutte le classi DAO devono implementare questa interfaccia per gestire il
 * sistema di persistenza che interagisce con {@link Utente gli utenti}.
 *
 * @author gabriele
 *
 */
public interface UtenteDAO extends DAO<Utente, Integer> {

    public Utente getByEmail(String email) throws DAOException;

    public Utente update(Utente user) throws DAOException;
    
    public Utente setRememberMe(Utente user, String token, Timestamp scadenzaToken) throws DAOException;    
    public Utente getByToken(String token) throws DAOException;
    
    public Utente setRecuperoPassword(Utente user, String recuperoToken, Timestamp scadenzaRecupero) throws DAOException;
    public Utente getByRecuperoToken(String recuperoToken) throws DAOException;
}
