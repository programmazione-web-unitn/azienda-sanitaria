/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.services;

import it.unitn.disi.as.aziendasanitaria.persistence.entities.Result;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Element;
import com.google.gson.Gson;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.FarmacoDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Farmaco;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.core.Context;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * REST Web Service
 *
 * @author davide
 */
@Path("listaFarmaci")
public class ListaFarmaciService {

    @Context 
    private ServletContext servletContext;
    
    private FarmacoDAO farmacoDao;
    private static final Logger logger = LogManager.getLogger(ListaFarmaciService.class);

    /**
     * Creates a new instance of ListaFarmaciService
     */
    public ListaFarmaciService() {
    }

    /**
     * Retrieves representation of an instance of it.unitn.disi.as.aziendasanitaria.services.ListaFarmaciService
     * @param term
     * @return an instance of java.lang.String
     * @throws javax.servlet.ServletException
     */
    @GET
    @Path("{term}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson(@PathParam("term") String term) throws ServletException {
        DAOFactory daoFactory = (DAOFactory) servletContext.getAttribute("daoFactory");
        try {
            farmacoDao = daoFactory.getDAO(FarmacoDAO.class);
        } catch (DAOFactoryException ex) {
            logger.error("Impossibile ottenere farmacoDao", ex);
            throw new ServletException("Impossibile ottenere farmacoDao", ex);
        }
                
        List<Element> results = new ArrayList<>();
        List<Farmaco> farmaci = new ArrayList<Farmaco>();
        try {
            farmaci = farmacoDao.getAll();
        } catch (DAOException ex) {
            logger.error("Impossibile ottenere la lista dei farmaci", ex);
            throw new ServletException("Impossibile ottenere la lista dei farmaci", ex);
        }
        
        if ((term == null) || term.length() == 0) {
            // restituiamo la lista di tutti i farmaci presenti nel db            
            for (int i = 0; i < farmaci.size(); i++) {
                Farmaco f = farmaci.get(i);
                results.add(new Element(f.getId(), f.getNome()));
            }
        } else {
            term = term.toLowerCase();
            for (int i = 0; i < farmaci.size(); i++) {
                Farmaco f = farmaci.get(i);
                if(f.getNome().toLowerCase().contains(term)){
                    results.add(new Element(f.getId(), f.getNome()));
                }
            }
        }

        Gson gson = new Gson();
        return gson.toJson(new Result(results.toArray(new Element[0])));
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() throws ServletException {
        return getJson(null);
    }

}
