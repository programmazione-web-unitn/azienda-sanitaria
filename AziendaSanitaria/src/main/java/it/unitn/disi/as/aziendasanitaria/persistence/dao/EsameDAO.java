/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao;

import it.unitn.disi.as.aziendasanitaria.persistence.entities.Esame;
import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author fran
 */
public interface EsameDAO extends DAO<Esame, Integer> {
    
    public List<Esame> getLastByPaziente(Integer idPaziente) throws DAOException;
    public List<Esame> getDaErogareByPaziente(Integer idPaziente) throws DAOException;

    public List<Esame> getByPaziente(Integer idPaziente) throws DAOException;

    public boolean isEsameDelPaziente(Integer idEsame, Integer idPaziente) throws DAOException;
    public boolean isEsameDiUnPaziente(Integer idEsame, Integer idMedicoDiBase) throws DAOException;

    public List<Esame> getErogatiByPaziente(Integer idPaziente) throws DAOException;
    public List<Esame> getErogateBySsp(String siglaProvincia) throws DAOException;
    public List<Esame> getErogateBySsp(String siglaProvincia, Date startDate, Date endDate) throws DAOException;
    
    public Esame insert(Esame esame) throws DAOException;
    public Esame update(Esame esame) throws DAOException;
    
    public List<List<Integer>> getSpesaFrom(int fromYear) throws DAOException;

}
