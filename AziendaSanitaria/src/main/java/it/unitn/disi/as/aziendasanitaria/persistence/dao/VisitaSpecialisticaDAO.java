/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao;

import it.unitn.disi.as.aziendasanitaria.persistence.entities.VisitaSpecialistica;
import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.util.List;

/**
 *
 * @author fran
 */
public interface VisitaSpecialisticaDAO extends DAO<VisitaSpecialistica, Integer> {

    public List<VisitaSpecialistica> getByPaziente(Integer idPaziente) throws DAOException;

    public List<VisitaSpecialistica> getLastByPaziente(Integer idPaziente) throws DAOException;

    public List<VisitaSpecialistica> getDaErogareByPaziente(Integer idPaziente) throws DAOException;

    public List<VisitaSpecialistica> getErogateByPaziente(Integer idPaziente) throws DAOException;

    //public VisitaSpecialistica update(VisitaSpecialistica visitaSpecialistica) throws DAOException;
    
    public VisitaSpecialistica insert(VisitaSpecialistica visita) throws DAOException;
    public VisitaSpecialistica insertRichiamo(VisitaSpecialistica visita) throws DAOException;
    
    public VisitaSpecialistica update(VisitaSpecialistica esame) throws DAOException;
    public VisitaSpecialistica visita(VisitaSpecialistica esame) throws DAOException;
    
    public boolean isVisitaDiUnPaziente(Integer idVisita, Integer idMedicoDiBase) throws DAOException;
    
    public List<List<Integer>> getSpesaFrom(int fromYear) throws DAOException;
}
