/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.servlets;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.EsameDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.ProvinciaDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.RicettaDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.VisitaSpecialisticaDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Esame;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Paziente;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Provincia;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Ricetta;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Ssn;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Ssp;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.VisitaSpecialistica;
import it.unitn.disi.as.aziendasanitaria.utilities.PDFReport;
import it.unitn.disi.as.aziendasanitaria.utilities.XLSReport;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author dima
 */
public class GenerateReportServlet extends HttpServlet {

    private EsameDAO esameDao;
    private RicettaDAO ricettaDao;
    private VisitaSpecialisticaDAO visitaSpecialisticaDao;
    private ProvinciaDAO provinciaDao;

    private static final Logger logger = LogManager.getLogger(GenerateReportServlet.class);

    @Override
    public void init() throws ServletException {
        String logoPath = getServletContext().getInitParameter("logoPath");
        if (logoPath == null) {
            logger.error("Il parametro logoPath non è definito nel web.xml");
            throw new ServletException("Il parametro logoPath non è definito nel web.xml");
        }
        PDFReport.init(logoPath);
        DAOFactory daoFactory = (DAOFactory) getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory");
        }
        try {
            esameDao = daoFactory.getDAO(EsameDAO.class);
            ricettaDao = daoFactory.getDAO(RicettaDAO.class);
            visitaSpecialisticaDao = daoFactory.getDAO(VisitaSpecialisticaDAO.class);
            provinciaDao = daoFactory.getDAO(ProvinciaDAO.class);
        } catch (DAOFactoryException ex) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory", ex);
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     */
    //servirebbe un dao per avere l'esame dal solo id
    //immagine del azienda con annesso path
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if ("pdf".equals(request.getParameter("report"))) {
            PDDocument pdf;
            response.setContentType("application/pdf");

            if ("esame".equals(request.getParameter("type"))) {
                if (request.getAttribute("esame") == null) {
                    logger.error("Generazione pdf esame: oggetto esame non presente");
                    throw new ServletException("Generazione pdf esame: oggetto esame non presente");
                }
                Esame esame = (Esame) request.getAttribute("esame");
                pdf = PDFReport.getPdf(esame);
                response.addHeader("Content-Disposition", "attachment; filename="
                        + "PDFreport_esame_" + esame.getId() + ".pdf");

            } else if ("ricetta".equals(request.getParameter("type"))) {
                if (request.getAttribute("ricetta") == null) {
                    logger.error("Generazione pdf ricetta: oggetto ricetta non presente");
                    throw new ServletException("Generazione pdf ricetta: oggetto ricetta non presente");
                }

                Ricetta ricetta = (Ricetta) request.getAttribute("ricetta");
                pdf = PDFReport.getPdf(ricetta);
                response.addHeader("Content-Disposition", "attachment; filename="
                        + "PDFreport_ricetta_" + ricetta.getId() + ".pdf");

            } else if ("allPaidTickets".equals(request.getParameter("type"))) {
                if (request.getAttribute("paziente") == null) {
                    logger.error("Generazione pdf ticket: oggetto paziente non presente");
                    throw new ServletException("Generazione pdf ticket: oggetto paziente non presente");
                }
                Paziente paziente = (Paziente) request.getAttribute("paziente");
                try {
                    List<Esame> listEsame = esameDao.getErogatiByPaziente(paziente.getId());
                    List<Ricetta> listRicetta = ricettaDao.getErogateByPaziente(paziente.getId());
                    List<VisitaSpecialistica> listVisitaSpecialistica = visitaSpecialisticaDao.getErogateByPaziente(paziente.getId());

                    pdf = PDFReport.getPdf(paziente, listEsame, listRicetta, listVisitaSpecialistica);
                    response.addHeader("Content-Disposition", "attachment; filename="
                            + "PDFreport_allPaidTickets_" + paziente.getCognome() + ".pdf");

                } catch (DAOException ex) {
                    logger.error("Impossibile caricare dati del paziente", ex);
                    throw new ServletException("Impossibile caricare dati del paziente", ex);
                }
            } else if ("sspPrestazioni".equals(request.getParameter("type"))) {

                Ssp ssp = (Ssp) request.getAttribute("user");
                if (ssp == null) {
                    logger.error("Generazione pdf esami ssp: ssp non presente");
                    throw new ServletException("Generazione pdf esami ssp: ssp non presente");
                }

                if (request.getParameter("prestStartDate") == null) {
                    logger.debug("Generazione pdf esami ssp: prestStartDate non presente");
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
                if (request.getParameter("prestEndDate") == null) {
                    logger.debug("Generazione pdf esami ssp: prestEndDate non presente");
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                Date startDate = null, endDate = null;
                
                try {
                    startDate = new Date(format.parse(request.getParameter("prestStartDate")).getTime());
                    endDate = new Date(format.parse(request.getParameter("prestEndDate")).getTime());
                } catch (ParseException ex) {
                    logger.debug("Generazione pdf esami ssp: date non valide");
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
                if (startDate.after(endDate)) {
                    logger.debug("Generazione pdf esami ssp: data inizio maggiore di data fine");
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
                try {
                    Provincia provincia = provinciaDao.getByPrimaryKey(ssp.getProvincia().getSigla());
                    List<Esame> listEsame = esameDao.getErogateBySsp(ssp.getProvincia().getSigla(), startDate, endDate);

                    pdf = PDFReport.getPdf(provincia, listEsame, startDate, endDate);
                    response.addHeader("Content-Disposition", "attachment; filename="
                            + "PDFreport_sspPrestazioni_" + ssp.getId() + ".pdf");
                } catch (DAOException ex) {
                    logger.error("Impossibile caricare dati provinciaDao, esameDao", ex);
                    throw new ServletException("Impossibile caricare dati provinciaDao, esameDao", ex);
                }
            } else {
                logger.debug("Tipo di report pdf non riconosciuto");
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
            try {
                OutputStream responseOutputStream = response.getOutputStream();
                pdf.save(responseOutputStream);
            } catch (IOException ex) {
                logger.error("Errore salvataggio PDF document");
                throw new ServletException("Errore salvataggio PDF document", ex);
            }
        } 
        
        else if ("xls".equals(request.getParameter("report"))) {
            XSSFWorkbook xls;
            response.setContentType("application/xls");
            if ("sspRicetteErogate".equals(request.getParameter("type"))) {

                Ssp ssp = (Ssp) request.getAttribute("user");
                if (ssp == null) {
                    logger.error("Generazione xls ricette ssp: ssp non presente");
                    throw new ServletException("Generazione xls ricette ssp: ssp non presente");
                }

                if (request.getParameter("siglaProvincia") == null) {
                    logger.debug("Generazione xls ricette ssp: siglaProvincia non presente");
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
                if (request.getParameter("ricStartDate") == null) {
                    logger.debug("Generazione xls ricette ssp: ricStartDate non presente");
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
                if (request.getParameter("ricEndDate") == null) {
                    logger.debug("Generazione xls ricette ssp: ricEndDate non presente");
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                Date startDate = null, endDate = null;
                try {
                    startDate = new Date(format.parse(request.getParameter("ricStartDate")).getTime());
                    endDate = new Date(format.parse(request.getParameter("ricEndDate")).getTime());
                } catch (ParseException ex) {
                    logger.debug("Generazione xls ricette ssp: date non valide");
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
                if (startDate.after(endDate)) {
                    logger.debug("Generazione xls ricette ssp: data inizio maggiore di data fine");
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
                try {
                    Provincia provincia = provinciaDao.getByPrimaryKey(ssp.getProvincia().getSigla());
                    List<Ricetta> listRicetta = ricettaDao.getErogateBySsp(ssp.getProvincia().getSigla(), startDate, endDate);

                    xls = XLSReport.getXls(provincia, listRicetta, startDate, endDate);
                    response.addHeader("Content-Disposition", "attachment; filename="
                            + "XLSreport_sspPrestazioni_" + ssp.getId() + ".xls");
                } catch (DAOException ex) {
                    logger.error("Impossibile caricare dati del ssp", ex);
                    throw new ServletException("Impossibile caricare dati del ssp", ex);
                }
            } else if ("ssnSpesaPerMedicoProvincia".equals(request.getParameter("type"))) {

                Ssn ssn = (Ssn) request.getAttribute("user");
                if (ssn == null) {
                    logger.error("Generazione xls spesa ssn: ssn non presente");
                    throw new ServletException("Generazione xls spesa ssn: ssn non presente");
                }
                try {
                    List<Ricetta> listRicetta = ricettaDao.getAllErogati();

                    xls = XLSReport.getXlsForSsn(listRicetta);

                    response.addHeader("Content-Disposition", "attachment; filename="
                            + "XLSreport_ssnSpesaPerMedicoProvincia_"
                            + ssn.getId() + ".xls");

                } catch (DAOException ex) {
                    logger.error("Impossibile caricare dati del ssn", ex);
                    throw new ServletException("Impossibile caricare dati del ssn", ex);
                }
            } else {
                logger.debug("Tipo di report xls non riconosciuto");
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
            try {
                OutputStream responseOutputStream = response.getOutputStream();
                xls.write(responseOutputStream);
            } catch (IOException ex) {
                logger.error("Errore salvataggio XLS document");
                throw new ServletException("Errore salvataggio XLS document", ex);
            }
        } else {
            logger.debug("Tipo di report non riconosciuto");
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
