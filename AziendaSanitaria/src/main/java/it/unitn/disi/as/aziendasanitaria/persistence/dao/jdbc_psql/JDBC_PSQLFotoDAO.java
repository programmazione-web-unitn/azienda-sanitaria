/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao.jdbc_psql;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.FotoDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Foto;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Paziente;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Provincia;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gabriele
 */
public class JDBC_PSQLFotoDAO extends JDBC_PSQL_DAO<Foto, Integer> implements FotoDAO {

    public JDBC_PSQLFotoDAO(Connection conn) {
        super(conn);
    }

    @Override
    public Long getCount() throws DAOException {
        try ( Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM foto");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count foto", ex);
        }

        return 0L;
    }

    /**
     *
     * @param primaryKey
     * @return
     * @throws DAOException
     */
    @Override
    public Foto getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null.");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT f.id AS f_id, f.path_foto AS f_path_foto, "
                + "f.data_ora_foto AS f_data_ora_foto, f.id_paziente AS f_id_paziente, "
                + "p.id AS p_id, p.nome AS p_nome, p.cognome AS p_cognome, "
                + "pr.sigla AS pr_sigla, pr.nome AS pr_nome, pr.regione AS pr_regione "
                + "FROM foto AS f "
                + "JOIN paziente AS p ON p.id = f.id_paziente "
                + "JOIN provincia AS pr ON pr.sigla = p.sigla_provincia "
                + "WHERE f.id = ? ")) {
            stm.setInt(1, primaryKey);
            try ( ResultSet rs = stm.executeQuery()) {
                rs.next();
                Provincia provincia = new Provincia(rs.getString("pr_sigla"),
                        rs.getString("pr_nome"),
                        rs.getString("pr_regione")
                );
                Paziente paziente = new Paziente(rs.getInt("p_id"),
                        rs.getString("p_nome"),
                        rs.getString("p_cognome")
                );
                paziente.setProvincia(provincia);
                Foto foto = new Foto(rs.getInt("f_id"),
                        paziente,
                        rs.getString("f_path_foto"),
                        rs.getTimestamp("f_data_ora_foto"));
                return foto;
            }
        } catch (SQLException e) {
            throw new DAOException("Impossible to get the foto", e);
        }
    }

    @Override
    public List<Foto> getAll() throws DAOException {
        List<Foto> listaFoto = new ArrayList<>();

        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery("SELECT * FROM foto ORDER BY id")) {

                while (rs.next()) {
                    Paziente paziente = new Paziente(rs.getInt("id_paziente"));
                    Foto foto = new Foto(rs.getInt("id"),
                            paziente,
                            rs.getString("path_foto"),
                            rs.getTimestamp("data_ora_foto"));
                    listaFoto.add(foto);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of foto", ex);
        }
        return listaFoto;
    }

    @Override
    public List<Foto> getByPaziente(Integer idPaziente) throws DAOException {
        if (idPaziente == null) {
            throw new DAOException("idPaziente is null.");
        }
        List<Foto> fotoPaziente = new ArrayList<>();

        try ( PreparedStatement stm = CON.prepareStatement("SELECT * FROM foto WHERE id_paziente = ? ORDER BY data_ora_foto")) {
            stm.setInt(1, idPaziente);
            try ( ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    Paziente paziente = new Paziente(rs.getInt("id_paziente"));

                    Foto foto = new Foto(rs.getInt("id"),
                            paziente,
                            rs.getString("path_foto"),
                            rs.getTimestamp("data_ora_foto"));

                    fotoPaziente.add(foto);
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Impossible to get the list of paziente's foto");
        }
        return fotoPaziente;
    }

    @Override
    public Foto insert(Foto foto) throws DAOException {
        if (foto == null) {
            throw new DAOException("parameter not valid", new IllegalArgumentException("The passed foto is null"));
        }

        try ( PreparedStatement std = CON.prepareStatement("INSERT INTO foto (id_paziente, path_foto, data_ora_foto) VALUES (?, ?, ?);")) {
            std.setInt(1, foto.getPaziente().getId());
            std.setString(2, foto.getPathFoto());
            std.setTimestamp(3, foto.getDataOraFoto());

            if (std.executeUpdate() == 1) {
                return foto;
            } else {
                throw new DAOException("Impossible to insert the foto");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to insert the foto", ex);
        }
    }

    @Override
    public boolean isFotoDiUnPaziente(Integer idFoto, Integer idMedicoDiBase) throws DAOException {
        if (idFoto == null) {
            throw new DAOException("idFoto is null");
        }
        if (idMedicoDiBase == null) {
            throw new DAOException("idMedicoDiBase is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT * "
                + "FROM foto AS f "
                + "JOIN paziente AS p ON f.id_paziente = p.id "
                + "WHERE f.id = ? and p.id_medico_di_base = ?")) {
            stm.setInt(1, idFoto);
            stm.setInt(2, idMedicoDiBase);
            try ( ResultSet rs = stm.executeQuery()) {
                return rs.next();
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the exam for the passed primary key", ex);
        }
    }

}
