/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.entities;

import java.sql.Date;
import java.sql.Timestamp;

/**
 *
 * @author fran
 */
public class Paziente extends Utente {

    protected String nome;
    protected String cognome;
    protected Date dataNascita;
    protected String luogoNascita;
    protected Provincia provinciaNascita;
    protected String codiceFiscale;
    protected String sesso;
    protected Provincia provincia;
    protected MedicoDiBase medicoDiBase;
    

    public Paziente(){
    }
    public Paziente(Integer id){
        super();
        this.id = id;
    }
    public Paziente(Integer id, String nome, String cognome){
        this(id);
        this.nome = nome;
        this.cognome = cognome;
    }
    public Paziente(Integer id, String nome, String cognome, String codiceFiscale){
        this(id);
        this.nome = nome;
        this.cognome = cognome;
        this.codiceFiscale = codiceFiscale;
    }
    public Paziente(Integer id, String email, String password, String salt, String tipo, String nome, String cognome, Date dataNascita, 
            String luogoNascita, Provincia provinciaNascita, String codiceFiscale, String sesso, Provincia provincia,
            MedicoDiBase medicoDiBase) {
        super(id, email, password, salt, tipo);
        this.nome = nome;
        this.cognome = cognome;
        this.dataNascita = dataNascita;
        this.luogoNascita = luogoNascita;
        this.provinciaNascita = provinciaNascita;
        this.codiceFiscale = codiceFiscale;
        this.sesso = sesso;
        this.provincia = provincia;
        this.medicoDiBase = medicoDiBase;
    }

    public Paziente(Integer id, String email, String password, String salt, String tipo, 
            String recuperoToken, Timestamp scadenzaRecupero, String token, Timestamp scadenzaToken, 
            String nome, String cognome, Date dataNascita, String luogoNascita, 
            Provincia provinciaNascita, String codiceFiscale, String sesso, Provincia provincia, 
            MedicoDiBase medicoDiBase) {
        
        super(id, email, password, salt, tipo, recuperoToken, scadenzaRecupero, token, scadenzaToken);
        this.nome = nome;
        this.cognome = cognome;
        this.dataNascita = dataNascita;
        this.luogoNascita = luogoNascita;
        this.provinciaNascita = provinciaNascita;
        this.codiceFiscale = codiceFiscale;
        this.sesso = sesso;
        this.provincia = provincia;
        this.medicoDiBase = medicoDiBase;
    }
    
    

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the cognome
     */
    public String getCognome() {
        return cognome;
    }

    /**
     * @param cognome the cognome to set
     */
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    /**
     * @return the dataNascita
     */
    public Date getDataNascita() {
        return dataNascita;
    }

    /**
     * @param dataNascita the dataNascita to set
     */
    public void setDataNascita(Date dataNascita) {
        this.dataNascita = dataNascita;
    }

    /**
     * @return the luogoNascita
     */
    public String getLuogoNascita() {
        return luogoNascita;
    }

    /**
     * @param luogoNascita the luogoNascita to set
     */
    public void setLuogoNascita(String luogoNascita) {
        this.luogoNascita = luogoNascita;
    }

    /**
     * @return the codiceFiscale
     */
    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    /**
     * @param codiceFiscale the codiceFiscale to set
     */
    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }

    /**
     * @return the sesso
     */
    public String getSesso() {
        return sesso;
    }

    /**
     * @param sesso the sesso to set
     */
    public void setSesso(String sesso) {
        this.sesso = sesso;
    }

    /**
     * @return the provincia
     */
    public Provincia getProvincia() {
        return provincia;
    }

    /**
     * @param provincia the siglaProvincia to set
     */
    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }
    
    public Provincia getProvinciaNascita() {
        return provinciaNascita;
    }

    public void setProvinciaNascita(Provincia provinciaNascita) {
        this.provinciaNascita = provinciaNascita;
    }
    
    public MedicoDiBase getMedicoDiBase() {
        return medicoDiBase;
    }

    public void setMedicoDiBase(MedicoDiBase medicoDiBase) {
        this.medicoDiBase = medicoDiBase;
    }
}
