/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.entities;

import java.sql.Timestamp;

/**
 *
 * @author fran
 */
public class VisitaSpecialistica {
    private Integer                 id;
    private MedicoDiBase        medicoDiBase;
    private Paziente            paziente;
    private TipoDiVisita        tipoDiVisita;
    private Timestamp           dataOraPrescrizione;
    private MedicoSpecialista   medicoSpecialista;
    private Timestamp           dataOraErogazione;
    private String              report;

    public VisitaSpecialistica(Integer id, MedicoDiBase medicoDiBase, Paziente paziente, 
            TipoDiVisita tipoDiVisita, Timestamp dataOraPrescrizione, MedicoSpecialista medicoSpecialista, 
            Timestamp dataOraErogazione, String report) {
        this.id = id;
        this.medicoDiBase = medicoDiBase;
        this.paziente = paziente;
        this.tipoDiVisita = tipoDiVisita;
        this.dataOraPrescrizione = dataOraPrescrizione;
        this.medicoSpecialista = medicoSpecialista;
        this.dataOraErogazione = dataOraErogazione;
        this.report = report;
    }
    
    public VisitaSpecialistica(Integer id, MedicoDiBase medicoDiBase, Paziente paziente, 
            TipoDiVisita tipoDiVisita, Timestamp dataOraPrescrizione) {
        this(id, medicoDiBase, paziente, tipoDiVisita, dataOraPrescrizione, null, null, null);
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the medicoDiBase
     */
    public MedicoDiBase getMedicoDiBase() {
        return medicoDiBase;
    }

    /**
     * @param medicoDiBase the medicoDiBase to set
     */
    public void setMedicoDiBase(MedicoDiBase medicoDiBase) {
        this.medicoDiBase = medicoDiBase;
    }

    /**
     * @return the paziente
     */
    public Paziente getPaziente() {
        return paziente;
    }

    /**
     * @param paziente the paziente to set
     */
    public void setPaziente(Paziente paziente) {
        this.paziente = paziente;
    }

    /**
     * @return the tipoDiVisita
     */
    public TipoDiVisita getTipoDiVisita() {
        return tipoDiVisita;
    }

    /**
     * @param tipoDiVisita the tipoDiVisita to set
     */
    public void setTipoDiVisita(TipoDiVisita tipoDiVisita) {
        this.tipoDiVisita = tipoDiVisita;
    }

    /**
     * @return the dataOraPrescrizione
     */
    public Timestamp getDataOraPrescrizione() {
        return dataOraPrescrizione;
    }

    /**
     * @param dataOraPrescrizione the dataOraPrescrizione to set
     */
    public void setDataOraPrescrizione(Timestamp dataOraPrescrizione) {
        this.dataOraPrescrizione = dataOraPrescrizione;
    }

    /**
     * @return the medicoSpecialista
     */
    public MedicoSpecialista getMedicoSpecialista() {
        return medicoSpecialista;
    }

    /**
     * @param medicoSpecialista the medicoSpecialista to set
     */
    public void setMedicoSpecialista(MedicoSpecialista medicoSpecialista) {
        this.medicoSpecialista = medicoSpecialista;
    }

    /**
     * @return the dataOraErogazione
     */
    public Timestamp getDataOraErogazione() {
        return dataOraErogazione;
    }

    /**
     * @param dataOraErogazione the dataOraErogazione to set
     */
    public void setDataOraErogazione(Timestamp dataOraErogazione) {
        this.dataOraErogazione = dataOraErogazione;
    }

    /**
     * @return the report
     */
    public String getReport() {
        return report;
    }

    /**
     * @param report the report to set
     */
    public void setReport(String report) {
        this.report = report;
    }
    
    

}
