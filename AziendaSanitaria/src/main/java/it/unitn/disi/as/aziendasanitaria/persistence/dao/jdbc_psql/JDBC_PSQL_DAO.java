/*
 * AziendaSanitaria
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao.jdbc_psql;

/**
 *
 * @author gabriele
 */
import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import java.sql.Connection;
import java.util.HashMap;

/**
 * Classe DAO di base che tutte le classi che utilizzano JDBC_PSQL devono estendere.
 * @param <ENTITY_CLASS> classe delle entità da gestire.
 * @param <PRIMARY_KEY_CLASS> classe della chiave primaria
 */
public abstract class JDBC_PSQL_DAO<ENTITY_CLASS, PRIMARY_KEY_CLASS> implements DAO<ENTITY_CLASS, PRIMARY_KEY_CLASS> {
    /**
     * La connessione JDBC utilizzata per accedere al sistema di persistenza.
     */
    protected final Connection CON;
    /**
     * Lista dei DAOs con cui questo DAO può interagire.
     */
    protected final HashMap<Class, DAO> FRIEND_DAOS;
    
    /**
     * Costruttore di base.
     * @param con la connessione.
     */
    protected JDBC_PSQL_DAO(Connection con) {
        super();
        this.CON = con;
        FRIEND_DAOS = new HashMap<>();
    }
    
    
    @Override
    public <DAO_CLASS extends DAO> DAO_CLASS getDAO(Class<DAO_CLASS> daoClass) throws DAOFactoryException {
        return (DAO_CLASS) FRIEND_DAOS.get(daoClass);
    }
}
