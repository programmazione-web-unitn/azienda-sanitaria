/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.filters;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.EsameDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.PazienteDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Esame;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoDiBase;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoSpecialista;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Paziente;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Ssp;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Utente;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author davide Filtro che verifica se l'esame richiesto appartiene al
 * paziente loggato oppure ad un paziente del medico loggato
 */
public class EsameFilter implements Filter {

    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured. 
    private FilterConfig filterConfig = null;
    private static final Logger logger = LogManager.getLogger(EsameFilter.class);

    public EsameFilter() {
    }

    private int doBeforeProcessing(ServletRequest servletRequest, ServletResponse response)
            throws IOException, ServletException {

        EsameDAO esameDao;
        PazienteDAO pazienteDao;
        DAOFactory daoFactory = (DAOFactory) servletRequest.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory");
        }
        try {
            esameDao = daoFactory.getDAO(EsameDAO.class);
            pazienteDao = daoFactory.getDAO(PazienteDAO.class);
        } catch (DAOFactoryException ex) {
            logger.error("Impossibile ottenere il dao factory", ex);
            throw new ServletException("Impossibile ottenere il dao factory", ex);
        }

        if (servletRequest instanceof HttpServletRequest) {
            HttpServletRequest request = (HttpServletRequest) servletRequest;
            Utente user = (Utente) request.getAttribute("user");

            int idEsame;
            try {
                idEsame = Integer.parseInt(request.getParameter("idEsame"));
            } catch (NumberFormatException ex) {
                logger.debug("id esame '" + request.getParameter("idEsame") + "' non valido", ex);
                return HttpServletResponse.SC_BAD_REQUEST;
            }

            Esame esame;
            try {
                esame = esameDao.getByPrimaryKey(idEsame);
            } catch (DAOException ex) {
                logger.debug("id esame '" + request.getParameter("idEsame") + "' non valido", ex);
                return HttpServletResponse.SC_NOT_FOUND;
            }

            if (user == null) {
                logger.error("Nessun utente loggato");
                return HttpServletResponse.SC_FORBIDDEN;
            } else if (user instanceof MedicoDiBase) {
                try {
                    if (!esameDao.isEsameDiUnPaziente(idEsame, user.getId()) && !esame.getPaziente().getId().equals(user.getId())) {
                        logger.debug("Il medico di base id='" + user.getId() + "' non può vedere l'esame id='" + idEsame + "'");
                        return HttpServletResponse.SC_FORBIDDEN;
                    }
                } catch (DAOException ex) {
                    logger.error("Impossibile verificare se l'esame appartiene ad un paziente del medico", ex);
                    throw new ServletException("Impossibile verificare se l'esame appartiene ad un paziente del medico", ex);
                }
            } else if (user instanceof MedicoSpecialista) {

            } else if (user instanceof Paziente) {
                if (!esame.getPaziente().getId().equals(user.getId())) {
                    logger.debug("Il paziente id='" + user.getId() + "' non può vedere l'esame id='" + idEsame + "'");
                    return HttpServletResponse.SC_FORBIDDEN;
                }
            } else if (user instanceof Ssp) {
                Paziente paziente;
                try {
                    paziente = pazienteDao.getByPrimaryKey(esame.getPaziente().getId());
                } catch (DAOException ex) {
                    logger.error("Impossibile caricare il paziente", ex);
                    throw new ServletException("Impossibile caricare il paziente", ex);
                }
                if (!paziente.getProvincia().equals(((Ssp) user).getProvincia())) {
                    logger.debug("Il SSP id='" + user.getId() + "' non può vedere l'esame id='" + idEsame + "'");
                    return HttpServletResponse.SC_FORBIDDEN;
                }
            } else {
                logger.debug("Accesso proibito all'untente con id='" + user.getId() + "'");
                return HttpServletResponse.SC_FORBIDDEN;
            }

            request.setAttribute("esame", esame);

            logger.debug("accesso all'esame id='" + esame.getId() + "' consentito");
            return HttpServletResponse.SC_OK;
        }
        return HttpServletResponse.SC_FORBIDDEN;
    }

    private void doAfterProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
    }

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {

        logger.debug("doFilter()");

        int statoRichiesta = doBeforeProcessing(request, response);

        if (statoRichiesta == HttpServletResponse.SC_OK) {
            chain.doFilter(request, response);
        } else if (response instanceof HttpServletResponse) {
            ((HttpServletResponse) response).sendError(statoRichiesta);
        }

        doAfterProcessing(request, response);

    }

    /**
     * Return the filter configuration object for this filter.
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    public void destroy() {
    }

    /**
     * Init method for this filter
     */
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            logger.debug("Initializing filter");
        }
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("EsameFilter()");
        }
        StringBuffer sb = new StringBuffer("EsameFilter(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }

}
