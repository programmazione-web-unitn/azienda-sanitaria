/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.entities;

import java.sql.Timestamp;

/**
 *
 * @author fran
 */
public class Ricetta {

    private Integer      id;
    private Farmaco      farmaco;
    private Integer      quantita;
    private MedicoDiBase medicoDiBase;
    private Paziente     paziente;
    private Timestamp    dataOraPrescrizione;
    private Farmacia     farmacia;
    private Timestamp    dataOraErogazione;

    public Ricetta(Integer id, Farmaco farmaco, Integer quantita, MedicoDiBase medicoDiBase, 
            Paziente paziente, Timestamp dataOraPrescrizione, Farmacia farmacia, Timestamp dataOraErogazione) {
        this.id = id;
        this.farmaco = farmaco;
        this.quantita = quantita;
        this.medicoDiBase = medicoDiBase;
        this.paziente = paziente;
        this.dataOraPrescrizione = dataOraPrescrizione;
        this.farmacia = farmacia;
        this.dataOraErogazione = dataOraErogazione;
    }
    
    public Ricetta(Integer id, Farmaco farmaco, Integer quantita, MedicoDiBase medicoDiBase, 
            Paziente paziente, Timestamp dataOraPrescrizione){
        this(id, farmaco, quantita, medicoDiBase, paziente, dataOraPrescrizione, null, null);
    }
    
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the farmaco
     */
    public Farmaco getFarmaco() {
        return farmaco;
    }

    /**
     * @param farmaco the farmaco to set
     */
    public void setFarmaco(Farmaco farmaco) {
        this.farmaco = farmaco;
    }

    /**
     * @return the quantita
     */
    public Integer getQuantita() {
        return quantita;
    }

    /**
     * @param quantita the quantita to set
     */
    public void setQuantita(Integer quantita) {
        this.quantita = quantita;
    }

    /**
     * @return the medicoDiBase
     */
    public MedicoDiBase getMedicoDiBase() {
        return medicoDiBase;
    }

    /**
     * @param medicoDiBase the medicoDiBase to set
     */
    public void setMedicoDiBase(MedicoDiBase medicoDiBase) {
        this.medicoDiBase = medicoDiBase;
    }

    /**
     * @return the idPaziente
     */
    public Paziente getPaziente() {
        return paziente;
    }

    /**
     * @param idPaziente the idPaziente to set
     */
    public void setPaziente(Paziente paziente) {
        this.paziente = paziente;
    }

    /**
     * @return the dataOraPrescrizione
     */
    public Timestamp getDataOraPrescrizione() {
        return dataOraPrescrizione;
    }

    /**
     * @param dataOraPrescrizione the dataOraPrescrizione to set
     */
    public void setDataOraPrescrizione(Timestamp dataOraPrescrizione) {
        this.dataOraPrescrizione = dataOraPrescrizione;
    }

    /**
     * @return the farmacia
     */
    public Farmacia getFarmacia() {
        return farmacia;
    }

    /**
     * @param farmacia the idFarmacia to set
     */
    public void setFarmacia(Farmacia farmacia) {
        this.farmacia = farmacia;
    }

    /**
     * @return the dataOraErogazione
     */
    public Timestamp getDataOraErogazione() {
        return dataOraErogazione;
    }

    /**
     * @param dataOraErogazione the dataOraErogazione to set
     */
    public void setDataOraErogazione(Timestamp dataOraErogazione) {
        this.dataOraErogazione = dataOraErogazione;
    }
    
    
}
