/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.entities;

import java.sql.Timestamp;

/**
 *
 * @author gabriele
 */
public class Farmacia extends Utente {

    private String comune;
    private Provincia provincia;    
    private String nome;
    
    public Farmacia(Integer id, String email, String password, String salt, String tipo, String nome, String comune, Provincia provincia) {
        super(id, email, password, salt, tipo);
        this.nome = nome;
        this.comune = comune;
        this.provincia = provincia;
    }

    public Farmacia(Integer id, String email, String password, String salt, String tipo, String recuperoToken, Timestamp scadenzaRecupero, String token, Timestamp scadenzaToken, String nome, String comune, Provincia provincia) {
        super(id, email, password, salt, tipo, recuperoToken, scadenzaRecupero, token, scadenzaToken);
        this.nome = nome;
        this.comune = comune;
        this.provincia = provincia;
    }

    public Farmacia(Integer id, String nome, String comune, Provincia provincia) {
        this.id = id;
        this.nome = nome;
        this.comune = comune;
        this.provincia = provincia;
    }
    
    
    /**
     * @return the comune
     */
    public String getComune() {
        return comune;
    }

    /**
     * @param comune the comune to set
     */
    public void setComune(String comune) {
        this.comune = comune;
    }

    /**
     * @return the provincia
     */
    public Provincia getProvincia() {
        return provincia;
    }

    /**
     * @param provincia the provincia to set
     */
    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
}
