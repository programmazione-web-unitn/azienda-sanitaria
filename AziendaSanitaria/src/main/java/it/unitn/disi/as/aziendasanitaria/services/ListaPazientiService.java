/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.services;

import it.unitn.disi.as.aziendasanitaria.persistence.entities.Result;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Element;
import com.google.gson.Gson;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.PazienteDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Farmacia;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoDiBase;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoSpecialista;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Paziente;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Ssn;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Ssp;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Utente;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * REST Web Service
 *
 * @author davide
 */

@Path("listaPazienti")
public class ListaPazientiService {
    
    @Context 
    private ServletContext servletContext;

    private PazienteDAO pazienteDao;
    private static final Logger logger = LogManager.getLogger(ListaPazientiService.class);
    
    /**
     * Creates a new instance of ListaPazientiService
     */
    public ListaPazientiService() {
    }

    /**
     * Retrieves representation of an instance of it.unitn.disi.as.aziendasanitaria.services.ListaPazientiService
     * @param term
     * @param request
     * @return an instance of java.lang.String
     * @throws javax.servlet.ServletException
     */
    @GET
    @Path("{term}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson(@PathParam("term") String term, @Context HttpServletRequest request) throws ServletException {
        
        DAOFactory daoFactory = (DAOFactory) servletContext.getAttribute("daoFactory");
        try {
            pazienteDao = daoFactory.getDAO(PazienteDAO.class);
        } catch (DAOFactoryException ex) {
            logger.error("Impossibile ottenere il dao paziente", ex);
            throw new ServletException("Impossibile ottenere il dao paziente", ex);
        }
        
        Utente user = (Utente) request.getAttribute("user");

        List<Element> results = new ArrayList<>();
        List<Paziente> pazienti = new ArrayList<Paziente>();
        try {
            if(user instanceof MedicoDiBase)
                pazienti = pazienteDao.getByMedicoDiBase((MedicoDiBase) user);

            else if(user instanceof MedicoSpecialista || user instanceof Farmacia || user instanceof Ssn)
                pazienti = pazienteDao.getAll();
            else if(user instanceof Ssp)
                pazienti = pazienteDao.getBySsp((Ssp) user);
        } catch (DAOException ex) {
            logger.error("Impossibile ottenere la lista dei pazienti", ex);
            throw new ServletException("Impossibile ottenere la lista dei pazienti", ex);
        }                
        
        if ((term == null) || term.length() == 0) {
            for (int i = 0; i < pazienti.size(); i++) {
                Paziente p = pazienti.get(i);
                results.add(new Element(p.getId(), p.getNome() + " " + p.getCognome()));
            }
        } else {
            term = term.toLowerCase();
            for (int i = 0; i < pazienti.size(); i++) {
                Paziente p = pazienti.get(i);
                if((p.getNome().toLowerCase()+" "+p.getCognome().toLowerCase()).contains(term) ||
                        p.getCodiceFiscale().toLowerCase().contains(term)){
                    results.add(new Element(p.getId(), p.getNome() + " " + p.getCognome()));
                }
            }
        }

        Gson gson = new Gson();
        return gson.toJson(new Result(results.toArray(new Element[0])));
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson(@Context HttpServletRequest request) throws ServletException {
        return getJson(null, request);
    }
    
}
