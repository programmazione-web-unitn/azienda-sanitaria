/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao;

import it.unitn.disi.as.aziendasanitaria.persistence.entities.Farmaco;
import it.unitn.disi.wp.commons.persistence.dao.DAO;

/**
 *
 * @author gabriele
 */
public interface FarmacoDAO extends DAO<Farmaco, Integer> {
    
}
