/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.servlets;

import it.unitn.disi.as.aziendasanitaria.persistence.entities.Foto;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author dima
 */
public class CaricaFotoServlet extends HttpServlet {

    private static final Logger logger = LogManager.getLogger(CaricaFotoServlet.class);

    public String uploadDir;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        uploadDir = getServletContext().getInitParameter("uploadDir");
        if (uploadDir == null) {
            logger.error("Il parametro uploadDir non è definito nel web.xml");
            throw new ServletException("Il parametro uploadDir non è definito nel web.xml");
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Foto foto = (Foto) request.getAttribute("foto");

        if (foto == null) {
            logger.debug("Foto non presente nella request");
            throw new ServletException("Foto non presente nella request");
        }

        String idPaziente = Integer.toString(foto.getPaziente().getId());
        // Get the absolute path of the image
        String filename = uploadDir + "/" + idPaziente + "/foto/" + foto.getPathFoto();
        File file = new File(filename);
        if (!(file.exists() && file.isFile())) {
            logger.debug("Il file della foto'" + filename + "' non esiste");
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        // retrieve mimeType dynamically
        String mime = request.getServletContext().getMimeType(filename);
        if (mime == null) {
            logger.info("Errore nell'acquisizione del formato mime del file +'" + filename + "'");
            throw new ServletException("Errore nell'acquisizione del formato mime del file +'" + filename + "'");
        }
        response.setContentType(mime);
        response.setContentLength((int) file.length());
        try ( OutputStream out = response.getOutputStream()) {
            // Copy the contents of the file to the output stream
            byte[] buf = new byte[1024 * 1024 * 5];
            FileInputStream in = new FileInputStream(file);
            int count;
            while ((count = in.read(buf)) >= 0) {
                out.write(buf, 0, count);
            }
        } catch (Exception e) {
            logger.info("Errore nel fornire il file +'" + filename + "'");
            throw new ServletException("Errore nel fornire il file +'" + filename + "'");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
