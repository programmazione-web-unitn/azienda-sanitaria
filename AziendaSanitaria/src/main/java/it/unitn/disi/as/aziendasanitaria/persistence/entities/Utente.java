/*
 * AziendaSanitaria
 */
package it.unitn.disi.as.aziendasanitaria.persistence.entities;

import java.sql.Timestamp;

/**
 *
 * @author gabriele
 */
public class Utente {

    protected Integer id;
    protected String email;
    protected String password;
    protected String salt;
    protected String tipo;
    protected String recuperoToken;
    protected Timestamp scadenzaRecupero;
    protected String token;
    protected Timestamp scadenzaToken;

    public Utente() {
    }

    public Utente(Integer id, String email, String password, String salt, String tipo) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.salt = salt;
        this.tipo = tipo;        
    }
    
    public Utente(Integer id, String email, String password, String salt, String tipo, 
            String recuperoToken, Timestamp scadenzaRecupero, String token, Timestamp scadenzaToken) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.salt = salt;
        this.tipo = tipo;
        this.recuperoToken = recuperoToken;
        this.scadenzaRecupero = scadenzaRecupero;
        this.token = token;
        this.scadenzaToken = scadenzaToken;
    }
    
    
    
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the salt
     */
    public String getSalt() {
        return salt;
    }

    /**
     * @param salt the salt to set
     */
    public void setSalt(String salt) {
        this.salt = salt;
    }

    /**
     * @return the recuperoToken
     */
    public String getRecuperoToken() {
        return recuperoToken;
    }

    /**
     * @param recuperoToken the recuperoToken to set
     */
    public void setRecuperoToken(String recuperoToken) {
        this.recuperoToken = recuperoToken;
    }

    /**
     * @return the scadenzaRecupero
     */
    public Timestamp getScadenzaRecupero() {
        return scadenzaRecupero;
    }

    /**
     * @param scadenzaRecupero the scadenzaRecupero to set
     */
    public void setScadenzaRecupero(Timestamp scadenzaRecupero) {
        this.scadenzaRecupero = scadenzaRecupero;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return the scadenzaToken
     */
    public Timestamp getScadenzaToken() {
        return scadenzaToken;
    }

    /**
     * @param scadenzaToken the scadenzaToken to set
     */
    public void setScadenzaToken(Timestamp scadenzaToken) {
        this.scadenzaToken = scadenzaToken;
    }

}
