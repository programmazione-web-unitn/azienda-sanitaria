/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.entities;

import java.sql.Timestamp;

/**
 *
 * @author gabriele
 */
public class Ssp extends Utente {
    
    private Provincia provincia;

    public Ssp(Integer id, String email, String password, String salt, String tipo, 
            String recuperoToken, Timestamp scadenzaRecupero, String token, 
            Timestamp scadenzaToken, Provincia provincia) {
        super(id, email, password, salt, tipo, recuperoToken, scadenzaRecupero, token, scadenzaToken);
        this.provincia = provincia;
    }
    
    public Ssp(Integer id, Provincia provincia){
        this.id = id;
        this.provincia = provincia;
    }

    /**
     * @return the provincia
     */
    public Provincia getProvincia() {
        return provincia;
    }

    /**
     * @param provincia the provincia to set
     */
    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }
}
