/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao.jdbc_psql;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.TipoDiEsameDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Ricetta;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.TipoDiEsame;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gabriele
 */
public class JDBC_PSQLTipoDiEsameDAO extends JDBC_PSQL_DAO<TipoDiEsame, Integer> implements TipoDiEsameDAO {

    public JDBC_PSQLTipoDiEsameDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try ( Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM tipo_di_esame");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count tipo di esame", ex);
        }

        return 0L;
    }

    @Override
    public TipoDiEsame getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement("SELECT * FROM tipo_di_esame WHERE id = ?")) {
            stm.setInt(1, primaryKey);
            try ( ResultSet rs = stm.executeQuery()) {

                rs.next();
                TipoDiEsame tipoDiEsame = new TipoDiEsame(rs.getInt("id"),
                        rs.getString("nome"),
                        rs.getInt("ticket")
                );
                return tipoDiEsame;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the tipo di esame for the passed primary key", ex);
        }
    }

    @Override
    public List<TipoDiEsame> getAll() throws DAOException {
        List<TipoDiEsame> tipiDiEsame = new ArrayList<>();

        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery("SELECT * FROM tipo_di_esame ORDER BY nome")) {

                while (rs.next()) {
                    TipoDiEsame tipoDiEsame = new TipoDiEsame(rs.getInt("id"),
                            rs.getString("nome"),
                            rs.getInt("ticket")
                    );
                    tipiDiEsame.add(tipoDiEsame);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of tipo di esame", ex);
        }

        return tipiDiEsame;
    }

}
