/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.servlets;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.EsameDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Esame;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Paziente;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Ssp;
import it.unitn.disi.as.aziendasanitaria.utilities.Authentication;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author dima
 */
public class ErogazioneEsameServlet extends HttpServlet {

    private EsameDAO esameDao;
    
    private static final Logger logger = LogManager.getLogger(ErogazioneEsameServlet.class);
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory");
        }
        try {
            esameDao = daoFactory.getDAO(EsameDAO.class);
        } catch (DAOFactoryException ex) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory", ex);
        }
        
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Ssp ssp = (Ssp) request.getAttribute("user");
        if(ssp == null){
            logger.error("Nessun ssp loggato");
            throw new ServletException("Nessun ssp loggato");
        }
        
        Esame esame = (Esame) request.getAttribute("esame");
        if(esame == null){
            logger.error("Nessun esame indicato");
            throw new ServletException("Nessun esame indicato");
        }
        
        Paziente paziente = (Paziente) request.getAttribute("paziente");
        if(paziente == null){
            logger.error("Nessun paziente indicato");
            throw new ServletException("Nessun paziente indicato");
        }
        
        String report = request.getParameter("report");
        if(report != null && report.trim().isEmpty()){
            logger.debug("Nessun report indicato");
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        
        Timestamp now = new Timestamp(new Date().getTime());
        esame.setDataOraErogazione(now);
        esame.setSsp(ssp);
        esame.setReport(report);
        try {
            esameDao.update(esame);
            logger.debug("esame aggionato nel database");
        } catch(DAOException ex) {
            logger.error("Impossibile aggiornare l'esame nel database", ex);
            throw new ServletException("Impossibile aggionare l'esame nel database", ex);
        }
        
        request.getSession().setAttribute("success", "Esame erogato correttamente");
        Authentication.redirectTo("Ssp/paziente?idPaziente=" + paziente.getId(), request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
