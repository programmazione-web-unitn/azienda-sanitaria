/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.filters;

import it.unitn.disi.as.aziendasanitaria.persistence.entities.Farmacia;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Utente;
import it.unitn.disi.as.aziendasanitaria.utilities.Authentication;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author davide
 */
public class FarmaciaFilter implements Filter {
    
    private static final Logger logger = LogManager.getLogger(FarmaciaFilter.class);

    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured. 
    private FilterConfig filterConfig = null;
    public FarmaciaFilter() {
    }

    private int doBeforeProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {

        if (request instanceof HttpServletRequest) {
            Utente user = (Utente) ((HttpServletRequest) request).getAttribute("user");
            if (user == null) {
                logger.error("Nessun utente loggato");
                return HttpServletResponse.SC_FORBIDDEN;
            } else if (user instanceof Farmacia) {
                return HttpServletResponse.SC_OK;
            } else {
                logger.debug("Accesso proibito all'untente con id='"+user.getId()+"'");
            }

        }
        return HttpServletResponse.SC_FORBIDDEN;
    }

    private void doAfterProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
    }

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {        
        logger.debug("doFilter()");
        
        int statoRichiesta = doBeforeProcessing(request, response);

        if (statoRichiesta == HttpServletResponse.SC_OK) {
            chain.doFilter(request, response);
        } else if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
            if (request.getAttribute("user") != null) {
                ((HttpServletResponse) response).sendError(statoRichiesta);
            } else {
                Authentication.redirectToLogin((HttpServletRequest) request, (HttpServletResponse) response);
            }
        }
        
        doAfterProcessing(request, response);

    }

    /**
     * Return the filter configuration object for this filter.
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    public void destroy() {
    }

    /**
     * Init method for this filter
     */
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
        if (filterConfig != null) {

            logger.debug("Initializing filter");
        }
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("FarmaciaFilter()");
        }
        StringBuffer sb = new StringBuffer("FarmaciaFilter(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }

}
