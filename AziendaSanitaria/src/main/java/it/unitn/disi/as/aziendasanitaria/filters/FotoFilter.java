/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.filters;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.FotoDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Farmacia;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Foto;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoDiBase;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoSpecialista;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Paziente;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Ssp;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Utente;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.io.IOException;
import java.util.List;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;

/**
 *
 * @author davide Filtro che verifica se la foto richiesta appartiene al
 * paziente loggato oppure ad un paziente del medico loggato
 */
public class FotoFilter implements Filter {

    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured. 
    private FilterConfig filterConfig = null;
    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(FotoFilter.class);

    public FotoFilter() {
    }

    private int doBeforeProcessing(ServletRequest servletRequest, ServletResponse response)
            throws IOException, ServletException {

        FotoDAO fotoDao;
        DAOFactory daoFactory = (DAOFactory) servletRequest.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory");
        }
        try {
            fotoDao = daoFactory.getDAO(FotoDAO.class);
        } catch (DAOFactoryException ex) {
            logger.error("Impossibile ottenere il dao factory", ex);
            throw new ServletException("Impossibile ottenere il dao factory", ex);
        }

        if (servletRequest instanceof HttpServletRequest) {
            HttpServletRequest request = (HttpServletRequest) servletRequest;
            Utente user = (Utente) request.getAttribute("user");

            if(request.getParameter("fotoProfilo") != null){
                Foto foto;
                try {
                    List<Foto> fotoPaziente = fotoDao.getByPaziente(user.getId());
                    foto = fotoPaziente.get(fotoPaziente.size()-1);
                } catch (DAOException ex) {
                    logger.error("Impossibile caricare la foto profilo del paziente", ex);
                    throw new ServletException("Impossibile caricare la foto profilo del paziente", ex);
                }
                
                request.setAttribute("foto", foto);
                logger.debug("accesso alla foto profilo consentito");
                return HttpServletResponse.SC_OK;
            }
            
            int idFoto;
            try {
                idFoto = Integer.parseInt(request.getParameter("idFoto"));
            } catch (NumberFormatException ex) {
                logger.debug("id foto '" + request.getParameter("idFoto") + "' non valido", ex);
                return HttpServletResponse.SC_BAD_REQUEST;
            }

            Foto foto;
            try {
                foto = fotoDao.getByPrimaryKey(idFoto);
            } catch (DAOException ex) {
                logger.debug("id foto '" + request.getParameter("idFoto") + "' non valido", ex);
                return HttpServletResponse.SC_NOT_FOUND;
            }

            if (user == null) {
                logger.error("Nessun utente loggato");
                return HttpServletResponse.SC_FORBIDDEN;
            } else if (user instanceof MedicoDiBase) {
                try {
                    if (!fotoDao.isFotoDiUnPaziente(idFoto, user.getId()) && !foto.getPaziente().getId().equals(user.getId())) {
                        logger.debug("Il medico di base id='" + user.getId() + "' non può vedere la foto id='" + idFoto + "'");
                        return HttpServletResponse.SC_FORBIDDEN;
                    }
                } catch (DAOException ex) {
                    logger.error("Impossibile verificare se la foto appartiene ad un paziente del medico", ex);
                    throw new ServletException("Impossibile verificare se la foto appartiene ad un paziente del medico", ex);
                }
            } else if (user instanceof MedicoSpecialista) {

            } else if (user instanceof Farmacia) {

            } else if (user instanceof Paziente) {
                if (!foto.getPaziente().getId().equals(user.getId())) {
                    logger.debug("Il paziente id='" + user.getId() + "' non può vedere la foto id='" + idFoto + "'");
                    return HttpServletResponse.SC_FORBIDDEN;
                }
            } else if (user instanceof Ssp) {
                if (!foto.getPaziente().getProvincia().equals(((Ssp) user).getProvincia())) {
                    logger.debug("Il SSP id='" + user.getId() + "' non può vedere la foto id='" + idFoto + "'");
                    return HttpServletResponse.SC_FORBIDDEN;
                }
            } else {
                logger.debug("Accesso proibito all'untente con id='" + user.getId() + "'");
                return HttpServletResponse.SC_FORBIDDEN;
            }

            request.setAttribute("foto", foto);

            logger.debug("accesso alla foto consentito");
            return HttpServletResponse.SC_OK;
        }
        return HttpServletResponse.SC_FORBIDDEN;
    }

    private void doAfterProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
    }

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {

        logger.debug("doFilter()");

        int statoRichiesta = doBeforeProcessing(request, response);

        if (statoRichiesta == HttpServletResponse.SC_OK) {
            chain.doFilter(request, response);
        } else if (response instanceof HttpServletResponse) {
            ((HttpServletResponse) response).sendError(statoRichiesta);
        }
        
        doAfterProcessing(request, response);

    }

    /**
     * Return the filter configuration object for this filter.
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    public void destroy() {
    }

    /**
     * Init method for this filter
     */
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            logger.debug("Initializing filter");
        }
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("FotoFilter()");
        }
        StringBuffer sb = new StringBuffer("FotoFilter(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }

}
