/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.servlets;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.VisitaDiBaseDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoDiBase;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Paziente;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.VisitaDiBase;
import it.unitn.disi.as.aziendasanitaria.utilities.Authentication;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author davide
 */
public class VisitaDiBaseServlet extends HttpServlet {

    private VisitaDiBaseDAO visitaDiBaseDao;
    
    private static final Logger logger = LogManager.getLogger(VisitaDiBaseServlet.class);
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory");
        }
        try {
            visitaDiBaseDao = daoFactory.getDAO(VisitaDiBaseDAO.class);
        } catch (DAOFactoryException ex) {
            logger.error("Impossibile ottenere il dao visitaDiBase");
            throw new ServletException("Impossibile ottenere il dao visitaDiBase", ex);
        }
        
    }
    
    /**
     * Inserisce una visita di base (con report) nel database
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        MedicoDiBase medico = (MedicoDiBase) request.getAttribute("user");
        if(medico == null){
            logger.error("Nessun medico loggato");
            throw new ServletException("Nessun medico loggato");
        }
        
        Paziente paziente = (Paziente) request.getAttribute("paziente");
        if(paziente == null){
            logger.error("Nessun paziente indicato");
            throw new ServletException("Nessun paziente indicato");
        }
        
        String report = request.getParameter("report");
        if(report != null && report.trim().isEmpty()){
            report = null;
        }
        
        Timestamp now = new Timestamp(new Date().getTime());
        VisitaDiBase visitaDiBase = new VisitaDiBase(null, medico, paziente, now, report);
        
        try {
            visitaDiBaseDao.insert(visitaDiBase);
            logger.debug("visita di base inserita nel database");
        } catch(DAOException ex) {
            logger.error("Impossibile inserire la visita di base", ex);
            throw new ServletException("Impossibile inserire la visita di base", ex);
        }
        
        request.getSession().setAttribute("success", "Report inserito correttamente");
        Authentication.redirectTo("Medico/paziente?idPaziente=" + paziente.getId(), request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
