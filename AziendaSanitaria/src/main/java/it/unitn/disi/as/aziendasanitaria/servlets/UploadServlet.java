/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.servlets;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.FotoDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Foto;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Paziente;
import it.unitn.disi.as.aziendasanitaria.utilities.Authentication;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Timestamp;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author giovanni
 */
@MultipartConfig
public class UploadServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private int MAX_UPLOAD_SIZE = 2000000;
    private String uploadDir;
    private FotoDAO fotoDao;
    private static final Logger logger = LogManager.getLogger(UploadServlet.class);

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        uploadDir = getServletContext().getInitParameter("uploadDir");
        if (uploadDir == null) {
            logger.error("Il parametro uploadDir non è definito nel web.xml");
            throw new ServletException("Il parametro uploadDir non è definito nel web.xml");
        }

        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory");
        }
        try {
            fotoDao = daoFactory.getDAO(FotoDAO.class);
        } catch (DAOFactoryException ex) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory", ex);
        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Paziente paziente = (Paziente) request.getAttribute("user");
        if (paziente == null) {
            logger.error("Nessun paziente loggato");
            throw new ServletException("Nessun paziente loggato");
        }

        response.setContentType("text/plain");
        try {

            File uploadDirFile = new File(uploadDir + "/" + Integer.toString(paziente.getId()) + "/foto");
            DateTimeFormatter dtfname = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss");
            DateTimeFormatter dtfdate = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime now = LocalDateTime.now();

            Part filePart = request.getPart("uploadedImage");
            if (filePart == null) {
                logger.debug("Tipo di file non valido");
                request.getSession().setAttribute("error", "Tipo di file non valido");
                Authentication.redirectTo("Paziente/profilo", request, response);
                return;
            }
            String filename;
            String datetime;
            if (filePart.getContentType().startsWith("image")) {
                filename = dtfname.format(now) + "." + filePart.getContentType().substring(6);
                datetime = dtfdate.format(now);
            } else {
                logger.debug("Tipo di file non valido");
                request.getSession().setAttribute("error", "Tipo di file non valido");
                Authentication.redirectTo("Paziente/profilo", request, response);
                return;
            }
            if (filePart != null && filePart.getSize() > 0) {
                File file = new File(uploadDirFile, filename);
                try (InputStream fileContent = filePart.getInputStream()) {
                    byte[] bytes = IOUtils.toByteArray(fileContent);
                    logger.debug("Grandezza immagine caricata " + bytes.length);
                    if(bytes.length > MAX_UPLOAD_SIZE){
                        logger.debug("Immagine selezionata troppo grande");
                        request.getSession().setAttribute("error", "L'immagine selezionata supera i "+ (MAX_UPLOAD_SIZE/1000000) + " MB");
                        Authentication.redirectTo("Paziente/profilo", request, response);
                        return;
                    }
                    try {
                        OutputStream os = new FileOutputStream(file);
                        os.write(bytes);
                        os.close();
                    } catch (Exception e) {
                        logger.error("Impossibile copiare la foto", e);
                        request.getSession().setAttribute("error", "Impossibile caricare la foto");
                        Authentication.redirectTo("Paziente/profilo", request, response);
                        return;
                    }
                }

                logger.debug("Immagine caricata: " + file.getAbsolutePath());

                Foto foto = new Foto(null, paziente, filename, Timestamp.valueOf(datetime));
                fotoDao.insert(foto);

                logger.debug("Foto cambiata correttamente");
                request.getSession().setAttribute("success", "Foto aggiornata");

            } else {
                logger.debug("Foto caricata non valida");
                request.getSession().setAttribute("error", "Impossibile caricare la foto");
            }

            Authentication.redirectTo("Paziente/profilo", request, response);

        } catch (DAOException e) {
            throw new ServletException("Impossibile caricare il file", e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        System.out.println("Entering DOGET");
//        processRequest(request, response);
//        System.out.println("Exit DOGET");
//    }
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
