/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao.jdbc_psql;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.VisitaSpecialisticaDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoDiBase;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoSpecialista;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Paziente;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Provincia;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.TipoDiVisita;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.VisitaSpecialistica;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fran
 */
public class JDBC_PSQLVisitaSpecialisticaDAO extends JDBC_PSQL_DAO<VisitaSpecialistica, Integer> implements VisitaSpecialisticaDAO {

    public JDBC_PSQLVisitaSpecialisticaDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try ( Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM visita_specialistica");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count visite specialistiche", ex);
        }
        return 0L;
    }

    @Override
    public VisitaSpecialistica getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT mb.id AS mb_id, mb.nome AS mb_nome, mb.cognome AS mb_cognome, "
                + "       ms.id AS ms_id, ms.nome AS ms_nome, ms.cognome AS ms_cognome,  "
                + "       p.id AS p_id, p.nome AS p_nome, p.cognome AS p_cognome,  "
                + "       pr.sigla as pr_sigla, pr.nome AS pr_nome, pr.regione AS pr_regione, "
                + "       v.id AS v_id,  "
                + "       v.data_ora_prescrizione AS v_data_ora_prescrizione, "
                + "       v.data_ora_erogazione AS v_data_ora_erogazione,  "
                + "       v.report AS v_report, "
                + "       tv.nome AS tv_nome, tv.id AS tv_id , tv.ticket AS tv_ticket "
                + "FROM visita_specialistica AS v "
                + "LEFT JOIN paziente AS mb ON v.id_medico_di_base = mb.id  "
                + "JOIN paziente AS p ON v.id_paziente = p.id "
                + "JOIN provincia AS pr ON p.sigla_provincia = pr.sigla "
                + "LEFT JOIN paziente AS ms ON v.id_medico_spec = ms.id "
                + "JOIN tipo_di_visita AS tv ON v.id_tipo_visita = tv.id  "
                + "WHERE v.id = ? ")) {
            stm.setInt(1, primaryKey);
            try ( ResultSet rs = stm.executeQuery()) {

                rs.next();
                MedicoDiBase medicoDiBase = null;
                if (rs.getObject("mb_id") != null) {
                    medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                            rs.getString("mb_nome"),
                            rs.getString("mb_cognome"));
                }

                Provincia provincia = new Provincia(rs.getString("pr_sigla"),
                        rs.getString("pr_nome"),
                        rs.getString("pr_regione"));

                Paziente paziente = new Paziente(rs.getInt("p_id"),
                        rs.getString("p_nome"),
                        rs.getString("p_cognome"));
                paziente.setProvincia(provincia);
                MedicoSpecialista medicoSpecialista = null;
                if (rs.getObject("ms_id") != null) {
                    medicoSpecialista = new MedicoSpecialista(rs.getInt("ms_id"),
                            rs.getString("ms_nome"),
                            rs.getString("ms_cognome"));
                }
                TipoDiVisita tipoVisita = new TipoDiVisita(rs.getInt("tv_id"),
                        rs.getString("tv_nome"),
                        rs.getInt("tv_ticket")
                );
                VisitaSpecialistica visita = new VisitaSpecialistica(rs.getInt("v_id"),
                        medicoDiBase,
                        paziente,
                        tipoVisita,
                        rs.getTimestamp("v_data_ora_prescrizione"),
                        medicoSpecialista,
                        rs.getTimestamp("v_data_ora_erogazione"),
                        rs.getString("v_report"));
                return visita;

            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of last user's visite specialistiche", ex);
        }
    }

    @Override
    public List<VisitaSpecialistica> getAll() throws DAOException {
        List<VisitaSpecialistica> visiteSpecialistiche = new ArrayList<>();

        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery(""
                    + "SELECT mb.id AS mb_id, mb.nome AS mb_nome, mb.cognome AS mb_cognome, "
                    + "       ms.id AS ms_id, ms.nome AS ms_nome, ms.cognome AS ms_cognome,  "
                    + "       p.id AS p_id, p.nome AS p_nome, p.cognome AS p_cognome,  "
                    + "       v.id AS v_id,  "
                    + "       v.data_ora_prescrizione AS v_data_ora_prescrizione, "
                    + "       v.data_ora_erogazione AS v_data_ora_erogazione,  "
                    + "       v.report AS v_report, "
                    + "       tv.nome AS tv_nome, tv.id AS tv_id, tv.ticket AS tv_ticket "
                    + "FROM visita_specialistica AS v "
                    + "LEFT JOIN paziente AS mb ON v.id_medico_di_base = mb.id  "
                    + "JOIN paziente AS p ON v.id_paziente = p.id "
                    + "LEFT JOIN paziente AS ms ON v.id_medico_spec = ms.id "
                    + "JOIN tipo_di_visita AS tv ON v.id_tipo_visita = tv.id  ")) {

                while (rs.next()) {
                    MedicoDiBase medicoDiBase = null;
                    if (rs.getObject("mb_id") != null) {
                        medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                                rs.getString("mb_nome"),
                                rs.getString("mb_cognome"));
                    }
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome"));
                    MedicoSpecialista medicoSpecialista = null;
                    if (rs.getObject("ms_id") != null) {
                        medicoSpecialista = new MedicoSpecialista(rs.getInt("ms_id"),
                                rs.getString("ms_nome"),
                                rs.getString("ms_cognome"));
                    }
                    TipoDiVisita tipoVisita = new TipoDiVisita(rs.getInt("tv_id"),
                            rs.getString("tv_nome"),
                            rs.getInt("tv_ticket")
                    );
                    VisitaSpecialistica visita = new VisitaSpecialistica(rs.getInt("v_id"),
                            medicoDiBase,
                            paziente,
                            tipoVisita,
                            rs.getTimestamp("v_data_ora_prescrizione"),
                            medicoSpecialista,
                            rs.getTimestamp("v_data_ora_erogazione"),
                            rs.getString("v_report"));

                    visiteSpecialistiche.add(visita);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of visite specialistiche", ex);
        }

        return visiteSpecialistiche;
    }

    @Override
    public List<VisitaSpecialistica> getByPaziente(Integer idPaziente) throws DAOException {
        if (idPaziente == null) {
            throw new DAOException("idPaziente is null");
        }

        List<VisitaSpecialistica> visiteSpecialistiche = new ArrayList<>();

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT mb.id AS mb_id, mb.nome AS mb_nome, mb.cognome AS mb_cognome, "
                + "       ms.id AS ms_id, ms.nome AS ms_nome, ms.cognome AS ms_cognome,  "
                + "       p.id AS p_id, p.nome AS p_nome, p.cognome AS p_cognome,  "
                + "       v.id AS v_id,  "
                + "       v.data_ora_prescrizione AS v_data_ora_prescrizione, "
                + "       v.data_ora_erogazione AS v_data_ora_erogazione,  "
                + "       v.report AS v_report, "
                + "       tv.nome AS tv_nome, tv.id AS tv_id, tv.ticket AS tv_ticket "
                + "FROM visita_specialistica AS v "
                + "LEFT JOIN paziente AS mb ON v.id_medico_di_base = mb.id  "
                + "JOIN paziente AS p ON v.id_paziente = p.id "
                + "LEFT JOIN paziente AS ms ON v.id_medico_spec = ms.id "
                + "JOIN tipo_di_visita AS tv ON v.id_tipo_visita = tv.id "
                + "WHERE p.id = ? ORDER BY v.data_ora_prescrizione ")) {
            stm.setInt(1, idPaziente);
            try ( ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    MedicoDiBase medicoDiBase = null;
                    if (rs.getObject("mb_id") != null) {
                        medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                                rs.getString("mb_nome"),
                                rs.getString("mb_cognome"));
                    }
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome"));
                    MedicoSpecialista medicoSpecialista = null;
                    if (rs.getObject("ms_id") != null) {
                        medicoSpecialista = new MedicoSpecialista(rs.getInt("ms_id"),
                                rs.getString("ms_nome"),
                                rs.getString("ms_cognome"));
                    }
                    TipoDiVisita tipoVisita = new TipoDiVisita(rs.getInt("tv_id"),
                            rs.getString("tv_nome"),
                            rs.getInt("tv_ticket")
                    );
                    VisitaSpecialistica visita = new VisitaSpecialistica(rs.getInt("v_id"),
                            medicoDiBase,
                            paziente,
                            tipoVisita,
                            rs.getTimestamp("v_data_ora_prescrizione"),
                            medicoSpecialista,
                            rs.getTimestamp("v_data_ora_erogazione"),
                            rs.getString("v_report"));

                    visiteSpecialistiche.add(visita);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of last user's visite specialistiche", ex);
        }

        return visiteSpecialistiche;
    }

    @Override
    public List<VisitaSpecialistica> getLastByPaziente(Integer idPaziente) throws DAOException {
        final int N_LAST_SPEC_EXAMINATIONS = 5;
        if (idPaziente == null) {
            throw new DAOException("idPaziente is null");
        }

        List<VisitaSpecialistica> visiteSpecialistiche = new ArrayList<>();

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT mb.id AS mb_id, mb.nome AS mb_nome, mb.cognome AS mb_cognome, "
                + "       ms.id AS ms_id, ms.nome AS ms_nome, ms.cognome AS ms_cognome,  "
                + "       p.id AS p_id, p.nome AS p_nome, p.cognome AS p_cognome,  "
                + "       v.id AS v_id,  "
                + "       v.data_ora_prescrizione AS v_data_ora_prescrizione, "
                + "       v.data_ora_erogazione AS v_data_ora_erogazione,  "
                + "       v.report AS v_report, "
                + "       tv.nome AS tv_nome, tv.id AS tv_id, tv.ticket AS tv_ticket "
                + "FROM visita_specialistica AS v "
                + "LEFT JOIN paziente AS mb ON v.id_medico_di_base = mb.id  "
                + "JOIN paziente AS p ON v.id_paziente = p.id "
                + "LEFT JOIN paziente AS ms ON v.id_medico_spec = ms.id "
                + "JOIN tipo_di_visita AS tv ON v.id_tipo_visita = tv.id "
                + "WHERE p.id = ? ORDER BY v.data_ora_prescrizione desc "
                + "LIMIT " + N_LAST_SPEC_EXAMINATIONS)) {
            stm.setInt(1, idPaziente);
            try ( ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    MedicoDiBase medicoDiBase = null;
                    if (rs.getObject("mb_id") != null) {
                        medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                                rs.getString("mb_nome"),
                                rs.getString("mb_cognome"));
                    }
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome"));
                    MedicoSpecialista medicoSpecialista = null;
                    if (rs.getObject("ms_id") != null) {
                        medicoSpecialista = new MedicoSpecialista(rs.getInt("ms_id"),
                                rs.getString("ms_nome"),
                                rs.getString("ms_cognome"));
                    }
                    TipoDiVisita tipoVisita = new TipoDiVisita(rs.getInt("tv_id"),
                            rs.getString("tv_nome"),
                            rs.getInt("tv_ticket")
                    );
                    VisitaSpecialistica visita = new VisitaSpecialistica(rs.getInt("v_id"),
                            medicoDiBase,
                            paziente,
                            tipoVisita,
                            rs.getTimestamp("v_data_ora_prescrizione"),
                            medicoSpecialista,
                            rs.getTimestamp("v_data_ora_erogazione"),
                            rs.getString("v_report"));

                    visiteSpecialistiche.add(visita);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of last user's exams", ex);
        }

        return visiteSpecialistiche;
    }

    @Override
    public List<VisitaSpecialistica> getErogateByPaziente(Integer idPaziente) throws DAOException {
        if (idPaziente == null) {
            throw new DAOException("idPaziente is null");
        }

        List<VisitaSpecialistica> visiteSpecialistiche = new ArrayList<>();

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT mb.id AS mb_id, mb.nome AS mb_nome, mb.cognome AS mb_cognome, "
                + "       ms.id AS ms_id, ms.nome AS ms_nome, ms.cognome AS ms_cognome,  "
                + "       p.id AS p_id, p.nome AS p_nome, p.cognome AS p_cognome,  "
                + "       v.id AS v_id,  "
                + "       v.data_ora_prescrizione AS v_data_ora_prescrizione, "
                + "       v.data_ora_erogazione AS v_data_ora_erogazione,  "
                + "       v.report AS v_report, "
                + "       tv.nome AS tv_nome, tv.id AS tv_id, tv.ticket AS tv_ticket "
                + "FROM visita_specialistica AS v "
                + "LEFT JOIN paziente AS mb ON v.id_medico_di_base = mb.id  "
                + "JOIN paziente AS p ON v.id_paziente = p.id "
                + "JOIN paziente AS ms ON v.id_medico_spec = ms.id "
                + "JOIN tipo_di_visita AS tv ON v.id_tipo_visita = tv.id "
                + "WHERE p.id = ? ORDER BY v.data_ora_prescrizione")) {
            stm.setInt(1, idPaziente);
            try ( ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    MedicoDiBase medicoDiBase = null;
                    if (rs.getObject("mb_id") != null) {
                        medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                                rs.getString("mb_nome"),
                                rs.getString("mb_cognome"));
                    }
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome"));
                    MedicoSpecialista medicoSpecialista = new MedicoSpecialista(rs.getInt("ms_id"),
                            rs.getString("ms_nome"),
                            rs.getString("ms_cognome"));

                    TipoDiVisita tipoVisita = new TipoDiVisita(rs.getInt("tv_id"),
                            rs.getString("tv_nome"),
                            rs.getInt("tv_ticket")
                    );
                    VisitaSpecialistica visita = new VisitaSpecialistica(rs.getInt("v_id"),
                            medicoDiBase,
                            paziente,
                            tipoVisita,
                            rs.getTimestamp("v_data_ora_prescrizione"),
                            medicoSpecialista,
                            rs.getTimestamp("v_data_ora_erogazione"),
                            rs.getString("v_report"));

                    visiteSpecialistiche.add(visita);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of user's visite specialistiche erogate", ex);
        }

        return visiteSpecialistiche;
    }

    @Override
    public List<VisitaSpecialistica> getDaErogareByPaziente(Integer idPaziente) throws DAOException {
        if (idPaziente == null) {
            throw new DAOException("idPaziente is null");
        }

        List<VisitaSpecialistica> visiteSpecialistiche = new ArrayList<>();

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT mb.id AS mb_id, mb.nome AS mb_nome, mb.cognome AS mb_cognome, "
                + "       p.id AS p_id, p.nome AS p_nome, p.cognome AS p_cognome,  "
                + "       v.id AS v_id,  "
                + "       v.data_ora_prescrizione AS v_data_ora_prescrizione, "
                + "       tv.nome AS tv_nome, tv.id AS tv_id, tv.ticket AS tv_ticket "
                + "FROM visita_specialistica AS v "
                + "LEFT JOIN paziente AS mb ON v.id_medico_di_base = mb.id  "
                + "JOIN paziente AS p ON v.id_paziente = p.id "
                + "JOIN tipo_di_visita AS tv ON v.id_tipo_visita = tv.id "
                + "WHERE p.id = ? AND v.id_medico_spec IS NULL "
                + "ORDER BY v.data_ora_prescrizione DESC ")) {
            stm.setInt(1, idPaziente);
            try ( ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    MedicoDiBase medicoDiBase = null;
                    if (rs.getObject("mb_id") != null) {
                        medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                                rs.getString("mb_nome"),
                                rs.getString("mb_cognome"));
                    }
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome"));

                    TipoDiVisita tipoVisita = new TipoDiVisita(rs.getInt("tv_id"),
                            rs.getString("tv_nome"),
                            rs.getInt("tv_ticket")
                    );
                    VisitaSpecialistica visita = new VisitaSpecialistica(rs.getInt("v_id"),
                            medicoDiBase,
                            paziente,
                            tipoVisita,
                            rs.getTimestamp("v_data_ora_prescrizione"),
                            null,
                            null,
                            null);

                    visiteSpecialistiche.add(visita);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of user's visite specialistiche not erogate", ex);
        }

        return visiteSpecialistiche;
    }

    @Override
    public VisitaSpecialistica insert(VisitaSpecialistica visita) throws DAOException {
        if (visita == null) {
            throw new DAOException("visita is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "INSERT INTO visita_specialistica (id_medico_di_base, id_paziente, id_tipo_visita, data_ora_prescrizione) VALUES "
                + "(?, ?, ?, ?);")) {
            stm.setInt(1, visita.getMedicoDiBase().getId());
            stm.setInt(2, visita.getPaziente().getId());
            stm.setInt(3, visita.getTipoDiVisita().getId());
            stm.setTimestamp(4, visita.getDataOraPrescrizione());

            if (stm.executeUpdate() == 1) {
                return visita;
            } else {
                throw new DAOException("Impossible to insert the visita");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to insert the visita", ex);
        }
    }
    @Override
    public VisitaSpecialistica insertRichiamo(VisitaSpecialistica visita) throws DAOException {
        if (visita == null) {
            throw new DAOException("visita is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "INSERT INTO visita_specialistica (id_paziente, id_tipo_visita, data_ora_prescrizione) VALUES "
                + "(?, ?, ?);")) {
            stm.setInt(1, visita.getPaziente().getId());
            stm.setInt(2, visita.getTipoDiVisita().getId());
            stm.setTimestamp(3, visita.getDataOraPrescrizione());

            if (stm.executeUpdate() == 1) {
                return visita;
            } else {
                throw new DAOException("Impossible to insert the visita");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to insert the visita", ex);
        }
    }

    @Override
    public VisitaSpecialistica update(VisitaSpecialistica visita) throws DAOException {
        if (visita == null) {
            throw new DAOException("esame is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(
                "UPDATE visita_specialistica SET id_medico_di_base = ? , "
                + "id_paziente = ? , "
                + "id_tipo_visita = ? , "
                + "data_ora_prescrizione = ? , "
                + "id_medico_spec = ? , "
                + "data_ora_erogazione = ? , "
                + "report = ?"
                + "WHERE id = ?")) {
            stm.setInt(1, visita.getMedicoDiBase().getId());
            stm.setInt(2, visita.getPaziente().getId());
            stm.setInt(3, visita.getTipoDiVisita().getId());
            stm.setTimestamp(4, visita.getDataOraPrescrizione());
            stm.setInt(5, visita.getMedicoSpecialista().getId());
            stm.setTimestamp(6, visita.getDataOraErogazione());
            stm.setString(7, visita.getReport());
            stm.setInt(8, visita.getId());

            if (stm.executeUpdate() == 1) {
                return visita;
            } else {
                throw new DAOException("Impossible to update the visita");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update the visita", ex);
        }
    }
    @Override
    public VisitaSpecialistica visita(VisitaSpecialistica visita) throws DAOException {
        if (visita == null) {
            throw new DAOException("esame is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(
                  "UPDATE visita_specialistica SET id_medico_spec = ? , "
                + "data_ora_erogazione = ? , "
                + "report = ?"
                + "WHERE id = ?")) {
            stm.setInt(1, visita.getMedicoSpecialista().getId());
            stm.setTimestamp(2, visita.getDataOraErogazione());
            stm.setString(3, visita.getReport());
            stm.setInt(4, visita.getId());

            if (stm.executeUpdate() == 1) {
                return visita;
            } else {
                throw new DAOException("Impossible to update the visita");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update the visita", ex);
        }
    }

    @Override
    public boolean isVisitaDiUnPaziente(Integer idVisita, Integer idMedicoDiBase) throws DAOException {
        if (idVisita == null) {
            throw new DAOException("idVisita is null");
        }
        if (idMedicoDiBase == null) {
            throw new DAOException("idMedicoDiBase is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT * "
                + "FROM visita_specialistica AS v "
                + "JOIN paziente AS p ON v.id_paziente = p.id "
                + "WHERE v.id = ? and p.id_medico_di_base = ?")) {
            stm.setInt(1, idVisita);
            stm.setInt(2, idMedicoDiBase);
            try ( ResultSet rs = stm.executeQuery()) {
                return rs.next();
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the exam for the passed primary key", ex);
        }
    }

    @Override
    public List<List<Integer>> getSpesaFrom(int fromYear) throws DAOException {
        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT ne.year AS year, SUM(ne.num * ne.ticket) AS spesa "
                + "FROM (SELECT EXTRACT(YEAR FROM v.data_ora_erogazione) AS year, tv.id AS id, COUNT(tv.id) AS num, tv.ticket AS ticket "
                + "      FROM visita_specialistica AS v "
                + "      JOIN tipo_di_visita AS tv ON v.id_tipo_visita = tv.id "
                + "      WHERE EXTRACT(YEAR FROM v.data_ora_erogazione) >= ? "
                + "      GROUP BY EXTRACT(YEAR FROM v.data_ora_erogazione), tv.id) AS ne "
                + "GROUP BY ne.year "
                + "ORDER BY ne.year;")) {
            stm.setInt(1, fromYear);
            try ( ResultSet rs = stm.executeQuery()) {
                List<List<Integer>> spesa = new ArrayList<>();
                while (rs.next()) {
                    List<Integer> annoSpesa = new ArrayList<Integer>(2);
                    annoSpesa.add(rs.getInt("year"));
                    annoSpesa.add(rs.getInt("spesa"));
                    spesa.add(annoSpesa);

                }
                return spesa;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the exams data", ex);
        }
    }

}
