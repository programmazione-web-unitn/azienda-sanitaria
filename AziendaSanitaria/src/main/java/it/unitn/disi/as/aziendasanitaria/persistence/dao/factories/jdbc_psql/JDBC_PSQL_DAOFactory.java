/*
 * AziendaSanitaria
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao.factories.jdbc_psql;


import it.unitn.disi.as.aziendasanitaria.persistence.dao.jdbc_psql.JDBC_PSQL_DAO;
import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Implementazione di {@code DAOFactory} per POSTGRESQL. Utilizza il pattern "singleton"
 */
public class JDBC_PSQL_DAOFactory implements DAOFactory {
    private final transient Connection CON;
    private final transient HashMap<Class, DAO> DAO_CACHE;
    private static final Logger logger = LogManager.getLogger(JDBC_PSQL_DAOFactory.class);

    private static JDBC_PSQL_DAOFactory instance;
    
    /**
     * Metodo chiamato prima di utilizzare l'istanza della classe.
     * @param dbUrl url del DB.
     * @param dbuser user del DB
     * @param dbpasswd password dell'utente dbuser
     * @throws DAOFactoryException se c'è un errore nella connessione
     * 
     */
    public static void configure(String dbUrl, String dbuser, String dbpasswd) throws DAOFactoryException {
        if (instance == null) {
            instance = new JDBC_PSQL_DAOFactory(dbUrl, dbuser, dbpasswd);
        } else {
            throw new DAOFactoryException("DAOFactory already configured. You can call configure only one time");
        }
    }
    
    /**
     * Restituisce l'istanza di questa {@link DAOFactory}.
     * @return l'istanza di questa {@code DAOFactory}.
     * @throws DAOFactoryException
     * 
     */
    public static JDBC_PSQL_DAOFactory getInstance() throws DAOFactoryException {
        if (instance == null) {
            throw new DAOFactoryException("DAOFactory not yet configured. Call DAOFactory.configure(String dbUrl) before use the class");
        }
        return instance;
    }
    
    /**
     * Costruttore privato per inizializzare l'istanza singleton della {@code DAOFactory}.
     * @param dbUrl l'url del DB.
     * @param dbuser user del DB
     * @param dbpasswd password dell'utente dbuser
     * @throws DAOFactoryException 
     * 
     */
    private JDBC_PSQL_DAOFactory(String dbUrl, String username, String password) throws DAOFactoryException {
        super();
        
        try {
            Class.forName("org.postgresql.Driver", true, getClass().getClassLoader());
        } catch (ClassNotFoundException cnfe) {
            throw new RuntimeException(cnfe.getMessage(), cnfe.getCause());
        }
        
        try {
            CON = DriverManager.getConnection(dbUrl, username, password);
        } catch (SQLException sqle) {
            throw new DAOFactoryException("Cannot create connection", sqle);
        }
        
        DAO_CACHE = new HashMap<>();
    }

    /**
     * Termina la connessione al DB.
     * 
     */
    @Override
    public void shutdown() {
        try {
            DriverManager.getConnection("jdbc:postgresql:;shutdown=true");
        } catch (SQLException sqle) {
            logger.info(sqle.getMessage());
        }
    }

    /**
     * Restituisce la classe concreta che implementa l'interfaccia DAO passata come
     * parametro.
     *
     * @param <DAO_CLASS> Nome della classe del {@code dao}.
     * @param daoInterface la classe dell'istanza del {@code dao} da ottenere.
     * @return Il {@code dao} concreto che implementa l'interfaccia passata come parametro.
     * @throws DAOFactoryException
     */
    @Override
    public <DAO_CLASS extends DAO> DAO_CLASS getDAO(Class<DAO_CLASS> daoInterface) throws DAOFactoryException {
        DAO dao = DAO_CACHE.get(daoInterface);
        if (dao != null) {
            return (DAO_CLASS) dao;
        }
        
        Package pkg = daoInterface.getPackage();
        String prefix = pkg.getName() + ".jdbc_psql.JDBC_PSQL";
        
        try {
            Class daoClass = Class.forName(prefix + daoInterface.getSimpleName());
            
            Constructor<DAO_CLASS> constructor = daoClass.getConstructor(Connection.class);
            DAO_CLASS daoInstance = constructor.newInstance(CON);
            if (!(daoInstance instanceof JDBC_PSQL_DAO)) {
                throw new DAOFactoryException("The daoInterface passed as parameter doesn't extend JDBCDAO class");
            }
            DAO_CACHE.put(daoInterface, daoInstance);
            return daoInstance;
        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException | SecurityException ex) {
            throw new DAOFactoryException("Impossible to return the DAO", ex);
        }
    }
}

