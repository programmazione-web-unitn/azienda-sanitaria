/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.entities;

import java.sql.Timestamp;

/**
 *
 * @author gabriele
 */
public class Foto {

    private Integer id;
    private Paziente paziente;
    private String pathFoto;
    private Timestamp dataOraFoto;

    public Foto(Integer id, Paziente paziente, String pathFoto, Timestamp dataOraFoto) {
        this.id = id;
        this.paziente = paziente;
        this.pathFoto = pathFoto;
        this.dataOraFoto = dataOraFoto;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Paziente getPaziente() {
        return paziente;
    }

    public void setPaziente(Paziente paziente) {
        this.paziente = paziente;
    }

    public String getPathFoto() {
        return pathFoto;
    }

    public void setPathFoto(String pathFoto) {
        this.pathFoto = pathFoto;
    }

    public Timestamp getDataOraFoto() {
        return dataOraFoto;
    }

    public void setDataOraFoto(Timestamp dataOraFoto) {
        this.dataOraFoto = dataOraFoto;
    }
}
