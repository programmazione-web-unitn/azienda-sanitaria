/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao.jdbc_psql;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.SspDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Provincia;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Ssp;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Utente;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gabriele
 */
public class JDBC_PSQLSspDAO extends JDBC_PSQL_DAO<Ssp, Integer> implements SspDAO {

    public JDBC_PSQLSspDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try ( Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM ssp");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count ssps", ex);
        }

        return 0L;  
    }

    @Override
    public Ssp getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT p.sigla AS p_sigla, p.nome  AS p_nome, p.regione  AS p_regione, "
                + "       u.id     AS ssp_id,    u.email  AS  ssp_email, u.password  AS ssp_password, "
                + "       u.tipo   AS ssp_tipo, u.salt AS ssp_salt, "
                + "       u.recupero_token AS ssp_rec_token, u.scadenza_recupero AS ssp_scad_rec, "
                + "       u.token AS ssp_token, u.scadenza_token AS ssp_scad_token "
                + "FROM ssp "
                + "JOIN utente AS u ON u.id = ssp.id "
                + "JOIN provincia AS p ON ssp.sigla_provincia = p.sigla "
                + "WHERE ssp.id = ?")) {
            stm.setInt(1, primaryKey);
            try ( ResultSet rs = stm.executeQuery()) {

                rs.next();
                
                Provincia provincia = new Provincia(rs.getString("p_sigla"),
                        rs.getString("p_nome"), 
                        rs.getString("p_regione")
                );
                Ssp ssp = new Ssp(rs.getInt("ssp_id"), 
                        rs.getString("ssp_email"), 
                        rs.getString("ssp_password"), 
                        rs.getString("ssp_salt"), 
                        rs.getString("ssp_tipo"),
                        rs.getString("ssp_rec_token"),
                        rs.getTimestamp("ssp_scad_rec"),
                        rs.getString("ssp_token"), 
                        rs.getTimestamp("ssp_scad_token"),
                        provincia
                );
                return ssp;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the ssp for the passed primary key", ex);
        }
    }

    @Override
    public List<Ssp> getAll() throws DAOException {
        List<Ssp> ssps = new ArrayList<>();

        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery(""
                + "SELECT p.sigla AS p_sigla, p.nome  AS p_nome, p.regione  AS p_regione, "
                + "       u.id     AS ssp_id,    u.email  AS  ssp_email, u.password  AS ssp_password, "
                + "       u.tipo   AS ssp_tipo, u.salt AS ssp_salt, "
                + "       u.recupero_token AS ssp_rec_token, u.scadenza_recupero AS ssp_scad_rec, "
                + "       u.token AS ssp_token, u.scadenza_token AS ssp_scad_token "
                + "FROM ssp "
                + "JOIN utente AS u ON u.id = ssp.id "
                + "JOIN provincia AS p ON ssp.sigla_provincia = p.sigla  ORDER BY id")) {

                while (rs.next()) {
                    Provincia provincia = new Provincia(rs.getString("p_sigla"),
                        rs.getString("p_nome"), 
                        rs.getString("p_regione")
                    );
                    Ssp ssp = new Ssp(rs.getInt("ssp_id"), 
                            rs.getString("ssp_email"), 
                            rs.getString("ssp_password"), 
                            rs.getString("ssp_salt"), 
                            rs.getString("ssp_tipo"),
                            rs.getString("ssp_rec_token"),
                            rs.getTimestamp("ssp_scad_rec"),
                            rs.getString("ssp_token"), 
                            rs.getTimestamp("ssp_scad_token"),
                            provincia
                    );
                    ssps.add(ssp);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of ssps", ex);
        }

        return ssps;
    }

//    @Override
//    public Ssp getByUtente(Utente utente) throws DAOException {
//        if (utente == null) {
//            throw new DAOException("utente is null");
//        }
//
//        try ( PreparedStatement stm = CON.prepareStatement(""
//                + "SELECT p.sigla AS p_sigla, p.nome  AS p_nome, p.regione  AS p_regione, "
//                + "       u.id     AS ssp_id,    u.email  AS  ssp_email, u.password  AS ssp_password, "
//                + "       u.tipo   AS ssp_tipo, u.salt AS ssp_salt "
//                + "FROM ssp "
//                + "JOIN utente AS u ON u.id = ssp.id "
//                + "JOIN provincia AS p ON ssp.sigla_provincia = p.sigla "
//                + "WHERE ssp.id = ? ")) {
//            stm.setInt(1, utente.getId());
//            try ( ResultSet rs = stm.executeQuery()) {
//                rs.next();
//                Provincia provincia = new Provincia(rs.getString("p_sigla"),
//                        rs.getString("p_nome"),
//                        rs.getString("p_regione")
//                );
//                Ssp ssp = new Ssp(rs.getInt("ssp_id"),
//                        rs.getString("ssp_email"),
//                        rs.getString("ssp_password"),
//                        rs.getString("ssp_salt"),
//                        rs.getString("ssp_tipo"),
//                        rs.getString("ssp_rec_token"),
//                        rs.getTimestamp("ssp_scad_rec"),
//                        rs.getString("ssp_token"),
//                        rs.getTimestamp("ssp_scad_token"),
//                        provincia
//                );
//                
//                return ssp;
//            }
//        } catch (SQLException ex) {
//            throw new DAOException("Impossible to get the ssp for the passed utente", ex);
//        }
//    }
    
}
