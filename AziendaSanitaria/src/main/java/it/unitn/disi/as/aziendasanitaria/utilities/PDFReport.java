/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.utilities;

import it.unitn.disi.as.aziendasanitaria.persistence.entities.Esame;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Farmacia;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Paziente;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Provincia;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Ricetta;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.VisitaSpecialistica;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.sql.Date;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

/**
 * @author dima genera il pdf in base alla classe
 */
public class PDFReport {

    private static final Logger logger = LogManager.getLogger(PDFReport.class);
    static String logoPath;
    public static void init(String path){
        logoPath=path;
    }
    public static PDImageXObject getLogo(PDDocument document) throws IOException{
        return PDImageXObject.createFromFile(logoPath,document);
    }

    public static PDDocument getPdf(Ricetta ricetta) {
        String nomePaziente = ricetta.getPaziente().getNome() + " " + ricetta.getPaziente().getCognome();
        String nomeMedicoDiBase = ricetta.getMedicoDiBase().getNome() + " " + ricetta.getMedicoDiBase().getCognome();
        String prescritto = new SimpleDateFormat("dd/MM/yyyy").format(ricetta.getDataOraPrescrizione().getTime());
        String erogato = new SimpleDateFormat("dd/MM/yyyy").format(ricetta.getDataOraErogazione().getTime());
        Farmacia farmacia = ricetta.getFarmacia();

        String titolo = farmacia.getProvincia().getSigla() + " Ricetta n." + ricetta.getId();
        String lab_soggetto = "Nome paziente", arg_soggetto = ":   " + nomePaziente;
        String lab_codiceFiscale = "Codice fiscale", arg_codiceFiscale = ":   " + ricetta.getPaziente().getCodiceFiscale();
        String lab_medico = "Prescritto da", arg_medico = ":   " + nomeMedicoDiBase;
        String lab_data1 = "Data prescrizione", arg_data1 = ":   " + prescritto;
        String lab_data2 = "Data erogazione", arg_data2 = ":   " + erogato;
        String lab_farmaco = "Farmaco", arg_farmaco = ":   " + ricetta.getFarmaco().getNome() + "  x" + ricetta.getQuantita();
        String lab_ticketFarmaco = "Totale", arg_ticketFarmaco = ":   " + (ricetta.getQuantita() * ricetta.getFarmaco().getTicket()) + ".00€";
        String lab_farmacia = "Erogato da:";
        String arg_farmacia = "    " + farmacia.getNome() + " ad " + farmacia.getComune() + " provincia di " + farmacia.getProvincia().getNome();
        
        String QRText=""
                + "Paziente: "+nomePaziente+"\n"
                + "Codice fiscale: "+ricetta.getPaziente().getCodiceFiscale()+"\n"
                + "Data prescrizione: "+prescritto+"\n"
                + "Id medico: "+ricetta.getMedicoDiBase().getId()+"\n"
                + "Id prescrizione: "+ricetta.getId()+"\n"
                + "Farmaco: "+ricetta.getFarmaco().getNome()+"\n"
                + "Quantita: "+ricetta.getQuantita()+"\n";
        int margin=35;
        
        PDDocument document = new PDDocument();
        PDDocumentInformation pdd = document.getDocumentInformation();

        pdd.setAuthor("Sito azienda sanitaria per conto della farmacia: " + ricetta.getFarmacia().getId());
        pdd.setTitle("ricetta di. " + ricetta.getPaziente().getId());
        pdd.setSubject(ricetta.getFarmaco().getNome());

        PDPage blankPage = new PDPage();
        document.addPage(blankPage);
        try {
            PDPageContentStream contentStream = new PDPageContentStream(document, blankPage);
            
            PDImageXObject QRCode=PDImageXObject.createFromFileByContent(QRGenerator.generate(QRText), document);
            contentStream.drawImage(QRCode, 200, 200, 200, 200);
            
            PDImageXObject logo=getLogo(document);
            contentStream.drawImage(logo, 20, 720, 180, 45);

            contentStream.beginText();
            contentStream.setFont(PDType1Font.TIMES_ROMAN, 21);
            contentStream.newLineAtOffset(230, 735);
            contentStream.showText(titolo);
            contentStream.endText();

            contentStream.beginText();
            contentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
            contentStream.setLeading(16.5f);
            contentStream.newLineAtOffset(margin, 685);
            contentStream.showText(lab_medico);
            contentStream.newLine();
            contentStream.showText(lab_data1);
            contentStream.newLine();
            contentStream.showText(lab_data2);
            contentStream.newLine();
            contentStream.showText(lab_soggetto);
            contentStream.newLine();
            contentStream.showText(lab_codiceFiscale);
            contentStream.newLine();
            contentStream.showText(lab_farmaco);
            contentStream.newLine();
            contentStream.showText(lab_ticketFarmaco);
            contentStream.endText();

            contentStream.beginText();
            contentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
            contentStream.setLeading(16.5f);
            contentStream.newLineAtOffset(160, 685);
            contentStream.showText(arg_medico);
            contentStream.newLine();
            contentStream.showText(arg_data1);
            contentStream.newLine();
            contentStream.showText(arg_data2);
            contentStream.newLine();
            contentStream.showText(arg_soggetto);
            contentStream.newLine();
            contentStream.showText(arg_codiceFiscale);
            contentStream.newLine();
            contentStream.showText(arg_farmaco);
            contentStream.newLine();
            contentStream.showText(arg_ticketFarmaco);
            contentStream.endText();

            contentStream.beginText();
            contentStream.setFont(PDType1Font.TIMES_ROMAN, 11);
            contentStream.setLeading(21.5f);
            contentStream.newLineAtOffset(100, 550);
            contentStream.showText(lab_farmacia);
            contentStream.newLine();
            contentStream.setFont(PDType1Font.TIMES_ROMAN, 13);
            contentStream.showText(arg_farmacia);
            contentStream.endText();

            contentStream.close();
        } catch (IOException e) {
            logger.error("errore salvataggio PDF document", e);
        }
        return document;
    }

    public static PDDocument getPdf(Esame esame) {
        String nomePaziente = esame.getPaziente().getNome() + " " + esame.getPaziente().getCognome();
        String nomeMedicoDiBase = esame.getMedicoDiBase().getNome() + " " + esame.getMedicoDiBase().getCognome();
        String prescritto = new SimpleDateFormat("dd/MM/yyyy").format(esame.getDataOraPrescrizione().getTime());
        String erogato = new SimpleDateFormat("dd/MM/yyyy").format(esame.getDataOraErogazione().getTime());

        String titolo = esame.getSsp().getProvincia().getSigla() + " Referto n." + esame.getId();
        String lab_soggetto = "Nome paziente", arg_soggetto = ":   " + nomePaziente;
        String lab_codiceFiscale = "Codice fiscale", arg_codiceFiscale = ":   " + esame.getPaziente().getCodiceFiscale();
        String lab_data1 = "Data prescrizione", arg_data1 = ":   " + prescritto;
        String lab_data2 = "Data erogazione", arg_data2 = ":   " + erogato;
        String lab_tipoEsame = "Tipo esame", arg_tipoEsame = ":   " + esame.getTipoDiEsame().getNome();
        String lab_ticketEsame = "Ticket", arg_ticketEsame = ":   " + esame.getTipoDiEsame().getTicket() + ".00€";
        String report = "Report: ";
        String rep = esame.getReport();
        String firma = "Firma medico di base:";
        String firmaMedico = "      " + nomeMedicoDiBase;
        int margin=35;

        PDDocument document = new PDDocument();
        PDDocumentInformation pdd = document.getDocumentInformation();

        pdd.setAuthor("Sito azienda sanitaria per conto del ssp: " + esame.getSsp().getProvincia().getNome());
        pdd.setTitle("Esame di. " + esame.getPaziente().getId());
        pdd.setSubject(esame.getTipoDiEsame().getNome());
        PDPage blankPage = new PDPage();
        document.addPage(blankPage);
        try {
            PDPageContentStream contentStream;
            contentStream = new PDPageContentStream(document, blankPage);
            
            PDImageXObject logo=getLogo(document);
            contentStream.drawImage(logo, 20, 720, 180, 45);
            contentStream.beginText();
            contentStream.setFont(PDType1Font.TIMES_ROMAN, 21);
            contentStream.newLineAtOffset(230, 735);
            contentStream.showText(titolo);
            contentStream.endText();

            contentStream.beginText();
            contentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
            contentStream.setLeading(16.5f);
            contentStream.newLineAtOffset(margin, 685);
            contentStream.showText(lab_data1);
            contentStream.newLine();
            contentStream.showText(lab_data2);
            contentStream.newLine();
            contentStream.showText(lab_soggetto);
            contentStream.newLine();
            contentStream.showText(lab_codiceFiscale);
            contentStream.newLine();
            contentStream.showText(lab_tipoEsame);
            contentStream.newLine();
            contentStream.showText(lab_ticketEsame);
            contentStream.newLine();
            contentStream.showText(report);
            contentStream.newLineAtOffset(30, 0);
            for (String s : rep.split("(?<=\\G.{85,}\\s)")) {
                contentStream.newLine();
                contentStream.showText(s);
            }
            contentStream.endText();

            contentStream.beginText();
            contentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
            contentStream.setLeading(16.5f);
            contentStream.newLineAtOffset(160, 685);
            contentStream.showText(arg_data1);
            contentStream.newLine();
            contentStream.showText(arg_data2);
            contentStream.newLine();
            contentStream.showText(arg_soggetto);
            contentStream.newLine();
            contentStream.showText(arg_codiceFiscale);
            contentStream.newLine();
            contentStream.showText(arg_tipoEsame);
            contentStream.newLine();
            contentStream.showText(arg_ticketEsame);
            contentStream.endText();

            contentStream.beginText();
            contentStream.setFont(PDType1Font.TIMES_ROMAN, 10);
            contentStream.setLeading(21.5f);
            contentStream.newLineAtOffset(400, 100);
            contentStream.showText(firma);
            contentStream.newLine();
            contentStream.setFont(PDType1Font.TIMES_ITALIC, 14);
            contentStream.showText(firmaMedico);
            contentStream.endText();

            contentStream.close();
        } catch (IOException e) {
            logger.error("errore salvataggio PDF document", e);
        }
        return document;
    }

    public static PDDocument getPdf(Paziente paziente, List<Esame> listEsame, List<Ricetta> listRicetta, List<VisitaSpecialistica> listVisitaSpecialistica) {
        ArrayList<Row> list = new ArrayList<>();
        for (Esame es : listEsame) {
            list.add(new Row(es.getDataOraErogazione(), "Esame", es.getTipoDiEsame().getNome(), "€" + es.getTipoDiEsame().getTicket() + ".00"));
        }
        for (Ricetta ri : listRicetta) {
            list.add(new Row(ri.getDataOraErogazione(), "Ricetta", ri.getFarmaco().getNome() + ", quantità " + ri.getQuantita(), "€" + (ri.getFarmaco().getTicket() * ri.getQuantita()) + ".00"));
        }
        for (VisitaSpecialistica vs : listVisitaSpecialistica) {
            list.add(new Row(vs.getDataOraErogazione(), "Visita specialistica", vs.getTipoDiVisita().getNome(), "€" + (vs.getMedicoDiBase() == null? 0 : vs.getTipoDiVisita().getTicket()) + ".00")); // richiamo €0,00
        }
        Collections.sort(list, Collections.reverseOrder());

        String titolo = "Ticket pagati da " + paziente.getNome()+ " " + listEsame.get(0).getPaziente().getCognome();
        String col1 = "Data erogazione", col2 = "Tipo", col3 = "Dettagli", col4 = "Costo";
        int x1 = 35, x2 = 130, x3 = 230, x4 = 520, x5 = 565;
        int margin = 3;
        int titleSize = 17;
        int colTSize = 13;
        int rowSize = 17;
        int textSize = 11;
        int PG = 33;

        PDDocument document = new PDDocument();
        PDDocumentInformation pdd = document.getDocumentInformation();

        pdd.setAuthor("Sito azienda sanitaria");
        pdd.setTitle("Tiket pagati di. " + paziente.getId());
        pdd.setSubject(paziente.getNome());
        try {
            PDPage page = new PDPage();
            PDPageContentStream contentStream = new PDPageContentStream(document, page);
            
            int linee = 0;
            if(list.size()==0){
                document.addPage(page);
                
                PDImageXObject logo=getLogo(document);
                contentStream.drawImage(logo, 20, 720, 180, 45);

                contentStream.beginText();
                contentStream.setFont(PDType1Font.TIMES_ROMAN, titleSize);
                contentStream.newLineAtOffset(200, 735);
                contentStream.showText(titolo);
                contentStream.endText();
                
                contentStream.beginText();
                contentStream.setLeading((rowSize + textSize) / 2);
                contentStream.setFont(PDType1Font.TIMES_ROMAN, textSize);

                contentStream.newLineAtOffset(x1 + margin, 680 - (linee % PG + 2) * rowSize);
                contentStream.showText("Non esistono ticket erogati dal paziente");
                contentStream.endText();
            }

            boolean newPage = true;
            for (int i = 0; i < list.size(); i++) {
                if (newPage) {
                    newPage = false;
                    contentStream.close();
                    page = new PDPage();
                    document.addPage(page);                    
                    contentStream = new PDPageContentStream(document, page);
                    
                    PDImageXObject logo=getLogo(document);
                    contentStream.drawImage(logo, 20, 720, 180, 45);
                    
                    contentStream.beginText();
                    contentStream.setFont(PDType1Font.TIMES_ROMAN, titleSize);
                    contentStream.newLineAtOffset(200, 735);
                    contentStream.showText(titolo);
                    contentStream.endText();
                    
                    contentStream.beginText();
                    contentStream.setFont(PDType1Font.TIMES_ROMAN, 10);
                    contentStream.newLineAtOffset(550, 30);
                    contentStream.showText("pagina " + (linee / PG + 1));
                    contentStream.endText();

                    contentStream.beginText();
                    contentStream.setFont(PDType1Font.TIMES_ROMAN, colTSize);
                    contentStream.newLineAtOffset(x1 + margin, 680);
                    contentStream.showText(col1);
                    contentStream.newLineAtOffset(x2 - x1, 0);
                    contentStream.showText(col2);
                    contentStream.newLineAtOffset(x3 - x2, 0);
                    contentStream.showText(col3);
                    contentStream.newLineAtOffset(x4 - x3, 0);
                    contentStream.showText(col4);
                    contentStream.endText();
                }
                contentStream.beginText();
                contentStream.setLeading((rowSize + textSize) / 2);
                contentStream.setFont(PDType1Font.TIMES_ROMAN, textSize);

                contentStream.newLineAtOffset(x1 + margin, 680 - (linee % PG + 2) * rowSize);
                contentStream.showText(new SimpleDateFormat("dd/MM/yyyy").format(list.get(i).time));

                contentStream.newLineAtOffset(x2 - x1, 0);
                contentStream.showText(list.get(i).a);

                contentStream.newLineAtOffset(x3 - x2, 0);
                contentStream.setFont(PDType1Font.TIMES_ROMAN, textSize - 2);
                boolean first = true;
                int origLinee=linee;
                for (String s : list.get(i).b.split("(?<=\\G.{65,}\\s)")) {
                    if (first) {
                        first = false;
                    } else {
                        contentStream.newLine();
                    }
                    contentStream.showText(s);
                    linee++;
                    if (linee % PG == 0) {
                        newPage = true;
                    }
                }
                contentStream.endText();

                contentStream.beginText();
                contentStream.newLineAtOffset(x4, 680 - (origLinee % PG + 2) * rowSize);
                contentStream.setFont(PDType1Font.TIMES_ROMAN, textSize);
                contentStream.showText(list.get(i).c);
                contentStream.endText();
            }
            contentStream.close();
        } catch (IOException e) {
            logger.error("errore salvataggio PDF document", e);
        }
        return document;
    }

    public static PDDocument getPdf(Provincia provincia, List<Esame> listEsame, Date startDate, Date endDate) {
        SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
        String titolo = "Esami erogati in provincia di " + provincia.getNome();
        String titolo2= " tra il "+ fmt.format(startDate) + " e il " + fmt.format(endDate);
        String col1 = "Data erogazione", col2 = "Tipo", col4 = "Costo";

        int x1 = 35, x2 = 130, x4 = 520, x5 = 565;
        int margin = 3;
        int titleSize = 16;
        int colTSize = 13;
        int rowSize = 17;
        int textSize = 11;
        int PG = 33;

        PDDocument document = new PDDocument();
        PDDocumentInformation pdd = document.getDocumentInformation();

        pdd.setAuthor("Sito azienda sanitaria");
        pdd.setTitle("Esami erogati in provincia di " + provincia.getNome());
        pdd.setSubject("Esami erogati in provincia di " + provincia.getNome());
        try {
            PDPage page = new PDPage();
            PDPageContentStream contentStream = new PDPageContentStream(document, page);
            
            int linee = 0;
            if(listEsame.size()==0){
                document.addPage(page);
                
                PDImageXObject logo=getLogo(document);
                contentStream.drawImage(logo, 20, 720, 180, 45);

                contentStream.beginText();
                contentStream.setFont(PDType1Font.TIMES_ROMAN, titleSize);
                contentStream.newLineAtOffset(200, 735);
                contentStream.showText(titolo);
                contentStream.endText();
                    
                contentStream.beginText();
                contentStream.setLeading((rowSize + textSize) / 2);
                contentStream.setFont(PDType1Font.TIMES_ROMAN, textSize);

                contentStream.newLineAtOffset(x1 + margin, 680 - (linee % PG + 2) * rowSize);
                contentStream.showText("Non si sono svolti esami durante l'intervallo specificato");
                contentStream.endText();
            }

            boolean newPage = true;
            for (int i = 0; i < listEsame.size(); i++) {
                if (newPage) {
                    newPage = false;
                    contentStream.close();
                    page = new PDPage();
                    document.addPage(page);
                    contentStream = new PDPageContentStream(document, page);
                    
                    PDImageXObject logo=getLogo(document);
                    contentStream.drawImage(logo, 20, 720, 180, 45);
                    
                    contentStream.beginText();
                    contentStream.setFont(PDType1Font.TIMES_ROMAN, titleSize);
                    contentStream.newLineAtOffset(180, 750);
                    contentStream.showText(titolo);
                    contentStream.endText();
                    contentStream.beginText();
                    contentStream.setFont(PDType1Font.TIMES_ROMAN, titleSize);
                    contentStream.newLineAtOffset(200, 730);
                    contentStream.showText(titolo2);
                    contentStream.endText();
                    
                    contentStream.beginText();
                    contentStream.setFont(PDType1Font.TIMES_ROMAN, 10);
                    contentStream.newLineAtOffset(550, 30);
                    contentStream.showText("pagina " + (linee / PG + 1));
                    contentStream.endText();

                    contentStream.beginText();
                    contentStream.setFont(PDType1Font.TIMES_ROMAN, colTSize);
                    contentStream.newLineAtOffset(x1 + margin, 680);
                    contentStream.showText(col1);
                    contentStream.newLineAtOffset(x2 - x1, 0);
                    contentStream.showText(col2);
                    contentStream.newLineAtOffset(x4 - x2, 0);
                    contentStream.showText(col4);
                    contentStream.endText();
                }
                contentStream.beginText();
                contentStream.setLeading((rowSize + textSize) / 2);
                contentStream.setFont(PDType1Font.TIMES_ROMAN, textSize);

                contentStream.newLineAtOffset(x1 + margin, 680 - (linee % PG + 2) * rowSize);
                contentStream.showText(new SimpleDateFormat("dd/MM/yyyy").format(listEsame.get(i).getDataOraErogazione()));
                
                contentStream.newLineAtOffset(x2 - x1, 0);
                boolean first = true;
                int origLinee=linee;
                for (String s : listEsame.get(i).getTipoDiEsame().getNome().split("(?<=\\G.{70,}\\s)")) {
                    if (first) {
                        first = false;
                    } else {
                        contentStream.newLine();
                    }
                    contentStream.showText(s);
                    linee++;
                    if (linee % PG == 0) {
                        newPage = true;
                    }
                }
                contentStream.endText();

                contentStream.beginText();
                contentStream.setFont(PDType1Font.TIMES_ROMAN, textSize);
                contentStream.newLineAtOffset(x4, 680 - (origLinee % PG + 2) * rowSize);
                contentStream.showText("€" + listEsame.get(i).getTipoDiEsame().getTicket() + ".00");
                contentStream.endText();
            }
            contentStream.close();
        } catch (IOException e) {
            logger.error("errore salvataggio PDF document", e);
        }
        return document;
    }
};

class Row implements Comparable<Row> {

    Timestamp time;
    String a, b, c;

    Row(Timestamp n, String a, String b, String c) {
        time = n;
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public int compareTo(Row arg0) {
        return time.compareTo(arg0.time);
    }
}
