/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao.jdbc_psql;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.TipoDiVisitaDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.TipoDiVisita;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author davide
 */
public class JDBC_PSQLTipoDiVisitaDAO extends JDBC_PSQL_DAO<TipoDiVisita, Integer> implements TipoDiVisitaDAO {

    public JDBC_PSQLTipoDiVisitaDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try ( Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM tipo_di_visita");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count tipo di visita", ex);
        }

        return 0L;
    }
    
    @Override
    public TipoDiVisita getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement("SELECT * FROM tipo_di_visita WHERE id = ?")) {
            stm.setInt(1, primaryKey);
            try ( ResultSet rs = stm.executeQuery()) {

                rs.next();
                TipoDiVisita tipoDiVisita = new TipoDiVisita(rs.getInt("id"),
                        rs.getString("nome"),
                        rs.getInt("ticket")
                );
                return tipoDiVisita;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the tipo di visita for the passed primary key", ex);
        }
    }
    
    @Override
    public List<TipoDiVisita> getAll() throws DAOException {
        List<TipoDiVisita> tipiDiVisita = new ArrayList<>();

        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery("SELECT * FROM tipo_di_visita ORDER BY nome")) {

                while (rs.next()) {
                    TipoDiVisita tipoDiVisita = new TipoDiVisita(rs.getInt("id"),
                            rs.getString("nome"),
                            rs.getInt("ticket")
                    );
                    tipiDiVisita.add(tipoDiVisita);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of tipo di visita", ex);
        }

        return tipiDiVisita;
    }    
    
}
