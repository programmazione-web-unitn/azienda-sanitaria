/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.entities;

import java.sql.Timestamp;

/**
 *
 * @author gabriele
 */
public class Ssn extends Utente {

    public Ssn() {
    }

    public Ssn(Integer id, String email, String password, String salt, String tipo) {
        super(id, email, password, salt, tipo);
    }

    public Ssn(Integer id, String email, String password, String salt, String tipo, String recuperoToken, Timestamp scadenzaRecupero, String token, Timestamp scadenzaToken) {
        super(id, email, password, salt, tipo, recuperoToken, scadenzaRecupero, token, scadenzaToken);
    }
        
}
