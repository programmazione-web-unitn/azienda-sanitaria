/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.servlets;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.PazienteDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.TipoDiVisitaDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.VisitaSpecialisticaDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Paziente;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Ssp;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.TipoDiVisita;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.VisitaSpecialistica;
import it.unitn.disi.as.aziendasanitaria.utilities.Authentication;
import it.unitn.disi.as.aziendasanitaria.utilities.MailSender;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author dima
 */
public class PrescrizioneRichiamoServlet extends HttpServlet {

    private MailSender mailSender;

    private VisitaSpecialisticaDAO visitaSpecialisticaDao;
    private PazienteDAO pazienteDao;
    private TipoDiVisitaDAO tipoDiVisitaDao;

    private static final Logger logger = LogManager.getLogger(PrescrizioneRichiamoServlet.class);

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory");
        }
        try {
            pazienteDao = daoFactory.getDAO(PazienteDAO.class);
            visitaSpecialisticaDao = daoFactory.getDAO(VisitaSpecialisticaDAO.class);
            tipoDiVisitaDao = daoFactory.getDAO(TipoDiVisitaDAO.class);
        } catch (DAOFactoryException ex) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory", ex);
        }

        mailSender = (MailSender) getServletContext().getAttribute("mailSender");
        if (mailSender == null) {
            logger.error("impossibile ottenere il mail sender dalla servlet context");
            throw new ServletException("impossibile ottenere il mail sender dalla servlet context");
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        final TipoDiVisita tipoDiVisita;
        int minEta, maxEta, idTipoDiVisita;
        boolean soloGiaFatto = "on".equals(request.getParameter("soloGiaFatto"));
        try {
            minEta = Integer.parseInt(request.getParameter("minEta"));
            maxEta = Integer.parseInt(request.getParameter("maxEta"));
            idTipoDiVisita = Integer.parseInt(request.getParameter("autocompleteVisiteSpec"));

            if (minEta < 0) {
                throw new NumberFormatException("Età minima deve essere maggiore di 0");
            }
            if (maxEta < minEta) {
                throw new NumberFormatException("Età massima deve essere maggiore di età minima");
            }

            tipoDiVisita = tipoDiVisitaDao.getByPrimaryKey(idTipoDiVisita);
        } catch (NumberFormatException ex) {
            logger.debug("Parametri non passati nella request o non validi", ex);
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        } catch (DAOException ex) {
            logger.debug("Tipo di visita non valido", ex);
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        Ssp ssp = (Ssp) request.getAttribute("user");
        if (ssp == null) {
            logger.error("ssp non presente");
            throw new ServletException("ssp non presente");
        }
        final List<Paziente> pazienti;

        Calendar a = Calendar.getInstance(Locale.US), b = Calendar.getInstance(Locale.US);
        a.add(Calendar.YEAR, -maxEta);
        a.set(a.get(Calendar.YEAR), 0, 0);
        b.add(Calendar.YEAR, -minEta + 1);
        b.set(b.get(Calendar.YEAR), 0, 0);

        java.sql.Date from = new java.sql.Date(a.getTimeInMillis()), to = new java.sql.Date(b.getTimeInMillis());

        try {
            if (soloGiaFatto) {
                pazienti = pazienteDao.getBySspConRichiamo(ssp, from, to);
            } else {
                pazienti = pazienteDao.getBySsp(ssp, from, to);
            }
        } catch (DAOException ex) {
            logger.error("Impossibile caricare dati pazienti, pazienteDao", ex);
            throw new ServletException("Impossibile caricare dati pazienti, pazienteDao", ex);
        }

        final Timestamp now = new Timestamp(new Date().getTime());
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (final Paziente paziente : pazienti) {
                    VisitaSpecialistica visita = new VisitaSpecialistica(null, null, paziente, tipoDiVisita, now);

                    try {
                        visitaSpecialisticaDao.insertRichiamo(visita);
                        logger.debug("Visita specialistica inserita nel database: " + paziente.getDataNascita() + " " + paziente.getNome());
                    } catch (DAOException ex) {
                        logger.error("Impossibile inserire la visita specialistica nel database", ex);
                    }

                    try {
                        String testo = "Gentile " + paziente.getNome() + " " + paziente.getCognome() + ",\n\n"
                                + "in data " + new SimpleDateFormat("dd-MM-yyyy").format(now)
                                + " è stato prescritto un richiamo '"
                                + tipoDiVisita.getNome() + "'.\n"
                                + "Per erogare la visita, recati da un medico specialista.\n\n"
                                + "Grazie,\n\n"
                                + "Team Azienda Sanitaria";
                        mailSender.send(paziente.getEmail(), "Prescrizione richiamo", testo);
                    } catch (MessagingException ex) {
                        logger.error("Impossibile inviare la mail all'indirizzo " + paziente.getEmail(), ex);
                    }
                }
            }
        }).start();

        request.getSession().setAttribute("success", "Richiamo effettuato per " + pazienti.size() + " pazienti");

        Authentication.redirectTo("Ssp/richiamo", request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
