/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao;

import it.unitn.disi.as.aziendasanitaria.persistence.entities.Ricetta;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.VisitaDiBase;
import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.util.List;

/**
 *
 * @author fran
 */
public interface VisitaDiBaseDAO extends DAO<VisitaDiBase, Integer> {

    public List<VisitaDiBase> getByPaziente(Integer idPaziente) throws DAOException;

//    public List<VisitaDiBase> getLastByPaziente(Integer idPaziente) throws DAOException;

    public VisitaDiBase update(VisitaDiBase visitaBase) throws DAOException;
    public VisitaDiBase insert(VisitaDiBase visitaBase) throws DAOException;
    
    public boolean isVisitaDiUnPaziente(Integer idVisita, Integer idMedicoDiBase) throws DAOException;
}
