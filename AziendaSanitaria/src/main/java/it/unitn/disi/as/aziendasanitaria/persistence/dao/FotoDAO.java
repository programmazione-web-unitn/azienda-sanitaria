/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao;

import it.unitn.disi.as.aziendasanitaria.persistence.entities.Foto;
import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.util.List;

/**
 *
 * @author gabriele
 */
public interface FotoDAO extends DAO<Foto, Integer> {    
    
    public List<Foto> getByPaziente(Integer idPaziente)throws DAOException;
    public Foto insert(Foto foto) throws DAOException;
    
    public boolean isFotoDiUnPaziente(Integer idFoto, Integer idMedicoDiBase) throws DAOException;
    
}
