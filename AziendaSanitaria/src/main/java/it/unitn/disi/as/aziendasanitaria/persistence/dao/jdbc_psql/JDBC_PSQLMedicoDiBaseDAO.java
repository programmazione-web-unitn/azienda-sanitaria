/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao.jdbc_psql;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.MedicoDiBaseDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoDiBase;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Provincia;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Utente;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gabriele
 */

public class JDBC_PSQLMedicoDiBaseDAO extends JDBC_PSQL_DAO<MedicoDiBase, Integer> implements MedicoDiBaseDAO {

    public JDBC_PSQLMedicoDiBaseDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try ( Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM medico_di_base");
            if (counter.next()) {
                return counter.getLong(1);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to count medico di base", ex);
        }

        return 0L;
    }

    @Override
    public MedicoDiBase getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT pn.sigla AS pn_sigla, pn.nome  AS pn_nome, pn.regione  AS pn_regione, "
                + "       pr.sigla AS pr_sigla, pr.nome  AS pr_nome, pr.regione  AS pr_regione, "
                + "       pm.sigla AS pm_sigla, pm.nome  AS pm_nome, pm.regione  AS pm_regione, "
                + "       pmmb.id  AS pmmb_id,  pmmb.nome AS pmmb_nome, pmmb.cognome AS pmmb_cognome, "
                + "       u.id     AS mb_id,    u.email  AS  mb_email, u.password  AS mb_password, "
                + "       u.tipo   AS mb_tipo,  p.nome   AS mb_nome,    p.cognome   AS mb_cognome,  "
                + "       p.data_nascita AS mb_data_nascita, p.luogo_nascita AS mb_luogo_nascita, "
                + "       p.codice_fiscale AS mb_codice_fiscale, p.sesso AS mb_sesso , "
                + "       mb.comune_medico AS mb_comune_medico, u.salt AS mb_salt "
                + "FROM medico_di_base as mb "
                + "JOIN paziente AS p ON mb.id = p.id "
                + "JOIN utente AS u ON mb.id = u.id "
                + "JOIN provincia AS pn ON p.sigla_provincia_nascita = pn.sigla "
                + "JOIN provincia AS pr ON p.sigla_provincia = pr.sigla "
                + "JOIN provincia AS pm ON mb.sigla_provincia_medico = pm.sigla "
                + "JOIN medico_di_base AS mmb ON p.id_medico_di_base = mmb.id "
                + "JOIN paziente AS pmmb ON mmb.id = pmmb.id "
                + "WHERE mb.id = ?")) {
            stm.setInt(1, primaryKey);
            try ( ResultSet rs = stm.executeQuery()) {

                rs.next();
                Provincia provinciaNascita = new Provincia(rs.getString("pn_sigla"),
                        rs.getString("pn_nome"),
                        rs.getString("pn_regione"));
                Provincia provinciaResidenza = new Provincia(rs.getString("pr_sigla"),
                        rs.getString("pr_nome"),
                        rs.getString("pr_regione"));
                Provincia provinciaMedico = new Provincia(rs.getString("pm_sigla"),
                        rs.getString("pm_nome"),
                        rs.getString("pm_regione"));
                MedicoDiBase medicoMedicoDiBase = new MedicoDiBase(
                        rs.getInt("pmmb_id"),
                        rs.getString("pmmb_nome"),
                        rs.getString("pmmb_cognome"));
                MedicoDiBase medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                        rs.getString("mb_email"),
                        rs.getString("mb_password"),
                        rs.getString("mb_salt"),
                        rs.getString("mb_tipo"),
                        rs.getString("mb_nome"),
                        rs.getString("mb_cognome"),
                        rs.getDate("mb_data_nascita"),
                        rs.getString("mb_luogo_nascita"),
                        provinciaNascita,
                        rs.getString("mb_codice_fiscale"),
                        rs.getString("mb_sesso"),
                        provinciaResidenza,
                        medicoMedicoDiBase,
                        rs.getString("mb_comune_medico"),
                        provinciaMedico
                );
                return medicoDiBase;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the medico di base for the passed primary key", ex);
        }
    }

    @Override
    public List<MedicoDiBase> getAll() throws DAOException {
        List<MedicoDiBase> medici = new ArrayList<>();

        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery(""
                    + "SELECT pn.sigla AS pn_sigla, pn.nome  AS pn_nome, pn.regione  AS pn_regione, "
                    + "       pr.sigla AS pr_sigla, pr.nome  AS pr_nome, pr.regione  AS pr_regione, "
                    + "       pm.sigla AS pm_sigla, pm.nome  AS pm_nome, pm.regione  AS pm_regione, "
                    + "       pmmb.id  AS pmmb_id,  pmmb.nome AS pmmb_nome, pmmb.cognome AS pmmb_cognome, "
                    + "       u.id     AS mb_id,    u.email  AS  mb_email, u.password  AS mb_password, "
                    + "       u.tipo   AS mb_tipo,  p.nome   AS mb_nome,    p.cognome   AS mb_cognome,  "
                    + "       p.data_nascita AS mb_data_nascita, p.luogo_nascita AS mb_luogo_nascita, "
                    + "       p.codice_fiscale AS mb_codice_fiscale, p.sesso AS mb_sesso , "
                    + "       mb.comune_medico AS mb_comune_medico, u.salt AS mb_salt "
                    + "FROM medico_di_base as mb "
                    + "JOIN paziente AS p ON mb.id = p.id "
                    + "JOIN utente AS u ON mb.id = u.id "
                    + "JOIN provincia AS pn ON p.sigla_provincia_nascita = pn.sigla "
                    + "JOIN provincia AS pr ON p.sigla_provincia = pr.sigla "
                    + "JOIN provincia AS pm ON mb.sigla_provincia_medico = pm.sigla "
                    + "JOIN medico_di_base AS mmb ON p.id_medico_di_base = mmb.id "
                    + "JOIN paziente AS pmmb ON mmb.id = pmmb.id")) {

                while (rs.next()) {
                    Provincia provinciaNascita = new Provincia(rs.getString("pn_sigla"),
                            rs.getString("pn_nome"),
                            rs.getString("pn_regione"));
                    Provincia provinciaResidenza = new Provincia(rs.getString("pr_sigla"),
                            rs.getString("pr_nome"),
                            rs.getString("pr_regione"));
                    Provincia provinciaMedico = new Provincia(rs.getString("pm_sigla"),
                            rs.getString("pm_nome"),
                            rs.getString("pm_regione"));
                    MedicoDiBase medicoMedicoDiBase = new MedicoDiBase(
                            rs.getInt("pmmb_id"),
                            rs.getString("pmmb_nome"),
                            rs.getString("pmmb_cognome"));
                    MedicoDiBase medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                            rs.getString("mb_email"),
                            rs.getString("mb_password"),
                            rs.getString("mb_salt"),
                            rs.getString("mb_tipo"),
                            rs.getString("mb_nome"),
                            rs.getString("mb_cognome"),
                            rs.getDate("mb_data_nascita"),
                            rs.getString("mb_luogo_nascita"),
                            provinciaNascita,
                            rs.getString("mb_codice_fiscale"),
                            rs.getString("mb_sesso"),
                            provinciaResidenza,
                            medicoMedicoDiBase,
                            rs.getString("mb_comune_medico"),
                            provinciaMedico
                    );

                    medici.add(medicoDiBase);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of medici di base", ex);
        }

        return medici;
    }

    @Override
    public MedicoDiBase update(MedicoDiBase medicoDiBase) throws DAOException {
        if (medicoDiBase == null) {
            throw new DAOException("parameter not valid", new IllegalArgumentException("The passed medico di base is null"));
        }

        try ( PreparedStatement std = CON.prepareStatement("UPDATE utente SET email = ?, password = ?, salt = ?, tipo = ?::user_type WHERE id = ?;"
                + "UPDATE paziente SET nome = ?, cognome = ?, data_nascita = ?, luogo_nascita = ?, "
                + "sigla_provincia_nascita = ?, codice_fiscale = ?, sesso = ?, sigla_provincia = ?, "
                + "id_medico_di_base = ? WHERE id = ?;"
                + "UPDATE medico_di_base SET comune_medico = ?, sigla_provincia_medico = ? WHERE id = ?;")) {
            std.setString(1, medicoDiBase.getEmail());
            std.setString(2, medicoDiBase.getPassword());
            std.setString(3, medicoDiBase.getSalt());
            std.setString(4, medicoDiBase.getTipo());
            std.setInt(5, medicoDiBase.getId());
            std.setString(6, medicoDiBase.getNome());
            std.setString(7, medicoDiBase.getCognome());
            std.setDate(8, medicoDiBase.getDataNascita());
            std.setString(9, medicoDiBase.getLuogoNascita());
            std.setString(10, medicoDiBase.getProvinciaNascita().getSigla());
            std.setString(11, medicoDiBase.getCodiceFiscale());
            std.setString(12, medicoDiBase.getSesso());
            std.setString(13, medicoDiBase.getProvincia().getSigla());
            std.setInt(14, medicoDiBase.getMedicoDiBase().getId());
            std.setInt(15, medicoDiBase.getId());
            std.setString(16, medicoDiBase.getComuneMedico());
            std.setString(17, medicoDiBase.getProvinciaMedico().getSigla());
            std.setInt(18, medicoDiBase.getId());

            if (std.executeUpdate() == 1) {
                return medicoDiBase;
            } else {
                throw new DAOException("Impossible to update the medico di base");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update the medico di base", ex);
        }
    }

    @Override
    public MedicoDiBase getByUtente(Utente utente) throws DAOException {
        if (utente == null) {
            throw new DAOException("utente is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT pn.sigla AS pn_sigla, pn.nome  AS pn_nome, pn.regione  AS pn_regione, "
                + "       pr.sigla AS pr_sigla, pr.nome  AS pr_nome, pr.regione  AS pr_regione, "
                + "       pm.sigla AS pm_sigla, pm.nome  AS pm_nome, pm.regione  AS pm_regione, "
                + "       pmmb.id  AS mmb_id,  pmmb.nome AS mmb_nome, pmmb.cognome AS mmb_cognome, "
                + "       p.nome   AS mb_nome,    p.cognome   AS mb_cognome,  "
                + "       p.data_nascita AS mb_data_nascita, p.luogo_nascita AS mb_luogo_nascita, "
                + "       p.codice_fiscale AS mb_codice_fiscale, p.sesso AS mb_sesso , "
                + "       mb.comune_medico AS mb_comune_medico "
                + "FROM medico_di_base as mb "
                + "JOIN paziente AS p ON mb.id = p.id "
                + "JOIN provincia AS pn ON p.sigla_provincia_nascita = pn.sigla "
                + "JOIN provincia AS pr ON p.sigla_provincia = pr.sigla "
                + "JOIN provincia AS pm ON mb.sigla_provincia_medico = pm.sigla "
                + "JOIN medico_di_base AS mmb ON p.id_medico_di_base = mmb.id "
                + "JOIN paziente AS pmmb ON mmb.id = pmmb.id "
                + "WHERE mb.id = ?")) {
            stm.setInt(1, utente.getId());
            try ( ResultSet rs = stm.executeQuery()) {

                rs.next();
                Provincia provinciaNascita = new Provincia(rs.getString("pn_sigla"),
                        rs.getString("pn_nome"),
                        rs.getString("pn_regione"));
                Provincia provinciaResidenza = new Provincia(rs.getString("pr_sigla"),
                        rs.getString("pr_nome"),
                        rs.getString("pr_regione"));
                Provincia provinciaMedico = new Provincia(rs.getString("pm_sigla"),
                        rs.getString("pm_nome"),
                        rs.getString("pm_regione"));
                MedicoDiBase medicoMedicoDiBase = new MedicoDiBase(
                        rs.getInt("mmb_id"),
                        rs.getString("mmb_nome"),
                        rs.getString("mmb_cognome"));
                MedicoDiBase medicoDiBase = new MedicoDiBase(utente.getId(),
                        utente.getEmail(),
                        utente.getPassword(),
                        utente.getSalt(),
                        utente.getTipo(),
                        rs.getString("mb_nome"),
                        rs.getString("mb_cognome"),
                        rs.getDate("mb_data_nascita"),
                        rs.getString("mb_luogo_nascita"),
                        provinciaNascita,
                        rs.getString("mb_codice_fiscale"),
                        rs.getString("mb_sesso"),
                        provinciaResidenza,
                        medicoMedicoDiBase,
                        rs.getString("mb_comune_medico"),
                        provinciaMedico
                );
                return medicoDiBase;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the medico di base for the passed user", ex);
        }
    }

}
