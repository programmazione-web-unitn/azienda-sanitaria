/*
 * Azienda Sanitaria
 */
package it.unitn.disi.as.aziendasanitaria.listeners;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.factories.jdbc_psql.JDBC_PSQL_DAOFactory;
//import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Listener che instaura la connessione con il database.
 *
 * @author gabriele
 */
public class DbConnectionListener implements ServletContextListener {
    
    private static final Logger logger = LogManager.getLogger(DbConnectionListener.class);

    /**
     * Instaura la connessione con il database e salva nella sessione un oggetto
     * di classe DAOFactory che permette di interfacciarsi facilmente con il DB.
     *
     * @param sce
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        //writing some logs at different levels
        logger.info("Inizializzazione connessione al database");

        String dburl = sce.getServletContext().getInitParameter("dburl");
        String dbuser = sce.getServletContext().getInitParameter("dbuser");
        String dbpasswd = sce.getServletContext().getInitParameter("dbpasswd");
        try {
            JDBC_PSQL_DAOFactory.configure(dburl, dbuser, dbpasswd);
            DAOFactory daoFactory = JDBC_PSQL_DAOFactory.getInstance();
            sce.getServletContext().setAttribute("daoFactory", daoFactory);
        } catch (DAOFactoryException ex) {
            logger.error("Errore durante la connessione al db", ex);
            throw new RuntimeException(ex);
        }
    }

    /**
     * Chiude l'interfaccia con il database.
     *
     * @param sce
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        DAOFactory daoFactory = (DAOFactory) sce.getServletContext().getAttribute("daoFactory");
        if (daoFactory != null) {
            daoFactory.shutdown();
        }
        daoFactory = null;
    }
}
