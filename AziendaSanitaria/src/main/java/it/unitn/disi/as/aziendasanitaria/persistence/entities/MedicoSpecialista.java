/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.entities;

import java.sql.Date;

/**
 *
 * @author gabriele
 */
public class MedicoSpecialista extends Paziente {

    public MedicoSpecialista(Integer id, String email, String password, String salt, String tipo, String nome, String cognome, Date dataNascita, 
            String luogoNascita, Provincia provinciaNascita, String codiceFiscale, String sesso, Provincia provincia,
            MedicoDiBase medicoDiBase) {
        super(id, email, password, salt, tipo, nome, cognome, dataNascita, luogoNascita, provinciaNascita,
                codiceFiscale, sesso, provincia, medicoDiBase);
    }
    
    public MedicoSpecialista(Integer id, String nome, String cognome){
        super(id, nome, cognome);
    }
}
