/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.servlets;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.MedicoDiBaseDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.PazienteDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoDiBase;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Paziente;
import it.unitn.disi.as.aziendasanitaria.utilities.Authentication;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author davide
 */
public class CambiaMedicoServlet extends HttpServlet {

    private PazienteDAO pazienteDao;
    private MedicoDiBaseDAO medicoDiBaseDao;
    
    private static final Logger logger = LogManager.getLogger(CambiaMedicoServlet.class);

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory");
        }
        try {
            pazienteDao = daoFactory.getDAO(PazienteDAO.class);
            medicoDiBaseDao = daoFactory.getDAO(MedicoDiBaseDAO.class);
        } catch (DAOFactoryException ex) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory", ex);
        }
    }
    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Paziente paziente = (Paziente) request.getAttribute("user");
        if(paziente == null){
            logger.error("Nessun paziente loggato");
            throw new ServletException("Nessun paziente loggato");
        }
        
        int idMedico;
        try {            
            idMedico = Integer.parseInt(request.getParameter("idMedico"));
        } catch(NumberFormatException ex){
            logger.debug("Parametro idMedico non passato o non intero", ex);
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        
        MedicoDiBase medico;
        try {
            medico = medicoDiBaseDao.getByPrimaryKey(idMedico);
        } catch(DAOException ex) {
            logger.debug("Impossibile ottenere il medico di base con id="+idMedico, ex);
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        
        if(paziente.getId() == idMedico) {
            logger.debug("Un medico non può avere sè stesso come paziente");
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        } else if(paziente.getMedicoDiBase().getId() == idMedico) {
            logger.debug("Medico già assegnato al paziente");
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        } else if(!paziente.getProvincia().equals(medico.getProvinciaMedico())) {
            logger.debug("Il medico è di una provincia diversa da quella del paziente");
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        
        paziente.setMedicoDiBase(medico);
        try {
            pazienteDao.update(paziente);
        } catch(DAOException ex) {
            logger.error("Impossibile aggiornare il paziente", ex);
            throw new ServletException("Impossibile aggiornare il paziente", ex);
        }
        
        request.getSession().setAttribute("success", "Medico cambiato correttamente");
        Authentication.redirectTo("Paziente/profilo", request, response);
//        request.getRequestDispatcher("/Paziente/profilo").forward(request, response);        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
