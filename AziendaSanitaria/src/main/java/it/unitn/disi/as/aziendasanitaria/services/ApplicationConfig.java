/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.services;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author davide
 */
@javax.ws.rs.ApplicationPath("services")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(it.unitn.disi.as.aziendasanitaria.services.ListaFarmaciService.class);
        resources.add(it.unitn.disi.as.aziendasanitaria.services.ListaMediciService.class);
        resources.add(it.unitn.disi.as.aziendasanitaria.services.ListaPazientiService.class);
        resources.add(it.unitn.disi.as.aziendasanitaria.services.ListaTipiDiEsameService.class);
        resources.add(it.unitn.disi.as.aziendasanitaria.services.ListaTipiDiVisitaService.class);
        resources.add(it.unitn.disi.as.aziendasanitaria.services.ParcoPazientiService.class);
    }
    
}
