/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.entities;

import java.sql.Timestamp;

/**
 *
 * @author fran
 */
public class Esame {

    private Integer id;
    private Paziente paziente;
    private MedicoDiBase medicoDiBase;
    private TipoDiEsame tipoDiEsame;
    private Timestamp dataOraPrescrizione;
    private Ssp ssp;
    private Timestamp dataOraErogazione;
    private String report;

    public Esame(Integer id, Paziente paziente, MedicoDiBase medicoDiBase, TipoDiEsame tipoDiEsame, Timestamp dataOraPrescrizione, Ssp ssp, Timestamp dataOraErogazione, String report) {
        this.id = id;
        this.paziente = paziente;
        this.medicoDiBase = medicoDiBase;
        this.tipoDiEsame = tipoDiEsame;
        this.dataOraPrescrizione = dataOraPrescrizione;
        this.ssp = ssp;
        this.dataOraErogazione = dataOraErogazione;
        this.report = report;
    }
    
    public Esame(Integer id, Paziente paziente, MedicoDiBase medicoDiBase, TipoDiEsame tipoDiEsame, Timestamp dataOraPrescrizione){
        this(id, paziente, medicoDiBase, tipoDiEsame, dataOraPrescrizione, null, null, null);
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the paziente
     */
    public Paziente getPaziente() {
        return paziente;
    }

    /**
     * @param paziente the paziente to set
     */
    public void setPaziente(Paziente paziente) {
        this.paziente = paziente;
    }

    /**
     * @return the medicoDiBase
     */
    public MedicoDiBase getMedicoDiBase() {
        return medicoDiBase;
    }

    /**
     * @param medicoDiBase the medicoDiBase to set
     */
    public void setMedicoDiBase(MedicoDiBase medicoDiBase) {
        this.medicoDiBase = medicoDiBase;
    }

    /**
     * @return the tipoDiEsame
     */
    public TipoDiEsame getTipoDiEsame() {
        return tipoDiEsame;
    }

    /**
     * @param tipoDiEsame the tipoDiEsame to set
     */
    public void setTipoDiEsame(TipoDiEsame tipoDiEsame) {
        this.tipoDiEsame = tipoDiEsame;
    }

    /**
     * @return the dataOraPrescrizione
     */
    public Timestamp getDataOraPrescrizione() {
        return dataOraPrescrizione;
    }

    /**
     * @param dataOraPrescrizione the dataOraPrescrizione to set
     */
    public void setDataOraPrescrizione(Timestamp dataOraPrescrizione) {
        this.dataOraPrescrizione = dataOraPrescrizione;
    }

    /**
     * @return the ssp
     */
    public Ssp getSsp() {
        return ssp;
    }

    /**
     * @param ssp the ssp to set
     */
    public void setSsp(Ssp ssp) {
        this.ssp = ssp;
    }

    /**
     * @return the dataOraErogazione
     */
    public Timestamp getDataOraErogazione() {
        return dataOraErogazione;
    }

    /**
     * @param dataOraErogazione the dataOraErogazione to set
     */
    public void setDataOraErogazione(Timestamp dataOraErogazione) {
        this.dataOraErogazione = dataOraErogazione;
    }

    /**
     * @return the report
     */
    public String getReport() {
        return report;
    }

    /**
     * @param report the report to set
     */
    public void setReport(String report) {
        this.report = report;
    }
}
