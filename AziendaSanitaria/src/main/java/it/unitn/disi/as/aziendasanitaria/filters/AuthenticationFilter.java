/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.filters;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.UtenteDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Utente;
import it.unitn.disi.as.aziendasanitaria.utilities.Authentication;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author davide
 */
public class AuthenticationFilter implements Filter {
    
    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured. 
    private FilterConfig filterConfig = null;
    private static final Logger logger = LogManager.getLogger(AuthenticationFilter.class);

    
    public AuthenticationFilter() {
    }    
    
    private void doBeforeProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
        
        ServletContext servletContext = ((HttpServletRequest) request).getServletContext();
        DAOFactory daoFactory = (DAOFactory) servletContext.getAttribute("daoFactory");
        if (daoFactory == null) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory");
        }
        UtenteDAO utenteDao;
        try {
            utenteDao = daoFactory.getDAO(UtenteDAO.class);
        } catch (DAOFactoryException ex) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory", ex);
        }
        
        if (request instanceof HttpServletRequest) {            
            HttpSession session = ((HttpServletRequest) request).getSession(false);    
            
            Utente user = null;
            if (session != null) {
                logger.debug("Sessione esistente");
                user = (Utente) session.getAttribute("user");
                
                if(user == null)
                    logger.debug("Utente non presente nella sessione");
            }
            
            if(user==null && ((HttpServletRequest) request).getCookies() != null){            
                for(Cookie cookie : ((HttpServletRequest) request).getCookies()){
                    if(cookie.getName().equals("rememberMe")){
                        logger.debug("Cookie rememberMe presente");
                        
                        String token = cookie.getValue();                        
                        try {
                            user = utenteDao.getByToken(token);
                            
                            // verifichiamo scadenzaToken > now  
                            if(user.getScadenzaToken().after(new Timestamp(new Date().getTime()))){
                                logger.debug("Token valido");
                                
                                user = Authentication.getUtenteSpecifico(daoFactory, user);
                                session = ((HttpServletRequest) request).getSession();
                                session.setAttribute("user", user);
                            } else {
                                
                                logger.debug("Token scaduto");                                
                                
                                // eliminiamo token da db
                                user.setToken(null);
                                user.setScadenzaToken(null);
                                try {
                                    utenteDao.update(user);
                                } catch (DAOException ex){
                                    logger.error("Eliminazione token dal db non riuscita");
                                    throw new ServletException("Eliminazione token dal db non riuscita", ex);
                                }
                                user = null;
                            }
                        } catch (DAOException ex) {
                            logger.debug("Impossibile ottenere l'utente dal token 'rememberMe'", ex);
                            user = null;
                        } catch (DAOFactoryException ex) {
                            logger.error("Impossibile ottenere il dao factory", ex);
                            throw new ServletException("Impossibile ottenere il dao factory", ex);
                        }
                        break;
                    }
                }
            }
            
            if (user != null){
                logger.debug("Utente con id='"+user.getId()+"' autenticato");
                request.setAttribute("user", user);
            }
        }
    }    
    
    private void doAfterProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
    }

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {
        
        logger.debug("doFilter()");

        
        doBeforeProcessing(request, response);        

        chain.doFilter(request, response);
        
        doAfterProcessing(request, response);
    }

    /**
     * Return the filter configuration object for this filter.
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    public void destroy() {        
    }

    /**
     * Init method for this filter
     */
    public void init(FilterConfig filterConfig) {        
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            logger.debug("Initializing filter");
        }
    }
    
}
