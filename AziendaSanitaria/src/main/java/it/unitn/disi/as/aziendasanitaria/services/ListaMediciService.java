/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.services;

import it.unitn.disi.as.aziendasanitaria.persistence.entities.Result;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Element;
import com.google.gson.Gson;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.MedicoDiBaseDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoDiBase;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Paziente;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * REST Web Service
 *
 * @author davide
 */
@Path("listaMedici")
public class ListaMediciService {
    
    @Context 
    private ServletContext servletContext;

    private MedicoDiBaseDAO medicoDiBaseDao;
    private static final Logger logger = LogManager.getLogger(ListaMediciService.class);

    /**
     * Creates a new instance of ListaMediciService
     */
    public ListaMediciService() {
    }

    /**
     * Retrieves representation of an instance of it.unitn.disi.as.aziendasanitaria.services.ListaMediciService
     * @param term
     * @param request
     * @return an instance of java.lang.String
     * @throws javax.servlet.ServletException
     */
    @GET
    @Path("{term}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson(@PathParam("term") String term, @Context HttpServletRequest request) throws ServletException {

        DAOFactory daoFactory = (DAOFactory) servletContext.getAttribute("daoFactory");
        try {
            medicoDiBaseDao = daoFactory.getDAO(MedicoDiBaseDAO.class);
        } catch (DAOFactoryException ex) {
            logger.error("Impossibile ottenere il dao medicoDiBase", ex);
            throw new ServletException("Impossibile ottenere il dao medicoDiBase", ex);
        }
        
        List<Element> results = new ArrayList<>();
        List<MedicoDiBase> mediciDiBase = new ArrayList<MedicoDiBase>();
        try {
            mediciDiBase = medicoDiBaseDao.getAll();
        } catch (DAOException ex) {
            logger.error("Impossibile ottenere la lista dei medici di base", ex);
            throw new ServletException("Impossibile ottenere la lista dei medici di base", ex);
        }
        
        Paziente paziente = (Paziente) request.getAttribute("user");
       
        if ((term == null) || term.length() == 0) {
            // restituiamo la lista dei mediciDiBase che possono diventare medici del paziente eccetto il paziente stesso e il medico già associato al paziente
            for (int i = 0; i < mediciDiBase.size(); i++) {
                MedicoDiBase m = mediciDiBase.get(i);
                if(!paziente.getId().equals(m.getId()) && !paziente.getMedicoDiBase().getId().equals(m.getId()) &&
                        paziente.getProvincia().equals(m.getProvinciaMedico()))
                    results.add(new Element(m.getId(), m.getNome() + " " + m.getCognome()));
            }
        } else {
            term = term.toLowerCase();
            for (int i = 0; i < mediciDiBase.size(); i++) {
                MedicoDiBase m = mediciDiBase.get(i);
                if((m.getNome().toLowerCase()+" "+m.getCognome().toLowerCase()).contains(term) &&
                        !paziente.getId().equals(m.getId()) && !paziente.getMedicoDiBase().getId().equals(m.getId()) &&
                        paziente.getProvincia().equals(m.getProvinciaMedico())){
                    results.add(new Element(m.getId(), m.getNome() + " " + m.getCognome()));
                }
            }
        }

        Gson gson = new Gson();
        return gson.toJson(new Result(results.toArray(new Element[0])));
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson(@Context HttpServletRequest request) throws ServletException {
        return getJson(null, request);
    }
    
}
