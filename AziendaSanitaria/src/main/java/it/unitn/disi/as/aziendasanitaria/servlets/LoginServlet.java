/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.servlets;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.UtenteDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Utente;
import it.unitn.disi.as.aziendasanitaria.utilities.Authentication;
import it.unitn.disi.as.aziendasanitaria.utilities.Security;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author fran
 */
public class LoginServlet extends HttpServlet {

    private UtenteDAO userDao;
    private DAOFactory daoFactory;

    private static final Logger logger = LogManager.getLogger(LoginServlet.class);

    @Override
    public void init() throws ServletException {
        daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory");
        }
        try {
            userDao = daoFactory.getDAO(UtenteDAO.class);
        } catch (DAOFactoryException ex) {
            logger.error("Impossibile ottenere il dao user");
            throw new ServletException("Impossibile ottenere il dao factory", ex);
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String email = request.getParameter("inputEmail");
        String password = request.getParameter("inputPassword");

        try {
            Utente user = userDao.getByEmail(email);

            if (user != null && user.getPassword().equals(Security.hash(password + user.getSalt()))) {
                logger.info("Credenziali corrette");
                user = Authentication.getUtenteSpecifico(daoFactory, user);
                request.getSession().setAttribute("user", user);

                if (request.getParameter("ricordami") != null) {
                    logger.debug("Funzione 'ricordami' richiesta");
                    String token = Security.randomAlphaNumeric(128);
                    Date date = new Date();
                    long oneDay = 24 * 60 * 60 * 1000;
                    Timestamp scadenzaToken = new Timestamp(date.getTime() + 30 * oneDay); // scadenza dopo 30 giorni

                    userDao.setRememberMe(user, token, scadenzaToken);
                    Cookie cookieRememberMe = new Cookie("rememberMe", token);
                    cookieRememberMe.setMaxAge((int) (30 * oneDay / 1000));
                    response.addCookie(cookieRememberMe);
                } else {
                    // eliminazione cookie rememberMe
                    Cookie cookieRicordami = new Cookie("rememberMe", "");
                    cookieRicordami.setMaxAge(0);
                    response.addCookie(cookieRicordami);
                }

                Authentication.redirectTo(Authentication.getUserPath(user)+"/home", request, response);
            } else {
                logger.debug("Credenziali errate");
                request.getSession().setAttribute("error", "Username o password errati");
                Authentication.redirectToLogin(request, response);
            }
        } catch (DAOException ex) {
            logger.debug("Utente non esistente", ex);
            request.getSession().setAttribute("error", "Username o password errati");
            Authentication.redirectToLogin(request, response);
        } catch (DAOFactoryException ex) {
            logger.error("Impossibile ottenere il dao factory", ex);
            throw new ServletException("Impossibile ottenere il dao factory", ex);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
