/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.utilities;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.UtenteDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Utente;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author fran
 */

public class Security {
    
    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final Logger logger = LogManager.getLogger(Security.class);
    
    public static String hash(String data) {
        try {
            // Static getInstance method is called with hashing SHA-256
            MessageDigest md = MessageDigest.getInstance("SHA-256");       

            // digest() method is called to calculate message digest 
            //  of an input digest() return array of byte 
            byte[] messageDigest = md.digest(data.getBytes());

            // Convert byte array into signum representation 
            BigInteger no = new BigInteger(1, messageDigest);

            // Convert message digest into hex value 
            String hashtext = no.toString(16);        
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch(NoSuchAlgorithmException ex) {
            logger.error("L'algoritmo SHA-256 non esiste");
        }
        return "none";
    }
    
    public static String randomAlphaNumeric(int count) {
        Random random = new SecureRandom();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < count; i++) {
            builder.append(ALPHA_NUMERIC_STRING.charAt(random.nextInt(ALPHA_NUMERIC_STRING.length())));
        }
        return builder.toString();
    }
    /**
     * Una password è sicura se rispetta le seguenti regole:
     * 
     * (?=.*[a-z])               contiene almeno un carattere minuscolo.
     * (?=.*[A-Z])               contiene almeno un carattere maiuscolo.
     * (?=.*[0-9])               contiene almeno un numero.
     * (?=.*[!@#\\$%\\^&\\*])    contiene almeno un carattere speciale tra {!, @, #, $, %, ^, &, *}.
     * .{8,}                     ha almeno 8 caratteri.
     * @param password
     * @return true se la password è sicura, false altrimenti.
     */
    public static boolean isPasswordSicura(String password){
        return password.matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*]).{8,}");
    }
    
    public static boolean setNuovaPassword(Utente user, String password, String confermaPassword, UtenteDAO userDao) throws DAOException {       
        if(!isPasswordSicura(password) || !password.equals(confermaPassword)){
            return false;
        }        
        user.setSalt(Security.randomAlphaNumeric(64));
        user.setPassword(Security.hash(password + user.getSalt()));
        user.setRecuperoToken(null);
        user.setScadenzaRecupero(null);
        userDao.update(user);
        return true;
    }

}
