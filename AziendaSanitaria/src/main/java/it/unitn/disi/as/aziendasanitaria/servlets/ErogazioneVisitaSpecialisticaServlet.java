/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.servlets;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.VisitaSpecialisticaDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoSpecialista;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Paziente;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.VisitaSpecialistica;
import it.unitn.disi.as.aziendasanitaria.utilities.Authentication;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author dima
 */
public class ErogazioneVisitaSpecialisticaServlet extends HttpServlet {

    private VisitaSpecialisticaDAO visitaSpecialisticaDao;
    
    private static final Logger logger = LogManager.getLogger(ErogazioneVisitaSpecialisticaServlet.class);
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory");
        }
        try {
            visitaSpecialisticaDao = daoFactory.getDAO(VisitaSpecialisticaDAO.class);
        } catch (DAOFactoryException ex) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory", ex);
        }
        
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        MedicoSpecialista medico = (MedicoSpecialista) request.getAttribute("user");
        if(medico == null){
            logger.error("Nessun medico specialista loggato");
            throw new ServletException("Nessun medico specialista loggato");
        }
        
        VisitaSpecialistica visita = (VisitaSpecialistica) request.getAttribute("visitaSpecialistica");
        if(visita == null){
            logger.error("Nessuna visita specialistica indicata");
            throw new ServletException("Nessuna visita specialistica indicata");
        }
        
        Paziente paziente = (Paziente) request.getAttribute("paziente");
        if(paziente == null){
            logger.error("Nessun paziente indicato");
            throw new ServletException("Nessun paziente indicato");
        }
        String report = request.getParameter("report");
        if(report != null && report.trim().isEmpty()){
            logger.error("Nessun report indicato");
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        Timestamp now = new Timestamp(new Date().getTime());
        visita.setDataOraErogazione(now);
        visita.setMedicoSpecialista(medico);
        visita.setReport(report);
        
        try {
            visitaSpecialisticaDao.visita(visita);
            logger.debug("visita specialistica aggionatro nel database");
        } catch(DAOException ex) {
            logger.error("Impossibile aggiornare la visita specialistica nel database", ex);
            throw new ServletException("Impossibile aggionare visita specialistica nel database", ex);
        }
        
        request.getSession().setAttribute("success", "Visita specialistica prescritta correttamente");
        Authentication.redirectTo("Specialista/paziente?idPaziente=" + paziente.getId(), request, response);
     
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
