/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.utilities;

import java.io.File;
import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;

/**
 *
 * @author davide
 */
public class QRGenerator {
    public static File generate(String s){
       return QRCode.from(s).to(ImageType.JPG).file();
    }
}
