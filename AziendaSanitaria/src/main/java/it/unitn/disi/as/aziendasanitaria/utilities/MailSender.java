/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.utilities;

import java.util.Date;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author davide
 */
public class MailSender {
    
    private static final Logger logger = LogManager.getLogger(MailSender.class);
    
    private Session session;
    private String host;    
    private String redirectEmail;
    
    public MailSender(final String host, final String port, final String username, final String password, final String redirectEmail) {
        this.host = host;
        this.redirectEmail = redirectEmail;
        
        Properties props = System.getProperties();
        props.setProperty("mail.smtp.host", host);
        props.setProperty("mail.smtp.port", port);
        props.setProperty("mail.smtp.socketFactory.port", port);
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");
        props.setProperty("mail.debug", "true");
        
        this.session = Session.getInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
    }
    
    public void send(String to, final String subject, final String text) throws MessagingException {
        if(redirectEmail != null)
            to = redirectEmail;
        
        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(host));
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to, false));
        msg.setSubject(subject);
        msg.setText(text);
        msg.setSentDate(new Date());
        Transport.send(msg);
        
        logger.debug("Inviata email:"+
                "\nmittente: "+session.getProperty("mail.smtp.host")+
                "\ndestinatario: "+to+
                "\noggetto: "+subject+
                "\nmessaggio: '"+text+"'");
    }
    
}
