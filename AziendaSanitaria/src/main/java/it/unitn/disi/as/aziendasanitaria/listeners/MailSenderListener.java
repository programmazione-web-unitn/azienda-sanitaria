/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.listeners;

import it.unitn.disi.as.aziendasanitaria.utilities.MailSender;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Web application lifecycle listener.
 *
 * @author davide
 */
public class MailSenderListener implements ServletContextListener {

    private static final Logger logger = LogManager.getLogger(MailSenderListener.class);
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        logger.info("Inizializzazione mail sender");
        
        String smtpUsername = sce.getServletContext().getInitParameter("smtp-username");
        String smtpPort = sce.getServletContext().getInitParameter("smtp-port");
        String smtpHostname = sce.getServletContext().getInitParameter("smtp-hostname");
        String smtpPassword = sce.getServletContext().getInitParameter("smtp-password");
        
        String debug = sce.getServletContext().getInitParameter("debug");
        String redirectEmail = sce.getServletContext().getInitParameter("redirectEmail");

        if (smtpUsername == null) {
            logger.error("il parametro smtp-username non è definito nel web.xml");
            throw new RuntimeException("il parametro smtp-username non è definito nel web.xml");
        }
        if (smtpPort == null) {
            logger.error("il parametro smtp-port non è definito nel web.xml");
            throw new RuntimeException("il parametro smtp-port non è definito nel web.xml");
        }
        if (smtpHostname == null) {
            logger.error("il parametro smtp-host non è definito nel web.xml");
            throw new RuntimeException("il parametro smtp-host non è definito nel web.xml");
        }
        if (smtpPassword == null) {
            logger.error("il parametro smtp-password non è definito nel web.xml");
            throw new RuntimeException("il parametro smtp-password non è definito nel web.xml");
        }
        
        if(debug == null || !debug.equals("true")){
            redirectEmail = null;
        }
        
        try {
            MailSender mailSender = new MailSender(smtpHostname, smtpPort, smtpUsername, smtpPassword, redirectEmail);
            sce.getServletContext().setAttribute("mailSender", mailSender);
        } catch(Exception ex) {
            logger.error("Errore durente la creazione del mail sender");
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        logger.info("Distruzione mail sender");
    }
}
