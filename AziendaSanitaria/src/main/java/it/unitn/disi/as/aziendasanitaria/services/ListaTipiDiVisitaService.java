/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.services;

import it.unitn.disi.as.aziendasanitaria.persistence.entities.Result;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Element;
import com.google.gson.Gson;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.TipoDiVisitaDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.TipoDiVisita;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.core.Context;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * REST Web Service
 *
 * @author davide
 */
@Path("listaTipiDiVisita")
public class ListaTipiDiVisitaService {

    @Context 
    private ServletContext servletContext;
    
    private TipoDiVisitaDAO tipoDiVisitaDao;
    private static final Logger logger = LogManager.getLogger(ListaTipiDiVisitaService.class);

    /**
     * Creates a new instance of ListaTipiDiVisitaService
     */
    public ListaTipiDiVisitaService() {
    }

    /**
     * Retrieves representation of an instance of it.unitn.disi.as.aziendasanitaria.services.ListaTipiDiVisitaService
     * @param term
     * @return an instance of java.lang.String
     * @throws javax.servlet.ServletException
     */
    @GET
    @Path("{term}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson(@PathParam("term") String term) throws ServletException {
        DAOFactory daoFactory = (DAOFactory) servletContext.getAttribute("daoFactory");
        try {
            tipoDiVisitaDao = daoFactory.getDAO(TipoDiVisitaDAO.class);
        } catch (DAOFactoryException ex) {
            logger.error("Impossibile ottenere tipoDiVisitaDao", ex);
            throw new ServletException("Impossibile ottenere tipoDiVisitaDao", ex);
        }
                
        List<Element> results = new ArrayList<>();
        List<TipoDiVisita> tipiDiVisita = new ArrayList<TipoDiVisita>();
        try {
            tipiDiVisita = tipoDiVisitaDao.getAll();
        } catch (DAOException ex) {
            logger.error("Impossibile ottenere la lista dei tipi di visite specialistiche", ex);
            throw new ServletException("Impossibile ottenere la lista dei tipi di visite specialistiche", ex);
        }
        
        if ((term == null) || term.length() == 0) {
            // restituiamo la lista di tutti i tipi di visite specialistiche presenti nel db            
            for (int i = 0; i < tipiDiVisita.size(); i++) {
                TipoDiVisita v = tipiDiVisita.get(i);
                results.add(new Element(v.getId(), v.getNome()));
            }
        } else {
            term = term.toLowerCase();
            for (int i = 0; i < tipiDiVisita.size(); i++) {
                TipoDiVisita v = tipiDiVisita.get(i);
                if(v.getNome().toLowerCase().contains(term)){
                    results.add(new Element(v.getId(), v.getNome()));
                }
            }
        }

        Gson gson = new Gson();
        return gson.toJson(new Result(results.toArray(new Element[0])));
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() throws ServletException {
        return getJson(null);
    }

}
