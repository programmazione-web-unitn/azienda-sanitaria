package it.unitn.disi.as.aziendasanitaria.persistence.dao.jdbc_psql;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.VisitaDiBaseDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoDiBase;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Paziente;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Provincia;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.VisitaDiBase;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fran
 */
public class JDBC_PSQLVisitaDiBaseDAO extends JDBC_PSQL_DAO<VisitaDiBase, Integer> implements VisitaDiBaseDAO {

    public JDBC_PSQLVisitaDiBaseDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try ( Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM visita_di_base");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count examinations", ex);
        }

        return 0L;
    }

    @Override
    public VisitaDiBase getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT mb.id as mb_id, mb.nome as mb_nome, mb.cognome as mb_cognome, "
                + "       p.id as p_id, p.nome as p_nome, p.cognome as p_cognome, "
                + "       pr.sigla AS pr_sigla, pr.nome AS pr_nome, pr.regione AS pr_regione, "
                + "       v.id as v_id, "
                + "       v.data_ora as v_data_ora, "
                + "       v.report as v_report "
                + "FROM visita_di_base AS v "
                + "JOIN paziente as mb on v.id_medico_di_base = mb.id "
                + "JOIN paziente as p ON v.id_paziente = p.id "
                + "JOIN provincia AS pr ON pr.sigla = p.sigla_provincia "
                + "WHERE v.id = ? ORDER BY v.data_ora DESC ")) {
            stm.setInt(1, primaryKey);
            try ( ResultSet rs = stm.executeQuery()) {

                rs.next();
                MedicoDiBase medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                        rs.getString("mb_nome"),
                        rs.getString("mb_cognome"));
                Provincia provincia = new Provincia(rs.getString("pr_sigla"),
                        rs.getString("pr_nome"),
                        rs.getString("pr_regione"));
                Paziente paziente = new Paziente(rs.getInt("p_id"),
                        rs.getString("p_nome"),
                        rs.getString("p_cognome"));
                paziente.setProvincia(provincia);
                VisitaDiBase visita = new VisitaDiBase(rs.getInt("v_id"),
                        medicoDiBase,
                        paziente,
                        rs.getTimestamp("v_data_ora"),
                        rs.getString("v_report")
                );
                return visita;

            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of last user's exams", ex);
        }

    }

    @Override
    public List<VisitaDiBase> getAll() throws DAOException {
        List<VisitaDiBase> visiteDiBase = new ArrayList<>();

        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery(""
                    + "SELECT mb.id as mb_id, mb.nome as mb_nome, mb.cognome as mb_cognome, "
                    + "       p.id as p_id, p.nome as p_nome, p.cognome as p_cognome, "
                    + "       v.id as v_id, "
                    + "       v.data_ora as v_data_ora, "
                    + "       v.report as v_report "
                    + "FROM visita_di_base AS v "
                    + "JOIN paziente as mb on v.id_medico_di_base = mb.id "
                    + "JOIN paziente as p ON v.id_paziente = p.id ")) {

                while (rs.next()) {
                    MedicoDiBase medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                            rs.getString("mb_nome"),
                            rs.getString("mb_cognome"));
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome")
                    );
                    VisitaDiBase visita = new VisitaDiBase(rs.getInt("v_id"),
                            medicoDiBase,
                            paziente,
                            rs.getTimestamp("v_data_ora"),
                            rs.getString("v_report")
                    );

                    visiteDiBase.add(visita);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }

        return visiteDiBase;
    }

    @Override
    public List<VisitaDiBase> getByPaziente(Integer idPaziente) throws DAOException {
        if (idPaziente == null) {
            throw new DAOException("idPaziente is null");
        }

        List<VisitaDiBase> visiteDiBase = new ArrayList<>();

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT mb.id as mb_id, mb.nome as mb_nome, mb.cognome as mb_cognome, "
                + "       p.id as p_id, p.nome as p_nome, p.cognome as p_cognome, "
                + "       v.id as v_id, "
                + "       v.data_ora as v_data_ora, "
                + "       v.report as v_report "
                + "FROM visita_di_base AS v "
                + "JOIN paziente as mb on v.id_medico_di_base = mb.id "
                + "JOIN paziente as p ON v.id_paziente = p.id "
                + "WHERE v.id_paziente = ? ORDER BY v.data_ora DESC ")) {
            stm.setInt(1, idPaziente);
            try ( ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    MedicoDiBase medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                            rs.getString("mb_nome"),
                            rs.getString("mb_cognome"));
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome")
                    );
                    VisitaDiBase visita = new VisitaDiBase(rs.getInt("v_id"),
                            medicoDiBase,
                            paziente,
                            rs.getTimestamp("v_data_ora"),
                            rs.getString("v_report")
                    );

                    visiteDiBase.add(visita);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of last user's exams", ex);
        }

        return visiteDiBase;
    }

//    @Override
//    public List<VisitaDiBase> getLastByPaziente(Integer idPaziente) throws DAOException {
//        final int N_LAST_EXAMINATION = 5;
//
//        if (idPaziente == null) {
//            throw new DAOException("idPaziente is null");
//        }
//
//        List<VisitaDiBase> visiteDiBase = new ArrayList<>();
//
//        try ( PreparedStatement stm = CON.prepareStatement(""
//                + "SELECT mb.id as mb_id, mb.nome as mb_nome, mb.cognome as mb_cognome, "
//                + "       p.id as p_id, p.nome as p_nome, p.cognome as p_cognome, "
//                + "       v.id as v_id, "
//                + "       v.data_ora as v_data_ora, "
//                + "       v.report as v_report "
//                + "FROM visita_di_base AS v "
//                + "JOIN paziente as mb on v.id_medico_di_base = mb.id "
//                + "JOIN paziente as p ON v.id_paziente = p.id "
//                + "WHERE v.id_paziente = ? ORDER BY v.data_ora DESC "
//                + "LIMIT " + N_LAST_EXAMINATION)) {
//            stm.setInt(1, idPaziente);
//            try ( ResultSet rs = stm.executeQuery()) {
//
//                while (rs.next()) {
//                    MedicoDiBase medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
//                            rs.getString("mb_nome"),
//                            rs.getString("mb_cognome"));
//                    Paziente paziente = new Paziente(rs.getInt("p_id"),
//                            rs.getString("p_nome"),
//                            rs.getString("p_cognome")
//                    );
//                    VisitaDiBase visita = new VisitaDiBase(rs.getInt("v_id"),
//                            medicoDiBase,
//                            paziente,
//                            rs.getTimestamp("v_data_ora"),
//                            rs.getString("v_report")
//                    );
//
//                    visiteDiBase.add(visita);
//                }
//            }
//        } catch (SQLException ex) {
//            throw new DAOException("Impossible to get the list of last user's exams", ex);
//        }
//
//        return visiteDiBase;
//    }

    @Override
    public VisitaDiBase update(VisitaDiBase arg0) throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public VisitaDiBase insert(VisitaDiBase visitaBase) throws DAOException {
        if (visitaBase == null) {
            throw new DAOException("visitaDiBase is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "INSERT INTO visita_di_base (id_medico_di_base, id_paziente, data_ora, report) VALUES "
                + "(?, ?, ?, ?);")) {
            stm.setInt(1, visitaBase.getMedicoDiBase().getId());
            stm.setInt(2, visitaBase.getPaziente().getId());
            stm.setTimestamp(3, visitaBase.getDataOra());
            stm.setString(4, visitaBase.getReport());
            
            if (stm.executeUpdate() == 1) {
                return visitaBase;
            } else {
                throw new DAOException("Impossible to insert the visitaDiBase");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to insert the visitaDiBase", ex);
        }
    }

    @Override
    public boolean isVisitaDiUnPaziente(Integer idVisita, Integer idMedicoDiBase) throws DAOException {
        if (idVisita == null) {
            throw new DAOException("idVisita is null");
        }
        if (idMedicoDiBase == null) {
            throw new DAOException("idMedicoDiBase is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT * "
                + "FROM visita_di_base AS v "
                + "JOIN paziente AS p ON v.id_paziente = p.id "
                + "WHERE v.id = ? and p.id_medico_di_base = ?")) {
            stm.setInt(1, idVisita);
            stm.setInt(2, idMedicoDiBase);
            try ( ResultSet rs = stm.executeQuery()) {
                return rs.next();                    
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the exam for the passed primary key", ex);
        }
    }

}
