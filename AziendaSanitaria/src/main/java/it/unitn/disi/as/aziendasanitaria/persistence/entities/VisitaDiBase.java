/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.entities;

import java.sql.Timestamp;

/**
 *
 * @author fran
 */
public class VisitaDiBase {
    private Integer             id;
    private MedicoDiBase    medicoDiBase;
    private Paziente        paziente;
    private Timestamp       dataOra;
    private String          report;    

    public VisitaDiBase(Integer id, MedicoDiBase medicoDiBase, Paziente paziente, Timestamp dataOra, String report) {
        this.id = id;
        this.medicoDiBase = medicoDiBase;
        this.paziente = paziente;
        this.dataOra = dataOra;
        this.report = report;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the medicoDiBase
     */
    public MedicoDiBase getMedicoDiBase() {
        return medicoDiBase;
    }

    /**
     * @param medicoDiBase the medicoDiBase to set
     */
    public void setMedicoDiBase(MedicoDiBase medicoDiBase) {
        this.medicoDiBase = medicoDiBase;
    }

    /**
     * @return the paziente
     */
    public Paziente getPaziente() {
        return paziente;
    }

    /**
     * @param paziente the paziente to set
     */
    public void setPaziente(Paziente paziente) {
        this.paziente = paziente;
    }

    /**
     * @return the dataOra
     */
    public Timestamp getDataOra() {
        return dataOra;
    }

    /**
     * @param dataOra the dataOra to set
     */
    public void setDataOra(Timestamp dataOra) {
        this.dataOra = dataOra;
    }

    /**
     * @return the report
     */
    public String getReport() {
        return report;
    }

    /**
     * @param report the report to set
     */
    public void setReport(String report) {
        this.report = report;
    }
    
    
}
