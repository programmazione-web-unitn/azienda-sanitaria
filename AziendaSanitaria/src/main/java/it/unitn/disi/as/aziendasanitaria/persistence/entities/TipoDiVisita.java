/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.entities;

/**
 *
 * @author fran
 */
public class TipoDiVisita {
        
    private Integer     id;
    private String      nome;
    private Integer     ticket;

    public TipoDiVisita(Integer id, String nome, Integer ticket) {
        this.id = id;
        this.nome = nome;
        this.ticket = ticket;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    /**
     * 
     * @return the ticket
     */
    public Integer getTicket() {
        return ticket;
    }
    
    /**
     * 
     * @param ticket 
     */
    public void setTicket(Integer ticket) {
        this.ticket = ticket;
    }
    
    
    
}
