/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao;

import it.unitn.disi.as.aziendasanitaria.persistence.entities.Ricetta;
import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author fran
 */
public interface RicettaDAO extends DAO<Ricetta, Integer> {
    
    public List<Ricetta> getAllErogati() throws DAOException;

    public List<Ricetta> getByPaziente(Integer idPaziente) throws DAOException;

    public List<Ricetta> getLastByPaziente(Integer idPaziente) throws DAOException;
    public List<Ricetta> getDaErogareByPaziente(Integer idPaziente) throws DAOException;

    public Ricetta update(Ricetta ricetta) throws DAOException;

    public List<Ricetta> getErogateByPaziente(Integer idPaziente) throws DAOException;
    
    public List<Ricetta> getErogateBySsp(String siglaProvincia) throws DAOException;
    public List<Ricetta> getErogateBySsp(String siglaProvincia, Date startDate, Date endDate) throws DAOException;
    
    public Ricetta insert(Ricetta ricetta) throws DAOException;
    
    public boolean isRicettaDiUnPaziente(Integer idRicetta, Integer idMedicoDiBase) throws DAOException;
    
    public List<List<Integer>> getSpesaFrom(int fromYear) throws DAOException;

}
