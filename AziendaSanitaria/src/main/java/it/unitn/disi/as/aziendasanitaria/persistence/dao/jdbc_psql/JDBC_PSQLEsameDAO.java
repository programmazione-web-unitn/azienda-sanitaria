/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao.jdbc_psql;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.EsameDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Esame;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoDiBase;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Paziente;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Provincia;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Ssp;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.TipoDiEsame;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author fran
 */
public class JDBC_PSQLEsameDAO extends JDBC_PSQL_DAO<Esame, Integer> implements EsameDAO {

    public JDBC_PSQLEsameDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try ( Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM esame");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count exams", ex);
        }

        return 0L;
    }

    @Override
    public Esame getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT p.id AS p_id, p.codice_fiscale AS p_codice_fiscale, p.nome AS p_nome, p.cognome AS p_cognome, "
                + "       mb.id AS mb_id, mb.nome AS mb_nome, mb.cognome AS mb_cognome, "
                + "       te.id AS te_id, te.nome AS te_nome, te.ticket AS te_ticket, "
                + "       pssp.sigla AS pssp_sigla, pssp.nome AS pssp_nome, "
                + "       pssp.regione AS pssp_regione, ssp.id AS ssp_id, "
                + "       e.id AS e_id, e.data_ora_prescrizione AS e_data_ora_prescrizione, "
                + "       e.data_ora_erogazione AS e_data_ora_erogazione, "
                + "       e.report AS e_report "
                + "FROM esame AS e "
                + "JOIN tipo_di_esame AS te ON e.id_tipo_esame = te.id "
                + "JOIN paziente AS p ON e.id_paziente = p.id "
                + "JOIN paziente AS mb ON e.id_medico_di_base = mb.id "
                + "LEFT JOIN ssp ON e.id_ssp = ssp.id "
                + "LEFT JOIN provincia AS pssp ON ssp.sigla_provincia = pssp.sigla "
                + "WHERE e.id = ?")) {
            stm.setInt(1, primaryKey);
            try ( ResultSet rs = stm.executeQuery()) {

                rs.next();
                Paziente paziente = new Paziente(rs.getInt("p_id"),
                        rs.getString("p_nome"),
                        rs.getString("p_cognome"),
                        rs.getString("p_codice_fiscale"));
                MedicoDiBase medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                        rs.getString("mb_nome"),
                        rs.getString("mb_cognome"));
                TipoDiEsame tipoDiEsame = new TipoDiEsame(rs.getInt("te_id"),
                        rs.getString("te_nome"),
                        rs.getInt("te_ticket")
                );
                Ssp ssp = null;
                if (rs.getObject("ssp_id") != null) {
                    Provincia provinciaSsp = new Provincia(rs.getString("pssp_sigla"),
                            rs.getString("pssp_nome"),
                            rs.getString("pssp_regione"));
                    ssp = new Ssp(rs.getInt("ssp_id"),
                            provinciaSsp
                    );
                }
                Esame esame = new Esame(rs.getInt("e_id"),
                        paziente,
                        medicoDiBase,
                        tipoDiEsame,
                        rs.getTimestamp("e_data_ora_prescrizione"),
                        ssp,
                        rs.getTimestamp("e_data_ora_erogazione"),
                        rs.getString("e_report"));
                return esame;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the exam for the passed primary key", ex);
        }
    }

    @Override
    public List<Esame> getAll() throws DAOException {
        List<Esame> esami = new ArrayList<>();

        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery(""
                    + "SELECT p.id AS p_id, p.nome AS p_nome, p.cognome AS p_cognome, "
                    + "       mb.id AS mb_id, mb.nome AS mb_nome, mb.cognome AS mb_cognome, "
                    + "       te.id AS te_id, te.nome AS te_nome, te.ticket AS te_ticket, "
                    + "       pssp.sigla AS pssp_sigla, pssp.nome AS pssp_nome, "
                    + "       pssp.regione AS pssp_regione, ssp.id AS ssp_id, "
                    + "       e.id AS e_id, e.data_ora_prescrizione AS e_data_ora_prescrizione, "
                    + "       e.data_ora_erogazione AS e_data_ora_erogazione, "
                    + "       e.report AS e_report "
                    + "FROM esame AS e "
                    + "JOIN tipo_di_esame AS te ON e.id_tipo_esame = te.id "
                    + "JOIN paziente AS p ON e.id_paziente = p.id "
                    + "JOIN paziente AS mb ON e.id_medico_di_base = mb.id "
                    + "LEFT JOIN ssp ON e.id_ssp = ssp.id "
                    + "LEFT JOIN provincia AS pssp ON ssp.sigla_provincia = pssp.sigla ")) {

                while (rs.next()) {
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome"));
                    MedicoDiBase medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                            rs.getString("mb_nome"),
                            rs.getString("mb_cognome"));
                    TipoDiEsame tipoDiEsame = new TipoDiEsame(rs.getInt("te_id"),
                            rs.getString("te_nome"),
                            rs.getInt("te_ticket")
                    );
                    Ssp ssp = null;
                    if (rs.getObject("ssp_id") != null) {
                        Provincia provinciaSsp = new Provincia(rs.getString("pssp_sigla"),
                                rs.getString("pssp_nome"),
                                rs.getString("pssp_regione"));
                        ssp = new Ssp(rs.getInt("ssp_id"),
                                provinciaSsp
                        );
                    }

                    Esame esame = new Esame(rs.getInt("e_id"),
                            paziente,
                            medicoDiBase,
                            tipoDiEsame,
                            rs.getTimestamp("e_data_ora_prescrizione"),
                            ssp,
                            rs.getTimestamp("e_data_ora_erogazione"),
                            rs.getString("e_report"));
                    esami.add(esame);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of exams", ex);
        }
        return esami;
    }

    @Override
    public List<Esame> getByPaziente(Integer idPaziente) throws DAOException {
        if (idPaziente == null) {
            throw new DAOException("idPaziente is null");
        }

        List<Esame> esami = new ArrayList<>();

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT p.id AS p_id, p.nome AS p_nome, p.cognome AS p_cognome, "
                + "       mb.id AS mb_id, mb.nome AS mb_nome, mb.cognome AS mb_cognome, "
                + "       te.id AS te_id, te.nome AS te_nome, te.ticket AS te_ticket, "
                + "       pssp.sigla AS pssp_sigla, pssp.nome AS pssp_nome, "
                + "       pssp.regione AS pssp_regione, ssp.id AS ssp_id, "
                + "       e.id AS e_id, e.data_ora_prescrizione AS e_data_ora_prescrizione, "
                + "       e.data_ora_erogazione AS e_data_ora_erogazione, "
                + "       e.report AS e_report "
                + "FROM esame AS e "
                + "JOIN tipo_di_esame AS te ON e.id_tipo_esame = te.id "
                + "JOIN paziente AS p ON e.id_paziente = p.id "
                + "JOIN paziente AS mb ON e.id_medico_di_base = mb.id "
                + "LEFT JOIN ssp ON e.id_ssp = ssp.id "
                + "LEFT JOIN provincia AS pssp ON ssp.sigla_provincia = pssp.sigla "
                + "WHERE id_paziente = ? ORDER BY data_ora_prescrizione DESC")) {
            stm.setInt(1, idPaziente);
            try ( ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome"));
                    MedicoDiBase medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                            rs.getString("mb_nome"),
                            rs.getString("mb_cognome"));
                    TipoDiEsame tipoDiEsame = new TipoDiEsame(rs.getInt("te_id"),
                            rs.getString("te_nome"),
                            rs.getInt("te_ticket")
                    );
                    Ssp ssp = null;
                    if (rs.getObject("ssp_id") != null) {
                        Provincia provinciaSsp = new Provincia(rs.getString("pssp_sigla"),
                                rs.getString("pssp_nome"),
                                rs.getString("pssp_regione"));
                        ssp = new Ssp(rs.getInt("ssp_id"),
                                provinciaSsp
                        );
                    }

                    Esame esame = new Esame(rs.getInt("e_id"),
                            paziente,
                            medicoDiBase,
                            tipoDiEsame,
                            rs.getTimestamp("e_data_ora_prescrizione"),
                            ssp,
                            rs.getTimestamp("e_data_ora_erogazione"),
                            rs.getString("e_report"));

                    esami.add(esame);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of user's exams", ex);
        }
        return esami;
    }

    @Override
    public List<Esame> getLastByPaziente(Integer idPaziente) throws DAOException {
        final int N_LAST_EXAMS = 5;
        if (idPaziente == null) {
            throw new DAOException("idPaziente is null");
        }

        List<Esame> esami = new ArrayList<>();

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT p.id AS p_id, p.nome AS p_nome, p.cognome AS p_cognome, "
                + "       mb.id AS mb_id, mb.nome AS mb_nome, mb.cognome AS mb_cognome, "
                + "       te.id AS te_id, te.nome AS te_nome, te.ticket AS te_ticket, "
                + "       pssp.sigla AS pssp_sigla, pssp.nome AS pssp_nome, "
                + "       pssp.regione AS pssp_regione, ssp.id AS ssp_id, "
                + "       e.id AS e_id, e.data_ora_prescrizione AS e_data_ora_prescrizione, "
                + "       e.data_ora_erogazione AS e_data_ora_erogazione, "
                + "       e.report AS e_report "
                + "FROM esame AS e "
                + "JOIN tipo_di_esame AS te ON e.id_tipo_esame = te.id "
                + "JOIN paziente AS p ON e.id_paziente = p.id "
                + "JOIN paziente AS mb ON e.id_medico_di_base = mb.id "
                + "LEFT JOIN ssp ON e.id_ssp = ssp.id "
                + "LEFT JOIN provincia AS pssp ON ssp.sigla_provincia = pssp.sigla "
                + "WHERE id_paziente = ? ORDER BY data_ora_prescrizione DESC "
                + "LIMIT " + N_LAST_EXAMS)) {
            stm.setInt(1, idPaziente);
            try ( ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome"));
                    MedicoDiBase medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                            rs.getString("mb_nome"),
                            rs.getString("mb_cognome"));
                    TipoDiEsame tipoDiEsame = new TipoDiEsame(rs.getInt("te_id"),
                            rs.getString("te_nome"),
                            rs.getInt("te_ticket")
                    );
                    Ssp ssp = null;
                    if (rs.getObject("ssp_id") != null) {
                        Provincia provinciaSsp = new Provincia(rs.getString("pssp_sigla"),
                                rs.getString("pssp_nome"),
                                rs.getString("pssp_regione"));
                        ssp = new Ssp(rs.getInt("ssp_id"),
                                provinciaSsp
                        );
                    }

                    Esame esame = new Esame(rs.getInt("e_id"),
                            paziente,
                            medicoDiBase,
                            tipoDiEsame,
                            rs.getTimestamp("e_data_ora_prescrizione"),
                            ssp,
                            rs.getTimestamp("e_data_ora_erogazione"),
                            rs.getString("e_report"));

                    esami.add(esame);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of user's exams", ex);
        }
        return esami;
    }

    @Override
    public boolean isEsameDelPaziente(Integer idEsame, Integer idPaziente) throws DAOException {
        if (idEsame == null) {
            throw new DAOException("idEsame is null");
        }
        if (idPaziente == null) {
            throw new DAOException("idPaziente is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT * "
                + "FROM esame AS e "
                + "WHERE e.id = ? and e.id_paziente = ?")) {
            stm.setInt(1, idEsame);
            stm.setInt(2, idPaziente);
            try ( ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the exam for the passed primary key", ex);
        }
    }

    @Override
    public boolean isEsameDiUnPaziente(Integer idEsame, Integer idMedicoDiBase) throws DAOException {
        if (idEsame == null) {
            throw new DAOException("idEsame is null");
        }
        if (idMedicoDiBase == null) {
            throw new DAOException("idMedicoDiBase is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT * "
                + "FROM esame AS e "
                + "JOIN paziente AS p ON e.id_paziente = p.id "
                + "WHERE e.id = ? and p.id_medico_di_base = ?")) {
            stm.setInt(1, idEsame);
            stm.setInt(2, idMedicoDiBase);
            try ( ResultSet rs = stm.executeQuery()) {
                return rs.next();
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the exam for the passed primary key", ex);
        }
    }

    @Override
    public List<Esame> getErogatiByPaziente(Integer idPaziente) throws DAOException {
        if (idPaziente == null) {
            throw new DAOException("idPaziente is null");
        }

        List<Esame> esami = new ArrayList<>();

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT p.id AS p_id, p.nome AS p_nome, p.cognome AS p_cognome, "
                + "       mb.id AS mb_id, mb.nome AS mb_nome, mb.cognome AS mb_cognome, "
                + "       te.id AS te_id, te.nome AS te_nome, te.ticket AS te_ticket, "
                + "       pssp.sigla AS pssp_sigla, pssp.nome AS pssp_nome, "
                + "       pssp.regione AS pssp_regione, ssp.id AS ssp_id, "
                + "       e.id AS e_id, e.data_ora_prescrizione AS e_data_ora_prescrizione, "
                + "       e.data_ora_erogazione AS e_data_ora_erogazione, "
                + "       e.report AS e_report "
                + "FROM esame AS e "
                + "JOIN tipo_di_esame AS te ON e.id_tipo_esame = te.id "
                + "JOIN paziente AS p ON e.id_paziente = p.id "
                + "JOIN paziente AS mb ON e.id_medico_di_base = mb.id "
                + "JOIN ssp ON e.id_ssp = ssp.id "
                + "JOIN provincia AS pssp ON ssp.sigla_provincia = pssp.sigla "
                + "WHERE id_paziente = ? ORDER BY data_ora_prescrizione DESC")) {
            stm.setInt(1, idPaziente);
            try ( ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome"));
                    MedicoDiBase medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                            rs.getString("mb_nome"),
                            rs.getString("mb_cognome"));
                    TipoDiEsame tipoDiEsame = new TipoDiEsame(rs.getInt("te_id"),
                            rs.getString("te_nome"),
                            rs.getInt("te_ticket")
                    );
                    Provincia provinciaSsp = new Provincia(rs.getString("pssp_sigla"),
                            rs.getString("pssp_nome"),
                            rs.getString("pssp_regione"));
                    Ssp ssp = new Ssp(rs.getInt("ssp_id"),
                            provinciaSsp
                    );

                    Esame esame = new Esame(rs.getInt("e_id"),
                            paziente,
                            medicoDiBase,
                            tipoDiEsame,
                            rs.getTimestamp("e_data_ora_prescrizione"),
                            ssp,
                            rs.getTimestamp("e_data_ora_erogazione"),
                            rs.getString("e_report"));

                    esami.add(esame);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of user's exams", ex);
        }
        return esami;
    }

    @Override
    public List<Esame> getDaErogareByPaziente(Integer idPaziente) throws DAOException {
        if (idPaziente == null) {
            throw new DAOException("idPaziente is null");
        }

        List<Esame> esami = new ArrayList<>();

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT p.id AS p_id, p.nome AS p_nome, p.cognome AS p_cognome, "
                + "       mb.id AS mb_id, mb.nome AS mb_nome, mb.cognome AS mb_cognome, "
                + "       te.id AS te_id, te.nome AS te_nome, te.ticket AS te_ticket, "
                + "       e.id AS e_id, e.data_ora_prescrizione AS e_data_ora_prescrizione "
                + "FROM esame AS e "
                + "JOIN tipo_di_esame AS te ON e.id_tipo_esame = te.id "
                + "JOIN paziente AS p ON e.id_paziente = p.id "
                + "JOIN paziente AS mb ON e.id_medico_di_base = mb.id "
                + "WHERE id_paziente = ? AND e.id_ssp IS NULL "
                + "ORDER BY data_ora_prescrizione DESC")) {
            stm.setInt(1, idPaziente);
            try ( ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome"));
                    MedicoDiBase medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                            rs.getString("mb_nome"),
                            rs.getString("mb_cognome"));
                    TipoDiEsame tipoDiEsame = new TipoDiEsame(rs.getInt("te_id"),
                            rs.getString("te_nome"),
                            rs.getInt("te_ticket")
                    );

                    Esame esame = new Esame(rs.getInt("e_id"),
                            paziente,
                            medicoDiBase,
                            tipoDiEsame,
                            rs.getTimestamp("e_data_ora_prescrizione"),
                            null,
                            null,
                            null
                    );

                    esami.add(esame);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of user's exams", ex);
        }
        return esami;
    }
    
    @Override
    public List<Esame> getErogateBySsp(String siglaProvincia) throws DAOException {
        return getErogateBySsp(siglaProvincia, new Date(0), new Date(System.currentTimeMillis()));
    }

    @Override
    public List<Esame> getErogateBySsp(String siglaProvincia, Date startDate, Date endDate) throws DAOException {
        if (siglaProvincia == null) {
            throw new DAOException("siglaSsp is null");
        }

        List<Esame> esami = new ArrayList<>();

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT p.id AS p_id, p.nome AS p_nome, p.cognome AS p_cognome, "
                + "       mb.id AS mb_id, mb.nome AS mb_nome, mb.cognome AS mb_cognome, "
                + "       te.id AS te_id, te.nome AS te_nome, te.ticket AS te_ticket, "
                + "       pssp.sigla AS pssp_sigla, pssp.nome AS pssp_nome, "
                + "       pssp.regione AS pssp_regione, ssp.id AS ssp_id, "
                + "       e.id AS e_id, e.data_ora_prescrizione AS e_data_ora_prescrizione, "
                + "       e.data_ora_erogazione AS e_data_ora_erogazione, "
                + "       e.report AS e_report "
                + "FROM esame AS e "
                + "JOIN tipo_di_esame AS te ON e.id_tipo_esame = te.id "
                + "JOIN paziente AS p ON e.id_paziente = p.id "
                + "JOIN paziente AS mb ON e.id_medico_di_base = mb.id "
                + "LEFT JOIN ssp ON e.id_ssp = ssp.id "
                + "LEFT JOIN provincia AS pssp ON ssp.sigla_provincia = pssp.sigla "
                + "WHERE ssp.sigla_provincia = ? "
                + "AND e.data_ora_erogazione BETWEEN ? AND ? "
                + "ORDER BY data_ora_erogazione DESC")) {
            stm.setString(1, siglaProvincia);
            stm.setDate(2, startDate);
            stm.setDate(3, endDate);

            try ( ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome"));
                    MedicoDiBase medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                            rs.getString("mb_nome"),
                            rs.getString("mb_cognome"));
                    TipoDiEsame tipoDiEsame = new TipoDiEsame(rs.getInt("te_id"),
                            rs.getString("te_nome"),
                            rs.getInt("te_ticket")
                    );
                    Ssp ssp = null;
                    if (rs.getObject("ssp_id") != null) {
                        Provincia provinciaSsp = new Provincia(rs.getString("pssp_sigla"),
                                rs.getString("pssp_nome"),
                                rs.getString("pssp_regione"));
                        ssp = new Ssp(rs.getInt("ssp_id"),
                                provinciaSsp
                        );
                    }

                    Esame esame = new Esame(rs.getInt("e_id"),
                            paziente,
                            medicoDiBase,
                            tipoDiEsame,
                            rs.getTimestamp("e_data_ora_prescrizione"),
                            ssp,
                            rs.getTimestamp("e_data_ora_erogazione"),
                            rs.getString("e_report"));

                    esami.add(esame);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of user's exams", ex);
        }
        return esami;
    }

    @Override
    public Esame insert(Esame esame) throws DAOException {
        if (esame == null) {
            throw new DAOException("esame is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "INSERT INTO esame (id_paziente, id_medico_di_base, id_tipo_esame, data_ora_prescrizione) VALUES "
                + "(?, ?, ?, ?);")) {
            stm.setInt(1, esame.getPaziente().getId());
            stm.setInt(2, esame.getMedicoDiBase().getId());
            stm.setInt(3, esame.getTipoDiEsame().getId());
            stm.setTimestamp(4, esame.getDataOraPrescrizione());

            if (stm.executeUpdate() == 1) {
                return esame;
            } else {
                throw new DAOException("Impossible to insert the esame");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to insert the esame", ex);
        }
    }

    @Override
    public Esame update(Esame esame) throws DAOException {
        if (esame == null) {
            throw new DAOException("esame is null");
        }
        
        try ( PreparedStatement stm = CON.prepareStatement(
                "UPDATE esame SET id_paziente = ? , "
                + "id_medico_di_base = ?, id_tipo_esame = ?, data_ora_prescrizione = ? , "
                + "id_ssp = ?, data_ora_erogazione = ?, report = ?"
                + "WHERE id = ?")) {
            stm.setInt(1, esame.getPaziente().getId());
            stm.setInt(2, esame.getMedicoDiBase().getId());
            stm.setInt(3, esame.getTipoDiEsame().getId());
            stm.setTimestamp(4, esame.getDataOraPrescrizione());
            stm.setInt(5, esame.getSsp().getId());
            stm.setTimestamp(6, esame.getDataOraErogazione());
            stm.setString(7, esame.getReport());
            stm.setInt(8, esame.getId());

            if (stm.executeUpdate() == 1) {
                return esame;
            } else {
                throw new DAOException("Impossible to update the esame");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update the esame", ex);
        }
    }

    @Override
    public List<List<Integer>> getSpesaFrom(int fromYear) throws DAOException {
        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT ne.year AS year, SUM(ne.num * ne.ticket) AS spesa "
                + "FROM (SELECT EXTRACT(YEAR FROM e.data_ora_erogazione) AS year, te.id AS id, COUNT(te.id) AS num, te.ticket AS ticket "
                + "      FROM esame AS e "
                + "      JOIN tipo_di_esame AS te ON e.id_tipo_esame = te.id "
                + "      WHERE EXTRACT(YEAR FROM e.data_ora_erogazione) >= ? "
                + "      GROUP BY EXTRACT(YEAR FROM e.data_ora_erogazione), te.id) AS ne "
                + "GROUP BY ne.year "
                + "ORDER BY ne.year;")) {
            stm.setInt(1, fromYear);
            try ( ResultSet rs = stm.executeQuery()) {
                List<List<Integer>> spesa = new ArrayList<>();
                while (rs.next()) {
                    List<Integer> annoSpesa = new ArrayList<Integer>(2);
                    annoSpesa.add(rs.getInt("year"));
                    annoSpesa.add(rs.getInt("spesa"));
                    spesa.add(annoSpesa);

                }
                return spesa;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the exams data", ex);
        }
    }

}
