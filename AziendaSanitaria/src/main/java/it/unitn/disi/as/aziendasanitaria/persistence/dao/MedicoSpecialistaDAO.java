/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao;

import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoSpecialista;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Utente;
import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;

/**
 *
 * @author gabriele
 */
public interface MedicoSpecialistaDAO extends DAO<MedicoSpecialista, Integer> {
    public MedicoSpecialista update(MedicoSpecialista medicoSpecialista) throws DAOException;
    public MedicoSpecialista getByUtente(Utente utente) throws DAOException;
}
