/*
 * AziendaSanitaria
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao.jdbc_psql;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.UtenteDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Utente;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementazione di {@link UtenteDAO}
 *
 * @author gabriele
 */
public class JDBC_PSQLUtenteDAO extends JDBC_PSQL_DAO<Utente, Integer> implements UtenteDAO {

    /**
     * Costruttore di default
     *
     * @param con connessione con il sistema di persistenza
     */
    public JDBC_PSQLUtenteDAO(Connection con) {
        super(con);
    }

    /**
     * Restituisce il numero di {@link Utente utenti} memorizzati.
     *
     * @return il numero di record utente presenti nel DB
     * @throws DAOException
     */
    @Override
    public Long getCount() throws DAOException {
        try ( Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM utente");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count utente", ex);
        }

        return 0L;
    }

    /**
     * Restituisce l'{@link Utente utente} con la chiave primaria data
     *
     * @param primaryKey l'id dell'utente
     * @returnl'utente con l'id dato se esiste, null se non esiste
     * @throws DAOException
     */
    @Override
    public Utente getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }
        try ( PreparedStatement stm = CON.prepareStatement("SELECT * FROM utente WHERE id = ?")) {
            stm.setInt(1, primaryKey);
            try ( ResultSet rs = stm.executeQuery()) {
                rs.next();
                Utente user = new Utente(rs.getInt("id"),
                        rs.getString("email"),
                        rs.getString("password"),
                        rs.getString("salt"),
                        rs.getString("tipo"));

                return user;
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed primary key", ex);
        }
    }

    /**
     * Restituisce l'utente con l'email data
     *
     * @param email
     * @return
     * @throws DAOException
     */
    @Override
    public Utente getByEmail(String email) throws DAOException {
        if (email == null) {
            throw new DAOException("Email is a mandatory field", new NullPointerException("email is null"));
        }
        
        try ( PreparedStatement stm = CON.prepareStatement("SELECT * FROM utente WHERE email = ?")) {
            stm.setString(1, email);
            try ( ResultSet rs = stm.executeQuery()) {
                rs.next();
                Utente user = new Utente(rs.getInt("id"),
                    rs.getString("email"),
                    rs.getString("password"),
                    rs.getString("salt"),
                    rs.getString("tipo"));
                return user;                
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user", ex);
        }
    }

    /**
     * Restituisce la lista di tutti gli utenti presenti nel sistema
     *
     * @return lista di tutti gli utenti presenti nel sistema
     * @throws DAOException
     */
    @Override
    public List<Utente> getAll() throws DAOException {
        List<Utente> users = new ArrayList<>();

        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery("SELECT * FROM utente ORDER BY id")) {

                while (rs.next()) {
                    Utente user = new Utente(rs.getInt("id"),
                        rs.getString("email"),
                        rs.getString("password"),
                        rs.getString("salt"),
                        rs.getString("tipo"));

                    users.add(user);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }

        return users;
    }

    /**
     * Aggiorna l'utente indicato e lo restituisce
     *
     * @param user utente da aggiornare
     * @return l'utente aggiornato
     * @throws DAOException
     */
    @Override
    public Utente update(Utente user) throws DAOException {
        if (user == null) {
            throw new DAOException("parameter not valid", new IllegalArgumentException("The passed user is null"));
        }

        try ( PreparedStatement std = CON.prepareStatement("UPDATE utente SET email = ?, password = ?, salt = ?, tipo = ?::user_type, "+
                "recupero_token = ?, scadenza_recupero = ?, token = ?, scadenza_token = ? WHERE id = ?")) {
            std.setString(1, user.getEmail());
            std.setString(2, user.getPassword());
            std.setString(3, user.getSalt());
            std.setString(4, user.getTipo());
            std.setString(5, user.getRecuperoToken());
            std.setTimestamp(6, user.getScadenzaRecupero());
            std.setString(7, user.getToken());
            std.setTimestamp(8, user.getScadenzaToken());
            std.setInt(9, user.getId());
            if (std.executeUpdate() == 1) {
                return user;
            } else {
                throw new DAOException("Impossible to update the user");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update the user", ex);
        }
    }

    @Override
    public Utente setRememberMe(Utente user, String token, Timestamp scadenzaToken) throws DAOException {
        if (user == null) {
            throw new DAOException("parameter not valid", new IllegalArgumentException("The passed user is null"));
        } else if(token == null) {
            throw new DAOException("parameter not valid", new IllegalArgumentException("The passed token is null"));
        } else if(scadenzaToken == null) {
            throw new DAOException("parameter not valid", new IllegalArgumentException("The passed scdenzaToken is null"));
        }

        try ( PreparedStatement std = CON.prepareStatement("UPDATE utente SET token = ?, scadenza_token = ? WHERE id = ?")) {
            std.setString(1, token);
            std.setTimestamp(2, scadenzaToken);
            std.setInt(3, user.getId());
            if (std.executeUpdate() == 1) {
                return user;
            } else {
                throw new DAOException("Impossible to update the user");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update the user", ex);
        }
    }

    @Override
    public Utente getByToken(String token) throws DAOException {
        if(token == null) {
            throw new DAOException("parameter not valid", new IllegalArgumentException("The passed token is null"));
        }

        try ( PreparedStatement stm = CON.prepareStatement("SELECT * FROM utente WHERE token = ?")) {
            stm.setString(1, token);
            try ( ResultSet rs = stm.executeQuery()) {
                rs.next();
                Utente user = new Utente(rs.getInt("id"),
                    rs.getString("email"),
                    rs.getString("password"),
                    rs.getString("salt"),
                    rs.getString("tipo"),
                    rs.getString("recupero_token"),
                    rs.getTimestamp("scadenza_recupero"),
                    rs.getString("token"),
                    rs.getTimestamp("scadenza_token"));
                return user;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user", ex);
        }
    }

    @Override
    public Utente setRecuperoPassword(Utente user, String recuperoToken, Timestamp scadenzaRecupero) throws DAOException {
        if (user == null) {
            throw new DAOException("parameter not valid", new IllegalArgumentException("The passed user is null"));
        } else if(recuperoToken == null) {
            throw new DAOException("parameter not valid", new IllegalArgumentException("The passed recuperoToken is null"));
        } else if(scadenzaRecupero == null) {
            throw new DAOException("parameter not valid", new IllegalArgumentException("The passed scadenzaRecupero is null"));
        }

        try ( PreparedStatement std = CON.prepareStatement("UPDATE utente SET recupero_token = ?, scadenza_recupero = ? WHERE id = ?")) {
            std.setString(1, recuperoToken);
            std.setTimestamp(2, scadenzaRecupero);
            std.setInt(3, user.getId());
            if (std.executeUpdate() == 1) {
                return user;
            } else {
                throw new DAOException("Impossible to update the user");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update the user", ex);
        }
    }

    @Override
    public Utente getByRecuperoToken(String recuperoToken) throws DAOException {
        if(recuperoToken == null) {
            throw new DAOException("parameter not valid", new IllegalArgumentException("The passed recuperoToken is null"));
        }

        try ( PreparedStatement stm = CON.prepareStatement("SELECT * FROM utente WHERE recupero_token = ?")) {
            stm.setString(1, recuperoToken);
            try ( ResultSet rs = stm.executeQuery()) {
                rs.next();
                Utente user = new Utente(rs.getInt("id"),
                    rs.getString("email"),
                    rs.getString("password"),
                    rs.getString("salt"),
                    rs.getString("tipo"),
                    rs.getString("recupero_token"),
                    rs.getTimestamp("scadenza_recupero"),
                    rs.getString("token"),
                    rs.getTimestamp("scadenza_token"));
                return user;                
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user", ex);
        }
    }
    
    
    
    
}
