/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.servlets;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.TipoDiVisitaDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.VisitaSpecialisticaDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoDiBase;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Paziente;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.TipoDiVisita;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.VisitaSpecialistica;
import it.unitn.disi.as.aziendasanitaria.utilities.Authentication;
import it.unitn.disi.as.aziendasanitaria.utilities.MailSender;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author davide
 */
public class PrescrizioneVisitaSpecialisticaServlet extends HttpServlet {

    private MailSender mailSender;
    
    private VisitaSpecialisticaDAO visitaSpecialisticaDao;
    private TipoDiVisitaDAO tipoDiVisitaDao;
    
    private static final Logger logger = LogManager.getLogger(PrescrizioneVisitaSpecialisticaServlet.class);
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory");
        }
        try {
            visitaSpecialisticaDao = daoFactory.getDAO(VisitaSpecialisticaDAO.class);
            tipoDiVisitaDao = daoFactory.getDAO(TipoDiVisitaDAO.class);
        } catch (DAOFactoryException ex) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory", ex);
        }
        
        mailSender = (MailSender) getServletContext().getAttribute("mailSender");
        if (mailSender == null) {
            logger.error("impossibile ottenere il mail sender dalla servlet context");
            throw new ServletException("impossibile ottenere il mail sender dalla servlet context");
        }
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        MedicoDiBase medico = (MedicoDiBase) request.getAttribute("user");
        if(medico == null){
            logger.error("Nessun medico loggato");
            throw new ServletException("Nessun medico loggato");
        }
        
        Paziente paziente = (Paziente) request.getAttribute("paziente");
        if(paziente == null){
            logger.error("Nessun paziente indicato");
            throw new ServletException("Nessun paziente indicato");
        }
        
        TipoDiVisita tipoDiVisita;
        try {
            int idTipoDiVisita = Integer.parseInt(request.getParameter("idTipoDiVisita"));
            tipoDiVisita = tipoDiVisitaDao.getByPrimaryKey(idTipoDiVisita);
        } catch(NumberFormatException ex) {
            logger.debug("idTipoDiVisita non passato nella request o non valido", ex);
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        } catch(DAOException ex) {
            logger.debug("idTipoDiVisita non valido", ex);
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }                
        
        Timestamp now = new Timestamp(new Date().getTime());
        VisitaSpecialistica visita = new VisitaSpecialistica(null, medico, paziente, tipoDiVisita, now);
        
        try {
            visitaSpecialisticaDao.insert(visita);
            logger.debug("visita specialistica inserita nel database");
        } catch(DAOException ex) {
            logger.error("Impossibile inserire la visita specialistica nel database", ex);
            throw new ServletException("Impossibile inserire la visita specialistica nel database", ex);
        }
        
        try {
            String testo = "Gentile utente,\n\n"
                    + "in data " +new SimpleDateFormat("dd-MM-yyyy").format(visita.getDataOraPrescrizione().getTime())
                    + " è stata prescritta una visita specialistica del tipo '" 
                    + visita.getTipoDiVisita().getNome()
                    + "'.\n"
                    + "Per erogare la visita, recati da un medico specialista.\n\n"
                    + "Grazie,\n\n"
                    + "Team Azienda Sanitaria";
            mailSender.send(paziente.getEmail(), "Prescrizione visita specialistica", testo);
            request.getSession().setAttribute("success", "Visita specialistica prescritta correttamente");
        } catch(MessagingException ex) {
            logger.error("Impossibile inviare la mail all'indirizzo "+paziente.getEmail(), ex);
            request.getSession().setAttribute("error", "Errore nell'invio dell'email");
        }        
        Authentication.redirectTo("Medico/paziente?idPaziente=" + paziente.getId(), request, response);
     
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
