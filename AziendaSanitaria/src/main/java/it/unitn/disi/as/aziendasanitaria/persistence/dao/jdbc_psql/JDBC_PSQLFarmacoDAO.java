/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao.jdbc_psql;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.FarmacoDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Farmaco;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.TipoDiEsame;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gabriele
 */
public class JDBC_PSQLFarmacoDAO extends JDBC_PSQL_DAO<Farmaco, Integer> implements FarmacoDAO {

    public JDBC_PSQLFarmacoDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try ( Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM farmaco");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count farmaci", ex);
        }

        return 0L;
    }

    @Override
    public Farmaco getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement("SELECT * FROM farmaco WHERE id = ?")) {
            stm.setInt(1, primaryKey);
            try ( ResultSet rs = stm.executeQuery()) {

                rs.next();
                Farmaco farmaco = new Farmaco(rs.getInt("id"),
                        rs.getString("nome"),
                        rs.getInt("ticket")
                );
                return farmaco;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the farmaco for the passed primary key", ex);
        }
    }

    @Override
    public List<Farmaco> getAll() throws DAOException {
        List<Farmaco> farmaci = new ArrayList<>();

        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery("SELECT * FROM farmaco ORDER BY nome")) {

                while (rs.next()) {
                    Farmaco farmaco = new Farmaco(rs.getInt("id"),
                            rs.getString("nome"),
                            rs.getInt("ticket")
                    );
                    farmaci.add(farmaco);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of farmaco", ex);
        }

        return farmaci;
    }

}
