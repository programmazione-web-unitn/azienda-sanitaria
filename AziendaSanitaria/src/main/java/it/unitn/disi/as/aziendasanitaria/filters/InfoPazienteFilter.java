/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.filters;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.EsameDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.FotoDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.PazienteDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.RicettaDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.VisitaDiBaseDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.VisitaSpecialisticaDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Esame;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Farmacia;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Foto;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoDiBase;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoSpecialista;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Paziente;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Ricetta;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Ssp;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Utente;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.VisitaDiBase;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.VisitaSpecialistica;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.io.IOException;
import java.util.List;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author fran
 *
 * Filtro che verifica che il paziente passato come parametro sia un paziente
 * del medico loggato
 */
public class InfoPazienteFilter implements Filter {

    private static final Logger logger = LogManager.getLogger(InfoPazienteFilter.class);

    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured.
    private FilterConfig filterConfig = null;

    public InfoPazienteFilter() {
    }

    private int doBeforeProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {

        PazienteDAO pazienteDao;
        EsameDAO esameDao;
        FotoDAO fotoDao;
        RicettaDAO ricettaDao;
        VisitaDiBaseDAO visitaBaseDAO;
        VisitaSpecialisticaDAO visitaSpecialisticaDAO;

        DAOFactory daoFactory = (DAOFactory) request.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory");
        }
        try {
            pazienteDao = daoFactory.getDAO(PazienteDAO.class);
            esameDao = daoFactory.getDAO(EsameDAO.class);
            fotoDao = daoFactory.getDAO(FotoDAO.class);
            ricettaDao = daoFactory.getDAO(RicettaDAO.class);
            visitaBaseDAO = daoFactory.getDAO(VisitaDiBaseDAO.class);
            visitaSpecialisticaDAO = daoFactory.getDAO(VisitaSpecialisticaDAO.class);

        } catch (DAOFactoryException ex) {
            logger.error("Impossibile ottenere il dao factory", ex);
            throw new ServletException("Impossibile ottenere il dao factory", ex);
        }

        if (request instanceof HttpServletRequest) {
            Utente user = (Utente) ((HttpServletRequest) request).getAttribute("user");

            if (user == null) {
                logger.error("Nessun utente loggato");
                return HttpServletResponse.SC_FORBIDDEN;
            } else if (user instanceof Paziente && !(user instanceof MedicoDiBase) && !(user instanceof MedicoSpecialista)) {
                logger.debug("Il paziente id='" + user.getId() + "' non può accedere a nessun paziente");
                return HttpServletResponse.SC_FORBIDDEN;
            } else {
                int idPaziente;
                Paziente paziente;
                try {
                    idPaziente = Integer.parseInt(request.getParameter("idPaziente"));
                    paziente = pazienteDao.getByPrimaryKey(idPaziente);
                } catch (NumberFormatException ex) {
                    logger.debug("id paziente '" + request.getParameter("idPaziente") + "' non valido", ex);
                    return HttpServletResponse.SC_BAD_REQUEST;
                } catch (DAOException ex) {
                    logger.debug("Paziente non trovato", ex);
                    return HttpServletResponse.SC_NOT_FOUND;
                }

                try {
                    if (user instanceof MedicoDiBase && !pazienteDao.isPazienteDelMedico(idPaziente, user.getId())) {
                        logger.debug("Il medico di base id='" + user.getId() + "' non può accedere al paziente id='" + idPaziente + "'");
                        return HttpServletResponse.SC_FORBIDDEN;
                    }

                    if (user instanceof Ssp && !paziente.getProvincia().equals(((Ssp) user).getProvincia())) {
                        logger.debug("L'ssp di id='" + user.getId() + "' non può accedere al paziente id='" + idPaziente + "'");
                        return HttpServletResponse.SC_FORBIDDEN;
                    }
                    List<Ricetta> tutteRicette;
                    List<Esame> tuttiEsami = esameDao.getByPaziente(idPaziente);
                    if (user instanceof Farmacia) {
                        tutteRicette = ricettaDao.getDaErogareByPaziente(idPaziente);
                    } else {
                        tutteRicette = ricettaDao.getByPaziente(idPaziente);
                    }
                    List<Foto> fotoPaziente = fotoDao.getByPaziente(idPaziente);
                    List<VisitaDiBase> tutteVisiteBase = visitaBaseDAO.getByPaziente(idPaziente);
                    List<VisitaSpecialistica> tutteVisiteSpec = visitaSpecialisticaDAO.getByPaziente(idPaziente);

                    request.setAttribute("paziente", paziente);
                    request.setAttribute("tuttiEsami", tuttiEsami);
                    request.setAttribute("tutteRicette", tutteRicette);
                    request.setAttribute("tutteVisiteBase", tutteVisiteBase);
                    request.setAttribute("tutteVisiteSpec", tutteVisiteSpec);

                    request.setAttribute("fotoProfiloPaziente", fotoPaziente.get(fotoPaziente.size() - 1));
                    request.setAttribute("fotoPaziente", fotoPaziente);
                    return HttpServletResponse.SC_OK;

                } catch (DAOException ex) {
                    logger.error("Impossibile caricare i dati del paziente", ex);
                    throw new ServletException("Impossibile caricare i dati del paziente", ex);
                }
            }
        }
        return HttpServletResponse.SC_FORBIDDEN;
    }

    private void doAfterProcessing(ServletRequest request, ServletResponse response)
            throws IOException, ServletException {
    }

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {

        logger.debug("doFilter()");

        int statoRichiesta = doBeforeProcessing(request, response);

        if (statoRichiesta == HttpServletResponse.SC_OK) {
            chain.doFilter(request, response);
        } else if (response instanceof HttpServletResponse) {
            ((HttpServletResponse) response).sendError(statoRichiesta);
        }

        doAfterProcessing(request, response);

    }

    /**
     * Return the filter configuration object for this filter.
     *
     * @return the filter configuration object
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    public void destroy() {
    }

    /**
     * Init method for this filter
     */
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            logger.debug("Initializing filter");
        }
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("InfoPazienteFilter()");
        }
        StringBuffer sb = new StringBuffer("InfoPazienteFilter(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }

}
