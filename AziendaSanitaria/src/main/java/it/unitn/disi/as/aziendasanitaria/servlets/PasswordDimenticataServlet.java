/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.servlets;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.UtenteDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Utente;
import it.unitn.disi.as.aziendasanitaria.utilities.Authentication;
import it.unitn.disi.as.aziendasanitaria.utilities.Security;
import it.unitn.disi.as.aziendasanitaria.utilities.MailSender;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author dima
 * attivo
 */
public class PasswordDimenticataServlet extends HttpServlet {

    private UtenteDAO userDao;
    private MailSender mailSender;

    private static final Logger logger = LogManager.getLogger(PasswordDimenticataServlet.class);

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossibile ottenere il dao factory");
        }
        try {
            userDao = daoFactory.getDAO(UtenteDAO.class);
        } catch (DAOFactoryException ex) {
            logger.error("Impossibile ottenere il dao user");
            throw new ServletException("Impossibile ottenere il dao factory", ex);
        }

        mailSender = (MailSender) getServletContext().getAttribute("mailSender");
        if (mailSender == null) {
            logger.error("Impossibile ottenere il mail sender dalla servlet context");
            throw new ServletException("Impossibile ottenere il mail sender dalla servlet context");
        }
    }

    // gestisce la richiesta del cambio password quando viene dimenticata
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Utente user = null;
        String email = request.getParameter("forgotEmail");
        if (email == null) {
            logger.debug("Email non fornita");
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        email = email.trim();

        String randomToken = null;
        try {
            user = userDao.getByEmail(email);
            randomToken = Security.randomAlphaNumeric(128);
            Date date = new Date();
            long oneHour = 60 * 60 * 1000;
            Timestamp scadenzaRecupero = new Timestamp(date.getTime() + oneHour);
            userDao.setRecuperoPassword(user, randomToken, scadenzaRecupero);
            logger.debug("Token recupero password impostato");
        } catch (DAOException ex) {
            logger.info("Email inesistente");
            request.getSession().setAttribute("error", "Email non trovata");
            Authentication.redirectToLogin(request, response);
            return;
        }

        try {
            String scheme = request.getScheme() + "://";
            String serverName = request.getServerName();
            String serverPort = (request.getServerPort() == 80) ? "" : ":" + request.getServerPort();
            String contextPath = request.getContextPath();
            String link = scheme + serverName + serverPort + contextPath + "/recuperoPassword?recuperoToken=" + randomToken;
            String emailBody = "Gentile utente,\n\n"
                    + "abbiamo ricevuto una richiesta di recupero password per il tuo account dell'Azienda Sanitaria.\n"
                    + "Se desideri modificare la tua password clicca sul seguente link e inserisci la nuova password:\n"
                    + link
                    + "\n\n"
                    + "Se non sei stato tu a inviare la richiesta, oppure non desideri cambiare la password, "
                    + "ignora questa email. Il link sopra è valido per la prossima ora.\n\n"
                    + "Grazie,\n\n"
                    + "Team Azienda Sanitaria";

            mailSender.send(email, "Recupero password", emailBody);

            logger.debug("Richiesta recupero password inviata");
            request.getSession().setAttribute("success", "Richiesta di recupero password inviata via mail");
            Authentication.redirectToLogin(request, response);

        } catch (MessagingException ex) {
            logger.error("Errore nell'invio dell'email", ex);
            throw new ServletException("Errore nell'invio dell'email");
        }
    }

    protected void processChangePassword(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Utente user = null;
        String recuperoToken = request.getParameter("recuperoToken");
        String nuovaPassword = request.getParameter("nuovaPassword");
        String confermaPassword = request.getParameter("confermaPassword");

        if (recuperoToken == null || nuovaPassword == null || confermaPassword == null) {
            logger.debug("parametro recuperoToken o nuovaPassword o confermaPassword non presente nella richiesta");
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        try {
            user = userDao.getByRecuperoToken(recuperoToken);
        } catch (DAOException ex) {
            logger.debug("Impossibile ottenere l'utente dal token di recupero", ex);
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        
        Date date = new Date();
        if (user == null || user.getScadenzaRecupero().before(new Timestamp(date.getTime()))) {
            logger.debug("Token di recupero non valido");
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        try {
            if (!Security.setNuovaPassword(user, nuovaPassword, confermaPassword, userDao)) {
                logger.debug("Nuova password non sicura o confermaPassword sbagliato");
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
        } catch (DAOException ex) {
            logger.error("Impossibile aggiornare la password dell'utente", ex);
            throw new ServletException("Impossibile aggiornare la password dell'utente", ex);
        }

        logger.info("Password aggiornata per l'utente " + user.getId());
        request.getSession().setAttribute("success", "Password aggiornata");
        Authentication.redirectToLogin(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processChangePassword(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
