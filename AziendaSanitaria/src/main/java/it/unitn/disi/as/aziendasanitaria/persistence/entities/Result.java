/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.entities;

import java.io.Serializable;

/**
 *
 * @author davide
 */
public class Result implements Serializable {

    private Element[] results;

    public Result(Element[] results) {
        this.results = results;
    }

    public Element[] getResults() {
        return results;
    }

    public void setResults(Element[] results) {
        this.results = results;
    }
}
