/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao.jdbc_psql;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.ProvinciaDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Provincia;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gabriele
 */
public class JDBC_PSQLProvinciaDAO extends JDBC_PSQL_DAO<Provincia, String> implements ProvinciaDAO {
    public JDBC_PSQLProvinciaDAO(Connection con){
        super(con);
    }
    @Override
    public Long getCount() throws DAOException {
        try ( Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM provincia");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count province", ex);
        }
        return 0L;
    }

    @Override
    public Provincia getByPrimaryKey(String primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }
        try ( PreparedStatement stm = CON.prepareStatement("SELECT * FROM provincia WHERE sigla = ?")) {
            stm.setString(1, primaryKey);
            try ( ResultSet rs = stm.executeQuery()) {

                rs.next();
                Provincia provincia = new Provincia(rs.getString("sigla"),
                        rs.getString("nome"),
                        rs.getString("regione"));
                return provincia;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the patient for the passed primary key", ex);
        }
    }

    @Override
    public List<Provincia> getAll() throws DAOException {
        List<Provincia> province = new ArrayList<>();

        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery("SELECT * FROM province ORDER BY sigla")) {

                while (rs.next()) {
                    Provincia provincia = new Provincia(
                            rs.getString("sigla"),
                            rs.getString("nome"),
                            rs.getString("regione")
                    );

                    province.add(provincia);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }

        return province;
    }
    
}
