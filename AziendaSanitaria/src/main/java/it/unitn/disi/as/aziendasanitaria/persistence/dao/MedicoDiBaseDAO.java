/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao;

import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoDiBase;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Utente;
import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;

/**
 *
 * @author gabriele
 */
public interface MedicoDiBaseDAO extends DAO<MedicoDiBase, Integer> {
    public MedicoDiBase update(MedicoDiBase medicoDiBase) throws DAOException;
    public MedicoDiBase getByUtente(Utente utente) throws DAOException;
}
