/*
 * AziendaSanitaria
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao;

import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoDiBase;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Paziente;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Ssp;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Utente;
import it.unitn.disi.wp.commons.persistence.dao.DAO;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.sql.Date;
import java.util.List;
import java.util.Map;

/**
 * Tutte le classi DAO devono implementare questa interfaccia per gestire il
 * sistema di persistenza che interagisce con {@link Utente gli utenti}.
 *
 * @author gabriele
 *
 */
public interface PazienteDAO extends DAO<Paziente, Integer> {

    /**
     *
     * @param paziente
     * @return
     * @throws DAOException
     */
    public Paziente update(Paziente paziente) throws DAOException;

    public Paziente getByUtente(Utente utente) throws DAOException;

    public List<Paziente> getByMedicoDiBase(MedicoDiBase medicoDiBase) throws DAOException;

    public List<Paziente> getBySsp(Ssp ssp) throws DAOException;
    public List<Paziente> getBySsp(Ssp ssp, Date from, Date to) throws DAOException;

    public boolean isPazienteDelMedico(Integer idPaziente, Integer idMedico) throws DAOException;

    public List<Map<String, Object>> getByMedicoDiBaseWithInfo(MedicoDiBase medicoDiBase) throws DAOException;

    public List<Paziente> getBySspConRichiamo(Ssp ssp, Date from, Date to) throws DAOException;
}
