/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao.jdbc_psql;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.RicettaDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Farmacia;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Farmaco;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoDiBase;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Paziente;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Provincia;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Ricetta;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fran
 */
public class JDBC_PSQLRicettaDAO extends JDBC_PSQL_DAO<Ricetta, Integer> implements RicettaDAO {

    public JDBC_PSQLRicettaDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try ( Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM ricetta");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count recipes", ex);
        }

        return 0L;
    }

    @Override
    public List<Ricetta> getAllErogati() throws DAOException {
        List<Ricetta> ricette = new ArrayList<>();

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT mb.id AS mb_id, mb.nome AS mb_nome, mb.cognome AS mb_cognome, "
                + "       p.id AS p_id, p.nome AS p_nome, p.cognome AS p_cognome, "
                + "       f.id AS f_id, f.nome AS f_nome, f.ticket AS f_ticket, "
                + "       r.id AS r_id, r.quantita AS r_quantita, "
                + "       r.data_ora_prescrizione AS r_data_ora_prescrizione, "
                + "       fa.id AS fa_id, fa.nome as fa_nome, fa.comune as fa_comune, "
                + "       pfa.sigla as pfa_sigla, pfa.nome as pfa_nome, pfa.regione as pfa_regione, "
                + "       r.data_ora_erogazione AS r_data_ora_erogazione "
                + "FROM ricetta AS r "
                + "JOIN farmaco AS f ON r.id_farmaco = f.id "
                + "JOIN paziente AS mb on r.id_medico_di_base = mb.id "
                + "JOIN paziente AS p ON r.id_paziente = p.id "
                + "JOIN farmacia AS fa ON r.id_farmacia = fa.id "
                + "JOIN provincia AS pfa ON pfa.sigla = fa.sigla_provincia "
                + "ORDER BY r.data_ora_erogazione DESC ")) {
            try ( ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    MedicoDiBase medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                            rs.getString("mb_nome"),
                            rs.getString("mb_cognome"));
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome")
                    );
                    Farmaco farmaco = new Farmaco(rs.getInt("f_id"),
                            rs.getString("f_nome"),
                            rs.getInt("f_ticket")
                    );
                    Provincia provinciaFarmacia = new Provincia(rs.getString("pfa_sigla"),
                            rs.getString("pfa_nome"),
                            rs.getString("pfa_regione")
                    );
                    Farmacia farmacia = new Farmacia(rs.getInt("fa_id"),
                            rs.getString("fa_nome"),
                            rs.getString("fa_comune"),
                            provinciaFarmacia);

                    Ricetta ricetta = new Ricetta(rs.getInt("r_id"),
                            farmaco,
                            rs.getInt("r_quantita"),
                            medicoDiBase,
                            paziente,
                            rs.getTimestamp("r_data_ora_prescrizione"),
                            farmacia,
                            rs.getTimestamp("r_data_ora_erogazione"));
                    ricette.add(ricetta);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of user's recipes erogate", ex);
        }
        return ricette;
    }

    @Override
    public Ricetta getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT mb.id AS mb_id, mb.nome AS mb_nome, mb.cognome AS mb_cognome, "
                + "       p.id AS p_id, p.codice_fiscale AS p_codice_fiscale, p.nome AS p_nome, p.cognome AS p_cognome, "
                + "       pr.sigla AS pr_sigla, pr.nome AS pr_nome, pr.regione AS pr_regione, "
                + "       f.id AS f_id, f.nome AS f_nome, f.ticket AS f_ticket, "
                + "       r.id AS r_id, r.quantita AS r_quantita, "
                + "       r.data_ora_prescrizione AS r_data_ora_prescrizione, "
                + "       fa.id AS fa_id, fa.nome as fa_nome, fa.comune as fa_comune, "
                + "       pfa.sigla as pfa_sigla, pfa.nome as pfa_nome, pfa.regione as pfa_regione, "
                + "       r.data_ora_erogazione AS r_data_ora_erogazione "
                + "FROM ricetta AS r "
                + "JOIN farmaco AS f ON r.id_farmaco = f.id "
                + "JOIN paziente AS mb on r.id_medico_di_base = mb.id "
                + "JOIN paziente AS p ON r.id_paziente = p.id "
                + "JOIN provincia AS pr ON p.sigla_provincia = pr.sigla "
                + "LEFT JOIN farmacia AS fa ON r.id_farmacia = fa.id "
                + "LEFT JOIN provincia AS pfa ON pfa.sigla = fa.sigla_provincia "
                + "WHERE r.id = ?")) {
            stm.setInt(1, primaryKey);
            try ( ResultSet rs = stm.executeQuery()) {

                rs.next();
                MedicoDiBase medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                        rs.getString("mb_nome"),
                        rs.getString("mb_cognome"));
                
                Provincia provincia = new Provincia(rs.getString("pr_sigla"),
                        rs.getString("pr_nome"), 
                        rs.getString("pr_regione"));
                Paziente paziente = new Paziente(rs.getInt("p_id"),
                        rs.getString("p_nome"),
                        rs.getString("p_cognome"),
                        rs.getString("p_codice_fiscale")
                );
                paziente.setProvincia(provincia);
                Farmaco farmaco = new Farmaco(rs.getInt("f_id"),
                        rs.getString("f_nome"),
                        rs.getInt("f_ticket")
                );
                Farmacia farmacia = null;
                if (rs.getObject("fa_id") != null) {
                    Provincia provinciaFarmacia = new Provincia(rs.getString("pfa_sigla"),
                            rs.getString("pfa_nome"),
                            rs.getString("pfa_regione")
                    );
                    farmacia = new Farmacia(rs.getInt("fa_id"),
                            rs.getString("fa_nome"),
                            rs.getString("fa_comune"),
                            provinciaFarmacia);
                }
                Ricetta ricetta = new Ricetta(rs.getInt("r_id"),
                        farmaco,
                        rs.getInt("r_quantita"),
                        medicoDiBase,
                        paziente,
                        rs.getTimestamp("r_data_ora_prescrizione"),
                        farmacia,
                        rs.getTimestamp("r_data_ora_erogazione"));
                return ricetta;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the recipe for the passed primary key", ex);
        }
    }

    @Override
    public List<Ricetta> getAll() throws DAOException {
        List<Ricetta> ricette = new ArrayList<>();

        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery(""
                    + "SELECT mb.id AS mb_id, mb.nome AS mb_nome, mb.cognome AS mb_cognome, "
                    + "       p.id AS p_id, p.nome AS p_nome, p.cognome AS p_cognome, "
                    + "       f.id AS f_id, f.nome AS f_nome, f.ticket AS f_ticket, "
                    + "       r.id AS r_id, r.quantita AS r_quantita, "
                    + "       r.data_ora_prescrizione AS r_data_ora_prescrizione, "
                    + "       fa.id AS fa_id, fa.nome as fa_nome, fa.comune as fa_comune, "
                    + "       pfa.sigla as pfa_sigla, pfa.nome as pfa_nome, pfa.regione as pfa_regione, "
                    + "       r.data_ora_erogazione AS r_data_ora_erogazione "
                    + "FROM ricetta AS r "
                    + "JOIN farmaco AS f ON r.id_farmaco = f.id "
                    + "JOIN paziente AS mb on r.id_medico_di_base = mb.id "
                    + "JOIN paziente AS p ON r.id_paziente = p.id "
                    + "LEFT JOIN farmacia AS fa ON r.id_farmacia = fa.id "
                    + "LEFT JOIN provincia AS pfa ON pfa.sigla = fa.sigla_provincia")) {

                while (rs.next()) {
                    MedicoDiBase medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                            rs.getString("mb_nome"),
                            rs.getString("mb_cognome"));
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome")
                    );
                    Farmaco farmaco = new Farmaco(rs.getInt("f_id"),
                            rs.getString("f_nome"),
                            rs.getInt("f_ticket")
                    );
                    Farmacia farmacia = null;
                    if (rs.getObject("fa_id") != null) {
                        Provincia provinciaFarmacia = new Provincia(rs.getString("pfa_sigla"),
                                rs.getString("pfa_nome"),
                                rs.getString("pfa_regione")
                        );
                        farmacia = new Farmacia(rs.getInt("fa_id"),
                                rs.getString("fa_nome"),
                                rs.getString("fa_comune"),
                                provinciaFarmacia);
                    }
                    Ricetta ricetta = new Ricetta(rs.getInt("r_id"),
                            farmaco,
                            rs.getInt("r_quantita"),
                            medicoDiBase,
                            paziente,
                            rs.getTimestamp("r_data_ora_prescrizione"),
                            farmacia,
                            rs.getTimestamp("r_data_ora_erogazione"));
                    ricette.add(ricetta);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }

        return ricette;
    }

    @Override
    public List<Ricetta> getByPaziente(Integer idPaziente) throws DAOException {
        if (idPaziente == null) {
            throw new DAOException("idPaziente is null");
        }

        List<Ricetta> ricette = new ArrayList<>();

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT mb.id AS mb_id, mb.nome AS mb_nome, mb.cognome AS mb_cognome, "
                + "       p.id AS p_id, p.nome AS p_nome, p.cognome AS p_cognome, "
                + "       f.id AS f_id, f.nome AS f_nome, f.ticket AS f_ticket, "
                + "       r.id AS r_id, r.quantita AS r_quantita, "
                + "       r.data_ora_prescrizione AS r_data_ora_prescrizione, "
                + "       fa.id AS fa_id, fa.nome as fa_nome, fa.comune as fa_comune, "
                + "       pfa.sigla as pfa_sigla, pfa.nome as pfa_nome, pfa.regione as pfa_regione, "
                + "       r.data_ora_erogazione AS r_data_ora_erogazione "
                + "FROM ricetta AS r "
                + "JOIN farmaco AS f ON r.id_farmaco = f.id "
                + "JOIN paziente AS mb on r.id_medico_di_base = mb.id "
                + "JOIN paziente AS p ON r.id_paziente = p.id "
                + "LEFT JOIN farmacia AS fa ON r.id_farmacia = fa.id "
                + "LEFT JOIN provincia AS pfa ON pfa.sigla = fa.sigla_provincia "
                + "WHERE r.id_paziente = ? ORDER BY r.data_ora_prescrizione DESC ")) {
            stm.setInt(1, idPaziente);
            try ( ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    MedicoDiBase medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                            rs.getString("mb_nome"),
                            rs.getString("mb_cognome"));
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome")
                    );
                    Farmaco farmaco = new Farmaco(rs.getInt("f_id"),
                            rs.getString("f_nome"),
                            rs.getInt("f_ticket")
                    );
                    Farmacia farmacia = null;
                    if (rs.getObject("fa_id") != null) {
                        Provincia provinciaFarmacia = new Provincia(rs.getString("pfa_sigla"),
                                rs.getString("pfa_nome"),
                                rs.getString("pfa_regione")
                        );
                        farmacia = new Farmacia(rs.getInt("fa_id"),
                                rs.getString("fa_nome"),
                                rs.getString("fa_comune"),
                                provinciaFarmacia);
                    }
                    Ricetta ricetta = new Ricetta(rs.getInt("r_id"),
                            farmaco,
                            rs.getInt("r_quantita"),
                            medicoDiBase,
                            paziente,
                            rs.getTimestamp("r_data_ora_prescrizione"),
                            farmacia,
                            rs.getTimestamp("r_data_ora_erogazione"));
                    ricette.add(ricetta);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of user's recipes", ex);
        }
        return ricette;
    }

    @Override
    public List<Ricetta> getLastByPaziente(Integer idPaziente) throws DAOException {
        int N_LAST_RECIPES = 5;

        if (idPaziente == null) {
            throw new DAOException("idPaziente is null");
        }

        List<Ricetta> ricette = new ArrayList<>();

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT mb.id AS mb_id, mb.nome AS mb_nome, mb.cognome AS mb_cognome, "
                + "       p.id AS p_id, p.nome AS p_nome, p.cognome AS p_cognome, "
                + "       f.id AS f_id, f.nome AS f_nome, f.ticket AS f_ticket, "
                + "       r.id AS r_id, r.quantita AS r_quantita, "
                + "       r.data_ora_prescrizione AS r_data_ora_prescrizione, "
                + "       fa.id AS fa_id, fa.nome as fa_nome, fa.comune as fa_comune, "
                + "       pfa.sigla as pfa_sigla, pfa.nome as pfa_nome, pfa.regione as pfa_regione, "
                + "       r.data_ora_erogazione AS r_data_ora_erogazione "
                + "FROM ricetta AS r "
                + "JOIN farmaco AS f ON r.id_farmaco = f.id "
                + "JOIN paziente AS mb on r.id_medico_di_base = mb.id "
                + "JOIN paziente AS p ON r.id_paziente = p.id "
                + "LEFT JOIN farmacia AS fa ON r.id_farmacia = fa.id "
                + "LEFT JOIN provincia AS pfa ON pfa.sigla = fa.sigla_provincia "
                + "WHERE r.id_paziente = ? ORDER BY r.data_ora_prescrizione DESC "
                + "LIMIT " + N_LAST_RECIPES)) {
            stm.setInt(1, idPaziente);
            try ( ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    MedicoDiBase medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                            rs.getString("mb_nome"),
                            rs.getString("mb_cognome"));
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome")
                    );
                    Farmaco farmaco = new Farmaco(rs.getInt("f_id"),
                            rs.getString("f_nome"),
                            rs.getInt("f_ticket")
                    );
                    Farmacia farmacia = null;
                    if (rs.getObject("fa_id") != null) {
                        Provincia provinciaFarmacia = new Provincia(rs.getString("pfa_sigla"),
                                rs.getString("pfa_nome"),
                                rs.getString("pfa_regione")
                        );
                        farmacia = new Farmacia(rs.getInt("fa_id"),
                                rs.getString("fa_nome"),
                                rs.getString("fa_comune"),
                                provinciaFarmacia);
                    }
                    Ricetta ricetta = new Ricetta(rs.getInt("r_id"),
                            farmaco,
                            rs.getInt("r_quantita"),
                            medicoDiBase,
                            paziente,
                            rs.getTimestamp("r_data_ora_prescrizione"),
                            farmacia,
                            rs.getTimestamp("r_data_ora_erogazione"));
                    ricette.add(ricetta);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of last user's recipes", ex);
        }
        return ricette;
    }

    @Override
    public Ricetta update(Ricetta ricetta) throws DAOException {
        if (ricetta == null) {
            throw new DAOException("parameter not valid", new IllegalArgumentException("The passed patient is null"));
        }

        try ( PreparedStatement std = CON.prepareStatement("UPDATE ricetta SET id_farmaco = ? , "
                + " quantita = ?, id_medico_di_base = ?, id_paziente = ?, data_ora_prescrizione = ? , "
                + "id_farmacia = ?, data_ora_erogazione = ?  "
                + "WHERE id = ?")) {

            std.setInt(1, ricetta.getFarmaco().getId());
            std.setInt(2, ricetta.getQuantita());
            std.setInt(3, ricetta.getMedicoDiBase().getId());
            std.setInt(4, ricetta.getPaziente().getId());
            std.setTimestamp(5, ricetta.getDataOraPrescrizione());
            std.setInt(6, ricetta.getFarmacia().getId());
            std.setTimestamp(7, ricetta.getDataOraErogazione());
            std.setInt(8, ricetta.getId());
            if (std.executeUpdate() == 1) {
                return ricetta;
            } else {
                throw new DAOException("Impossible to update the recipe");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update the recipe", ex);
        }
    }

    @Override
    public List<Ricetta> getErogateByPaziente(Integer idPaziente) throws DAOException {
        if (idPaziente == null) {
            throw new DAOException("idPaziente is null");
        }

        List<Ricetta> ricette = new ArrayList<>();

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT mb.id AS mb_id, mb.nome AS mb_nome, mb.cognome AS mb_cognome, "
                + "       p.id AS p_id, p.nome AS p_nome, p.cognome AS p_cognome, "
                + "       f.id AS f_id, f.nome AS f_nome, f.ticket AS f_ticket, "
                + "       r.id AS r_id, r.quantita AS r_quantita, "
                + "       r.data_ora_prescrizione AS r_data_ora_prescrizione, "
                + "       fa.id AS fa_id, fa.nome as fa_nome, fa.comune as fa_comune, "
                + "       pfa.sigla as pfa_sigla, pfa.nome as pfa_nome, pfa.regione as pfa_regione, "
                + "       r.data_ora_erogazione AS r_data_ora_erogazione "
                + "FROM ricetta AS r "
                + "JOIN farmaco AS f ON r.id_farmaco = f.id "
                + "JOIN paziente AS mb on r.id_medico_di_base = mb.id "
                + "JOIN paziente AS p ON r.id_paziente = p.id "
                + "JOIN farmacia AS fa ON r.id_farmacia = fa.id "
                + "JOIN provincia AS pfa ON pfa.sigla = fa.sigla_provincia "
                + "WHERE r.id_paziente = ?"
                + "ORDER BY r.data_ora_erogazione DESC ")) {
            stm.setInt(1, idPaziente);
            try ( ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    MedicoDiBase medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                            rs.getString("mb_nome"),
                            rs.getString("mb_cognome"));
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome")
                    );
                    Farmaco farmaco = new Farmaco(rs.getInt("f_id"),
                            rs.getString("f_nome"),
                            rs.getInt("f_ticket")
                    );
                    Provincia provinciaFarmacia = new Provincia(rs.getString("pfa_sigla"),
                            rs.getString("pfa_nome"),
                            rs.getString("pfa_regione")
                    );
                    Farmacia farmacia = new Farmacia(rs.getInt("fa_id"),
                            rs.getString("fa_nome"),
                            rs.getString("fa_comune"),
                            provinciaFarmacia);

                    Ricetta ricetta = new Ricetta(rs.getInt("r_id"),
                            farmaco,
                            rs.getInt("r_quantita"),
                            medicoDiBase,
                            paziente,
                            rs.getTimestamp("r_data_ora_prescrizione"),
                            farmacia,
                            rs.getTimestamp("r_data_ora_erogazione"));
                    ricette.add(ricetta);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of user's recipes erogate", ex);
        }
        return ricette;
    }

    @Override
    public List<Ricetta> getDaErogareByPaziente(Integer idPaziente) throws DAOException {
        if (idPaziente == null) {
            throw new DAOException("idPaziente is null");
        }

        List<Ricetta> ricette = new ArrayList<>();

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT mb.id AS mb_id, mb.nome AS mb_nome, mb.cognome AS mb_cognome, "
                + "       p.id AS p_id, p.nome AS p_nome, p.cognome AS p_cognome, "
                + "       f.id AS f_id, f.nome AS f_nome, f.ticket AS f_ticket, "
                + "       r.id AS r_id, r.quantita AS r_quantita, "
                + "       r.data_ora_prescrizione AS r_data_ora_prescrizione "
                + "FROM ricetta AS r "
                + "JOIN farmaco AS f ON r.id_farmaco = f.id "
                + "JOIN paziente AS mb on r.id_medico_di_base = mb.id "
                + "JOIN paziente AS p ON r.id_paziente = p.id "
                + "WHERE r.id_paziente = ? AND r.id_farmacia IS NULL "
                + "ORDER BY r.data_ora_prescrizione DESC ")) {
            stm.setInt(1, idPaziente);
            try ( ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    MedicoDiBase medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                            rs.getString("mb_nome"),
                            rs.getString("mb_cognome"));
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome")
                    );
                    Farmaco farmaco = new Farmaco(rs.getInt("f_id"),
                            rs.getString("f_nome"),
                            rs.getInt("f_ticket")
                    );

                    Ricetta ricetta = new Ricetta(rs.getInt("r_id"),
                            farmaco,
                            rs.getInt("r_quantita"),
                            medicoDiBase,
                            paziente,
                            rs.getTimestamp("r_data_ora_prescrizione"),
                            null,
                            null
                    );
                    ricette.add(ricetta);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of user's recipes not erogate", ex);
        }
        return ricette;
    }
    @Override
    public List<Ricetta> getErogateBySsp(String siglaProvincia) throws DAOException {
        return getErogateBySsp(siglaProvincia, new Date(0), new Date(System.currentTimeMillis()));
    }

    @Override
    public List<Ricetta> getErogateBySsp(String siglaProvincia, Date startDate, Date endDate) throws DAOException {
        if (siglaProvincia == null) {
            throw new DAOException("idPaziente is null");
        }

        List<Ricetta> ricette = new ArrayList<>();

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT mb.id AS mb_id, mb.nome AS mb_nome, mb.cognome AS mb_cognome, "
                + "       p.id AS p_id, p.codice_fiscale AS p_codice_fiscale, p.nome AS p_nome, p.cognome AS p_cognome, "
                + "       f.id AS f_id, f.nome AS f_nome, f.ticket AS f_ticket, "
                + "       r.id AS r_id, r.quantita AS r_quantita, "
                + "       r.data_ora_prescrizione AS r_data_ora_prescrizione, "
                + "       fa.id AS fa_id, fa.nome as fa_nome, fa.comune as fa_comune, "
                + "       pfa.sigla as pfa_sigla, pfa.nome as pfa_nome, pfa.regione as pfa_regione, "
                + "       r.data_ora_erogazione AS r_data_ora_erogazione "
                + "FROM ricetta AS r "
                + "JOIN farmaco AS f ON r.id_farmaco = f.id "
                + "JOIN paziente AS mb on r.id_medico_di_base = mb.id "
                + "JOIN paziente AS p ON r.id_paziente = p.id "
                + "JOIN farmacia AS fa ON r.id_farmacia = fa.id "
                + "JOIN provincia AS pfa ON pfa.sigla = fa.sigla_provincia "
                + "WHERE pfa.sigla = ? "
                + "AND r.data_ora_erogazione BETWEEN ? AND ? "
                + "ORDER BY r.data_ora_erogazione DESC ")) {
            stm.setString(1, siglaProvincia);
            stm.setDate(2, startDate);
            stm.setDate(3, endDate);

            try ( ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    MedicoDiBase medicoDiBase = new MedicoDiBase(rs.getInt("mb_id"),
                            rs.getString("mb_nome"),
                            rs.getString("mb_cognome"));
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome"),
                            rs.getString("p_codice_fiscale")
                    );
                    Farmaco farmaco = new Farmaco(rs.getInt("f_id"),
                            rs.getString("f_nome"),
                            rs.getInt("f_ticket")
                    );
                    Provincia provinciaFarmacia = new Provincia(rs.getString("pfa_sigla"),
                            rs.getString("pfa_nome"),
                            rs.getString("pfa_regione")
                    );
                    Farmacia farmacia = new Farmacia(rs.getInt("fa_id"),
                            rs.getString("fa_nome"),
                            rs.getString("fa_comune"),
                            provinciaFarmacia);

                    Ricetta ricetta = new Ricetta(rs.getInt("r_id"),
                            farmaco,
                            rs.getInt("r_quantita"),
                            medicoDiBase,
                            paziente,
                            rs.getTimestamp("r_data_ora_prescrizione"),
                            farmacia,
                            rs.getTimestamp("r_data_ora_erogazione"));
                    ricette.add(ricetta);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of user's recipes erogate", ex);
        }
        return ricette;
    }

    @Override
    public Ricetta insert(Ricetta ricetta) throws DAOException {
        if (ricetta == null) {
            throw new DAOException("ricetta is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "INSERT INTO ricetta (id_farmaco, quantita, id_medico_di_base, id_paziente, data_ora_prescrizione) VALUES "
                + "(?, ?, ?, ?, ?);")) {
            stm.setInt(1, ricetta.getFarmaco().getId());
            stm.setInt(2, ricetta.getQuantita());
            stm.setInt(3, ricetta.getMedicoDiBase().getId());
            stm.setInt(4, ricetta.getPaziente().getId());
            stm.setTimestamp(5, ricetta.getDataOraPrescrizione());

            if (stm.executeUpdate() == 1) {
                return ricetta;
            } else {
                throw new DAOException("Impossible to insert the ricetta");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to insert the ricetta", ex);
        }
    }

    @Override
    public boolean isRicettaDiUnPaziente(Integer idRicetta, Integer idMedicoDiBase) throws DAOException {
        if (idRicetta == null) {
            throw new DAOException("idRicetta is null");
        }
        if (idMedicoDiBase == null) {
            throw new DAOException("idMedicoDiBase is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT * "
                + "FROM ricetta AS r "
                + "JOIN paziente AS p ON r.id_paziente = p.id "
                + "WHERE r.id = ? and p.id_medico_di_base = ?")) {
            stm.setInt(1, idRicetta);
            stm.setInt(2, idMedicoDiBase);
            try ( ResultSet rs = stm.executeQuery()) {
                return rs.next();
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the exam for the passed primary key", ex);
        }
    }

    @Override
    public List<List<Integer>> getSpesaFrom(int fromYear) throws DAOException {
        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT ne.year AS year, SUM(ne.num * ne.ticket) AS spesa "
                + "FROM (SELECT EXTRACT(YEAR FROM r.data_ora_erogazione) AS year, f.id AS id, COUNT(f.id) AS num, f.ticket AS ticket "
                + "      FROM ricetta AS r "
                + "      JOIN farmaco AS f ON r.id_farmaco = f.id "
                + "      WHERE EXTRACT(YEAR FROM r.data_ora_erogazione) >= ? "
                + "      GROUP BY EXTRACT(YEAR FROM r.data_ora_erogazione), f.id) AS ne "
                + "GROUP BY ne.year "
                + "ORDER BY ne.year;")) {
            stm.setInt(1, fromYear);
            try ( ResultSet rs = stm.executeQuery()) {
                List<List<Integer>> spesa = new ArrayList<>();
                while (rs.next()) {
                    List<Integer> annoSpesa = new ArrayList<Integer>(2);
                    annoSpesa.add(rs.getInt("year"));
                    annoSpesa.add(rs.getInt("spesa"));
                    spesa.add(annoSpesa);

                }
                return spesa;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the exams data", ex);
        }
    }

    

}
