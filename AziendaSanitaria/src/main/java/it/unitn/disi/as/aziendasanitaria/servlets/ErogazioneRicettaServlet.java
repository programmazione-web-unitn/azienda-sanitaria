/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package it.unitn.disi.as.aziendasanitaria.servlets;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.RicettaDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Farmacia;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Farmaco;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Paziente;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Ricetta;
import it.unitn.disi.as.aziendasanitaria.utilities.Authentication;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author dima
 */
public class ErogazioneRicettaServlet extends HttpServlet {
    private RicettaDAO ricettaDao;
    
    private static final Logger logger = LogManager.getLogger(ErogazioneRicettaServlet.class);
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory");
        }
        try {
            ricettaDao = daoFactory.getDAO(RicettaDAO.class);
        } catch (DAOFactoryException ex) {
            logger.error("Impossibile ottenere il dao factory");
            throw new ServletException("Impossibile ottenere il dao factory", ex);
        }
        
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Farmacia farmacia = (Farmacia) request.getAttribute("user");
        if(farmacia == null){
            logger.error("Nessuna farmacia loggata");
            throw new ServletException("Nessuna farmacia loggata");
        }
        
        Ricetta ricetta = (Ricetta) request.getAttribute("ricetta");
        if(ricetta == null){
            logger.error("Nessuna ricetta indicata");
            throw new ServletException("Nessuna ricetta indicata");
        }
        
        Paziente paziente = ricetta.getPaziente();
        if(paziente == null){
            logger.error("Nessun paziente indicato");
            throw new ServletException("Nessun paziente indicato");
        }
        Farmaco farmaco = ricetta.getFarmaco();
        if(farmaco == null){
            logger.error("Nessun farmaco indicato");
            throw new ServletException("Nessun farmaco indicato");
        }
        
        Timestamp now = new Timestamp(new Date().getTime());
        ricetta.setDataOraErogazione(now);
        ricetta.setFarmacia(farmacia);
        ricetta.setFarmaco(farmaco);
        
        try {
            ricettaDao.update(ricetta);
            logger.debug("ricetta aggionata nel database");
        } catch(DAOException ex) {
            logger.error("Impossibile aggiornare la ricetta nel database", ex);
            throw new ServletException("Impossibile aggionare la ricetta nel database", ex);
        }
        request.getSession().setAttribute("success", "Ricetta erogata correttamente");
        Authentication.redirectTo("Farmacia/paziente?idPaziente=" + paziente.getId(), request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
