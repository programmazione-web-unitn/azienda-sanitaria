/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao.jdbc_psql;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.MedicoSpecialistaDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoDiBase;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoSpecialista;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Provincia;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Utente;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gabriele
 */
public class JDBC_PSQLMedicoSpecialistaDAO extends JDBC_PSQL_DAO<MedicoSpecialista, Integer> implements MedicoSpecialistaDAO {

    public JDBC_PSQLMedicoSpecialistaDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try ( Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM medico_specialista");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count medico specialista", ex);
        }

        return 0L;
    }

    @Override
    public MedicoSpecialista getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }
        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT pn.sigla AS pn_sigla, pn.nome  AS pn_nome, pn.regione  AS pn_regione, "
                + "       pr.sigla AS pr_sigla, pr.nome  AS pr_nome, pr.regione  AS pr_regione, "
                + "       pmms.id AS mms_id, pmms.nome AS mms_nome, pmms.cognome AS mms_cognome, "
                + "       u.id AS ms_id, u.email AS ms_email, u.password AS ms_password, "
                + "       u.tipo AS ms_tipo, p.nome AS ms_nome, p.cognome AS ms_cognome, "
                + "       p.data_nascita AS ms_data_nascita, p.luogo_nascita AS ms_luogo_nascita, "
                + "       p.codice_fiscale AS ms_codice_fiscale, p.sesso AS ms_sesso, u.salt AS ms_salt "
                + "FROM medico_specialista as ms "
                + "JOIN paziente AS p ON ms.id = p.id "
                + "JOIN utente AS u ON ms.id = u.id "
                + "JOIN provincia AS pn ON p.sigla_provincia_nascita = pn.sigla "
                + "JOIN provincia AS pr ON p.sigla_provincia = pr.sigla "
                + "JOIN medico_di_base AS mms ON p.id_medico_di_base = mms.id "
                + "JOIN paziente AS pmms ON mms.id = pmms.id "
                + "WHERE ms.id = ?")) {
            stm.setInt(1, primaryKey);
            try ( ResultSet rs = stm.executeQuery()) {
                rs.next();

                Provincia provinciaNascita = new Provincia(rs.getString("pn_sigla"),
                        rs.getString("pn_nome"),
                        rs.getString("pn_regione"));
                Provincia provinciaResidenza = new Provincia(rs.getString("pr_sigla"),
                        rs.getString("pr_nome"),
                        rs.getString("pr_regione"));
                MedicoDiBase medicoDiBase = new MedicoDiBase(rs.getInt("mms_id"),
                        rs.getString("mms_nome"),
                        rs.getString("mms_cognome"));
                MedicoSpecialista medicoSpecialista = new MedicoSpecialista(rs.getInt("ms_id"),
                        rs.getString("ms_email"),
                        rs.getString("ms_password"),
                        rs.getString("ms_salt"),
                        rs.getString("ms_tipo"),
                        rs.getString("ms_nome"),
                        rs.getString("ms_cognome"),
                        rs.getDate("ms_data_nascita"),
                        rs.getString("ms_luogo_nascita"),
                        provinciaNascita,
                        rs.getString("ms_codice_fiscale"),
                        rs.getString("ms_sesso"),
                        provinciaResidenza,
                        medicoDiBase
                );
                return medicoSpecialista;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the medico specialista for the passed primary key", ex);
        }
    }

    @Override
    public List<MedicoSpecialista> getAll() throws DAOException {
        List<MedicoSpecialista> specialisti = new ArrayList<>();

        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery(""
                    + "SELECT pn.sigla AS pn_sigla, pn.nome  AS pn_nome, pn.regione  AS pn_regione, "
                    + "       pr.sigla AS pr_sigla, pr.nome  AS pr_nome, pr.regione  AS pr_regione, "
                    + "       pmms.id AS mms_id, pmms.nome AS mms_nome, pmms.cognome AS mms_cognome, "
                    + "       u.id AS ms_id, u.email AS ms_email, u.password AS ms_password, "
                    + "       u.tipo AS ms_tipo, p.nome AS ms_nome, p.cognome AS ms_cognome, "
                    + "       p.data_nascita AS ms_data_nascita, p.luogo_nascita AS ms_luogo_nascita, "
                    + "       p.codice_fiscale AS ms_codice_fiscale, p.sesso AS ms_sesso, u.salt AS ms_salt "
                    + "FROM medico_specialista as ms "
                    + "JOIN paziente AS p ON ms.id = p.id "
                    + "JOIN utente AS u ON ms.id = u.id "
                    + "JOIN provincia AS pn ON p.sigla_provincia_nascita = pn.sigla "
                    + "JOIN provincia AS pr ON p.sigla_provincia = pr.sigla "
                    + "JOIN medico_di_base AS mms ON p.id_medico_di_base = mms.id "
                    + "JOIN paziente AS pmms ON mms.id = pmms.id")) {

                while (rs.next()) {
                    Provincia provinciaNascita = new Provincia(rs.getString("pn.sigla"),
                            rs.getString("pn.nome"),
                            rs.getString("pn.regione"));
                    Provincia provinciaResidenza = new Provincia(rs.getString("pr.sigla"),
                            rs.getString("pr.nome"),
                            rs.getString("pr.regione"));
                    MedicoDiBase medicoDiBase = new MedicoDiBase(rs.getInt("mms.id"),
                            rs.getString("pmms.nome"),
                            rs.getString("pmms.cognome"));
                    MedicoSpecialista medicoSpecialista = new MedicoSpecialista(rs.getInt("id"),
                            rs.getString("email"),
                            rs.getString("password"),
                            rs.getString("ms_salt"),
                            rs.getString("tipo"),
                            rs.getString("nome"),
                            rs.getString("cognome"),
                            rs.getDate("data_nascita"),
                            rs.getString("luogo_nascita"),
                            provinciaNascita,
                            rs.getString("codice_fiscale"),
                            rs.getString("sesso"),
                            provinciaResidenza,
                            medicoDiBase
                    );

                    specialisti.add(medicoSpecialista);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of medici specialisti", ex);
        }

        return specialisti;
    }

    @Override
    public MedicoSpecialista update(MedicoSpecialista medicoSpecialista) throws DAOException {
        if (medicoSpecialista == null) {
            throw new DAOException("parameter not valid", new IllegalArgumentException("The passed medico specialista is null"));
        }

        try ( PreparedStatement std = CON.prepareStatement("UPDATE utente SET email = ? password = ?, salt = ?, tipo = ?::user_type WHERE id = ?;"
                + "UPDATE paziente SET nome = ?, cognome = ?, data_nascita = ?, luogo_nascita = ?, "
                + "sigla_provincia_nascita = ?, codice_fiscale = ?, "
                + "sesso = ?, sigla_provincia = ?, id_medico_di_base = ? WHERE id = ?")) {
            std.setString(1, medicoSpecialista.getEmail());
            std.setString(2, medicoSpecialista.getPassword());
            std.setString(3, medicoSpecialista.getSalt());
            std.setString(4, medicoSpecialista.getTipo());
            std.setInt(5, medicoSpecialista.getId());

            std.setString(6, medicoSpecialista.getNome());
            std.setString(7, medicoSpecialista.getCognome());
            std.setDate(8, medicoSpecialista.getDataNascita());
            std.setString(9, medicoSpecialista.getLuogoNascita());
            std.setString(10, medicoSpecialista.getProvinciaNascita().getSigla());
            std.setString(11, medicoSpecialista.getCodiceFiscale());
            std.setString(12, medicoSpecialista.getSesso());
            std.setString(13, medicoSpecialista.getProvincia().getSigla());
            std.setInt(14, medicoSpecialista.getMedicoDiBase().getId());
            std.setInt(15, medicoSpecialista.getId());
            if (std.executeUpdate() == 1) {
                return medicoSpecialista;
            } else {
                throw new DAOException("Impossible to update the medico specialista");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update the medico specialista", ex);
        }
    }

    @Override
    public MedicoSpecialista getByUtente(Utente utente) throws DAOException {
        if (utente == null) {
            throw new DAOException("utente is null");
        }
        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT pn.sigla AS pn_sigla, pn.nome  AS pn_nome, pn.regione  AS pn_regione, "
                + "       pr.sigla AS pr_sigla, pr.nome  AS pr_nome, pr.regione  AS pr_regione, "
                + "       pmms.id AS mms_id, pmms.nome AS mms_nome, pmms.cognome AS mms_cognome, "
                + "       p.nome AS ms_nome, p.cognome AS ms_cognome, "
                + "       p.data_nascita AS ms_data_nascita, p.luogo_nascita AS ms_luogo_nascita, "
                + "       p.codice_fiscale AS ms_codice_fiscale, p.sesso AS ms_sesso "
                + "FROM medico_specialista as ms "
                + "JOIN paziente AS p ON ms.id = p.id "
                + "JOIN provincia AS pn ON p.sigla_provincia_nascita = pn.sigla "
                + "JOIN provincia AS pr ON p.sigla_provincia = pr.sigla "
                + "JOIN medico_di_base AS mms ON p.id_medico_di_base = mms.id "
                + "JOIN paziente AS pmms ON mms.id = pmms.id "
                + "WHERE ms.id = ?")) {
            stm.setInt(1, utente.getId());
            try ( ResultSet rs = stm.executeQuery()) {
                rs.next();

                Provincia provinciaNascita = new Provincia(rs.getString("pn_sigla"),
                        rs.getString("pn_nome"),
                        rs.getString("pn_regione"));
                Provincia provinciaResidenza = new Provincia(rs.getString("pr_sigla"),
                        rs.getString("pr_nome"),
                        rs.getString("pr_regione"));
                MedicoDiBase medicoDiBase = new MedicoDiBase(rs.getInt("mms_id"),
                        rs.getString("mms_nome"),
                        rs.getString("mms_cognome"));
                MedicoSpecialista medicoSpecialista = new MedicoSpecialista(utente.getId(),
                        utente.getEmail(),
                        utente.getPassword(),
                        utente.getSalt(),
                        utente.getTipo(),
                        rs.getString("ms_nome"),
                        rs.getString("ms_cognome"),
                        rs.getDate("ms_data_nascita"),
                        rs.getString("ms_luogo_nascita"),
                        provinciaNascita,
                        rs.getString("ms_codice_fiscale"),
                        rs.getString("ms_sesso"),
                        provinciaResidenza,
                        medicoDiBase
                );
                return medicoSpecialista;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the medico specialista for the passed utente", ex);
        }
    }

}
