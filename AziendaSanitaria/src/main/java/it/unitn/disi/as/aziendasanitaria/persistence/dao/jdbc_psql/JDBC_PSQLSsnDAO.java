/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao.jdbc_psql;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.SsnDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Ssn;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gabriele
 */
public class JDBC_PSQLSsnDAO extends JDBC_PSQL_DAO<Ssn, Integer> implements SsnDAO {

    public JDBC_PSQLSsnDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try ( Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM ssn");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count ssns", ex);
        }

        return 0L;    
    }

    @Override
    public Ssn getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT * "
                + "FROM ssn "
                + "NATURAL JOIN utente "
                + "WHERE id = ?")) {
            stm.setInt(1, primaryKey);
            try ( ResultSet rs = stm.executeQuery()) {
                rs.next();

                Ssn ssn = new Ssn(
                        rs.getInt("id"),
                        rs.getString("email"),
                        rs.getString("password"),
                        rs.getString("salt"),
                        rs.getString("tipo"),
                        rs.getString("recupero_token"),
                        rs.getTimestamp("scadenza_recupero"),
                        rs.getString("token"),
                        rs.getTimestamp("scadenza_token")
                );
                return ssn;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the ssn for the passed primary key", ex);
        }
    }

    @Override
    public List<Ssn> getAll() throws DAOException {
        List<Ssn> ssns = new ArrayList<>();

        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery(""
                    + "SELECT * FROM ssn "
                    + "NATURAL JOIN utente ORDER BY id")) {

                while (rs.next()) {
                    Ssn ssn = new Ssn(rs.getInt("id"),
                        rs.getString("email"),
                        rs.getString("password"),
                        rs.getString("salt"),
                        rs.getString("tipo"),
                        rs.getString("recupero_token"),
                        rs.getTimestamp("scadenza_recupero"),
                        rs.getString("token"),
                        rs.getTimestamp("scadenza_token")
                    );

                    ssns.add(ssn);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of ssns", ex);
        }

        return ssns;
    }

}
