/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.persistence.dao.jdbc_psql;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.PazienteDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.MedicoDiBase;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Paziente;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Provincia;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Ssp;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Utente;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author fran
 */
public class JDBC_PSQLPazienteDAO extends JDBC_PSQL_DAO<Paziente, Integer> implements PazienteDAO {

    public JDBC_PSQLPazienteDAO(Connection con) {
        super(con);
    }

    @Override
    public Long getCount() throws DAOException {
        try ( Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM paziente");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count paziente", ex);
        }

        return 0L;
    }

    @Override
    public Paziente getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT pn.sigla AS pn_sigla, pn.nome  AS pn_nome, pn.regione  AS pn_regione, "
                + "       pr.sigla AS pr_sigla, pr.nome  AS pr_nome, pr.regione  AS pr_regione, "
                + "       mb.id    AS mb_id,    pmb.nome AS mb_nome, pmb.cognome AS mb_cognome, "
                + "       u.id     AS p_id,     u.email AS  p_email, u.password  AS p_password, "
                + "       u.tipo   AS p_tipo,   p.nome AS p_nome,    p.cognome   AS p_cognome,  "
                + "       p.data_nascita AS p_data_nascita, p.luogo_nascita AS p_luogo_nascita, "
                + "       p.codice_fiscale AS p_codice_fiscale, p.sesso AS p_sesso, u.salt AS p_salt,"
                + "       u.recupero_token AS p_rec_token, u.scadenza_recupero AS p_scad_rec,"
                + "       u.token AS p_token, u.scadenza_token AS p_scad_token "
                + "FROM paziente AS p "
                + "JOIN utente as u ON p.id = u.id "
                + "JOIN provincia as pn ON p.sigla_provincia_nascita = pn.sigla "
                + "JOIN provincia as pr ON p.sigla_provincia = pr.sigla "
                + "JOIN medico_di_base as mb ON p.id_medico_di_base = mb.id "
                + "JOIN paziente AS pmb ON mb.id = pmb.id "
                + "WHERE p.id = ?")) {
            stm.setInt(1, primaryKey);
            try ( ResultSet rs = stm.executeQuery()) {
                rs.next();
                Provincia provinciaNascita = new Provincia(rs.getString("pn_sigla"),
                        rs.getString("pn_nome"),
                        rs.getString("pn_regione"));
                Provincia provinciaResidenza = new Provincia(rs.getString("pr_sigla"),
                        rs.getString("pr_nome"),
                        rs.getString("pr_regione"));
                MedicoDiBase medicoDiBase = new MedicoDiBase(
                        rs.getInt("mb_id"),
                        rs.getString("mb_nome"),
                        rs.getString("mb_cognome"));
                Paziente paziente = new Paziente(rs.getInt("p_id"),
                        rs.getString("p_email"),
                        rs.getString("p_password"),
                        rs.getString("p_salt"),
                        rs.getString("p_tipo"),
                        rs.getString("p_rec_token"),
                        rs.getTimestamp("p_scad_rec"),
                        rs.getString("p_token"),
                        rs.getTimestamp("p_scad_token"),
                        rs.getString("p_nome"),
                        rs.getString("p_cognome"),
                        rs.getDate("p_data_nascita"),
                        rs.getString("p_luogo_nascita"),
                        provinciaNascita,
                        rs.getString("p_codice_fiscale"),
                        rs.getString("p_sesso"),
                        provinciaResidenza,
                        medicoDiBase
                );
                return paziente;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the patient for the passed primary key", ex);
        }
    }

    @Override
    public List<Paziente> getAll() throws DAOException {
        List<Paziente> pazienti = new ArrayList<>();

        try ( Statement stm = CON.createStatement()) {
            try ( ResultSet rs = stm.executeQuery(""
                    + "SELECT pn.sigla AS pn_sigla, pn.nome  AS pn_nome, pn.regione  AS pn_regione, "
                    + "       pr.sigla AS pr_sigla, pr.nome  AS pr_nome, pr.regione  AS pr_regione, "
                    + "       mb.id    AS mb_id,    pmb.nome AS mb_nome, pmb.cognome AS mb_cognome, "
                    + "       u.id     AS p_id,     u.email AS  p_email, u.password  AS p_password, "
                    + "       u.tipo   AS p_tipo,   p.nome AS p_nome,    p.cognome   AS p_cognome,  "
                    + "       p.data_nascita AS p_data_nascita, p.luogo_nascita AS p_luogo_nascita, "
                    + "       p.codice_fiscale AS p_codice_fiscale, p.sesso AS p_sesso, u.salt AS p_salt, "
                    + "       u.recupero_token AS p_rec_token, u.scadenza_recupero AS p_scad_rec,"
                    + "       u.token AS p_token, u.scadenza_token AS p_scad_token "
                    + "FROM paziente AS p "
                    + "JOIN utente as u ON p.id = u.id "
                    + "JOIN provincia as pn ON p.sigla_provincia_nascita = pn.sigla "
                    + "JOIN provincia as pr ON p.sigla_provincia = pr.sigla "
                    + "JOIN medico_di_base as mb ON p.id_medico_di_base = mb.id "
                    + "JOIN paziente AS pmb ON mb.id = pmb.id ORDER BY p.cognome, p.nome")) {
                while (rs.next()) {
                    Provincia provinciaNascita = new Provincia(rs.getString("pn_sigla"),
                            rs.getString("pn_nome"),
                            rs.getString("pn_regione"));
                    Provincia provinciaResidenza = new Provincia(rs.getString("pr_sigla"),
                            rs.getString("pr_nome"),
                            rs.getString("pr_regione"));
                    MedicoDiBase medicoDiBase = new MedicoDiBase(
                            rs.getInt("mb_id"),
                            rs.getString("mb_nome"),
                            rs.getString("mb_cognome"));
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_email"),
                            rs.getString("p_password"),
                            rs.getString("p_salt"),
                            rs.getString("p_tipo"),
                            rs.getString("p_rec_token"),
                            rs.getTimestamp("p_scad_rec"),
                            rs.getString("p_token"),
                            rs.getTimestamp("p_scad_token"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome"),
                            rs.getDate("p_data_nascita"),
                            rs.getString("p_luogo_nascita"),
                            provinciaNascita,
                            rs.getString("p_codice_fiscale"),
                            rs.getString("p_sesso"),
                            provinciaResidenza,
                            medicoDiBase
                    );
                    pazienti.add(paziente);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }

        return pazienti;
    }

    @Override
    public Paziente update(Paziente paziente) throws DAOException {

        if (paziente == null) {
            throw new DAOException("parameter not valid", new IllegalArgumentException("The passed patient is null"));
        }

        try ( PreparedStatement std = CON.prepareStatement(""
                + "UPDATE utente SET email = ?, password = ?, salt = ?, tipo = ?::user_type WHERE id = ?;"
                + "UPDATE paziente SET nome = ?, cognome = ?, data_nascita = ?, luogo_nascita = ?, "
                + "sigla_provincia_nascita = ?, codice_fiscale = ?, sesso = ?::gender, sigla_provincia = ?,"
                + "id_medico_di_base = ? WHERE id = ?")) {
            std.setString(1, paziente.getEmail());
            std.setString(2, paziente.getPassword());
            std.setString(3, paziente.getSalt());
            std.setString(4, paziente.getTipo());

            std.setInt(5, paziente.getId());

            std.setString(6, paziente.getNome());
            std.setString(7, paziente.getCognome());
            std.setDate(8, paziente.getDataNascita());
            std.setString(9, paziente.getLuogoNascita());
            std.setString(10, paziente.getProvinciaNascita().getSigla());
            std.setString(11, paziente.getCodiceFiscale());
            std.setString(12, paziente.getSesso());
            std.setString(13, paziente.getProvincia().getSigla());
            std.setInt(14, paziente.getMedicoDiBase().getId());
            std.setInt(15, paziente.getId());
            if (std.executeUpdate() == 1) {
                return paziente;
            } else {
                throw new DAOException("Impossible to update the paziente");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update the paziente", ex);
        }
    }

    @Override
    public Paziente getByUtente(Utente utente) throws DAOException {
        if (utente == null) {
            throw new DAOException("utente is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT pn.sigla AS pn_sigla, pn.nome  AS pn_nome, pn.regione  AS pn_regione, "
                + "       pr.sigla AS pr_sigla, pr.nome  AS pr_nome, pr.regione  AS pr_regione, "
                + "       mb.id    AS mb_id,    pmb.nome AS mb_nome, pmb.cognome AS mb_cognome, "
                + "       p.nome AS p_nome,    p.cognome   AS p_cognome,  "
                + "       p.data_nascita AS p_data_nascita, p.luogo_nascita AS p_luogo_nascita, "
                + "       p.codice_fiscale AS p_codice_fiscale, p.sesso AS p_sesso "
                + "FROM paziente AS p "
                + "JOIN provincia as pn ON p.sigla_provincia_nascita = pn.sigla "
                + "JOIN provincia as pr ON p.sigla_provincia = pr.sigla "
                + "JOIN medico_di_base as mb ON p.id_medico_di_base = mb.id "
                + "JOIN paziente AS pmb ON mb.id = pmb.id "
                + "WHERE p.id = ?")) {
            stm.setInt(1, utente.getId());
            try ( ResultSet rs = stm.executeQuery()) {
                rs.next();
                Provincia provinciaNascita = new Provincia(rs.getString("pn_sigla"),
                        rs.getString("pn_nome"),
                        rs.getString("pn_regione"));
                Provincia provinciaResidenza = new Provincia(rs.getString("pr_sigla"),
                        rs.getString("pr_nome"),
                        rs.getString("pr_regione"));
                MedicoDiBase medicoDiBase = new MedicoDiBase(
                        rs.getInt("mb_id"),
                        rs.getString("mb_nome"),
                        rs.getString("mb_cognome"));
                Paziente paziente = new Paziente(utente.getId(),
                        utente.getEmail(),
                        utente.getPassword(),
                        utente.getSalt(),
                        utente.getTipo(),
                        utente.getRecuperoToken(),
                        utente.getScadenzaRecupero(),
                        utente.getToken(),
                        utente.getScadenzaToken(),
                        rs.getString("p_nome"),
                        rs.getString("p_cognome"),
                        rs.getDate("p_data_nascita"),
                        rs.getString("p_luogo_nascita"),
                        provinciaNascita,
                        rs.getString("p_codice_fiscale"),
                        rs.getString("p_sesso"),
                        provinciaResidenza,
                        medicoDiBase
                );
                return paziente;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the paziente for the passed utente", ex);
        }
    }

    @Override
    public List<Paziente> getByMedicoDiBase(MedicoDiBase medicoDiBase) throws DAOException {
        if (medicoDiBase == null) {
            throw new DAOException("medicoDiBase is null");
        }
        List<Paziente> pazienti = new ArrayList<>();

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT pn.sigla AS pn_sigla, pn.nome  AS pn_nome, pn.regione  AS pn_regione, "
                + "       pr.sigla AS pr_sigla, pr.nome  AS pr_nome, pr.regione  AS pr_regione, "
                + "       u.id     AS p_id,     u.email AS  p_email, u.password  AS p_password, "
                + "       u.tipo   AS p_tipo,   p.nome AS p_nome,    p.cognome   AS p_cognome,  "
                + "       p.data_nascita AS p_data_nascita, p.luogo_nascita AS p_luogo_nascita, "
                + "       p.codice_fiscale AS p_codice_fiscale, p.sesso AS p_sesso, u.salt AS p_salt, "
                + "       u.recupero_token AS p_rec_token, u.scadenza_recupero AS p_scad_rec,"
                + "       u.token AS p_token, u.scadenza_token AS p_scad_token "
                + "FROM paziente AS p "
                + "JOIN utente as u ON p.id = u.id "
                + "JOIN provincia as pn ON p.sigla_provincia_nascita = pn.sigla "
                + "JOIN provincia as pr ON p.sigla_provincia = pr.sigla "
                + "JOIN medico_di_base as mb ON p.id_medico_di_base = mb.id "
                + "JOIN paziente AS pmb ON mb.id = pmb.id "
                + "WHERE mb.id = ? ORDER BY p.cognome, p.nome")) {
            stm.setInt(1, medicoDiBase.getId());
            try ( ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    Provincia provinciaNascita = new Provincia(rs.getString("pn_sigla"),
                            rs.getString("pn_nome"),
                            rs.getString("pn_regione"));
                    Provincia provinciaResidenza = new Provincia(rs.getString("pr_sigla"),
                            rs.getString("pr_nome"),
                            rs.getString("pr_regione"));
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_email"),
                            rs.getString("p_password"),
                            rs.getString("p_salt"),
                            rs.getString("p_tipo"),
                            rs.getString("p_rec_token"),
                            rs.getTimestamp("p_scad_rec"),
                            rs.getString("p_token"),
                            rs.getTimestamp("p_scad_token"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome"),
                            rs.getDate("p_data_nascita"),
                            rs.getString("p_luogo_nascita"),
                            provinciaNascita,
                            rs.getString("p_codice_fiscale"),
                            rs.getString("p_sesso"),
                            provinciaResidenza,
                            medicoDiBase
                    );
                    pazienti.add(paziente);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }

        return pazienti;
    }

    @Override
    public boolean isPazienteDelMedico(Integer idPaziente, Integer idMedico) throws DAOException {
        if (idPaziente == null) {
            throw new DAOException("idPaziente is null");
        }
        if (idMedico == null) {
            throw new DAOException("idMedico is null");
        }

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT * "
                + "FROM paziente AS p "
                + "WHERE p.id = ? and p.id_medico_di_base = ?")) {
            stm.setInt(1, idPaziente);
            stm.setInt(2, idMedico);
            try ( ResultSet rs = stm.executeQuery()) {
                if (rs.next()) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (SQLException ex) {
            throw new DAOException(ex);
        }
    }

    @Override
    public List<Map<String, Object>> getByMedicoDiBaseWithInfo(MedicoDiBase medicoDiBase) throws DAOException {
        if (medicoDiBase == null) {
            throw new DAOException("medicoDiBase is null");
        }
        List<Map<String, Object>> pazienti = new ArrayList<>();
        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT p.id AS p_id, max(p.codice_fiscale) AS p_codice_fiscale, "
                + "       max(p.nome) AS p_nome, max(p.cognome) AS p_cognome, "
                + "       max(vs.data_ora_prescrizione) AS ultima_visita_presc, "
                + "       max(r.data_ora_prescrizione) AS ultima_ricetta_presc, "
                + "       max(e.data_ora_prescrizione) AS ultimo_esame_presc "
                + "FROM paziente AS p "
                + "LEFT JOIN visita_specialistica AS vs ON vs.id_paziente = p.id "
                + "LEFT JOIN ricetta AS r ON r.id_paziente = p.id "
                + "LEFT JOIN esame AS e ON e.id_paziente = p.id "
                + "WHERE p.id_medico_di_base = ? "
                + "GROUP BY p.id "
                + "ORDER BY p_cognome, p_nome ")) {
            stm.setInt(1, medicoDiBase.getId());
            try ( ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome")
                    );
                    paziente.setCodiceFiscale(rs.getString("p_codice_fiscale"));
                    Timestamp ultima_visita_presc = rs.getTimestamp("ultima_visita_presc");
                    Timestamp ultima_ricetta_presc = rs.getTimestamp("ultima_ricetta_presc");
                    Timestamp ultimo_esame_presc = rs.getTimestamp("ultimo_esame_presc");
                    Map listItem = new HashMap();
                    listItem.put("paziente", paziente);
                    listItem.put("ultima_visita_presc", ultima_visita_presc);
                    listItem.put("ultima_ricetta_presc", ultima_ricetta_presc);
                    listItem.put("ultimo_esame_presc", ultimo_esame_presc);

                    pazienti.add(listItem);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients", ex);
        }

        return pazienti;
    }

    @Override
    public List<Paziente> getBySsp(Ssp ssp) throws DAOException {
        if (ssp == null) {
            throw new DAOException("ssp is null");
        }
        List<Paziente> pazienti = new ArrayList<>();

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT p.id as p_id, p.data_nascita as p_data_nascita, p.nome as p_nome, p.cognome as p_cognome, "
                + "       p.codice_fiscale as p_codice_fiscale "
                + "FROM paziente AS p "
                + "WHERE p.sigla_provincia = ?")) {
            stm.setString(1, ssp.getProvincia().getSigla());

            try ( ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome"));
                    paziente.setDataNascita(rs.getDate("p_data_nascita"));
                    paziente.setCodiceFiscale(rs.getString("p_codice_fiscale"));
                    pazienti.add(paziente);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients by ssp", ex);
        }

        return pazienti;
    }

    @Override
    public List<Paziente> getBySsp(Ssp ssp, Date from, Date to) throws DAOException {
        if (ssp == null) {
            throw new DAOException("ssp is null");
        }
        List<Paziente> pazienti = new ArrayList<>();

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT p.id as p_id, p.data_nascita as p_data_nascita, u.email as p_email, p.nome as p_nome, p.cognome as p_cognome, "
                + "       p.codice_fiscale as p_codice_fiscale "
                + "FROM paziente AS p JOIN utente AS u ON u.id = p.id "
                + "WHERE p.sigla_provincia = ? AND p.data_nascita >= ? AND p.data_nascita <= ?;")) {
            stm.setString(1, ssp.getProvincia().getSigla());
            stm.setDate(2, from);
            stm.setDate(3, to);

            try ( ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome"));
                    paziente.setEmail(rs.getString("p_email"));
                    paziente.setDataNascita(rs.getDate("p_data_nascita"));
                    paziente.setCodiceFiscale(rs.getString("p_codice_fiscale"));
                    pazienti.add(paziente);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients by ssp", ex);
        }

        return pazienti;
    }
    
    @Override
    public List<Paziente> getBySspConRichiamo(Ssp ssp, Date from, Date to) throws DAOException {
        if (ssp == null) {
            throw new DAOException("ssp is null");
        }
        List<Paziente> pazienti = new ArrayList<>();

        try ( PreparedStatement stm = CON.prepareStatement(""
                + "SELECT MAX(u.email) as p_email, p.id as p_id, p.data_nascita as p_data_nascita, p.nome as p_nome, p.cognome as p_cognome, "
                + "       p.codice_fiscale as p_codice_fiscale "
                + "FROM paziente AS p "
                + "JOIN utente AS u ON u.id = p.id "
                + "JOIN visita_specialistica AS vs ON vs.id_paziente = p.id "
                + "WHERE p.sigla_provincia = ? AND vs.id_medico_di_base IS NULL AND p.data_nascita >= ? AND p.data_nascita <= ? "
                + "GROUP BY p.id")) {
            stm.setString(1, ssp.getProvincia().getSigla());
            stm.setDate(2, from);
            stm.setDate(3, to);

            try ( ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    Paziente paziente = new Paziente(rs.getInt("p_id"),
                            rs.getString("p_nome"),
                            rs.getString("p_cognome"));
                    paziente.setEmail(rs.getString("p_email"));
                    paziente.setDataNascita(rs.getDate("p_data_nascita"));
                    paziente.setCodiceFiscale(rs.getString("p_codice_fiscale"));
                    pazienti.add(paziente);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of patients by ssp", ex);
        }

        return pazienti;
    }
}
