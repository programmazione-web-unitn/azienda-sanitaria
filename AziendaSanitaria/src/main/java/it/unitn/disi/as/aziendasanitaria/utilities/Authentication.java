/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.as.aziendasanitaria.utilities;

import it.unitn.disi.as.aziendasanitaria.persistence.dao.FarmaciaDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.MedicoDiBaseDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.MedicoSpecialistaDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.PazienteDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.SspDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.dao.SsnDAO;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Paziente;
import it.unitn.disi.as.aziendasanitaria.persistence.entities.Utente;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.commons.persistence.dao.factories.DAOFactory;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author gabriele
 */
public class Authentication {

    public static Utente getUtenteSpecifico(DAOFactory daoFactory, Utente user) throws DAOException, DAOFactoryException {
        PazienteDAO pazienteDao = daoFactory.getDAO(PazienteDAO.class);
        MedicoDiBaseDAO medicoDiBaseDao = daoFactory.getDAO(MedicoDiBaseDAO.class);
        MedicoSpecialistaDAO medicoSpecialistaDao = daoFactory.getDAO(MedicoSpecialistaDAO.class);
        FarmaciaDAO farmaciaDao = daoFactory.getDAO(FarmaciaDAO.class);
        SspDAO sspDao = daoFactory.getDAO(SspDAO.class);
        SsnDAO ssnDao = daoFactory.getDAO(SsnDAO.class);

        if (user.getTipo().equals("P")) {
            user = pazienteDao.getByUtente(user);
        } else if (user.getTipo().equals("MB")) {
            user = medicoDiBaseDao.getByUtente(user);
        } else if (user.getTipo().equals("MS")) {
            user = medicoSpecialistaDao.getByUtente(user);
        } else if (user.getTipo().equals("F")) {
            user = farmaciaDao.getByUtente(user);
        } else if (user.getTipo().equals("SSP")) {
            user = sspDao.getByPrimaryKey(user.getId());
        } else if (user.getTipo().equals("SSN")) {
            user = ssnDao.getByPrimaryKey(user.getId());
        }

        return user;
    }

    public static String getUserPath(Utente user){
        if (user.getTipo().equals("P")) {
            return "Paziente";
        } else if (user.getTipo().equals("MB")) {
            return "Medico";
        } else if (user.getTipo().equals("MS")) {
            return "Specialista";
        } else if (user.getTipo().equals("F")) {
            return "Farmacia";
        } else if (user.getTipo().equals("SSP")) {
            return "Ssp";
        } else {
            return "Ssn";
        }
    }

    public static void redirectToProfile(Utente user, HttpServletRequest request, HttpServletResponse response) throws IOException{
        String userPath;
        if(user instanceof Paziente){
            userPath = "Paziente";
        } else 
            userPath = getUserPath(user);
        redirectTo(userPath + "/profilo", request, response);
    }
    
    public static void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException{
        redirectTo("login", request, response);
    }
    public static void redirectTo(String path, HttpServletRequest request, HttpServletResponse response) throws IOException{
        String contextPath = request.getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        response.sendRedirect(response.encodeRedirectURL(contextPath + path));
    }
}
